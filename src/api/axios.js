import axios from "axios";
const BASE_URL = "https://apikimphat.hahoba.com/api/";

export const api = axios.create({
  baseURL: BASE_URL,
  // timeout: 30000,
});

// dùng để refresh token
export const apiPrivate = axios.create({
  baseURL: BASE_URL,
  headers: { ContentType: "application/json" },
  withCredentials: true,
});
