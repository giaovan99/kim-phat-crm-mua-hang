import React from "react";
import ReactDOM from "react-dom/client";
import "./index.scss";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { routes } from "./routes/routes.js";
import { store } from "./redux/store.js";
import { Provider } from "react-redux";
import { ConfigProvider } from "antd";
import viVN from "antd/locale/vi_VN";

const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(
  <ConfigProvider locale={viVN}>
    <Provider store={store}>
      <BrowserRouter>
        <Routes>
          {routes.map((route, index) => (
            <Route path={route.path} element={route.element} key={index}>
              {route.children?.map((child, index) => {
                return (
                  <Route
                    path={child.path}
                    key={index}
                    element={child.element}
                  />
                );
              })}
            </Route>
          ))}
        </Routes>
      </BrowserRouter>
    </Provider>
  </ConfigProvider>
);
