import React from "react";
import Lottie from "lottie-react";
import "./accountTemplate.scss";
import login_animate from "../assets/lottieAnimation/accountAnimation3.json";
import { Provider } from "react-redux";
import { store } from "../redux/store";

export default function AccountTemplate({ Components }) {
  return (
    <Provider store={store}>
      <section className="section section-account">
        <div className="container-login100">
          <div className="wrap-login100">
            {/* ======================= ảnh bên trái ======================= */}
            <div className="login100-pic js-tilt">
              <Lottie animationData={login_animate} loop={true} />
            </div>
            {/* ======================= Form đăng nhập bên phải ======================= */}
            {<Components />}
          </div>
        </div>
      </section>
    </Provider>
  );
}
