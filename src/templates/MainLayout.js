import React, { useEffect, useState } from "react";
import SideBar from "../Components/SideBar/SideBar";
import { Layout } from "antd";
import HeaderLayout from "../Components/Header/Header";
import FooterLayout from "../Components/FooterLayout/FooterLayout";
import checkLogin from "../utils/checkLogin";
import { useLocation } from "react-router-dom";
import { useDispatch } from "react-redux";
import { getLocalStorage } from "../utils/localStorage";
import { setLoginInfo, setSettingInfo } from "../redux/slice/loginInfoSlice";
import { getSetting } from "../utils/setting";
import { goToLogin2 } from "../utils/others";
import Spinner from "../Components/Spinner/Spinner";

export default function MainLayout({ Components }) {
  const { Content } = Layout;
  const location = useLocation();
  // const useMatch = useLocation();
  const dispatch = useDispatch();

  // Khai báo state
  const [collapsed, setCollapsed] = useState(false); //Quản lý tình trạng đóng mở sidebar

  const getSettingInfo = async () => {
    let res = await getSetting();
    if (res.status) {
      dispatch(setSettingInfo(res.data));
    }
  };

  useEffect(() => {
    const dataLocal = getLocalStorage("loginInfo");

    if (dataLocal) {
      dispatch(setLoginInfo(dataLocal));
    }
    // // lấy thông tin setting và đây lên redux
    getSettingInfo();
  }, []);

  // // Kiểm tra đăng nhập mỗi khi url thay đổi
  useEffect(() => {
    // Step: Check đăng nhập -----(true)----> Tiếp tục truy cập trang con
    //                      |
    //                       -----(false)----> Trả về trang chủ

    // Kiểm tra xem đã đăng nhập hay chưa
    let check = checkLogin();

    if (!check) {
      goToLogin2();
    }

    // Kiểm tra xem có quyền truy cập trang hay không
    let loginInfoLocal = getLocalStorage("loginInfo");
    if (loginInfoLocal) {
      // Lấy phần path từ URL
      const pathName = location.pathname;
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      if (pathSegments[1] !== "profile") {
        let fun = loginInfoLocal?.permission[pathSegments[1]]?.read;
        if (!fun) {
          goToLogin2();
        } else {
          // Kiểm tra xem có đang ở trang add/fix hay ko && có quyền add/fix hay ko
          if (pathSegments.length > 2) {
            let checkPermission;
            if (pathSegments[2] === "add") {
              checkPermission =
                loginInfoLocal?.permission[pathSegments[1]]?.create;
            } else if (pathSegments[2] === "fix") {
              checkPermission =
                loginInfoLocal?.permission[pathSegments[1]]?.update;
            } else if (pathSegments[2] === "manage") {
              checkPermission =
                loginInfoLocal?.permission[
                  pathSegments[1] + "/" + pathSegments[2]
                ]?.read;
            } else {
              checkPermission =
                loginInfoLocal?.permission[
                  pathSegments[1] + "/" + pathSegments[2]
                ]?.read;
            }
            if (!checkPermission) {
              goToLogin2();
            }
          }
        }
      }
    } else {
      goToLogin2();
    }
  }, [location]);

  return (
    <>
      <Spinner open={true} />
      <Layout className="min-h-[100vh]">
        <SideBar collapsed={collapsed} />
        <Layout
          style={collapsed ? { marginLeft: "4.6rem" } : { marginLeft: 250 }}
        >
          <HeaderLayout collapsed={collapsed} setCollapsed={setCollapsed} />
          <Content
            style={{
              margin: "16px 20px 16px ",
              flexGrow: "1",
            }}
          >
            <main>
              <Components />
            </main>
          </Content>
          <FooterLayout />
        </Layout>
      </Layout>
    </>
  );
}
