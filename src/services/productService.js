// API về sản phẩm
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const productService = {
  // 1 - Danh sách các product
  productList: async (data) => {
    try {
      return await api.get(
        `product?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&suppliers_name=${
          data.suppliers_name ? data.suppliers_name : ""
        }&department=${data.department ? data.department : ""}&package_case=${
          data.package_case ? data.package_case : ""
        }&deleted=${data.deleted ? data.deleted : ""}&search_type=${
          data.search_type ? data.search_type : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 2 - Lấy danh sách đơn vị tính
  unitList: async (data) => {
    try {
      return await api.get(
        `option/unit?token=${data.token}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 3 - Lấy danh sách loại hoá đơn
  vatList: async (data) => {
    try {
      return await api.get(
        `option/vat_order??token=${data.token}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 4 - Xoá sản phẩm
  deleteProduct: async (data) => {
    try {
      return await api.post("product/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 5 - Chỉnh sửa sản phẩm
  updateProduct: async (data) => {
    try {
      return await api.post(`product/${data.id}?token=${data.token}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 6 -  Tạo sản phẩm
  createProduct: async (data) => {
    try {
      return await api.post("product", data);
    } catch (err) {
      showError();
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
    }
  },

  // 7 - Lấy thông tin cụ thể 1 sản phẩm
  getProductDetails: async (data) => {
    try {
      return await api.get(`product/${data.id}?token=${data.token}`);
    } catch (err) {
      showError();
      console.log(err);
    }
  },

  // 8 - Cập nhật đơn vị tính
  updateUnut: async (data) => {
    try {
      return await api.post(`option/unit`, data);
    } catch (err) {
      showError();
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 9 - Cập nhật loại hoá đơn
  updateTaxCode: async (data) => {
    try {
      return await api.post(`option/vat_order`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 10 - Lịch sử chỉnh sửa sản phẩm
  productHistory: async (data) => {
    try {
      return await api.get(`product-history/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 10 - Lịch sử chỉnh sửa sản phẩm - tất cả
  productHistoryAll: async (data) => {
    try {
      return await api.get(
        `product-history?token=${data.token}&page=${data.page}&startDate=${
          data.startDate ? data.startDate : ""
        }&endDate=${data.endDate ? data.endDate : ""}&product_code=${
          data.product_code ? data.product_code : ""
        }&product_name=${data.product_name ? data.product_name : ""}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 10 - Xuất file export sản phẩm
  productExport: async (data) => {
    try {
      return await api.get(`product/export?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
};
