// API ve bao cao
// API về phân quyền
import { api } from "../api/axios";

export const reportService = {
  // 1 - Lấy danh sách report ve san pham
  reportProductList: async (data) => {
    return await api.get(
      `report_product?token=${data.token}&page=${data.page}&show_all=${
        data.show_all ? data.show_all : ""
      }&start_date=${data.start_date ? data.start_date : ""}&end_date=${
        data.end_date ? data.end_date : ""
      }&department=${data.department ? data.department : ""}`
    );
  },

  // 2 - Xuất file Excel report về sản phẩm
  exportReportProduct: async (data) => {
    return await api.get(
      `export_report_product?token=${data.token}&start_date=${
        data.start_date ? data.start_date : ""
      }&end_date=${data.end_date ? data.end_date : ""}&department=${
        data.department ? data.department : ""
      }`
    );
  },

  // 3 - Lấy danh sách report về sản phẩm theo giá
  reportProductPriceList: async (data) => {
    return await api.get(
      `report_product_price?token=${data.token}&page=${data.page}&show_all=${
        data.show_all ? data.show_all : ""
      }&start_date=${data.start_date ? data.start_date : ""}&end_date=${
        data.end_date ? data.end_date : ""
      }&staff=${data.staff ? data.staff : ""}`
    );
  },

  // 4 - Xuất file Excel report về sản phẩm theo giá
  exportReportProductPrice: async (data) => {
    return await api.get(
      `export_report_product_price?token=${data.token}&start_date=${
        data.start_date ? data.start_date : ""
      }&end_date=${data.end_date ? data.end_date : ""}&staff=${
        data.staff ? data.staff : ""
      }`
    );
  },

  // 5 - Lấy danh sách report về tạm ứng hoàn ứng
  reportAMList: async (data) => {
    return await api.get(
      `report_am_and_pr?token=${data.token}&page=${data.page}&show_all=${
        data.show_all ? data.show_all : ""
      }&start_date=${data.start_date ? data.start_date : ""}&end_date=${
        data.end_date ? data.end_date : ""
      }&staff_number=${data.staff_number ? data.staff_number : ""}`
    );
  },

  // 6 - Xuất file Excel report về sản phẩm theo giá
  exportReportAM: async (data) => {
    return await api.get(
      `export_report_am_and_pr?token=${data.token}&start_date=${
        data.start_date ? data.start_date : ""
      }&end_date=${data.end_date ? data.end_date : ""}&staff_number=${
        data.staff_number ? data.staff_number : ""
      }`
    );
  },
};
