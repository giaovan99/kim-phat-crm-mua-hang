// API về cài đặt
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const settingService = {
  // 1 - Lấy thông tin cài đặt
  getSetting: async (data) => {
    try {
      return await api.get(`config?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 2 - Cập nhật thông tin cài đặt
  updateSetting: async (data) => {
    try {
      return await api.post(`config?token=${data.token}`, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 3 - Lấy danh sách thông số hệ thống
  getOptions: async (data) => {
    try {
      return await api.get(
        `options?token=${data.token}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&deleted=${
          data.deleted ? data.deleted : ""
        }`
      );
    } catch (err) {
      // showError();
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  //Lấy thông tin option cụ thể
  getOptionId: async (data) => {
    try {
      return await api.get(`options/${data.id}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // Tạo thông số
  createOption: async (data) => {
    try {
      return await api.post(`options`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // Cập nhật options
  updateOption: async (data) => {
    try {
      return await api.post(`options/${data.id}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
};
