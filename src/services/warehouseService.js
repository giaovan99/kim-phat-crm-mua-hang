// API về kho
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const warehouseService = {
  // 1 - Danh sách kho => truyền vào params gồm token + page (mặc định 20 giá trị/page)
  warehouseList: async (data) => {
    try {
      return await api.get(
        `warehouse?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&deleted=${
          data.deleted ? data.deleted : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 2 - Lấy thông tin kho cụ thể
  getWarehouse: async (data) => {
    try {
      return await api.get(`warehouse/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 3 - Chỉnh sửa kho
  warehouseUpdate: async (data) => {
    try {
      return await api.post(`warehouse/${data.id}?token=${data.token}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 5 - Tạo kho
  createWarehouse: async (data) => {
    try {
      return await api.post("warehouse", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 4 - Xoá kho
  deleteWarehouse: async (data) => {
    try {
      return await api.post("warehouse/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 5 - Lấy danh sách phiếu nhập kho
  importWarehouseList: async (data) => {
    try {
      return await api.get(
        `stock_import?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&po_ids=${data.po_ids ? data.po_ids : ""}&nk_ids=${
          data.nk_ids ? data.nk_ids : ""
        }&department=${data.department ? data.department : ""}&supplier_name=${
          data.supplier_name ? data.supplier_name : ""
        }&shippingMethod=${
          data.shippingMethod ? data.shippingMethod : ""
        }&warehouse=${data.warehouse ? data.warehouse : ""}&deleted=${
          data.deleted ? data.deleted : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 6 - Lấy thông tin phiếu nhập kho cụ thể
  getImportWarehouse: async (data) => {
    try {
      return await api.get(`stock_import/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 8 - Chỉnh sửa phiếu nhập kho
  updateImportWarehouse: async (data) => {
    try {
      return await api.post(
        `stock_import/${data.id}?token=${data.token}`,
        data
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 9 - Tạo phiếu nhập kho
  createImportWarehouse: async (data) => {
    try {
      return await api.post("stock_import", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 10 - Xoá phiếu nhập kho
  deleteImportWarehouse: async (data) => {
    try {
      return await api.post("stock_import/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 1 - Lấy danh sách trạng thái đơn đề nghị
  requestStatus: async (data) => {
    try {
      return await api.get(`option/stock_import_status?token=${data.token}}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 5 - Lấy danh sách sản phẩm nhập kho
  stockImportPartList: async (data) => {
    try {
      return await api.get(
        `stock_import_part?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&si_status=${
          data.si_status ? data.si_status : ""
        }&deleted=${data.deleted ? data.deleted : ""}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // In danh sách sản phẩm nhập kho
  stockImportPrint: async (data) => {
    try {
      return await api.post(`stock_import/export?token=${data.token}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
};
