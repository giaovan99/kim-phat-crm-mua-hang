import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin } from "../utils/others";

export const advanceService = {
  // 1 - Lấy danh sách ví tiền tạm ứng
  advanceWalletList: async (data) => {
    try {
      return await api.get(
        `advance_wallet?token=${data.token}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin(err);
    }
  },

  // 2 - Danh sách phiếu tạm ứng
  advanceList: async (data) => {
    try {
      return await api.get(
        `requests_for_advance?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&status=${
          data.status ? data.status : ""
        }&staff_number=${data.staff_number ? data.staff_number : ""}&type=${
          data.type ? data.type : ""
        }&deleted=${data.deleted ? data.deleted : ""}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin(err);
    }
  },

  // 3 - Lấy thông tin phiếu tạm ứng cụ thể
  getAdvanceSlip: async (data) => {
    try {
      return await api.get(
        `requests_for_advance/${data.id}?token=${data.token}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin(err);
    }
  },

  // 4 - Chỉnh sửa phiếu tạm ứng
  updateAdvanceSlip: async (data) => {
    try {
      return await api.post(
        `requests_for_advance/${data.id}?token=${data.token}`,
        data
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin(err);
    }
  },

  // 5 - Tạo đơn hàng
  createAdvanceSlip: async (data) => {
    try {
      return await api.post("requests_for_advance", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);

      goToLogin();
    }
  },

  // 6 - Xoá phiếu tạm ứng
  deleteAdvanceSlip: async (data) => {
    try {
      return await api.post("requests_for_advance/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 7 - Lấy danh sách trạng thái phiếu
  advanceStatus: async (data) => {
    try {
      return await api.get(
        `option/requests_for_advance_status?token=${data.token}}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 8 - Lấy danh sách loại phiếu tạm ứng
  getTypeAM: async (data) => {
    try {
      return await api.get(
        `option/advance_money_type?token=${data.token}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
};
