// API về nhà cung cấp
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const supplierService = {
  // 1 - Danh sách các supplier
  supplierList: async (data) => {
    try {
      return await api.get(
        `suppliers?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&deleted=${
          data.deleted ? data.deleted : ""
        }&search_type=${data.search_type ? data.search_type : ""}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 1.1 - Lấy thông tin NCC cụ thể
  getNCC: async (data) => {
    try {
      return await api.get(`suppliers/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 2 - Chỉnh sửa NCC
  nccUpdate: async (data) => {
    try {
      return await api.post(`suppliers/${data.id}?token=${data.token}`, data);
    } catch (err) {
      // // goToLogin();
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 3 - Tạo user
  createNCC: async (data) => {
    try {
      return await api.post("suppliers", data);
    } catch (err) {
      // // goToLogin();
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 5 - Xoá NCC
  deleteNCC: async (data) => {
    try {
      return await api.post("suppliers/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
};
