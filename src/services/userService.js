// API về người dùng
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const userService = {
  // 1 - Danh sách các user => truyền vào params gồm token + page (mặc định 20 giá trị/page)
  userList: async (data) => {
    try {
      return await api.get(
        `users?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&department=${
          data.department ? data.department : ""
        }&deleted=${data.deleted ? data.deleted : ""}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 1.1 - Lấy thông tin user cụ thể
  getUser: async (data) => {
    try {
      return await api.get(`users/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 2 - Chỉnh sửa người dùng
  userUpdate: async (data) => {
    try {
      return await api.post(`users/${data.id}?token=${data.token}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 5 - Tạo user
  createUser: async (data) => {
    try {
      return await api.post("users", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 3 - Lấy danh sách chức vụ
  positionList: async (data) => {
    try {
      return await api.get(
        `option/position?token=${data.token}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 4 - Lấy danh sách bộ phận
  departmentList: async (data) => {
    try {
      return await api.get(
        `option/department?token=${data.token}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 5 - Xoá người dùng
  deleteUsser: async (data) => {
    try {
      return await api.post("users/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 6 - Cập nhật chức vụ
  updatePosition: async (data) => {
    try {
      return await api.post(`option/position`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 7 - Cập nhật bộ phận
  updateDepartment: async (data) => {
    try {
      return await api.post(`option/department`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
};
