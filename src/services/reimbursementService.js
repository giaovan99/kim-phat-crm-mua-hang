// API về phân quyền
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const reimbursementService = {
  // 1 - uploadFile
  importFile: async (data) => {
    try {
      return await api.post(`import_file?token=${data.token}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 1 - accessFile
  accessFile: async (data) => {
    try {
      return await api.post(`access_file?token=${data.token}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      // goToLogin();
    }
  },

  // 1 - accessFile
  processFile: async (data) => {
    try {
      return await api.post(`process_file?token=${data.token}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 2 - Lấy danh sách phiếu hoàn ứng
  reimbursementList: async (data) => {
    try {
      return await api.get(
        `payment_request?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&status=${
          data.status ? data.status : ""
        }&staff_number=${
          data.staff_number ? data.staff_number : ""
        }&paymentMethod=${
          data.paymentMethod ? data.paymentMethod : ""
        }&supplier_name=${
          data.supplier_name ? data.supplier_name : ""
        }&product_code=${
          data.product_code ? data.product_code : ""
        }&product_name=${data.product_name ? data.product_name : ""}&deleted=${
          data.deleted ? data.deleted : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 6 - Lấy thông tin phiếu hoàn ứng cụ thể
  getReimbursementSlip: async (data) => {
    try {
      return await api.get(`payment_request/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 8 - Chỉnh sửa phiếu hoàn ứng
  updateReimbursementSlip: async (data) => {
    try {
      return await api.post(
        `payment_request/${data.id}?token=${data.token}`,
        data,
        {
          headers: {
            "Content-Type": "multipart/form-data",
          },
        }
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 9 - Tạo phiếu hoàn ứng
  createReimbursementSlip: async (data) => {
    try {
      return await api.post(`payment_request`, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 10 - Xoá phiếu hoàn ứng
  deleteReimbursementSlip: async (data) => {
    try {
      return await api.post("payment_request/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 11 - Lấy danh sách trạng thái phiếu hoàn ứng
  reimbursementStatus: async (data) => {
    try {
      return await api.get(
        `option/payment_request_status?token=${data.token}}`
      );
    } catch (err) {
      // showError();
      // // goToLogin();
      console.log(err);
    }
  },

  // 12 - Xuất file export phiếu hoàn ứng
  reimbursementExport: async (data) => {
    try {
      return await api.get(
        `payment_request/export/details?token=${data.token}&id=${
          data.id ? data.id : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // // 5 - Lấy danh sách sản phẩm hoàn ứng
  // reimbursementStatus: async (data) => {
  //   try {
  //     return await api.get(
  //       `stock_import_part?token=${data.token}&page=${data.page}&show_all=${
  //         data.show_all ? data.show_all : ""
  //       }&keywords=${data.keywords ? data.keywords : ""}&deleted=${
  //         data.deleted ? data.deleted : ""
  //       }`
  //     );
  //   } catch (err) {
  //     // showError();
  // // goToLogin();
  //     console.log(err);
  // // goToLogin();
  //   }
  // },
};
