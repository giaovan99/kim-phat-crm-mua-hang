// API về phân quyền
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const rolesService = {
  // 1 - Danh sách các roles
  roleList: async (data) => {
    try {
      return await api.get(
        `roles?token=${data.token}&page=${data.page}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  //  2 - Thêm phân quyền
  addRole: async (data) => {
    try {
      return await api.post("roles", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 3 - Lấy thông tin phân quyền
  getRoles: async (data) => {
    try {
      return await api.get(`roles/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 4 - Cập nhật phân quyền
  updateRole: async (data) => {
    try {
      return await api.post(`roles/${data.id}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 5 - Danh sách tính năng
  funtions: async (data) => {
    try {
      return await api.get(`functions?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 6 - Xoá phân quyền
  deleteRole: async (data) => {
    try {
      return await api.post("roles/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
};
