// API về đăng nhập, đăng xuất,...
import { api } from "../api/axios";

export const importFileService = {
  // throw-exception
  throwException: async (data) => {
    try {
      return await api.post("throw-exception", data);
    } catch (err) {
      // showError();
      // goToLogin();
      console.log(err);
    }
  },
};
