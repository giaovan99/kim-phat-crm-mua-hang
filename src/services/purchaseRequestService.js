// API về đề nghị mua hàng
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const purchaseRequestService = {
  // 1 - Lấy danh sách trạng thái đơn đề nghị
  requestStatus: async (data) => {
    try {
      return await api.get(
        `option/purchase_proposal_status?token=${data.token}}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 2 - Lấy danh sách đơn đề nghị mua hàng
  requestList: async (data) => {
    console.log(data);
    try {
      return await api.get(
        `purchase_proposal?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&status=${
          data.status ? data.status : ""
        }&status_completed=${data.status_completed}&department=${
          data.department ? data.department : ""
        }&created_by=${data.created_by ? data.created_by : ""}&deleted=${
          data.deleted ? data.deleted : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 3 - Lấy thông tin đơn đề nghị mua hàng cụ thể
  getPurchaseRequest: async (data) => {
    try {
      return await api.get(`purchase_proposal/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 8 - Chỉnh sửa đơn đề nghị mua hàng
  updatePurchaseRequest: async (data) => {
    try {
      return await api.post(
        `purchase_proposal/${data.id}?token=${data.token}`,
        data
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 9 - Tạo đơn đề nghị mua hàng
  createPurchaseRequest: async (data) => {
    try {
      return await api.post("purchase_proposal", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 10 - Xoá đơn đề nghị mua hàng
  deletePurchaseRequest: async (data) => {
    try {
      return await api.post("purchase_proposal/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 11 - Lấy danh sách loại hình
  requestType: async (data) => {
    try {
      return await api.get(
        `option/purchase_proposal_type?token=${data.token}}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 12 - In phiếu đề nghị
  printPP: async (data) => {
    try {
      return await api.get(
        `purchase_proposal/print/${data.id}?token=${data.token}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 12 - Xuất file excel cho Đền nghị mua hàng
  ppExport: async (data) => {
    try {
      return await api.get(
        `purchase_proposal/export/details?token=${data.token}&id=${
          data.id ? data.id : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
};
