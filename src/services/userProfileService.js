// API về thông tin người đang đăng nhập
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const userProfileService = {
  // 1 - Get thông tin cá nhân
  profile: async (data) => {
    try {
      return await api.get(`profile?token=${data}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 2 - Update Profile
  updateProfile: async (data) => {
    try {
      return await api.post(`profile`, data, {
        headers: {
          "Content-Type": "multipart/form-data",
        },
      });
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  //  3 - Change Password
  changePassword: async (data) => {
    try {
      return await api.post(
        `change-password?token=${data.token}&password=${data.password}&confirm_password=${data.confirm_password}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
};
