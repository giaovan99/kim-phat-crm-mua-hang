// API về đăng nhập, đăng xuất,...
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const authService = {
  // 1 - Đăng nhập
  login: async (data) => {
    try {
      return await api.post("auth/login", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 2 - Đăng xuất
  logout: async (data) => {
    try {
      return await api.post(`auth/logout?token=${data}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },

  // 3 - Quên mật khẩu
  forgotPassword: async (data) => {
    try {
      return await api.post("forgot-password", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 4 - Reset mật khẩu
  resetPassword: async (data) => {
    try {
      return await api.post("reset-password", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 5 - Check Token Reset Passwords
  checkToken: async (data) => {
    try {
      return await api.post("check-token", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
};
