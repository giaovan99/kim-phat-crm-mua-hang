// API về đơn hàng
import { api } from "../api/axios";
import { throwException } from "../utils/importFile";
import { goToLogin, showError } from "../utils/others";

export const orderService = {
  // 1 - Lấy danh sách phương thức giao hàng
  shippingMethodList: async (data) => {
    try {
      return await api.get(
        `option/purchase_order_shipment?token=${data.token}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 2 - Cập nhật đơn phương thức giao hàng
  updateShippingMethod: async (data) => {
    try {
      return await api.post(`option/purchase_order_shipment`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 3 - Lấy danh sách phương thức thanh toán
  paymentMethodList: async (data) => {
    try {
      return await api.get(
        `option/purchase_order_payment?token=${data.token}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 4 - Cập nhật đơn phương thức thanh toán
  updatePaymentMethod: async (data) => {
    try {
      return await api.post(`option/purchase_order_payment`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 3 - Lấy danh sách yêu cầu về hoá đơn
  invoiceRequestList: async (data) => {
    try {
      return await api.get(
        `option/invoice_request?token=${data.token}&keywords=${
          data.keywords ? data.keywords : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 4 - Cập nhật đơn yêu cầu về hoá đơn
  updateInvoiceRequest: async (data) => {
    console.log("cập nhật yêu cầu hoá đơn");
    try {
      return await api.post(`option/invoice_request`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 5 - Lấy danh sách trạng thái đơn hàng
  orderStatus: async (data) => {
    try {
      return await api.get(`option/purchase_order_status?token=${data.token}}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 6 - Lấy danh sách đơn hàng
  orderList: async (data) => {
    try {
      return await api.get(
        `purchase_order?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&startDate=${
          data.startDate ? data.startDate : ""
        }&buyer_id=${data.buyer_id ? data.buyer_id : ""}&endDate=${
          data.endDate ? data.endDate : ""
        }&deleted=${data.deleted ? data.deleted : ""}&status=${
          data.status ? data.status : ""
        }&paymentMethod=${
          data.paymentMethod ? data.paymentMethod : ""
        }&status_payment=${data.status_payment ? data.status_payment : ""}`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 7 - Lấy thông tin đơn hàng cụ thể
  getOrder: async (data) => {
    try {
      return await api.get(`purchase_order/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 8 - Chỉnh sửa đơn hàng
  updateOrder: async (data) => {
    try {
      return await api.post(
        `purchase_order/${data.id}?token=${data.token}`,
        data
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 9 - Tạo đơn hàng
  createOrder: async (data) => {
    try {
      return await api.post("purchase_order", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  // 10 - Xoá đơn hàng
  deleteOrder: async (data) => {
    try {
      return await api.post("purchase_order/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },
  //11 - Lấy danh sách cty
  companyList: async (data) => {
    try {
      return await api.get(
        `company?token=${data.token}&page=${data.page}&show_all=${
          data.show_all ? data.show_all : ""
        }&keywords=${data.keywords ? data.keywords : ""}&deleted=${
          data.deleted ? data.deleted : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
  // 12 - Lấy thông tin cty cụ thể
  getCompany: async (data) => {
    // console.log(data);
    try {
      return await api.get(`company/${data.id}?token=${data.token}`);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 13 - Chỉnh sửa cty
  companyUpdate: async (data) => {
    try {
      return await api.post(`company/${data.id}?token=${data.token}`, data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 15 - Tạo cty
  createCompany: async (data) => {
    try {
      return await api.post("company", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 14 - Xoá cty
  deleteCompany: async (data) => {
    try {
      return await api.post("company/delete", data);
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 5 - Lấy danh sách sản phẩm nhập kho từ đơn hàng
  stockImportPartListPO: async (data) => {
    try {
      return await api.get(
        `purchase_order_stock_import?token=${data.token}&page=${
          data.page
        }&show_all=${data.show_all ? data.show_all : ""}&keywords=${
          data.keywords ? data.keywords : ""
        }&nk_code=${data.nk_code ? data.nk_code : ""}&hd_code=${
          data.hd_code ? data.hd_code : ""
        }&waiting=${data.waiting ? data.waiting : ""}&deleted=${
          data.deleted ? data.deleted : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // In danh sách sản phẩm nhập kho từ đơn hàng
  stockImportPrintPO: async (data) => {
    try {
      return await api.post(
        `purchase_order_stock_import/export?token=${data.token}`,
        data
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // Lấy danh sách đơn hàng trong phiếu đề nghị mua hàng
  orderList2: async (data) => {
    try {
      return await api.get(
        `purchase_order?&key=id&value=code&token=${
          data.token
        }&page=1&show_all=true&keywords=${
          data.keywords ? data.keywords : ""
        }&startDate=${data.startDate ? data.startDate : ""}&endDate=${
          data.endDate ? data.endDate : ""
        }&deleted=${data.deleted ? data.deleted : ""}&status=${
          data.status ? data.status : ""
        }&paymentMethod=${
          data.paymentMethod ? data.paymentMethod : ""
        }&status_payment=true`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
      // // goToLogin();
    }
  },

  // 12 - Xuất file excel cho đơn hàng
  orderExport: async (data) => {
    try {
      return await api.get(
        `purchase_order/export/details?token=${data.token}&id=${
          data.id ? data.id : ""
        }`
      );
    } catch (err) {
      throwException({
        data: data,
        message: err,
      });
      console.log(err);
      goToLogin();
    }
  },
};
