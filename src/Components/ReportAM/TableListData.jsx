import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { setPage } from "../../redux/slice/pageSlice";
import { useDispatch, useSelector } from "react-redux";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import { reportAMList } from "../../utils/reportAPI";
import { convertToArray, stringToDate } from "../../utils/others";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};
const TableListData = ({ formik, reload, setReload }) => {
  let page = useSelector((state) => state.pageSlice.value);
  const dispatch = useDispatch();
  const [listData1, setListData1] = useState([]);
  const [listData2, setListData2] = useState([]);

  // 3 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    size: "small",
  };

  // 3 - Kéo data list phiếu hoàn ứngtheo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = { ...formik.values };

    let res = await reportAMList(page, keyParam);
    // console.log(res);
    // console.log(res);
    if (res.status) {
      setListData1(res.data.tam_ung);
      setListData2(res.data.hoan_ung);
    }
    dispatch(setSpinner(false));
  };

  // 9 - Lần đầu load trang => Kéo về danh sách user & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    getListData(page);
  }, []);

  // 10 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      // console.log("run");
      // dispatch(resetPage());
      getListData(page);
      setReload(false);
    }
  }, [reload]);
  Table.SELECTION_COLUMN.fixed = "left";

  const columns1 = [
    {
      title: "Báo cáo tạm ứng",
      children: [
        {
          title: "STT",
          dataIndex: "index",
          rowScope: "row",
          fixed: "left",
          width: 50,
          render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
        },
        {
          title: "Mã phiếu",
          dataIndex: "code",
          key: "code",
          width: 87,
          className: "text-center",
        },
        {
          title: "Ngày tạm ứng",
          dataIndex: "created_at",
          width: 87,
          key: "created_at",
          render: (text) => <p className="text-center">{stringToDate(text)}</p>, //
        },
        {
          title: "Người tạm ứng",
          dataIndex: "staff_number_name",
          key: "staff_number_name",
        },
        {
          title: "Số tiền tạm ứng",
          dataIndex: "total",
          width: 100,
          key: "total",
          render: (text, record) => {
            return (
              <p className="text-right">
                {text ? Number(text).toLocaleString("en-US") : ""}
              </p>
            );
          },
        },
      ],
    },
  ];
  const columns2 = [
    {
      title: "Báo cáo hoàn ứng",
      children: [
        {
          title: "STT",
          dataIndex: "index",
          rowScope: "row",
          fixed: "left",
          width: 50,
          render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
        },
        {
          title: "Mã phiếu",
          dataIndex: "code",
          width: 87,
          key: "code",
          className: "text-center",
        },
        {
          title: "Ngày thanh toán",
          width: 87,
          dataIndex: "created_at",
          key: "created_at",
          render: (text) => <p className="text-center">{stringToDate(text)}</p>, //
        },
        {
          title: "Người đề nghị TT",
          dataIndex: "staff_number_name",
          key: "staff_number_name",
        },
        {
          title: "Mã đơn hàng",
          dataIndex: "po_no",
          width: 87,
          key: "po_no",
          className: "text-center",
          render: (text, record) => (
            <p className="text-center">{convertToArray(record.po_no)}</p>
          ),
        },
        {
          title: "Tổng tiền",
          dataIndex: "total",
          key: "total",
          render: (text, record) => {
            return (
              <p className="text-right">
                {text ? Number(text).toLocaleString("en-US") : ""}
              </p>
            );
          },
        },
        {
          title: "PTTT",
          dataIndex: "paymentMethod",
          key: "po_no",
        },
      ],
    },
  ];

  return (
    <div className="table-render-API flex gap-2">
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 600,
        }}
        columns={columns1}
        dataSource={listData1}
        total={listData1.length}
        pageSize={listData1.length}
      />
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 600,
          x: 650,
        }}
        columns={columns2}
        dataSource={listData2}
        total={listData2.length}
        pageSize={listData2.length}
      />
    </div>
  );
};

export default TableListData;
