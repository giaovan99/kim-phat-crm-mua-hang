import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
import { DatePicker, Select } from "antd";
import { normalize } from "../../utils/others";
const { RangePicker } = DatePicker;
export default function Actions({ listStaff, formik }) {
  // Filter `option.key` match the user type `input`
  const filterOption = (input, option) =>
    normalize(option?.label ?? "").includes(normalize(input));

  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div className="flex gap-4 items-end">
            {/* Filter thời gian */}
            <div className="form-group relative">
              <div className="form-style form-style-block ">
                <label>Thời gian</label>
                <RangePicker
                  size="medium"
                  variant="borderless"
                  format="DD/MM/YYYY"
                  onChange={(value, dateString) => {
                    formik.setFieldValue("start_date", dateString[0]);
                    formik.setFieldValue("end_date", dateString[1]);
                  }}
                  className="h-[38px]"
                />
              </div>
            </div>
            {/* Filter người đề nghị */}
            <div className="form-group">
              <div className="form-style form-style-block">
                <label htmlFor="staff_number">Người đề nghị</label>
                <Select
                  showSearch
                  placeholder="Chọn người đề nghị"
                  optionFilterProp="children"
                  onChange={(value) => {
                    formik.setFieldValue("staff_number", value);
                  }}
                  filterOption={filterOption}
                  options={listStaff}
                  value={
                    formik.values.staff_number
                      ? formik.values.staff_number
                      : null
                  }
                  className="min-w-[140px]"
                />
              </div>
            </div>
            {/* Lọc */}
            <button className="btn-main-yl btn-actions" type="submit">
              <HiOutlineSearch />
              Lọc
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
