import { useFormik } from "formik";
import React from "react";
import * as yup from "yup";
import { changePwd } from "../../../utils/userProfile";
import { useDispatch } from "react-redux";
import { setSpinner } from "../../../redux/slice/spinnerSlice";

export default function ChangePassword() {
  let dispatch = useDispatch();
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      password: "",
      confirm_password: "",
    },
    validationSchema: yup.object({
      password: yup
        .string()
        .required("Mật khẩu mới không để trống")
        .matches(/^().{8,}$/, "Mật khẩu yêu cầu ít nhất 8 ký tự"),
      confirm_password: yup
        .string()
        .required("Xác nhận lại mật khẩu không để trống")
        .oneOf([yup.ref("password"), null], "Mật khẩu chưa trùng khớp"),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      changePwd(values, formik);
      dispatch(setSpinner(false));
    },
  });
  const { handleBlur, handleChange, touched, errors } = formik;

  return (
    <div className="change-password">
      <form id="formChangePassword" onSubmit={formik.handleSubmit}>
        <div className="grid grid-cols-2 gap-3">
          {/* ======================= Mật khẩu mới ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="password">Mật khẩu mới</label>
              <input
                className="grow"
                type="password"
                placeholder="Nhập mật khẩu mới"
                name="password"
                id="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.password && errors.password ? (
              <p className="mt-1 text-red-500 text-sm" id="password-warning">
                {errors.password}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Xác nhận lại mật khẩu mới ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="confirm_password">
                Xác nhận lại mật khẩu mới
              </label>
              <input
                className="grow"
                type="password"
                placeholder="Nhập lại mật khẩu mới"
                name="confirm_password"
                id="confirm_password"
                value={formik.values.confirm_password}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.confirm_password && errors.confirm_password ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="confirm_password-warning"
              >
                {errors.confirm_password}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
        {/* ======================= btn actions ======================= */}
        <div className="flex justify-end gap-3 mt-3">
          <button className="btn-main-yl" type="submit">
            Đổi mật khẩu
          </button>
          <button
            className="btn-cancel"
            type="button"
            onClick={formik.resetForm}
          >
            Huỷ
          </button>
        </div>
      </form>
    </div>
  );
}
