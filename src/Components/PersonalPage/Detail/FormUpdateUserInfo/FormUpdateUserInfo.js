import React, { useEffect } from "react";
import { Modal, Image } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { BiUpload } from "react-icons/bi";
import { getProfile, updateProfile } from "../../../../utils/userProfile";
import { useDispatch } from "react-redux";
import { setInfo } from "../../../../redux/slice/userProfileSlice";
import { setLocalStorage } from "../../../../utils/localStorage";
import Swal from "sweetalert2";
import { setSpinner } from "../../../../redux/slice/spinnerSlice";

export default function FormUpdateUserInfo({
  isModalOpen,
  userInfo,
  setIsModalOpen,
}) {
  let dispatch = useDispatch();
  const handleOk = () => {
    formik.handleSubmit();
  };

  const handleCancel = () => {
    formik.setValues(userInfo);
    setIsModalOpen(false);
  };

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      email: "",
      full_name: "",
      phone: "",
      avatar: "./img/imgNotFound/not-found.jpeg",
    },
    validationSchema: yup.object({
      full_name: yup.string().required("Họ và tên không để trống"),
      phone: yup
        .string()
        .required("Số điện thoại không để trống")
        .matches(
          /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
          "Số điện thoại không hợp lệ"
        ),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      let res = await updateProfile(values);
      if (res) {
        await fSaveProfile();
        Swal.fire({
          title: "Cập nhật thông tin",
          text: "Cập nhật thông tin thành công!",
          icon: "success",
        });
        setIsModalOpen(false);
      }
      dispatch(setSpinner(false));
    },
  });
  const fSaveProfile = async () => {
    let res = await getProfile();
    setLocalStorage("profile", JSON.stringify(res.data));
    if (res.status) {
      dispatch(setInfo(res.data));
    }
  };
  const { handleBlur, handleChange, touched, errors } = formik;

  // Khi userInfo thay đổi thì formik cũng thay đổi
  useEffect(() => {
    if (userInfo) {
      formik.setValues({
        email: userInfo.email,
        full_name: userInfo.full_name,
        phone: userInfo.phone,
        avatar: userInfo.avatar
          ? userInfo.avatar
          : "./img/imgNotFound/not-found.jpeg",
      });
    }
  }, [userInfo]);

  return (
    <Modal
      title="Cập nhật thông tin"
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Cập nhật"
      cancelText="Huỷ"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center",
      }}
    >
      <form id="updatePersonalDetail">
        <div className="grid grid-cols-2 gap-3">
          {/* ======================= Ảnh đại diện ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label className="mb-3">Ảnh đại diện</label>
              <div className="flex gap-7 items-center">
                <Image
                  className="overflow-hidden rounded-full object-cover object-center"
                  width={180}
                  height={180}
                  src={
                    typeof formik.values.avatar == "object" &&
                    formik.values.avatar
                      ? URL.createObjectURL(formik.values.avatar)
                      : formik.values.avatar
                  }
                />
                <label
                  htmlFor="avatar"
                  className="flex gap-3 items-center btn-main-yl"
                >
                  <BiUpload /> Tải ảnh lên
                </label>
              </div>
              <input
                id="avatar"
                name="avatar"
                type="file"
                accept="image/*"
                className="hidden"
                onChange={(event) => {
                  formik.setFieldValue("avatar", event.target.files[0]);
                }}
              />
            </div>
          </div>
          {/* ======================= Họ và tên ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="full_name">Họ tên</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập họ và tên"
                name="full_name"
                id="full_name"
                value={formik.values.full_name}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.full_name && errors.full_name ? (
              <p className="mt-1 text-red-500 text-sm" id="full_name-warning">
                {errors.full_name}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Số điện thoại ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="phone">Số điện thoại</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập số điện thoại"
                name="phone"
                id="phone"
                value={formik.values.phone}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.phone && errors.phone ? (
              <p className="mt-1 text-red-500 text-sm" id="phone-warning">
                {errors.phone}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
      </form>
    </Modal>
  );
}
