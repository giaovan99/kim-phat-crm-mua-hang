import React, { useState } from "react";
import { Image } from "antd";
import { BsPencilSquare } from "react-icons/bs";
import "./detail.scss";
import FormUpdateUserInfo from "./FormUpdateUserInfo/FormUpdateUserInfo";

import { useSelector } from "react-redux";

export default function Detail() {
  const { userInfo } = useSelector((state) => state.userProfileSlice);

  const [isModalOpen, setIsModalOpen] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };

  // Check quyền truy cập

  // Step: Lấy danh sách Roles && Lấy thông tin Profile  -----(true)----> Tiếp tục truy cập trang
  //                      |
  //                       -----(false)----> Trả về trang chủ

  return (
    <div className="personal-detail">
      <div className="grid grid-cols-4 gap-4">
        {/* ======================= image ======================= */}
        <div className="image">
          <Image
            className="object-cover object-center overflow-hidden rounded-full"
            width={200}
            height={200}
            src={
              userInfo?.avatar
                ? userInfo.avatar
                : "./img/imgNotFound/not-found.jpeg"
            }
          />
        </div>
        {/* ======================= Info ======================= */}
        <div className="col-span-3 flex flex-col justify-between">
          <ul>
            <li>
              {/* ======================= Họ tên ======================= */}
              <p className="title">Họ và tên:</p>
              <p className="description">{userInfo?.full_name}</p>
            </li>
            <li>
              {/* ======================= Email ======================= */}
              <p className="title">Email:</p>
              <p className="description">{userInfo?.email}</p>
            </li>
            <li>
              {/* ======================= Số điện thoại ======================= */}
              <p className="title">Số điện thoại:</p>
              <p className="description">{userInfo?.phone}</p>
            </li>
            <li>
              {/* ======================= Chức vụ ======================= */}
              <p className="title">Chức vụ:</p>
              <p className="description">{userInfo?.position}</p>
            </li>
            <li>
              {/* ======================= Bộ phận ======================= */}
              <p className="title">Bộ phận:</p>
              <p className="description">{userInfo?.department_name}</p>
            </li>
          </ul>
          {/* ======================= btn cập nhật thông tin ======================= */}
          <div>
            <button className="btn-main-yl flex gap-2" onClick={showModal}>
              <BsPencilSquare />
              Cập nhật thông tin
            </button>
            <FormUpdateUserInfo
              isModalOpen={isModalOpen}
              setIsModalOpen={setIsModalOpen}
              userInfo={userInfo}
            />
          </div>
        </div>
      </div>
    </div>
  );
}
