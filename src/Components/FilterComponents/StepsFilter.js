import React, { useEffect, useState } from "react";
import { Tabs } from "antd";
import { useDispatch } from "react-redux";
import { setCurrent } from "../../redux/slice/statusSlice";

export default function StepsFilter({ sttArr, setReload }) {
  let [items, setItems] = useState([]);
  //  let current = useSelector(
  let dispatch = useDispatch();

  // thay đổi trạng thái
  const onChange = (value) => {
    dispatch(setCurrent(value));
    setReload(true);
  };

  // Convert arr trạng thái
  const convertArr = () => {
    setItems(sttArr);
    dispatch(setCurrent(""));
    setReload(true);
  };

  useEffect(() => {
    convertArr();
  }, [sttArr]);

  return (
    <div className="my-3 flex justify-center">
      <Tabs
        defaultActiveKey="1"
        items={items}
        onChange={onChange}
        className="stepFilter"
      />
    </div>
  );
}
