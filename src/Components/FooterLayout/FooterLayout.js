import React, { useEffect, useState } from "react";
import { Layout } from "antd";
import { useSelector } from "react-redux";
const { Footer } = Layout;
export default function FooterLayout() {
  const settingInfo = useSelector((state) => state.loginInfoSlice.settingInfo);
  const [footerScript, setFooterScript] = useState();
  useEffect(() => {
    if (settingInfo.length > 0) {
      settingInfo.forEach((item) => {
        // Gắn giá trị cho footer text
        if (item.keyword == "copy_right") {
          setFooterScript(item.value);
        }
        // Gắn giá trị cho favicon
        if (item.keyword == "favicon") {
          const iconLink = document.querySelector('link[rel="icon"]');
          if (iconLink && item.url) {
            iconLink.setAttribute("href", item.url);
          } else {
            console.error("Icon link element not found");
          }
        }
        // Gắn giá trị cho favicon
        // document.querySelector('link[rel="icon"]').href = "upload/mamma.png";
      });
    }
  }, [settingInfo]);

  return <Footer style={{ textAlign: "center" }}>{footerScript}</Footer>;
}
