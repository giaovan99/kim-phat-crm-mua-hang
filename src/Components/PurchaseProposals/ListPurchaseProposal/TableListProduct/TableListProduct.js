import { Table } from "antd";
import React from "react";
import { stringToDate } from "../../../../utils/others";
const tableProps = {
  bordered: true,
  size: "small",
};
const paginationObj = {
  position: ["none", "bottomCenter"],
  hideOnSinglePage: true,
  pageSize: 20,
};
export default function TableListProduct({ type, cloneProduct }) {
  const columns = [
    {
      title: "Mã sản phẩm",
      dataIndex: "sku",
      key: "sku",
      width: 100,
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
      fixed: "left",
      width: 170,
    },
    {
      title: "Quy cách",
      dataIndex: "specifications",
      key: "specifications",
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 80,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },

    {
      title: "Số lượng",
      children: [
        {
          title: "Đề nghị",
          dataIndex: "qty",
          key: "qty",
          width: 130,
          render: (text, record) => {
            return (
              <p className="text-center">
                {text ? Number(text).toLocaleString("en-US") : 0}
              </p>
            );
          },
        },
        {
          title: "Tồn kho",
          dataIndex: "qty_inventory",
          key: "qty_inventory",
          width: 130,
          render: (text, record) => {
            return (
              <p className="text-center">
                {text ? Number(text).toLocaleString("en-US") : 0}
              </p>
            );
          },
        },
        {
          title: "Cần đặt",
          dataIndex: "qty_purchase",
          key: "qty_purchase",
          width: 130,
          render: (text, record) => {
            return (
              <p className="text-center">
                {text ? Number(text).toLocaleString("en-US") : 0}
              </p>
            );
          },
        },
        {
          title: "Đã đặt",
          dataIndex: "qty_ordered",
          key: "qty_ordered",
          width: 130,
          render: (text, record) => {
            return (
              <p className="text-center">
                {text ? Number(text).toLocaleString("en-US") : 0}
              </p>
            );
          },
        },
      ],
    },
    {
      title: "Ngày cần giao",
      dataIndex: "delivery_date",
      key: "delivery_date",
      width: 150,
      render: (text, record) => {
        return (
          <p className="text-center">
            {record.delivery_date ? stringToDate(text) : ""}
          </p>
        );
      },
    },
    {
      title: "Chỉ định người mua hàng",
      dataIndex: "buyer_name",
      key: "buyer_name",
      width: 150,
    },
  ];
  const columns2 = [
    {
      title: "Mã sản phẩm",
      dataIndex: "sku",
      key: "sku",
      width: 100,
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
      fixed: "left",
      width: 170,
    },
    {
      title: "Quy cách",
      dataIndex: "specifications",
      key: "specifications",
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 80,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Đơn hàng",
      hidden: true,
      children: [
        {
          title: "PO",
          dataIndex: "po_export_code",
          key: "po_export_code",
          width: 100,
          render: (text, record) => {
            return <p className="text-center">{text}</p>;
          },
        },
        {
          title: "SKU",
          dataIndex: "po_export_sku",
          key: "po_export_sku",
          width: 100,
          render: (text, record) => {
            return <p className="text-center">{text}</p>;
          },
        },
        {
          title: "Master Case Pack",
          dataIndex: "po_export_pack",
          key: "po_export_pack",
          width: 100,
          render: (text, record) => {
            return <p className="text-center">{text}</p>;
          },
        },
      ],
    },
    {
      title: "Số lượng",
      children: [
        {
          title: "Đề nghị",
          dataIndex: "qty",
          key: "qty",
          width: 100,
          render: (text, record) => {
            return (
              <p className="text-center">
                {text ? Number(text).toLocaleString("en-US") : ""}
              </p>
            );
          },
        },
        {
          title: "Tồn kho",
          dataIndex: "qty_inventory",
          key: "qty_inventory",
          width: 100,
          render: (text, record) => {
            return (
              <p className="text-center">
                {text ? Number(text).toLocaleString("en-US") : ""}
              </p>
            );
          },
        },
        {
          title: "Cần đặt",
          dataIndex: "qty_purchase",
          key: "qty_purchase",
          width: 100,
          render: (text, record) => {
            return (
              <p className="text-center">
                {text ? Number(text).toLocaleString("en-US") : ""}
              </p>
            );
          },
        },
      ],
    },
    {
      title: "Ngày cần giao",
      dataIndex: "purchase_date",
      key: "purchase_date",
      width: 130,
      render: (text, record) => {
        return (
          <p className="text-center">
            {record.purchase_date ? stringToDate(text) : ""}
          </p>
        );
      },
    },
    {
      title: "Chỉ định người mua hàng",
      dataIndex: "buyer_name",
      key: "buyer_name",
      width: 130,
    },
  ];
  return (
    <Table
      {...tableProps}
      pagination={{
        ...paginationObj,
      }}
      scroll={{
        y: 600,
        x: type === "1" ? 1400 : 1700,
      }}
      columns={type === "1" ? columns : columns2}
      dataSource={cloneProduct}
    />
  );
}
