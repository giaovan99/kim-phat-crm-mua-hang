import React, { useEffect, useState } from "react";
import { Tag, Table, Tooltip, message } from "antd";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import {
  deletePurchaseRequestId,
  getPurchaseRequest,
  getPurchaseRequestList,
  ppExport,
  updatePurchaseRequest,
} from "../../../utils/purchaseRequest";
import { setPage } from "../../../redux/slice/pageSlice";
import { otherKeys } from "../../../utils/user";
import { showError, stringToDate } from "../../../utils/others";
import { FaFilePdf } from "react-icons/fa6";
import ReactToPrint from "react-to-print";
import PrintPP from "../../ExportToPDF/PurchaseProposalPDF/PrintPP";
import DeleteBtnItemComponent from "../../ButtonComponent/DeleteBtnItemComponent";
import FixBtnComponent from "../../ButtonComponent/FixBtnComponent";
import RenderWithViewComponent from "../../ButtonComponent/RenderIdView";
import ViewBtnComponent from "../../ButtonComponent/ViewBtnComponent";
import UndoBtnComponent from "../../ButtonComponent/UndoBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import TableLength from "../../ButtonComponent/TableLength";
import ExportDetailsBtnComponent from "../../ButtonComponent/ExportDetailsBtnComponent";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListPurchaseProposals({
  showViewDetailModal,
  reload,
  setReload,
  deletedPage,
  rowSelection,
  permission,
  setProposal,
  searchFilter,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();
  let navigate = useNavigate();
  let page = useSelector((state) => state.pageSlice.value);
  // 2 - Chuyển hướng sang trang chi tiết
  const goToFixProposal = (key) => {
    navigate(`/purchase_proposal/fix/${key}`);
  };
  let current = useSelector((state) => state.statusSlice.current);

  // 3 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 4 - Kéo data list user theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }
    keyParam = {
      ...keyParam,
      ...searchFilter,
      status: current,
      status_completed: current == 4 ? 1 : 0,
    };

    let res = await getPurchaseRequestList(page, keyParam);
    if (res.status) {
      setListData(res.data.data);
      setTotal(res.data.total);
      setPageSize(res.data.per_page);
    }
    dispatch(setSpinner(false));
  };

  // 5 - Xác nhận xoá đơn hàng
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deleteData(id, value);
  };

  // 6 - Xoá đơn hàng
  const deleteData = async (id, input) => {
    dispatch(setSpinner(true));
    let res;
    if (input == 1) {
      res = await deletePurchaseRequestId({ id: id, deleted: true });
    } else {
      res = await deletePurchaseRequestId({ id: id });
    }
    if (res) {
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 7 - Hoàn tác xoá
  const undoDelete = async (id) => {
    dispatch(setSpinner(true));
    let res;
    res = await updatePurchaseRequest({ id: id, deleted: false });
    // console.log(res);
    if (res) {
      // console.log("reload trang");
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 8 - Kích hoạt đơn hàng
  const confirmUndo = (id) => {
    undoDelete(id);
  };

  // 9 - Xuất file excel
  const exportFile = async (id) => {
    const res = await ppExport({ id, type: "items" });
    if (res) {
      // Tạo thẻ <a> để tải file
      const link = document.createElement("a");
      link.href = res;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  };

  // 9 - Lần đầu load trang => Kéo về danh sách user & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    getListData(page);
  }, []);

  // 10 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      // dispatch(resetPage());
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  {
    /* ======================= Start IN  ======================= */
  }
  // 2 - lấy chi tiết phiếu
  const getPurchaseRequestInfo = async (id) => {
    dispatch(setSpinner(true));
    let res = await getPurchaseRequest(id);
    if (res.data.success) {
      setDetails(res.data.data);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  const componentRef = React.useRef(null);

  const onBeforeGetContentResolve = React.useRef(null);

  const [loading, setLoading] = React.useState(false);
  const [PPDetails, setDetails] = useState();

  const handleAfterPrint = () => {
    // console.log("`onAfterPrint` called");
    onBeforeGetContentResolve.current = null;
    setLoading(false);
  };

  const handleBeforePrint = React.useCallback(() => {
    // console.log("`onBeforePrint` called");
  }, []);

  const handleOnBeforeGetContent = async (id) => {
    // console.log("`onBeforeGetContent` called");
    await getPurchaseRequestInfo(id);
    // console.log("dont");
    // return componentRef.current;
  };

  React.useEffect(() => {
    if (loading && onBeforeGetContentResolve.current) {
      // Resolves the Promise, letting `react-to-print` know that the DOM updates are completed
      onBeforeGetContentResolve.current();
    }
  }, [loading]);

  const reactToPrintTrigger = React.useCallback(() => {
    // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
    // to the root node of the returned component as it will be overwritten.

    // Bad: the `onClick` here will be overwritten by `react-to-print`
    // return <button onClick={() => alert('This will not work')}>Print this out!</button>;

    // Good
    return (
      <button id="btnPdf">
        <FaFilePdf className="text-[#B6190D]" />
      </button>
    );
  }, []);

  {
    /* ======================= End IN  ======================= */
  }

  Table.SELECTION_COLUMN.fixed = "left";
  const columns = [
    {
      title: "Mã phiếu",
      dataIndex: "code",
      key: "code",
      width: 100,
      fixed: "left",
      render: (text, record) => (
        <RenderWithViewComponent
          onClickF={() => {
            showViewDetailModal(record.id);
          }}
          text={text}
        />
      ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "created_at",
      key: "created_at",
      width: 100,
      render: (text, record) => {
        return <p className="text-center">{text ? stringToDate(text) : ""}</p>;
      },
    },
    ...(permission?.purchase_proposal?.full
      ? [
          {
            title: "Người tạo",
            dataIndex: "created_by_name",
            key: "created_by_name",
            render: (text, record) => {
              return <p className="text-center">{text ? text : ""}</p>;
            },
          },
        ]
      : []),
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      width: 140,
      render: (text, record) => {
        return (
          <p className="text-center">
            {text === "Khởi tạo" ? (
              <Tag color="green">{text}</Tag>
            ) : text === "Đang trình ký" ? (
              <Tag color="red">{text}</Tag>
            ) : (
              <Tag color="blue">{text}</Tag>
            )}
          </p>
        );
      },
    },
    {
      title: "Loại phiếu",
      dataIndex: "type_name",
      key: "type_name",
      width: 110,
    },
    {
      title: "Người đề nghị",
      dataIndex: "proposer_name",
      key: "proposer_name",
    },
    // {
    //   title: "Bộ phận",
    //   dataIndex: "department_name",
    //   key: "department_name",
    //   width: 130,
    // },
    {
      title: "Mục đích sử dụng",
      dataIndex: "intended_use",
      key: "intended_use",
    },
    {
      title: "Nhóm hàng hoá",
      dataIndex: "purchasing_group",
      key: "purchasing_group",
    },
    // {
    //   title: "Nơi nhận hàng",
    //   dataIndex: "receiving_destination",
    //   key: "receiving_destination",
    // },
    {
      title: "Hành động",
      key: "action",
      width: 150,
      fixed: "right",
      render: (record) => {
        // console.log(record);
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Xem sản phẩm & chỉnh trạng thái  ======================= */}
            <ViewBtnComponent
              onClickF={() => {
                showViewDetailModal(record.id);
              }}
              text={"Xem sản phẩm & chỉnh trạng thái"}
            />
            {/* ======================= Chỉnh sửa ======================= */}
            {permission?.purchase_proposal?.update ? (
              <FixBtnComponent
                deleted={deletedPage}
                onClickF={() => goToFixProposal(record.id)}
              />
            ) : (
              <></>
            )}
            {/* ======================= Undo ======================= */}
            {permission?.purchase_proposal?.update ? (
              <UndoBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  confirmUndo(record.id);
                }}
                text={"phiếu"}
              />
            ) : (
              <></>
            )}
            {/* ======================= In file PDF ======================= */}
            {!deletedPage && permission?.purchase_proposal?.read ? (
              <Tooltip placement="bottom" title="Xuất file PDF phiếu đã chọn">
                <div>
                  <ReactToPrint
                    content={() => componentRef.current}
                    documentTitle="Xuất phiếu đề nghị mua hàng"
                    onAfterPrint={handleAfterPrint}
                    onBeforeGetContent={() => {
                      return new Promise(async (resolve) => {
                        await handleOnBeforeGetContent(record.id);
                        onBeforeGetContentResolve.current = resolve;
                        setLoading(true);
                      });
                    }}
                    onBeforePrint={handleBeforePrint}
                    removeAfterPrint
                    trigger={reactToPrintTrigger}
                  />
                  <div className="hidden">
                    <PrintPP forwardedRef={componentRef} details={PPDetails} />
                  </div>
                </div>
              </Tooltip>
            ) : (
              <></>
            )}
            {/* ======================= Xuất file Excel =======================  */}
            {permission?.purchase_proposal?.read === 1 && (
              <ExportDetailsBtnComponent
                onClickF={() => exportFile(record.id)}
                text={"Xuất file excel chi tiết"}
              />
            )}
            {/* ======================= Xoá ======================= */}
            {permission?.purchase_proposal?.delete ? (
              <DeleteBtnItemComponent
                onClickF={() => {
                  if (deletedPage) {
                    confirm(record.id, 1);
                  } else {
                    confirm(record.id, 0);
                  }
                }}
                text={"phiếu"}
              />
            ) : (
              <></>
            )}
          </div>
        );
      },
    },
  ];
  return (
    <div className="table-render-API">
      <TableLength page={page} pageSize={pageSize} total={total} />
      <Table
        {...tableProps}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          x: 1100,
          y: 600,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
