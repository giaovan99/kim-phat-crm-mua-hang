import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
import { useNavigate } from "react-router-dom";
import DropdownOptions from "../../DropdownOptions/DropdownOptions";
import AddBtnComponent from "../../ButtonComponent/AddBtnComponent";
import { normalize } from "../../../utils/others";
import { Select } from "antd";

export default function Actions({
  setReload,
  deletedPage,
  keySelected,
  setKeySelected,
  listStatus,
  itemSelected,
  setItemSelected,
  permission,
  listDepartment,
  setSearchFilter,
  listStaff,
}) {
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      keywords: "",
      department: "",
      created_by: "",
    },
    onSubmit: async (values) => {
      setSearchFilter(values);
      setReload(true);
    },
  });

  let navigate = useNavigate();
  const goToAddProposal = () => {
    navigate("/purchase_proposal/add");
  };

  const onSearch = (value) => {
    // console.log("search:", value);
  };
  const filterOption = (input, option) =>
    normalize(option?.label ?? "").includes(normalize(input));

  const onChangeSelect = (value, field) => {
    formik.setFieldValue(field, value);
  };

  return (
    <div className="actions flex-wrap gap-2">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div
            className="flex flex-wrap-reverse gap-2"
            style={{ alignItems: "end" }}
          >
            {permission?.purchase_proposal?.update === 1 && (
              <DropdownOptions
                arrId={keySelected}
                deletedPage={deletedPage}
                page={"purchaseOrder"}
                setReload={setReload}
                setKeySelected={setKeySelected}
                listStatus={listStatus}
                arrItem={itemSelected}
                setItemSelected={setItemSelected}
              />
            )}

            {/* ======================= Khung tìm kiếm ======================= */}
            <div className="form-group">
              <div className="form-style items-end flex-wrap">
                {/* Mã phiếu */}
                <div>
                  <label>Tìm</label>
                  <input
                    id="keywords"
                    name="keywords"
                    type="text"
                    placeholder="Tìm mã phiếu"
                    value={formik.values.keywords}
                    onChange={formik.handleChange}
                    className="max-w-[120px]"
                  />
                </div>
                {/* Bộ phận */}
                <div>
                  <label>Bộ phận</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "department")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listDepartment}
                    value={
                      formik.values.department != ""
                        ? formik.values.department
                        : null
                    }
                    className="w-[162px]"
                  />
                </div>
                {/* Người tạo */}
                <div>
                  <label>Người tạo</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "created_by")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listStaff}
                    value={formik?.values?.created_by || null}
                    className="w-[162px]"
                  />
                </div>
                <div className="flex gap-2">
                  <button className="btn-main-yl btn-actions" type="submit">
                    <HiOutlineSearch />
                    Tìm
                  </button>
                  <button
                    className="ant-btn btn-actions text-white bg-black ant-btn-default rounded-lg"
                    type="button"
                    onClick={formik.handleReset}
                  >
                    Reset
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      {!deletedPage && permission?.purchase_proposal?.create ? (
        <div className="flex gap-3 grow min-w-[220px] justify-end">
          <AddBtnComponent
            onClickF={() => goToAddProposal()}
            text={"Tạo đề nghị mua hàng"}
          />
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}
