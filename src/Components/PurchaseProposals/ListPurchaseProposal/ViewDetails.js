import React, { useState } from "react";
import TableListProduct from "./TableListProduct/TableListProduct";
import { Modal } from "antd";
import { useEffect } from "react";
import { showError } from "../../../utils/others";
import { getPurchaseRequest } from "../../../utils/purchaseRequest";
import SetStatus2Step from "../../SetStatus2Step/SetStatusProposal";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

export default function ViewDetails({
  isModalOpen,
  setIsModalOpen,
  proposal,
  setProposal,
  statusOptions,
  deletedPage,
  setReload,
  permission,
}) {
  // 1 - Khai báo state
  let [cloneProduct, setCloneProduct] = useState();
  let [type, setType] = useState();
  const [current, setCurrent] = useState(0);
  const dispatch = useDispatch();
  let [code, setCode] = useState();
  // 2 - Lấy thông tin đề nghị
  const getPurchaseRequestInfo = async () => {
    dispatch(setSpinner(true));
    let res = await getPurchaseRequest(proposal);
    if (res.data.success) {
      let data = res.data.data;
      setCode(data.code);
      setType(data.type);
      setCurrent(Number(data.status));
      setCloneProduct(data.items);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  //  3 - Lần đầu khi load
  useEffect(() => {
    if (proposal) {
      // lấy thông tin người dùng trong danh sách người dùng & show ra màn hình
      getPurchaseRequestInfo();
    }
  }, [proposal]);

  // 4 - Reset form
  const resetForm = () => {
    setProposal(null);
  };

  //  6 - Khi huỷ lưu form
  const handleCancel = () => {
    resetForm();
    setIsModalOpen(false);
  };

  return (
    <Modal
      title={`Xem sản phẩm và chỉnh trạng thái phiếu đề nghị - ${code || ""}`}
      centered
      open={isModalOpen}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Đóng"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center hidden",
      }}
      classNames={{ wrapper: "main-modal modal-view-detail" }}
    >
      <div className="tableListProducts">
        <form
          id="formUpdate"
          // className={deletedPage ? "pointer-events-none" : ""}
        >
          <div className="mb-5">
            {/* ======================= Title ======================= */}
            <h2 className="text-[1rem] font-medium mb-1 text-bl">
              Trạng thái đơn đề nghị
            </h2>
            {/* ======================= Step ======================= */}
            <SetStatus2Step
              statusOptions={statusOptions}
              current={current}
              setCurrent={setCurrent}
              now={proposal}
              page={"purchaseProposal"}
              deletedPage={deletedPage}
              setReload={setReload}
              permissionUpdate={permission?.purchase_proposal?.update}
            />
          </div>
          {cloneProduct && (
            <div>
              {/* ======================= Title ======================= */}
              <h2 className="text-[1rem] font-medium mb-1 text-bl">
                Danh sách sản phẩm
              </h2>
              {/* ======================= Table List Products ======================= */}
              <TableListProduct
                cloneProduct={cloneProduct}
                setCloneProduct={setCloneProduct}
                type={type}
              />
            </div>
          )}
        </form>
      </div>
    </Modal>
  );
}
