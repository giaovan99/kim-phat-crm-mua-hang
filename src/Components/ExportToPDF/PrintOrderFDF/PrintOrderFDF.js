// import "./PrintPP.scss";
import "../printTemplate.scss";
import React, { useEffect, useRef } from "react";
import moment from "moment";
import "moment/locale/vi";
import { Table } from "antd";
const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
  borderColor: "gray",
};

const PrintOrder = ({ details, forwardedRef }) => {
  const localRef = useRef(null);
  // console.log(forwardedRef);
  // Effect để chắc chắn rằng ref đã được mount trước khi truyền lên
  useEffect(() => {
    forwardedRef.current = localRef.current;
    // console.log(details);
  }, [forwardedRef]);

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      fixed: "left",
      width: 35,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Mã số",
      dataIndex: "sku",
      key: "sku",
      width: 60,
      className: "text-center",
    },
    {
      title: "Tên hàng",
      dataIndex: "product_name",
      key: "product_name",
      width: 150,
    },
    {
      title: "Quy cách sản phẩm",
      dataIndex: "packaging",
      key: "packaging",
    },
    {
      title: "Quy cách đóng gói",
      dataIndex: "package_case",
      key: "package_case",
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      className: "text-center",
    },
    {
      title: "Số lượng",
      dataIndex: "qty",
      key: "qty",

      className: "text-center",
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
      className: "text-right",
      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Thành tiền",
      dataIndex: "sub_total",
      key: "sub_total",
      className: "text-right",
      width: 90,
      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Thuế GTGT",
      dataIndex: "vat_name",
      key: "vat_name",
      className: "text-center",
      width: 50,
    },
  ];

  return (
    <div className="book" ref={localRef}>
      <div className="page">
        {/* header */}
        <div className="flex gap-5 header">
          {details?.company_name === 1 && (
            <div className="logo">
              <img src="./img/logos/logoPDF.png" width={80} />
            </div>
          )}
          <div className="list-company-info grow">
            {details?.company_id === 1 ? (
              <div className="company-info mb-3">
                <p>Bên mua:</p>
                <div>
                  <p className="company-name ">
                    <strong>CÔNG TY TNHH MTV GỐM SỨ MỸ NGHỆ KIM PHÁT</strong>{" "}
                  </p>
                  <p>
                    Địa chỉ: Số 22/1 đường DT743, KP.Bình Quới A, Phường Bình
                    Chuẩn, TP.Thuận An, Bình Dương
                  </p>
                  <p>MST: 3700 30 40 23</p>
                </div>
              </div>
            ) : (
              <div className="company-info">
                <p>Bên mua:</p>
                <div>
                  <p className="company-name ">
                    <strong>CÔNG TY TNHH PHƯƠNG HẠNH</strong>
                  </p>
                  <p>Mã số thuế: 3700144387</p>
                  <p>
                    ĐC:Số 22/1, Đường ĐT 743, Khu phố Bình Quới A, P. Bình
                    Chuẩn, TP. Thuận An,Bình Dương
                  </p>
                </div>
              </div>
            )}
          </div>
        </div>
        {/* Title */}
        <div className="title">ĐƠN ĐẶT HÀNG</div>
        {/* Info */}
        <div className="info">
          <div className="grid grid-cols-2">
            <p>Đơn đặt hàng SỐ {details?.code}</p>
            <p>NCC: {details?.suppliers_name}</p>
            <p>Ngày: {moment(details?.created_at).locale("vi").format("LL")}</p>
            <p>Địa chỉ: {details?.supplier_address}</p>
            <p>Người đặt hàng: {details?.buyer_name}</p>
            <p>Mã số NCC: {details?.supplier_code}</p>
            <p>SĐT: {details?.buyer_phone}</p>
            <p>Người liên hệ: {details?.supplier_contact_name}</p>
            <p>Email: {details?.buyer_email}</p>
            <p>SĐT: {details?.supplier_contact_phone}</p>
          </div>
        </div>
        {/* Table sản phẩm */}
        <div className="table-render-API table-render-API2 mt-3">
          <Table
            {...tableProps}
            columns={columns}
            dataSource={details?.items}
            pagination={{
              pageSize: details?.items?.length,
              hideOnSinglePage: true,
            }}
          />
        </div>
        <div>
          {/* Tổng cộng */}
          <div className="flex">
            <p className="grow border border-black text-center p-2">
              <b>Tổng cộng</b>
            </p>
            <p className="w-[90px] text-right border border-black p-2">
              <b>
                {details?.sub_total
                  ? Number(details?.sub_total).toLocaleString("en-US")
                  : ""}
              </b>
            </p>
            <p className="w-[50px] border border-black"></p>
          </div>
          {/* Thuế GTGT theo từng mức VAT */}
          {typeof details?.tax_summary === "object" &&
            details?.tax_summary?.map(({ vat, tax }, index) => {
              if (vat !== "0%") {
                return (
                  <div className="flex" key={vat}>
                    <p className="grow border border-black text-center p-2">
                      Thuế GTGT ({vat})
                    </p>
                    <p className="w-[90px] text-right border border-black p-2">
                      {Number(tax).toLocaleString("en-US")}
                    </p>
                    <p className="w-[50px] border border-black"></p>
                  </div>
                );
              } else {
                return;
              }
            })}
          {/* Tổng giá trị đơn hàng */}
          <div className="flex">
            <p className="grow border border-black text-center p-2">
              <b>Tổng giá trị đơn hàng</b>
            </p>
            <p className="w-[90px] text-right border border-black p-2">
              <b>
                {details?.total
                  ? Number(details?.total).toLocaleString("en-US")
                  : ""}
              </b>
            </p>
            <p className="w-[50px] border border-black"></p>
          </div>
          {/* Đặt cọc */}
          <div className="flex">
            <p className="grow border border-black text-center p-2">Đặt cọc</p>
            <p className="w-[90px] text-right border border-black p-2">
              {details?.deposit
                ? Number(details?.deposit).toLocaleString("en-US")
                : ""}
            </p>
            <p className="w-[50px] border border-black"></p>
          </div>
          {/* Số tiền cần thanh toán */}
          <div className="flex">
            <p className="grow border border-black text-center p-2">
              <b>Số tiền cần thanh toán</b>
            </p>
            <p className="w-[90px] text-right border border-black p-2">
              <b>
                {details?.total_paid
                  ? Number(details?.total_paid).toLocaleString("en-US")
                  : ""}
              </b>
            </p>
            <p className="w-[50px] border border-black"></p>
          </div>
        </div>

        {/* Ghi chú */}
        <p className="note mt-2">
          <span>Ghi chú: </span>
          {details?.note}
        </p>
        {/* Điều kiện thanh toán */}
        <p className="note mt-2">
          <span>Điều kiện thanh toán: </span>
          {details?.paymentMethod_name}
        </p>
        {/* Phương thức giao hàng */}
        <p className="note mt-2">
          <span>Phương thức giao hàng: </span>
          {details?.shippingMethod_name}
        </p>
        {/* Ngày giao hàng dự kiến */}
        <p className="note mt-2">
          <span>Ngày giao hàng dự kiến: </span>
          {moment(details?.delivery_date).locale("vi").format("LL")}
        </p>
        {/* Yêu cầu về hóa đơn:*/}
        <p className="note mt-2">
          <span>Yêu cầu về hóa đơn: </span>
          {details?.invoice_request}
        </p>
        {/* Yêu cầu về giấy tờ:*/}
        <p className="note mt-2">
          <span>Yêu cầu về giấy tờ: </span>
          {details?.document_required}
        </p>

        {/* Ký tên */}
        <div className="sign grid grid-cols-2 mt-3">
          <p className="text-center">Đại diện bởi</p>
          <p className="text-center">Xác nhận của Nhà cung cấp</p>
        </div>
      </div>
    </div>
  );
};

export default PrintOrder;
