import "./PrintPP.scss";
import "../printTemplate.scss";
import React, { useEffect, useRef } from "react";
import moment from "moment";
import "moment/locale/vi";
import { Table } from "antd";
import { stringToDate } from "../../../utils/others";
const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
  borderColor: "black",
};

const PrintPP = ({ details, forwardedRef }) => {
  const localRef = useRef(null);
  // Effect để chắc chắn rằng ref đã được mount trước khi truyền lên
  useEffect(() => {
    forwardedRef.current = localRef.current;
    // console.log(details);
  }, [forwardedRef]);

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      fixed: "left",
      width: 45,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Tên hàng hóa",
      dataIndex: "product_name",
      key: "product_name",
      width: 180,
    },
    {
      title: "Quy cách",
      dataIndex: "specifications",
      key: "specifications",
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      className: "text-center",
    },

    {
      title: (
        <>
          SL
          <br /> đề nghị
        </>
      ),
      dataIndex: "qty",
      key: "qty",

      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: (
        <>
          SL
          <br />
          tồn kho
          <br />
          (nếu có)
        </>
      ),
      dataIndex: "qty_inventory",

      key: "qty_inventory",
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : 0}
          </p>
        );
      },
    },
    {
      title: (
        <>
          SL
          <br /> cần đặt
        </>
      ),
      dataIndex: "qty_purchase",

      key: "qty_purchase",
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: <p className="text-red-600">Ngày cần giao</p>,
      dataIndex: "purchase_date",
      key: "purchase_date",

      render: (text, record) => {
        return (
          <p className="text-center text-red-600">
            {record.purchase_date ? stringToDate(text) : ""}
          </p>
        );
      },
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
    },
  ];

  const columns2 = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      fixed: "left",
      width: 45,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Tên hàng hóa",
      dataIndex: "product_name",
      key: "product_name",
      width: 200,
    },
    {
      title: "Quy cách",
      dataIndex: "specifications",
      key: "specifications",
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      className: "text-center",
    },
    {
      title: "Đơn hàng",
      children: [
        {
          title: "PO",
          dataIndex: "po_export_code",
          key: "po_export_code",
        },
        {
          title: "SKU",
          dataIndex: "po_export_sku",
          key: "po_export_sku",
        },
        {
          title: "Master Case Pack",
          dataIndex: "po_export_pack",
          key: "po_export_pack",
        },
      ],
    },
    {
      title: (
        <>
          SL
          <br /> đề nghị
        </>
      ),
      dataIndex: "qty",
      key: "qty",

      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : 0}
          </p>
        );
      },
    },
    {
      title: (
        <>
          SL
          <br />
          tồn kho
          <br />
          (nếu có)
        </>
      ),
      dataIndex: "qty_inventory",

      key: "qty_inventory",
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : 0}
          </p>
        );
      },
    },
    {
      title: (
        <>
          SL
          <br /> cần đặt
        </>
      ),
      dataIndex: "qty_purchase",
      key: "qty_purchase",
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : 0}
          </p>
        );
      },
    },
    {
      title: <p className="text-red-600">Ngày cần giao</p>,
      dataIndex: "purchase_date",
      key: "purchase_date",
      render: (text, record) => {
        return (
          <p className="text-center text-red-600">
            {record.purchase_date ? stringToDate(text) : ""}
          </p>
        );
      },
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
    },
  ];
  return (
    <div className="book" ref={localRef}>
      <div className="page">
        {/* header */}
        <div className="flex justify-between header">
          <p> CTY TNHH MTV GSMN KIM PHÁT</p>
          <p className="bieuMauCode">BM-01/QĐ-05/5</p>
        </div>
        {/* Title */}
        <div className="title">PHIẾU ĐỀ NGHỊ MUA HÀNG HÓA</div>
        <p className="text-center mb-3">
          Ngày {moment().locale("vi").format("LL")}
        </p>
        {/* Info */}
        <div className="info">
          <div className="grid grid-cols-2">
            <p>Họ và tên: {details?.proposer_name}</p>
            <p>Bộ phận: {details?.department_name}</p>
            <p>Nhóm hàng hóa: {details?.purchasing_group}</p>
            <p>Nơi nhận hàng: {details?.receiving_destination}</p>
          </div>
          <p>Mục đích sử dụng:{details?.intended_use}</p>
        </div>
        {/* Table sản phẩm */}
        <div className="table-render-API table-render-API2 mt-2">
          <Table
            {...tableProps}
            columns={details?.type == "1" ? columns : columns2}
            dataSource={details?.items}
            pagination={{
              pageSize: details?.items?.length,
              hideOnSinglePage: true,
            }}
          />
        </div>
        {/* Ghi chú */}
        <div className="note mt-2">
          <p>
            <span className="font-bold italic underline">Ghi chú:</span> Biểu
            mẫu này dùng chung cho tất cả các BP của Cty, BP nào không cần thông
            tin " Đơn hàng" thì dấu (bỏ) các cột thuộc cột " Đơn hàng" đi
          </p>
        </div>
        {/* Ký tên */}
        <div className="sign grid grid-cols-6 mt-2">
          <p className="text-center">Người Đề Nghị</p>
          <p className="text-center">Thủ Kho</p>
          <p className="text-center">
            Kế Toán <br /> <span className="italic">(Kiểm tra)</span>
          </p>
          <p className="text-center">PT Bộ Phận</p>
          <p className="text-center">Trưởng Kho</p>
          <p className="text-center">Ban Giám Đốc</p>
        </div>
      </div>
    </div>
  );
};

export default PrintPP;
