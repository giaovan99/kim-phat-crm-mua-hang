// import "./PrintPP.scss";
import "../printTemplate.scss";
import React, { useEffect, useRef } from "react";
import moment from "moment";
import "moment/locale/vi";
import { Table } from "antd";
const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
  borderColor: "gray",
};

const PrintImportWarehouse = ({ details, forwardedRef }) => {
  const localRef = useRef(null);
  // console.log(forwardedRef);
  // Effect để chắc chắn rằng ref đã được mount trước khi truyền lên
  useEffect(() => {
    forwardedRef.current = localRef.current;
    // console.log(details);
  }, [forwardedRef]);

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      fixed: "left",
      width: 35,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Mã số",
      dataIndex: "sku",
      key: "sku",
      width: 60,
      className: "text-center",
    },
    {
      title: "Tên hàng hoá",
      dataIndex: "product_name",
      key: "product_name",
      width: 120,
    },
    {
      title: (
        <>
          Mã số <br />
          (Quy cách)
        </>
      ),
      dataIndex: "package_case",
      key: "package_case",
      width: 120,
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 60,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Số lượng",
      children: [
        {
          title: (
            <>
              Theo
              <br /> chứng từ
            </>
          ),
          dataIndex: "qty",
          key: "qty",
          width: 60,
          render: (text, record) => {
            return (
              <p className="text-center">
                {Number(text).toLocaleString("en-US")}
              </p>
            );
          },
        },
        {
          title: "Thực nhập",
          dataIndex: "qty_actual",
          key: "qty_actual",
          width: 60,
          render: (text, record) => {
            return (
              <p className="text-center">
                {Number(text).toLocaleString("en-US")}
              </p>
            );
          },
        },
      ],
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      width: 100,
    },
  ];

  return (
    <div className="book" ref={localRef}>
      <div className="page">
        {/* header */}
        <div className="flex justify-between header">
          <p> CTY TNHH MTV GSMN KIM PHÁT</p>
          <p className="bieuMauCode">BM-06/QĐ-05/5</p>
        </div>
        {/* Title */}
        <div className="title">PHIẾU NHẬP KHO </div>
        <p className="text-center mb-3">
          Ngày {moment().locale("vi").format("LL")}
        </p>
        {/* Info */}
        <div className="info">
          <p>
            Họ và tên người giao:{" "}
            {details?.po_detail?.shippingMethod_name !== "Tự đến lấy"
              ? details?.po_detail?.suppliers_short_name
              : ""}
          </p>

          <div className="grid grid-cols-2">
            <p>Nhập tại kho: {details?.warehouse_name}</p>
            <p>Địa điểm: {details?.department_name}</p>
          </div>
        </div>
        {/* Table sản phẩm */}
        <div className="table-render-API table-render-API2 mt-2">
          <Table
            {...tableProps}
            columns={columns}
            dataSource={details?.items}
            pagination={{
              pageSize: details?.items?.length,
              hideOnSinglePage: true,
            }}
          />
        </div>
        <br />
        <p className="flex">
          <span>
            {" "}
            Số chứng từ gốc kèm theo: {details?.po_no + "; " + details?.code}
          </span>
        </p>
        <br />
        {/* Ký tên */}
        <div className="sign grid grid-cols-3 mt-2">
          <p className="text-center">
            <span className="font-bold">Người lập phiếu (Thủ kho)</span>
            <br />
            (Ký, họ tên)
            <br />
            <br />
            <br />
            {details?.staff_number_fullname}
          </p>
          <p className="text-center font-bold">PT.Bộ Phận</p>

          <p className="text-center font-bold">Bảo Vệ</p>
        </div>
      </div>
    </div>
  );
};

export default PrintImportWarehouse;
