import "../PurchaseProposalPDF/PrintPP.scss";
import "../printTemplate.scss";
import React, { useEffect, useRef } from "react";
import moment from "moment";
import "moment/locale/vi";
import { Table } from "antd";
import { convertToWord, stringToDate } from "../../../utils/others";
const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
  borderColor: "black",
};

const PrintPR = ({ details, forwardedRef }) => {
  const localRef = useRef(null);

  useEffect(() => {
    forwardedRef.current = localRef.current;
  }, [forwardedRef]);

  const columns = [
    {
      title: "Chứng từ",
      children: [
        {
          title: "Ngày",
          dataIndex: "payment_date",
          key: "payment_date",
          render: (text) => {
            return (
              <p className="text-center">{text ? stringToDate(text) : ""}</p>
            );
          },
        },
        {
          title: "Số",
          dataIndex: "payment_no",
          key: "payment_no",
        },
      ],
    },
    {
      title: "Ngày giao hàng",
      dataIndex: "delivery_actual_date",
      key: "delivery_actual_date",

      render: (text, record) => (
        <p className="text-center">{text ? stringToDate(text) : ""}</p>
      ),
    },
    {
      title: "Nội dung",
      dataIndex: "product_name",
      key: "product_name",
      width: 150,
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 50,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Số lượng",
      dataIndex: "qty",
      key: "qty",
      width: 50,
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
      width: 70,
      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: (
        <>
          Thuế
          <br />
          GTGT
        </>
      ),
      dataIndex: "vat_name",
      key: "vat_name",
      width: 45,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: <>Thành tiền</>,
      key: "sub_total",
      dataIndex: "sub_total",
      width: 80,
      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: <>Ghi chú</>,
      key: "note",
      dataIndex: "note",
      width: 100,
    },
  ];

  return (
    <div className="book" ref={localRef}>
      <div className="page">
        {/* header */}
        {/* header */}
        <div className="flex justify-between header">
          <p>
            {details?.company == 1
              ? "CTY TNHH MTV GSMN KIM PHÁT"
              : "CTY TNHH PHƯƠNG HẠNH"}
          </p>
          <p className="bieuMauCode">BM-04/QĐ-05/5</p>
        </div>
        {/* Title */}
        <div className="title">PHIẾU ĐỀ NGHỊ THANH TOÁN</div>
        {/* Info */}
        <div className="info">
          <div className="grid grid-cols-2">
            <p>
              Họ & tên: {details?.staff_number_full_name}{" "}
              {details?.print_suppliers
                ? "(" + details?.suppliers_name.join(", ") + ")"
                : ""}
            </p>
            <p>Bộ phận: {details?.department_name}</p>
            <p>
              Ngày:{" "}
              {/* {details?.print_date
                ? "Từ ngày ............................... đến ngày ..............................."
                : moment().locale("vi").format("LL")} */}
              {details?.print_date
                ? "Từ ngày ............................... đến ngày ..............................."
                : "......./......./......."}
            </p>
          </div>
        </div>
        {/* Table sản phẩm */}
        <div className="table-render-API table-render-API2 mt-2">
          <Table
            {...tableProps}
            columns={columns}
            dataSource={details?.items}
            pagination={{
              pageSize: details?.items?.length,
              hideOnSinglePage: true,
            }}
          />
        </div>
        <table className="w-full border-collapse border-t-0 border-t-white mt-[-2px]">
          <tr>
            <th className="border-t-0">Tổng</th>
            <td className="w-[80px] text-right font-bold border-t-0">
              {details?.sub_total
                ? Number(details?.sub_total).toLocaleString("en-US")
                : ""}
            </td>
            <td className="w-[100px]"></td>
          </tr>
          <tr>
            <th>Thuế VAT:{details?.vat_name}</th>
            <td className="w-[80px] text-right font-bold">
              {details?.total_vat
                ? Number(details?.total_vat).toLocaleString("en-US")
                : ""}
            </td>
            <td className="w-[100px]"></td>
          </tr>
          <tr>
            <th>Tổng cộng</th>
            <td className="w-[80px] text-right font-bold">
              {details?.total
                ? Number(details?.total).toLocaleString("en-US")
                : ""}
            </td>
            <td className="w-[100px]"></td>
          </tr>
          {!details?.advance_money_first ? (
            <></>
          ) : (
            <tr>
              <td className="text-center">Tạm ứng lần 1</td>
              <td className="w-[80px] text-right">
                {details?.advance_money_first
                  ? Number(details?.advance_money_first).toLocaleString("en-US")
                  : 0}
              </td>
              <td className="w-[100px]"></td>
            </tr>
          )}
          {!details?.advance_money_second ? (
            <></>
          ) : (
            <tr>
              <td className="text-center">Tạm ứng lần 2</td>
              <td className="w-[80px] text-right">
                {details?.advance_money_second
                  ? Number(details?.advance_money_second).toLocaleString(
                      "en-US"
                    )
                  : 0}
              </td>
              <td className="w-[100px]"></td>
            </tr>
          )}
          {!details?.advance_money_first ? (
            <></>
          ) : (
            <tr>
              <th>Còn lại</th>
              <td className="w-[80px] text-right">
                {details?.remaining_amount
                  ? Number(details?.remaining_amount).toLocaleString("en-US")
                  : 0}
              </td>
              <td className="w-[100px]"></td>
            </tr>
          )}
        </table>

        {/* Số tiền bằng chữ*/}
        <div className="note mt-2">
          <p>
            Số tiền bằng chữ:{" "}
            {details?.remaining_amount
              ? convertToWord(Number(details?.remaining_amount) * 1)
              : "không đồng"}
          </p>
        </div>
        {/* Ký tên */}
        <p className="text-right">
          Ngày ......... tháng ......... năm .........
        </p>
        <div className="sign grid grid-cols-4 mt-2">
          <p className="text-center">
            Người Đề Nghị
            <br />
            (hoặc BP mua hàng)
          </p>
          <p className="text-center">
            PT Bộ Phận
            <br />
            (Nếu có)
          </p>
          <p className="text-center">Kế toán trưởng</p>
          <p className="text-center">Ban Giám Đốc</p>
        </div>
      </div>
    </div>
  );
};

export default PrintPR;
