import "../printTemplate.scss";
import React, { useEffect, useRef, useState } from "react";
import moment from "moment";
import "moment/locale/vi";
import { Table } from "antd";
import { stringToDate } from "../../../utils/others";
const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
  borderColor: "black",
};

const PrintFromHTML = ({ details, forwardedRef }) => {
  const localRef = useRef(null);
  // console.log(forwardedRef);
  // Effect để chắc chắn rằng ref đã được mount trước khi truyền lên
  useEffect(() => {
    forwardedRef.current = localRef.current;
  }, [forwardedRef]);

  return (
    <div className="book" ref={localRef}>
      <div dangerouslySetInnerHTML={{ __html: details }} />
    </div>
  );
};

export default PrintFromHTML;
