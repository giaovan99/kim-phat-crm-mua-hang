import "../printTemplate.scss";
import "./PrintAM.scss";
import React, { useEffect, useRef } from "react";
import moment from "moment";
import "moment/locale/vi";
import { convertToWord, returnCompanyInfo } from "../../../utils/others";

const PrintAM = ({ details, forwardedRef, companyInfo }) => {
  const localRef = useRef(null);
  //   console.log(forwardedRef);
  // Effect để chắc chắn rằng ref đã được mount trước khi truyền lên

  useEffect(() => {
    forwardedRef.current = localRef.current;
  }, [forwardedRef]);
  return (
    <div className="book" ref={localRef}>
      <div className="page">
        {/* header */}
        <div className="flex justify-between header">
          <p>
            {details?.company
              ? returnCompanyInfo(details?.company, companyInfo)?.name_legal
              : ""}
          </p>
          <p className="tt-bm">BM - 07/HC/3</p>
        </div>
        {/* Title */}
        <div className="title">PHIẾU TẠM ỨNG</div>
        {/* Info */}
        <div className="info">
          <p className="text-center">
            Ngày {moment().locale("vi").format("LL")}
          </p>
          <div className="grid grid-cols-1">
            <p>Họ và tên: {details?.staff_number_name}</p>
            <p>Bộ phận: Mua hàng</p>
            <p>
              Số tiền tạm ứng:{" "}
              {details?.total
                ? Number(details?.total).toLocaleString("en-US")
                : ""}
              đ ({convertToWord(Number(details?.total))})
            </p>

            <p>
              Thời hạn thanh toán:{" "}
              {details?.puchase_date ? details?.puchase_date : ""}
            </p>
            <p>Lý do tạm ứng: {details?.description}</p>
          </div>
        </div>

        {/* Ký tên */}
        <div className="sign grid grid-cols-3 mt-2">
          <p className="text-center font-bold">Người Đề Nghị</p>
          <p className="text-center font-bold"> PT.Bộ phận</p>
          <p className="text-center font-bold"> Ban Giám Đốc</p>
        </div>
      </div>
    </div>
  );
};

export default PrintAM;
