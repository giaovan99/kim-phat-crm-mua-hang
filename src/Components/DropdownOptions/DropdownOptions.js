import { Button, Dropdown, Popconfirm, message } from "antd";
import React, { useEffect, useState } from "react";
import { DownOutlined } from "@ant-design/icons";
import {
  deleteProductId,
  deletedTaxCode,
  deletedUnit,
  updateProduct,
} from "../../utils/product";
import Swal from "sweetalert2";
import {
  deleteCompanyId,
  deleteOrderId,
  deletedPaymentMethod,
  deletedShippingMethod,
  updateOrder,
} from "../../utils/order";
import { deleteNCCId, updateNCC } from "../../utils/supplier";
import { deleteAdvanceSlipId, updateAdvanceSlip } from "../../utils/advance";
import {
  deletePurchaseRequestId,
  updatePurchaseRequest,
} from "../../utils/purchaseRequest";
import {
  deleteImportWarehouseId,
  deleteWarehouseId,
  updateImportWarehouse,
  warehouseUpdate,
} from "../../utils/warehouse";
import {
  deleteUserId,
  deletedDepartment,
  deletedPosition,
  updateUser,
} from "../../utils/user";
import { deleteRole } from "../../utils/roles";
import {
  deleteReimbursementSlipId,
  updateReimbursementSlip,
} from "../../utils/reimbursement";
import { useLocation } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setSpinner } from "../../redux/slice/spinnerSlice";

export default function DropdownOptions({
  arrId,
  page,
  deletedPage,
  setReload,
  setKeySelected,
  listStatus = [],
  arrItem = [],
  setItemSelected = "",
}) {
  let success = true;
  let dispatch = useDispatch();
  useEffect(() => {
    success = true;
  }, [page]);

  // Xoá tất cả
  const deleteAll = () => {
    // console.log("xoá tất cả");
    // Chạy vòng lặp xoá từng phần tử
    dispatch(setSpinner(true));
    if (arrId.length > 0) {
      Promise.all(arrId.map((id) => deleteId(id)))
        .then(() => {
          if (success) {
            Swal.fire({
              title: "Xoá",
              text: "Xoá thành công!",
              icon: "success",
            });
            setReload(true);
            setKeySelected([]);
            success = true;
          }
          dispatch(setSpinner(false));
        })
        .catch((err) => {
          console.log(err);
          dispatch(setSpinner(false));
        });
    }
  };
  async function deleteId(id) {
    let value;
    //   Nếu đang ở trang delete thì value sẽ có deleted = true (xoá hẳn), ngược lại deleted = 1 (xoá tạm)
    if (deletedPage) {
      value = {
        id: id,
        deleted: 1,
      };
    } else if (
      page === "shippingMethods" ||
      page === "paymentMethods" ||
      page === "taxCode" ||
      page === "warehouse" ||
      page === "unit" ||
      page === "department" ||
      page === "position" ||
      page === "roles" ||
      page === "companyList"
    ) {
      value = { ...id, deleted: 1 };
    } else {
      value = {
        id: id,
      };
    }
    let res;
    switch (page) {
      default: {
        break;
      }
      case "product": {
        res = await deleteProductId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "order": {
        res = await deleteOrderId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "supplier": {
        res = await deleteNCCId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "advanceMoney": {
        res = await deleteAdvanceSlipId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "purchaseOrder": {
        res = await deletePurchaseRequestId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "warehouseSlip": {
        res = await deleteImportWarehouseId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "reimbursement": {
        res = await deleteReimbursementSlipId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "users": {
        res = await deleteUserId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "shippingMethods": {
        res = await deletedShippingMethod(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "paymentMethods": {
        res = await deletedPaymentMethod(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "unit": {
        res = await deletedUnit(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "taxCode": {
        res = await deletedTaxCode(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "warehouse": {
        res = await deleteWarehouseId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "companyList": {
        res = await deleteCompanyId(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "roles": {
        res = await deleteRole(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "position": {
        res = await deletedPosition(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "department": {
        res = await deletedDepartment(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
    }
  }
  // Undo tất cả
  const undoAll = async () => {
    dispatch(setSpinner(true));
    let success = true;
    if (arrId.length > 0) {
      // Chạy vòng lặp undo từng phần tử
      Promise.all(arrId.map((id) => undoId(id, success)))
        .then(() => {
          if (success) {
            Swal.fire({
              title: "Kích hoạt",
              text: "Kích hoạt thành công!",
              icon: "success",
            });
            setReload(true);
            setKeySelected([]);
            success = true;
          }
          dispatch(setSpinner(false));
        })
        .catch((err) => {
          console.log(err);
          dispatch(setSpinner(false));
        });
    }
  };
  async function undoId(id) {
    let value = { id: id, deleted: false };
    //   Nếu đang ở trang delete thì value sẽ có deleted = true (xoá hẳn), ngược lại deleted = 1 (xoá tạm)
    let res;
    switch (page) {
      default: {
        break;
      }
      case "product": {
        res = await updateProduct(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "order": {
        res = await updateOrder(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "supplier": {
        res = await updateNCC(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "advanceMoney": {
        res = await updateAdvanceSlip(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "purchaseOrder": {
        res = await updatePurchaseRequest(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "warehouseSlip": {
        res = await warehouseUpdate(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "reimbursement": {
        res = await updateReimbursementSlip(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
      case "users": {
        res = await updateUser(value, true);
        if (!res) {
          success = false;
        }
        break;
      }
    }
  }

  // Đổi trạng thái tất cả
  const updateStatusAll = (step) => {
    dispatch(setSpinner(true));
    if (arrId.length > 0) {
      console.log(arrItem);
      let errId = [];
      // Chạy vòng lặp undo từng phần tử
      Promise.all(arrItem.map((item) => updateStatus(item, step, errId)))
        .then(() => {
          if (errId.length > 0) {
            Swal.fire({
              title: "Cập nhật trạng thái",
              text:
                "Không thể đổi trạng thái: " +
                errId.map((item) => item.code).join(", "),
              icon: "warning",
            });
          } else {
            Swal.fire({
              title: "Cập nhật trạng thái",
              text: "Tất cả đổi trạng thái thành công",
              icon: "success",
            });
          }
          setReload(true);
          setKeySelected([]);
          setItemSelected([]);
          dispatch(setSpinner(false));
        })
        .catch((err) => {
          console.log(err);
          dispatch(setSpinner(false));
        });
    }
  };

  async function updateStatus(item, step, errId) {
    console.log(item);
    switch (page) {
      default: {
        break;
      }
      case "purchaseOrder": {
        if (item.status >= 3 && step < 3) {
          errId.push(item);
        } else {
          await updatePurchaseRequest({ id: item.id, status: step });
        }
        break;
      }
      case "order": {
        if (item.status >= 3 && step < 3) {
          errId.push(item);
        } else {
          await updateOrder({ id: item.id, status: step });
        }
        break;
      }
      case "warehouseSlip": {
        if (item.status >= 2 && step < 2) {
          errId.push(item);
        } else {
          await updateImportWarehouse({ id: item.id, status: step });
        }
        break;
      }
      case "reimbursement": {
        if (item.status >= 2 && step < 2) {
          errId.push(item);
        } else {
          await updateReimbursementSlip({ id: item.id, status: step });
        }
        break;
      }
      case "advanceMoney": {
        if (item.status >= 2 && step < 2) {
          errId.push(item);
        } else {
          await updateAdvanceSlip({ id: item.id, status: step });
        }
        break;
      }
    }
  }

  const cancel = (e) => {
    // console.log(e);
    message.error("Đã huỷ");
  };

  const items = [
    {
      label: (
        <Popconfirm
          placement="topRight"
          title="Xác nhận"
          description="Bạn chắc chắn muốn xoá?"
          onCancel={cancel}
          onConfirm={deleteAll}
          okText="Xoá"
          cancelText="Huỷ"
          okButtonProps={{ className: "btn-main-yl" }}
          className="popover-danger"
        >
          <button className="text-red-700">Xoá tất cả</button>
        </Popconfirm>
      ),
      key: "1",
    },
    deletedPage
      ? {
          label: (
            <button className="text-blue-700" onClick={undoAll}>
              Kích hoạt tất cả
            </button>
          ),
          key: "2",
        }
      : null,
    listStatus.length > 0 && !deletedPage
      ? {
          label: <span className="text-blue-500">Đổi trạng thái</span>,
          key: "3",
          children: listStatus.map((item) => ({
            label: (
              <Popconfirm
                placement="topRight"
                key={item.label}
                title="Đổi trạng thái"
                description={
                  <p>
                    Xác nhận đổi trạng thái các phiếu đã chọn thành{" "}
                    <b>{item.label}</b>
                  </p>
                }
                onCancel={cancel}
                onConfirm={() => updateStatusAll(item.value)}
                okText="Đổi"
                cancelText="Huỷ"
                okButtonProps={{ className: "btn-main-yl" }}
                className="popover-danger"
              >
                <button className="text-blue-500">{item.label}</button>
              </Popconfirm>
            ),
            key: item.label,
          })),
        }
      : null,
  ];

  const menuProps = {
    items,
  };

  // Kiểm tra xem có quyền chỉnh sửa hay không
  let [check, setCheck] = useState(false);
  const location = useLocation();
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  useEffect(() => {
    if (permission) {
      let key = location.pathname.replace("/", "");
      if (permission[key] && permission[key]?.update) {
        setCheck(true);
      } else {
        setCheck(false);
      }
    }
  }, [permission]);

  return (
    <div>
      {check && (
        <Dropdown menu={menuProps} trigger={["click"]}>
          <Button className="flex justify-center items-center dropdownBtn h-[38px]">
            <DownOutlined />
          </Button>
        </Dropdown>
      )}
    </div>
  );
}
