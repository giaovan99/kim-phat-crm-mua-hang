import React from "react";
import Spreadsheet from "react-spreadsheet";

const SpreadSheetUs = ({ API }) => {
  let rowEmpty = Array(10).fill([]);
  let { dataReview } = API;
  return <Spreadsheet data={[...dataReview, ...rowEmpty]} />;
};

export default SpreadSheetUs;
