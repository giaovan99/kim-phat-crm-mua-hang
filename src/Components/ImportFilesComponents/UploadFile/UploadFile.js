import React from "react";
import { InboxOutlined } from "@ant-design/icons";
import { message, Upload } from "antd";
import { getToken } from "../../../utils/others";
const { Dragger } = Upload;

export default function UploadFile({ setDomainFile, setNextStep }) {
  // Usage in your component

  let token = getToken();
  const props = {
    name: "file",
    multiple: false,
    maxCount: 1,
    action: `https://apikimphat.hahoba.com/api/import_file?token=${token}`,
    accept:
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel",
    onChange(info) {
      if (info.file.status !== "uploading") {
        // console.log(info.file, info.fileList);
      }
      if (info.file.status === "done") {
        message.success(`${info.file.name} tải file lên thành công`);
        setNextStep(true);
        // console.log(info.file.response);
        setDomainFile(info.file.response.data.fileName);
      } else if (info.file.status === "error") {
        message.error(`${info.file.name} tải file lên thất bại.`);
      }
    },
  };
  return (
    <>
      <Dragger {...props}>
        <p className="ant-upload-drag-icon">
          <InboxOutlined />
        </p>
        <p className="ant-upload-text">Nhấn hoặc thả file vào khu vực này</p>
      </Dragger>
    </>
  );
}
