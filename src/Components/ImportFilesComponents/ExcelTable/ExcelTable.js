import React from "react";

export default function ExcelTable({ html }) {
  return (
    <table
      className="table table-hover "
      cellPadding={0}
      cellSpacing={0}
      width="100%"
    >
      <tbody id="sheetData" dangerouslySetInnerHTML={{ __html: html }} />
    </table>
  );
}
