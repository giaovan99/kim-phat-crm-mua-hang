import { Select } from "antd";
import React from "react";
// import SpreadSheetUs from "../Spreadsheet/SpreadSheetUs";
import ExcelTable from "../ExcelTable/ExcelTable";

export default function ConfimColumn({ API, formik }) {
  // let { data, excelCol, excelSheet, excelRow, dataReview } = API;
  // let rowEmpty = Array(10).fill([]);
  // console.log(rowEmpty);
  // let { dataReview } = API;
  let excelSheet = API.excelSheet.map((item) => ({ value: item, label: item }));
  let excelCol = API.excelCol.map((item, index) => ({
    value: item,
    label: item,
  }));
  let excelRow = Array(API.excelRow)
    .fill(null)
    .map((item, index) => ({ value: index + 1, label: index + 1 }));
  // console.log(excelRow);
  // let excelRow = API.excelRow.map((item) => ({ value: item, label: item }));
  // console.log("excelSheet", excelSheet);
  const onSearch = (value) => {
    // console.log("search:", value);
  };
  let onChangeSelect = (value, key) => {
    formik.setFieldValue(key, value);
  };

  return (
    <div className="confirmFile">
      <div className="grid grid-cols-10 gap-4">
        {/* Hiển thị options */}
        <div className="col-span-4">
          {/* ======================= Sheet ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="sheet">Sheet</label>
              <Select
                id="sheet"
                name="sheet"
                showSearch
                placeholder="Chọn bảng"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "sheet")}
                onSearch={onSearch}
                options={excelSheet}
                value={formik.values.sheet != "" ? formik.values.sheet : null}
              />
            </div>
          </div>
          {/* ======================= Người đề nghị ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="proposer">
                Người đề nghị (hàng-cột)<span className="text-red-500">*</span>
              </label>
              <div className="flex gap-2 cols">
                <Select
                  id="proposer"
                  name="proposer"
                  showSearch
                  placeholder="Chọn hàng"
                  optionFilterProp="children"
                  onChange={(value) => onChangeSelect(value, "proposer.row")}
                  onSearch={onSearch}
                  options={excelRow}
                  value={
                    formik.values.proposer.row != ""
                      ? formik.values.proposer.row
                      : null
                  }
                />
                <Select
                  id="proposer"
                  name="proposer"
                  showSearch
                  placeholder="Chọn cột"
                  optionFilterProp="children"
                  onChange={(value) => onChangeSelect(value, "proposer.col")}
                  onSearch={onSearch}
                  options={excelCol}
                  value={
                    formik.values.proposer.col != ""
                      ? formik.values.proposer.col
                      : null
                  }
                />
              </div>
            </div>
          </div>
          {/* =======================  Bộ phận ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="department">
                Bộ phận (hàng-cột)<span className="text-red-500">*</span>
              </label>
              <div className="flex gap-2 cols">
                <Select
                  id="department"
                  name="department"
                  showSearch
                  placeholder="Chọn hàng"
                  optionFilterProp="children"
                  onChange={(value) => onChangeSelect(value, "department.row")}
                  onSearch={onSearch}
                  options={excelRow}
                  value={
                    formik.values.department.row != ""
                      ? formik.values.department.row
                      : null
                  }
                />
                <Select
                  id="department"
                  name="department"
                  showSearch
                  placeholder="Chọn cột"
                  optionFilterProp="children"
                  onChange={(value) => onChangeSelect(value, "department.col")}
                  onSearch={onSearch}
                  options={excelCol}
                  value={
                    formik.values.department.col != ""
                      ? formik.values.department.col
                      : null
                  }
                />
              </div>
            </div>
          </div>
          {/* =======================  Mục đích sử dụng ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="uses">
                Mục đích sử dụng (hàng-cột)
                <span className="text-red-500">*</span>
              </label>
              <div className="flex gap-2 cols">
                <Select
                  id="uses"
                  name="uses"
                  showSearch
                  placeholder="Chọn hàng"
                  optionFilterProp="children"
                  onChange={(value) => onChangeSelect(value, "uses.row")}
                  onSearch={onSearch}
                  options={excelRow}
                  value={
                    formik.values.uses.row != "" ? formik.values.uses.row : null
                  }
                />
                <Select
                  id="uses"
                  name="uses"
                  showSearch
                  placeholder="Chọn cột"
                  optionFilterProp="children"
                  onChange={(value) => onChangeSelect(value, "uses.col")}
                  onSearch={onSearch}
                  options={excelCol}
                  value={
                    formik.values.uses.col != "" ? formik.values.uses.col : null
                  }
                />
              </div>
            </div>
          </div>
          <div className="col-span-full text-[1rem] font-medium mb-1 text-bl">
            Sản phẩm
          </div>
          {/* =======================  Header title ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="products">
                Header title
                <span className="text-red-500">*</span>
              </label>
              <Select
                id="products"
                name="products"
                showSearch
                placeholder="Chọn hàng"
                optionFilterProp="children"
                onChange={(value) =>
                  onChangeSelect(value, "products.headerTile")
                }
                onSearch={onSearch}
                options={excelRow}
                value={
                  formik.values.products.headerTile != ""
                    ? formik.values.products.headerTile
                    : null
                }
              />
            </div>
          </div>
          {/* =======================  Bắt đầu - kết thúc ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label>
                Bắt đầu - kết thúc (hàng)
                <span className="text-red-500">*</span>
              </label>
              <div className="flex gap-2 cols">
                <Select
                  id="products.startRow"
                  name="products.startRow"
                  showSearch
                  placeholder="Chọn hàng"
                  optionFilterProp="children"
                  onChange={(value) =>
                    onChangeSelect(value, "products.startRow")
                  }
                  onSearch={onSearch}
                  options={excelRow}
                  value={
                    formik.values.products.startRow != ""
                      ? formik.values.products.startRow
                      : null
                  }
                />
                <Select
                  id="products.endRow"
                  name="products.endRow"
                  showSearch
                  placeholder="Chọn hàng"
                  optionFilterProp="children"
                  onChange={(value) => onChangeSelect(value, "products.endRow")}
                  onSearch={onSearch}
                  options={excelRow}
                  value={
                    formik.values.products.endRow != ""
                      ? formik.values.products.endRow
                      : null
                  }
                />
              </div>
            </div>
          </div>
          {/* =======================  Mã sản phẩm ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="products.productKey">Mã sản phẩm</label>
              <Select
                id="products.productKey"
                name="products.productKey"
                showSearch
                placeholder="Chọn cột"
                optionFilterProp="children"
                onChange={(value) =>
                  onChangeSelect(value, "products.productKey")
                }
                onSearch={onSearch}
                options={excelCol}
                value={
                  formik.values.products.productKey != ""
                    ? formik.values.products.productKey
                    : null
                }
              />
            </div>
          </div>
          {/* =======================  Tên sản phẩm ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="products.productName">
                Tên sản phẩm <span className="text-red-500">*</span>
              </label>
              <Select
                id="products.productName"
                name="products.productName"
                showSearch
                placeholder="Chọn cột"
                optionFilterProp="children"
                onChange={(value) =>
                  onChangeSelect(value, "products.productName")
                }
                onSearch={onSearch}
                options={excelCol}
                value={
                  formik.values.products.productName != ""
                    ? formik.values.products.productName
                    : null
                }
              />
            </div>
          </div>
          {/* =======================  Quy cách ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="products.specifications">
                Quy cách <span className="text-red-500">*</span>
              </label>
              <Select
                id="products.specifications"
                name="products.specifications"
                showSearch
                placeholder="Chọn cột"
                optionFilterProp="children"
                onChange={(value) =>
                  onChangeSelect(value, "products.specifications")
                }
                onSearch={onSearch}
                options={excelCol}
                value={
                  formik.values.products.specifications != ""
                    ? formik.values.products.specifications
                    : null
                }
              />
            </div>
          </div>
          {/* =======================  ĐVT ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="products.unit">
                ĐVT <span className="text-red-500">*</span>
              </label>
              <Select
                id="products.unit"
                name="products.unit"
                showSearch
                placeholder="Chọn cột"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "products.unit")}
                onSearch={onSearch}
                options={excelCol}
                value={
                  formik.values.products.unit != ""
                    ? formik.values.products.unit
                    : null
                }
              />
            </div>
          </div>
          {/* =======================  Số lượng đề nghị ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="products.suggestedQuantity">
                Số lượng đề nghị <span className="text-red-500">*</span>
              </label>
              <Select
                id="products.suggestedQuantity"
                name="products.suggestedQuantity"
                showSearch
                placeholder="Chọn cột"
                optionFilterProp="children"
                onChange={(value) =>
                  onChangeSelect(value, "products.suggestedQuantity")
                }
                onSearch={onSearch}
                options={excelCol}
                value={
                  formik.values.products.suggestedQuantity != ""
                    ? formik.values.products.suggestedQuantity
                    : null
                }
              />
            </div>
          </div>
          {/* ======================= Chỉ định người mua ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="products.buyer">Chỉ định người mua</label>
              <Select
                id="products.buyer"
                name="products.buyer"
                showSearch
                placeholder="Chọn cột"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "products.buyer")}
                onSearch={onSearch}
                options={excelCol}
                value={
                  formik.values.products.buyer != ""
                    ? formik.values.products.buyer
                    : null
                }
              />
            </div>
          </div>
        </div>
        {/* Hiên thị review file */}
        <div className="col-span-6 overflow-scroll">
          {/* <SpreadSheetUs API={API} /> */}
          <ExcelTable />
        </div>
      </div>
    </div>
  );
}
