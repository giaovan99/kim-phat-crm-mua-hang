import { Select } from "antd";
import React, { useEffect, useState } from "react";
// import SpreadSheetUs from "../Spreadsheet/SpreadSheetUs";
import ExcelTable from "../ExcelTable/ExcelTable";
import { convertObjectToArray } from "../../../utils/others";

const arrMainTitle = ["Sheet", "Header Title", "Start Row", "End Row"];

export default function ConfirmColumn2({ formik, API, getResSample }) {
  const [newAPI, setNewAPI] = useState();
  const [mainTitle, setMainTitle] = useState();
  const [label, setLabel] = useState();
  const [colOption, setColOption] = useState();
  const [rowOption, setRowOption] = useState();
  const [sheetOption, setSheetOption] = useState();

  const convertColumns = () => {
    return convertObjectToArray(API.columns);
  };
  const convertRows = () => {
    return Array(API.row)
      .fill(null)
      .map((item, index) => {
        return {
          label: index + 1,
          value: index + 1,
        };
      });
  };
  const convertSheet = () => {
    let arr = [];
    API.allSheetNames.forEach((item, index) => {
      arr.push({
        label: item,
        value: index,
      });
    });
    // console.log(arr);
    return arr;
  };
  const createLabel = (arr) => {
    const valuesArray = Object.values(arr);
    setLabel(valuesArray);
  };

  let onChangeSelect = (key, value) => {
    // console.log(key, value);
    setNewAPI({ ...newAPI, [key]: value });
  };

  let onChangeSelectTitle = (key, event) => {
    setMainTitle({ ...mainTitle, [key]: event });
    // formik.setFieldValue(key, event);
    if (key == "sheet" || key == "header_title") {
      getResSample({
        sheet: mainTitle.sheet,
        header_title: mainTitle.header_title,
        [key]: event,
      });
    }
  };

  const setUpNewAPIData = () => {
    // The replacement value
    const replacementValue = null;

    // Create a new object with replaced values
    const transformedObject = Object.fromEntries(
      Object.entries(API.fields).map(([key]) => [key, replacementValue])
    );
    return transformedObject;
  };

  useEffect(() => {
    if (API) {
      setNewAPI(setUpNewAPIData());
      createLabel(API.fields);
      setColOption(convertColumns());
      setRowOption(convertRows());
      setSheetOption(convertSheet());
      if (!mainTitle) {
        setMainTitle({
          sheet: 0,
          header_title: 1,
          start_row: 2,
          end_row: API.row,
        });
      }
    }
  }, [API]);

  useEffect(() => {
    if (mainTitle) {
      formik.setFieldValue("sheet", mainTitle.sheet);
      formik.setFieldValue("header_title", mainTitle.header_title);
      formik.setFieldValue("start_row", mainTitle.start_row);
      formik.setFieldValue("end_row", mainTitle.end_row);
    }
  }, [mainTitle]);

  useEffect(() => {
    if (API) {
      setNewAPI(setUpNewAPIData());
      createLabel(API.fields);
      setColOption(convertColumns());
      setRowOption(convertRows());
    }
  }, [API]);

  useEffect(() => {
    if (newAPI) {
      formik.setFieldValue("mapping_fields", { ...newAPI });
    }
  }, [newAPI]);

  const checkRequired = (key) => {
    for (const item of API.require_fields) {
      if (item == key) {
        return true;
      }
    }
  };

  // console.log(colOption);

  let renderSelect = () => {
    return Object.entries(newAPI).map(([key, col], index) => {
      return (
        <div className="form-group">
          <div className="form-style">
            <label htmlFor={key}>
              {label[index]}
              {checkRequired(key) && <span className="text-red-600">(*)</span>}
            </label>
            <div className="flex gap-2 cols">
              <Select
                id={key}
                name={key}
                defaultValue={""}
                showSearch
                onChange={(value) => onChangeSelect(key, value)}
                options={colOption}
                value={col ? col : ""}
              />
            </div>
          </div>
        </div>
      );
    });
  };
  let renderSelectTitle = () => {
    if (mainTitle) {
      return Object.entries(mainTitle).map(([key, col], index) => {
        return (
          <div>
            <div className="form-group">
              <div className="form-style">
                <label htmlFor={key}>
                  {arrMainTitle[index]} {key != "sheet" ? "(Hàng)" : "(Trang)"}
                </label>
                <div className="flex gap-2 cols">
                  <Select
                    id={key}
                    name={key}
                    showSearch
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelectTitle(key, value)}
                    options={key != "sheet" ? rowOption : sheetOption}
                    value={col}
                  />
                </div>
              </div>
            </div>
            {formik.touched[key] && formik.errors[key] ? (
              <p className="mt-1 text-red-500 text-sm" id="{key}-warning">
                {formik.errors[key]}
              </p>
            ) : (
              <></>
            )}
          </div>
        );
      });
    }
  };
  return (
    <div className="confirmFile">
      <div className="grid grid-cols-10 gap-4">
        {/* Hiển thị options */}
        <div className="col-span-4 ">
          <div className="border p-3 mb-3">{renderSelectTitle()}</div>
          <div className="border p-3">
            {formik.errors.mapping_fields ? (
              <p className="mt-1 text-red-500 text-sm" id="{key}-warning">
                {formik.errors.mapping_fields}
              </p>
            ) : (
              <></>
            )}
            <div>{newAPI && renderSelect()}</div>
          </div>
        </div>
        {/* Hiên thị review file */}
        <div className="col-span-6 overflow-scroll h-full max-h-[700px]">
          <ExcelTable html={API?.table} />
        </div>
      </div>
    </div>
  );
}
