import { Modal } from "antd";
import React, { useState } from "react";
import UploadFile from "./UploadFile/UploadFile";
import ConfimColumn from "./ConfimColumn/ConfimColumn";
import { useFormik } from "formik";
import ConfirmColumn2 from "./ConfirmColumn2/ConfirmColumn2";
import Swal from "sweetalert2";
import { accessFile, processFile } from "../../utils/reimbursement";
import * as yup from "yup";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

export default function ImportFilesComponent({
  isModalOpen,
  setIsModalOpen,
  confirmFileStyle,
  title,
  module,
  setReload,
  setAddProductExcel = null,
}) {
  // Quy ước: 1 = giai đoạn upload file, 2 = giai đoạn confirm file
  let [step, setStep] = useState(1);
  let [domainFile, setDomainFile] = useState();
  let [API, setAPI] = useState();
  let [nextStep, setNextStep] = useState(false);
  let dispatch = useDispatch();

  // Khai báo formik xác nhận hàng cột
  let confirmFile = useFormik({
    initialValues: {
      sheet: "",
      proposer: "",
      department: "",
      uses: "",
      products: "",
    },
    onSubmit: async (values) => {
      setIsModalOpen(false);
      Swal.fire({
        title: "",
        text: "Lưu thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
    },
  });

  const submit2 = async (values) => {
    dispatch(setSpinner(true));
    let res = await processFile({
      file: domainFile,
      sheet: 0,
      module: module,
      ...values,
    });
    dispatch(setSpinner(false));
    return res;
  };

  // Khai báo formik xác nhận hàng cột
  let confirmFile2 = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      start_row: yup.string().required("Nhập hàng bắt đầu"),
      end_row: yup.string().required("Nhập hàng kết thúc"),
      header_title: yup.string().required("Nhập hàng tiêu đề"),
      mapping_fields: yup
        .object()
        .test(
          "hasNullValue",
          "Vui lòng chọn cột cho các trường dữ liệu",
          (value) => {
            let isValid = true;
            if (value) {
              for (const fieldValue of Object.entries(value)) {
                API.require_fields.forEach((item) => {
                  if (item == fieldValue[0]) {
                    if (!fieldValue[1]) {
                      isValid = false;
                    }
                  }
                });
              }
              return isValid;
            }
          }
        ),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      let res = await submit2(values);
      if (res.data.success) {
        if (setAddProductExcel) {
          setAddProductExcel(res.data.data);
        }
        setIsModalOpen(false);
        Swal.fire({
          title: "",
          text: res.data.message,
          icon: "success",
          confirmButtonText: "Đóng",
        });
        setReload(true);
        setStep(1);
        setNextStep(false);
      }
      dispatch(setSpinner(false));
    },
  });

  // console.log(domainFile);

  const getResSample = async (values) => {
    dispatch(setSpinner(true));
    let res = await accessFile({
      file: domainFile,
      module: module,
      ...values,
    });
    if (res.data) {
      setAPI(res.data);
      dispatch(setSpinner(false));
    }
  };

  let handleOK = () => {
    if (step == 1) {
      getResSample({ sheet: 0 });
      setStep(2);
    } else {
      if (confirmFileStyle == 1) {
        confirmFile.handleSubmit();
      } else if (confirmFileStyle == 2) {
        confirmFile2.handleSubmit();
      }
    }
  };
  // console.log(confirmFile2.errors);
  let handleCancel = () => {
    setIsModalOpen(false);
    setStep(1);
    setNextStep(false);
  };
  return (
    <Modal
      title={title}
      centered
      open={isModalOpen}
      onCancel={handleCancel}
      onOk={handleOK}
      okText={step == 1 ? "Tiếp" : "Lưu"}
      cancelText="Đóng"
      okButtonProps={{
        className: nextStep
          ? "btn-main-yl inline-flex items-center justify-center"
          : "hidden",
      }}
      classNames={{ wrapper: "main-modal modal-view-detail" }}
    >
      <div className="importFile">
        {step == 1 ? (
          <UploadFile setDomainFile={setDomainFile} setNextStep={setNextStep} />
        ) : confirmFileStyle == 1 ? (
          <ConfimColumn API={API} formik={confirmFile} />
        ) : (
          <ConfirmColumn2
            API={API}
            formik={confirmFile2}
            getResSample={getResSample}
          />
        )}
      </div>
    </Modal>
  );
}
