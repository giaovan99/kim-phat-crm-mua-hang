import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { deleteNCCId, getListSupplier, updateNCC } from "../../utils/supplier";
import { setPage } from "../../redux/slice/pageSlice";
import { otherKeys } from "../../utils/user";
import DeleteBtnItemComponent from "../ButtonComponent/DeleteBtnItemComponent";
import FixBtnComponent from "../ButtonComponent/FixBtnComponent";
import RenderWithFixComponent from "../ButtonComponent/RenderIdFix";
import UndoBtnComponent from "../ButtonComponent/UndoBtnComponent";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import TableLength from "../ButtonComponent/TableLength";
const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableSuppliers({
  showModal,
  loadPage,
  reload,
  setReload,
  deletedPage,
  rowSelection,
  permission,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();

  let page = useSelector((state) => state.pageSlice.value);
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list nhà cung cấp theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));

    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }
    if (keyParam || keywords) {
      keyParam = { ...keyParam, keywords, search_type: "start_with" };
    }

    // console.log(keyParam);

    let res = await getListSupplier(page, keyParam);
    if (res.status) {
      setListData(res.data);
      setTotal(res.rawData.total);
      setPageSize(res.rawData.per_page);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Lần đầu load trang => Kéo về danh sách user & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    loadPage();
    getListData(page);
  }, []);

  // 5 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  // 6 - Xác nhận xoá nhà cung cấp
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deleteData(id, value);
  };

  // 7 - Xoá NCC
  const deleteData = async (id, input) => {
    dispatch(setSpinner(true));

    // console.log(id);
    let res;
    if (input == 1) {
      res = await deleteNCCId({ id: id, deleted: true });
    } else {
      res = await deleteNCCId({ id: id });
    }
    // console.log(res);
    if (res) {
      // console.log("reload trang");
      setReload(true);
    }
    // Load lại từ trang 1
    dispatch(setSpinner(false));
  };

  // 9 - Hoàn tác xoá
  const undoDelete = async (id) => {
    dispatch(setSpinner(true));

    let res;
    res = await updateNCC({ id: id, deleted: false });
    // console.log(res);
    if (res) {
      // console.log("reload trang");
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 10 - Kích hoạt người dùng
  const confirmUndo = (id) => {
    undoDelete(id);
  };
  // 8 - Định nghĩa các cột trong table
  const columns = [
    {
      title: "Mã NCC",
      dataIndex: "code",
      key: "code",
      width: 100,
      fixed: "left",
      render: (text, record) =>
        permission?.suppliers?.update ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record.id);
            }}
            text={text}
            textAlign={"left"}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Nhà cung cấp",
      dataIndex: "company_name",
      key: "company_name",
      width: 180,
      fixed: "left",
      render: (text, record) =>
        permission?.suppliers?.update ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record.id);
            }}
            text={text}
            textAlign={"left"}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Tên rút gọn",
      dataIndex: "company_name_short",
      key: "company_name_short",
      width: 160,
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Mã số thuế",
      dataIndex: "tax_id_number",
      key: "tax_id_number",
      width: 130,
    },
    {
      title: "Tên người liên hệ",
      dataIndex: "contact_name",
      key: "contact_name",
      width: 150,
    },
    {
      title: "Số điện thoại người liên hệ",
      dataIndex: "contact_phone",
      key: "contact_phone",
      width: 120,
    },
    {
      title: "Lĩnh vực kinh doanh",
      dataIndex: "business_type",
      key: "business_type",
      width: 170,
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      width: 150,
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            {permission?.suppliers?.update === 1 && (
              <FixBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  showModal(record.id);
                }}
              />
            )}

            {/* ======================= Undo ======================= */}
            {permission?.suppliers?.update === 1 && (
              <UndoBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  confirmUndo(record.id);
                }}
                text={"phiếu"}
              />
            )}

            {/* ======================= Xoá ======================= */}
            {permission?.suppliers?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => {
                  if (deletedPage) {
                    confirm(record.id, 1);
                  } else {
                    confirm(record.id, 0);
                  }
                }}
                text={"NCC"}
              />
            )}
          </div>
        );
      },
    },
  ];
  return (
    <div className="table-render-API">
      <TableLength page={page} pageSize={pageSize} total={total} />
      <Table
        {...tableProps}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 600,
          x: 1500,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
