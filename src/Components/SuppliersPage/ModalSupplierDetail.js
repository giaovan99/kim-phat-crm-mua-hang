import React, { useEffect } from "react";
import { Modal } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { createNCC, getNCC, updateNCC } from "../../utils/supplier";
import { showError } from "../../utils/others";
import InputPhone from "../Input/InputPhone";
import { useDispatch } from "react-redux";
import { setSpinner } from "../../redux/slice/spinnerSlice";

export default function ModalSupplierDetail({
  isModalOpen,
  setIsModalOpen,
  supplier,
  setSupplier,
  setReload,
  deletedPage,
}) {
  let dispatch = useDispatch();
  // 1 - Khai báo state
  const initial = {
    company_name: "",
    company_name_short: "",
    address: "",
    tax_id_number: "",
    contact_name: "",
    contact_phone: "",
    business_type: "",
    note: "",
  };

  // 2 - Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      company_name: yup.string().required("Tên nhà cung cấp không để trống"),
      contact_phone: yup
        .string()
        .nullable()
        .matches(
          /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
          "Số điện thoại không hợp lệ"
        ),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));

      // console.log(values);
      let res;
      if (supplier) {
        // console.log("sửa");
        res = await updateNCC(values);
      } else {
        res = await createNCC(values, formik);
      }
      if (res) {
        setSupplier(null);
        resetForm();
        //  đóng modal
        setIsModalOpen(false);
        //  Load lại trang 1
        setReload(true);
      }
      dispatch(setSpinner(false));
    },
  });
  const { handleBlur, handleChange, touched, errors } = formik;

  // 3 - Lấy thông tin NCC
  const getDataInfo = async () => {
    dispatch(setSpinner(true));

    let res = await getNCC(supplier);
    if (res.data.success) {
      let data = res.data.data;
      formik.setValues(data);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  //  4 - Lần đầu khi load trang => check xem là add hay fix user
  useEffect(() => {
    if (supplier) {
      // lấy thông tin nhà cung cấp trong danh sách nhà cung cấp & show ra màn hình
      getDataInfo();
    } else {
      formik.setValues(initial);
    }
  }, [supplier]);

  // 5 - Khi lưu Form
  const handleOk = () => {
    formik.handleSubmit();
  };

  // 6 - Reset Form
  const resetForm = () => {
    formik.resetForm();
    formik.setValues(initial);
  };

  // 7 - Khi cancel form
  const handleCancel = () => {
    setSupplier(null);
    resetForm();
    setIsModalOpen(false);
  };

  return (
    <Modal
      title={supplier ? "Chỉnh sửa nhà cung cấp" : "Thêm nhà cung cấp"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: !deletedPage
          ? "btn-main-yl inline-flex items-center justify-center"
          : "hidden",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form
        id="formAddSupplier"
        onSubmit={formik.handleSubmit}
        className={!deletedPage ? "" : "pointer-events-none"}
      >
        <div className="grid grid-cols-2 gap-2">
          {/* ======================= Nhà cung cấp ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="company_name">
                Nhà cung cấp <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên nhà cung cấp"
                name="company_name"
                id="company_name"
                value={formik.values.company_name}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.company_name && errors.company_name ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="company_name-warning"
              >
                {errors.company_name}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Tên rút gọn ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="company_name_short">Tên rút gọn</label>
              <input
                className="grow"
                type="text"
                placeholder="Tên rút gọn nếu không nhập thì sẽ tự động là tên nhà cung cấp"
                name="company_name_short"
                id="company_name_short"
                value={formik.values.company_name_short}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.company_name_short && errors.company_name_short ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="company_name_short-warning"
              >
                {errors.company_name_short}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Địa chỉ ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="address">Địa chỉ</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập địa chỉ"
                name="address"
                id="address"
                value={formik.values.address}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.address && errors.address ? (
              <p className="mt-1 text-red-500 text-sm" id="address-warning">
                {errors.address}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Mã số thuế ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="tax_id_number">Mã số thuế</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập MST"
                name="tax_id_number"
                id="tax_id_number"
                value={formik.values.tax_id_number}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.tax_id_number && errors.tax_id_number ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="tax_id_number-warning"
              >
                {errors.tax_id_number}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Lĩnh vực kinh doanh ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="business_type">Lĩnh vực kinh doanh</label>
              <textarea
                className="grow"
                type="text"
                placeholder="Nhập lĩnh vực kinh doanh"
                name="business_type"
                id="business_type"
                value={formik.values.business_type}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              ></textarea>
            </div>
            {touched.business_type && errors.business_type ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="business_type-warning"
              >
                {errors.business_type}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Tên người liên hệ ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="contact_name">Tên người liên hệ</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên người liên hệ"
                name="contact_name"
                id="contact_name"
                value={formik.values.contact_name}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.contact_name && errors.contact_name ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="contact_name-warning"
              >
                {errors.contact_name}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Số điện thoại người liên hệ ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="contact_phone">Số điện thoại người liên hệ</label>
              <InputPhone
                className="grow"
                placeholder="Nhập sđt người liên hệ"
                name="contact_phone"
                value={formik.values.contact_phone}
                formik={formik}
              />
            </div>
            {touched.contact_phone && errors.contact_phone ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="contact_phone-warning"
              >
                {errors.contact_phone}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Ghi chú ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="note">Ghi chú</label>
              <textarea
                className="grow"
                type="text"
                placeholder="Nhập ghi chú"
                name="note"
                id="note"
                value={formik.values.note}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              ></textarea>
            </div>
            {touched.note && errors.note ? (
              <p className="mt-1 text-red-500 text-sm" id="note-warning">
                {errors.note}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
      </form>
    </Modal>
  );
}
