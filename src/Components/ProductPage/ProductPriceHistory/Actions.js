import { DatePicker } from "antd";
import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
const { RangePicker } = DatePicker;

export default function Actions({ setReload, setSearchFilter }) {
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      startDate: "",
      endDate: "",
      product_code: "",
      product_name: "",
    },
    onSubmit: async (values) => {
      setSearchFilter(values);
      setReload(true);
    },
  });

  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div
            className="flex flex-wrap-reverse gap-2"
            style={{ alignItems: "end" }}
          >
            {/* ======================= Khung tìm kiếm ======================= */}
            <div className="form-group">
              <div className="form-style items-end flex-wrap">
                {/* Thời gian */}
                <div>
                  <label>Thời gian</label>
                  <RangePicker
                    size="large"
                    variant="borderless"
                    format="DD/MM/YYYY"
                    onChange={(value, dateString) => {
                      formik.setFieldValue("startDate", dateString[0]);
                      formik.setFieldValue("endDate", dateString[1]);
                    }}
                  />
                </div>
                {/* Mã sản phẩm */}
                <div>
                  <label>Mã sản phẩm</label>
                  <input
                    id="product_code"
                    name="product_code"
                    type="text"
                    placeholder="Nhà cung cấp"
                    value={formik.values.product_code}
                    onChange={formik.handleChange}
                    className="max-w-[250px]"
                  />
                </div>
                {/* Tên sản phẩm */}
                <div>
                  <label>Tên sản phẩm</label>
                  <input
                    id="product_code"
                    name="product_code"
                    type="text"
                    placeholder="Tên sản phẩm"
                    value={formik.values.product_code}
                    onChange={formik.handleChange}
                    className="max-w-[250px]"
                  />
                </div>
                <div className="flex gap-2">
                  <button className="btn-main-yl btn-actions" type="submit">
                    <HiOutlineSearch />
                    Tìm
                  </button>
                  <button
                    className="ant-btn btn-actions text-white bg-black ant-btn-default rounded-lg"
                    type="button"
                    onClick={formik.handleReset}
                  >
                    Reset
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
