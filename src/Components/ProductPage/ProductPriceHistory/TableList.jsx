import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { setPage } from "../../../redux/slice/pageSlice";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { productHistoryAll } from "../../../utils/product";
const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

const TableList = ({
  loadPage,
  reload,
  setReload,
  searchFilter,
  rowSelection,
}) => {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();

  let page = useSelector((state) => state.pageSlice.value);
  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      // console.log(page);
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list nhà cung cấp theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    let keyParam = {};
    keyParam = { ...searchFilter };
    let res = await productHistoryAll({ page, keyParam });
    if (res.status) {
      setListData(res.data.data.data);
      setTotal(res.data.data.total);
      setPageSize(res.data.data.per_page);
    }
    dispatch(setSpinner(false));
  };
  // 8 - Lần đầu load trang => Kéo về danh sách user & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    loadPage();
    getListData(page);
  }, []);

  // 9 - Khi reload == true thì load lại API danh sách
  useEffect(() => {
    if (reload) {
      // loadPage();
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  const columns = [
    {
      title: "Ngày cập nhật",
      dataIndex: "title",
      key: "title",
    },
    {
      title: "Mã sản phẩm",
      dataIndex: "product_sku",
      key: "product_sku",
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
    },
    {
      title: "Người cập nhật",
      dataIndex: "user_full_name",
      key: "user_full_name",
    },
    {
      title: "Thay đổi giá",
      dataIndex: "message",
      key: "message",
      width: 300,
    },
  ];
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
};

export default TableList;
