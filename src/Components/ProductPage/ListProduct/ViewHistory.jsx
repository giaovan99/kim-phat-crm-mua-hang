import { Modal, Table } from "antd";
import React, { useEffect, useState } from "react";
import { stringToDate } from "../../../utils/others";
import { productHistory } from "../../../utils/product";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

const paginationObj = {
  position: ["none", "bottomCenter"],
  hideOnSinglePage: true,
  pageSize: 15,
};
const tableProps = {
  bordered: true,
  size: "small",
};
export default function ViewHistory({
  isModalOpen,
  setIsModalOpen,
  product,
  setProduct,
}) {
  const [arrHistory, setArrHistory] = useState([]);
  let dispatch = useDispatch();

  const handleCancel = () => {
    setIsModalOpen(false);
    setProduct(null);
  };
  const columns = [
    {
      title: "Thời gian",
      dataIndex: "title",
      key: "title",
      fixed: "left",

      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
    },
    {
      title: "Họ tên",
      dataIndex: "user_full_name",
      key: "user_full_name",
    },
    {
      title: "Thông tin chỉnh sửa",
      dataIndex: "message",
      key: "message",
    },
  ];

  const getAPI = async () => {
    dispatch(setSpinner(true));

    let res = await productHistory(product);
    // console.log(res);
    if (res.data.success) {
      setArrHistory(res.data.data);
    }

    dispatch(setSpinner(false));
  };

  useEffect(() => {
    // gọi API lây lịch sử chỉnh sửa
    getAPI();
  }, [product]);

  return (
    <Modal
      title={"Tất cả lịch sử chỉnh sửa"}
      centered
      open={isModalOpen}
      onCancel={handleCancel}
      footer={null}
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        columns={columns}
        dataSource={arrHistory}
      />
    </Modal>
  );
}
