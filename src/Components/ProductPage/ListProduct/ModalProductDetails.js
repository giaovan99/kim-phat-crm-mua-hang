import React, { useCallback, useEffect } from "react";
import { Field, FormikProvider, useFormik } from "formik";
import * as yup from "yup";
import { Modal, Select, Spin } from "antd";
import moment from "moment";
import {
  createProduct,
  getProductDetails,
  updateProduct,
} from "../../../utils/product";
import { debounce, normalize, showError } from "../../../utils/others";
import InputPriceNotTable from "../../Input/InpurPriceNotTable";
import { useDispatch } from "react-redux";
import { setSpinner } from "../../../redux/slice/spinnerSlice";

const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

export default function ModalProductDetails({
  isModalOpen,
  setIsModalOpen,
  product,
  listUnit,
  listStaff,
  listVAT,
  listNCC,
  setProduct,
  setReload,
  deletedPage,
  getListTaxCode,
  loadingSelect,
  getSupplier = true,
  getListUnit,
  getListStaffProduct,
}) {
  let dispatch = useDispatch();

  // 1 - Khai báo state
  const initial = {
    name: "",
    buyer: "",
    created_at: moment(),
    suppliers_id: "",
    price: "",
    unit: "",
    vat: "",
    package_case: "",
    packaging: "",
  };
  // 2 - Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      name: yup.string().required("Tên sản phẩm không để trống"),
      buyer: yup.string().required("Người mua hàng không để trống"),
      price: yup.number().nullable(),
      unit: yup.string().required("Đơn vị tính không để trống"),
      vat: yup.string().required("Loại hoá đơn không để trống"),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      // console.log(values);
      let res;
      if (product) {
        // console.log("sửa");
        res = await updateProduct(values);
        // console.log(res);
      } else {
        res = await createProduct(values, formik);
      }
      if (res) {
        resetForm();
        setProduct(null);
        //  đóng modal
        setIsModalOpen(false);
        //  Load lại trang 1
        setReload(true);
      }
      dispatch(setSpinner(false));
    },
  });
  const { handleBlur, handleChange, touched, errors } = formik;

  // 3 - Lấy thông tin sản phẩm
  const getDataInfo = async () => {
    dispatch(setSpinner(true));

    let res = await getProductDetails(product);
    // console.log(res);
    if (res.data.success) {
      let data = res.data.data;
      // console.log(data);
      formik.setValues(data);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };
  // console.log(formik.errors, errors.unit);

  //  4 - Lần đầu khi load trang => check xem là add hay fix
  useEffect(() => {
    // console.log(product);
    if (product) {
      // lấy thông tin người dùng trong danh sách người dùng & show ra màn hình
      getDataInfo();
    } else {
      formik.setValues(initial);
    }
  }, [product]);

  // 5 - Khi select thay đổi -> cập nhật formik
  const onChangeSelect = (value, field) => {
    formik.setFieldValue(field, value);
  };

  // 6 - Khi lưu form
  const handleOk = () => {
    formik.handleSubmit();
  };
  // 7 - Khi cancel form
  const handleCancel = () => {
    setProduct(null);
    resetForm();
    setIsModalOpen(false);
  };

  //  8 - Reset Form
  const resetForm = () => {
    formik.resetForm();
    formik.setValues(initial);
  };
  const debouncedFetchOptions = useCallback(
    debounce(async (page, keywords) => {
      getSupplier(1, keywords);
    }, 300),
    []
  );
  // console.log(formik.values);

  return (
    <Modal
      title={product ? "Chỉnh sửa sản phẩm" : "Thêm sản phẩm"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: !deletedPage
          ? "btn-main-yl inline-flex items-center justify-center"
          : "hidden",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <FormikProvider value={formik}>
        <form
          id="formAddProduct"
          onSubmit={formik.handleSubmit}
          className={!deletedPage ? "" : "pointer-events-none"}
        >
          <div className="grid grid-cols-2 gap-2">
            {/* ======================= Tên sản phẩm ======================= */}
            <div className="form-group ">
              <div className="form-style">
                <label htmlFor="name">
                  Tên sản phẩm <span className="text-red-500">*</span>
                </label>
                <input
                  className="grow"
                  type="text"
                  placeholder="Nhập tên sản phẩm"
                  name="name"
                  id="name"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                />
              </div>
              {touched.name && errors.name ? (
                <p className="mt-1 text-red-500 text-sm" id="name-warning">
                  {errors.name}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Người mua hàng ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="buyer">
                  Người mua hàng
                  <span className="text-red-500">*</span>
                </label>
                <Field name="buyer">
                  {({ field }) => {
                    return (
                      <Select
                        {...field}
                        id="buyer"
                        name="buyer"
                        showSearch
                        placeholder="Chọn đơn vị tính"
                        optionFilterProp="children"
                        onChange={(value) => onChangeSelect(value, "buyer")}
                        filterOption={filterOption}
                        options={listStaff}
                        value={
                          formik.values.buyer != "" ? formik.values.buyer : null
                        }
                        onBlur={handleBlur}
                        onDropdownVisibleChange={getListStaffProduct}
                        notFoundContent={
                          loadingSelect ? <Spin size="small" /> : null
                        }
                      />
                    );
                  }}
                </Field>
              </div>
              {touched.buyer && errors.buyer ? (
                <p className="mt-1 text-red-500 text-sm" id="buyer-warning">
                  {errors.buyer}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Ngày tạo ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="created_at">Ngày tạo</label>
                <input
                  className="grow"
                  type="date"
                  name="created_at"
                  id="created_at"
                  value={moment(formik.values.created_at).format("yyyy-MM-DD")}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
              {touched.created_at && errors.created_at ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="created_at-warning"
                >
                  {errors.created_at}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Nhà cung cấp ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="suppliers_id">Nhà cung cấp</label>
                <Field name="suppliers_id">
                  {({ field }) => {
                    return (
                      <Select
                        {...field}
                        id="suppliers_id"
                        name="suppliers_id"
                        showSearch
                        placeholder="Chọn nhà cung cấp"
                        optionFilterProp="children"
                        onChange={(value) => {
                          onChangeSelect(value, "suppliers_id");
                        }}
                        onSearch={(value) => {
                          if (getSupplier) {
                            value && debouncedFetchOptions(1, value);
                          }
                        }}
                        filterOption={filterOption}
                        options={listNCC}
                        value={
                          listNCC.length <= 0 && formik.values.suppliers_name
                            ? formik.values.suppliers_name
                            : formik.values.suppliers_id != ""
                            ? formik.values.suppliers_id
                            : null
                        }
                        onBlur={handleBlur}
                        loadingSelect={loadingSelect}
                        onDropdownVisibleChange={(open) => {
                          if (getSupplier) {
                            open && getSupplier(1, "", true);
                          }
                        }}
                        notFoundContent={
                          loadingSelect ? <Spin size="small" /> : null
                        }
                      />
                    );
                  }}
                </Field>
              </div>
              {touched.suppliers_id && errors.suppliers_id ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="suppliers_id-warning"
                >
                  {errors.suppliers_id}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Đơn giá, Đơn vị tính, Hoá đơn ======================= */}
            <div className="grid grid-cols-3 gap-2 col-span-2">
              {/* ======================= Đơn giá ======================= */}
              <div className="form-group ">
                <div className="form-style">
                  <label htmlFor="price">
                    Đơn giá
                    {/* <span className="text-red-500">*</span> */}
                  </label>
                  <InputPriceNotTable
                    className="grow"
                    placeholder="Nhập đơn giá"
                    name="price"
                    id="price"
                    value={formik.values.price}
                    onChange={onChangeSelect}
                    onBlur={handleBlur}
                  />
                </div>
                {touched.price && errors.price ? (
                  <p className="mt-1 text-red-500 text-sm" id="price-warning">
                    {errors.price}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Đơn vị tính ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="unit">
                    Đơn vị tính
                    <span className="text-red-500">*</span>
                  </label>
                  <Field name="unit">
                    {({ field }) => {
                      return (
                        <Select
                          {...field}
                          id="unit"
                          name="unit"
                          showSearch
                          placeholder="Chọn đơn vị tính"
                          optionFilterProp="children"
                          onChange={(value) => onChangeSelect(value, "unit")}
                          filterOption={filterOption}
                          options={listUnit}
                          value={
                            formik.values.unit != "" ? formik.values.unit : null
                          }
                          onBlur={handleBlur}
                          onDropdownVisibleChange={getListUnit}
                          notFoundContent={
                            loadingSelect ? <Spin size="small" /> : null
                          }
                        />
                      );
                    }}
                  </Field>
                </div>
                {touched.unit && errors.unit ? (
                  <p className="mt-1 text-red-500 text-sm" id="unit-warning">
                    {errors.unit}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Hoá đơn ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="vat">
                    Loại hoá đơn <span className="text-red-500">*</span>
                  </label>
                  <Field name="unit">
                    {({ field }) => {
                      return (
                        <Select
                          {...field}
                          id="vat"
                          name="vat"
                          showSearch
                          placeholder="Chọn loại hoá đơn"
                          optionFilterProp="children"
                          onChange={(value) => onChangeSelect(value, "vat")}
                          filterOption={filterOption}
                          options={listVAT}
                          value={
                            formik.values.vat != "" ? formik.values.vat : null
                          }
                          onBlur={handleBlur}
                          onDropdownVisibleChange={getListTaxCode}
                          notFoundContent={
                            loadingSelect ? <Spin size="small" /> : null
                          }
                        />
                      );
                    }}
                  </Field>
                </div>
                {touched.vat && errors.vat ? (
                  <p className="mt-1 text-red-500 text-sm" id="vat-warning">
                    {errors.vat}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Quy cách sản phẩm ======================= */}
              <div className="form-group col-span-full">
                <div className="form-style">
                  <label htmlFor="package_case">Quy cách sản phẩm</label>
                  <textarea
                    className="grow"
                    type="text"
                    placeholder="Nhập quy cách sản phẩm"
                    name="package_case"
                    id="package_case"
                    value={formik.values.package_case}
                    onChange={formik.handleChange}
                    onBlur={handleBlur}
                  ></textarea>
                </div>
                {touched.package_case && errors.package_case ? (
                  <p className="mt-1 text-red-500 text-sm" id="name-warning">
                    {errors.package_case}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Quy cách đóng gói ======================= */}
              <div className="form-group col-span-full">
                <div className="form-style">
                  <label htmlFor="packaging">Quy cách đóng gói</label>
                  <textarea
                    className="grow"
                    type="text"
                    placeholder="Nhập quy cách đóng gói"
                    name="packaging"
                    id="packaging"
                    value={formik.values.packaging}
                    onChange={formik.handleChange}
                    onBlur={handleBlur}
                  ></textarea>
                </div>
                {touched.packaging && errors.packaging ? (
                  <p className="mt-1 text-red-500 text-sm" id="name-warning">
                    {errors.packaging}
                  </p>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </form>
      </FormikProvider>
    </Modal>
  );
}
