import React, { useEffect, useState } from "react";
import { Table, Tooltip, message } from "antd";
import { TbHistoryToggle } from "react-icons/tb";
import { useDispatch, useSelector } from "react-redux";
import { stringToDate } from "../../../utils/others";
import { setPage } from "../../../redux/slice/pageSlice";
import {
  deleteProductId,
  getListProduct,
  updateProduct,
} from "../../../utils/product";
import { otherKeys } from "../../../utils/user";
import DeleteBtnItemComponent from "../../ButtonComponent/DeleteBtnItemComponent";
import FixBtnComponent from "../../ButtonComponent/FixBtnComponent";
import RenderWithFixComponent from "../../ButtonComponent/RenderIdFix";
import UndoBtnComponent from "../../ButtonComponent/UndoBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import TableLength from "../../ButtonComponent/TableLength";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListProduct({
  showModal,
  showHistoryProductModal,
  loadPage,
  reload,
  setReload,
  deletedPage,
  rowSelection,
  setKeySelected,
  permission,
  searchFilter,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();

  let page = useSelector((state) => state.pageSlice.value);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      // console.log(page);
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list nhà cung cấp theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));

    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }
    if (keyParam) {
      keyParam = { ...keyParam, search_type: "start_with" };
    }
    keyParam = { ...keyParam, ...searchFilter };

    let res = await getListProduct(page, keyParam);
    // console.log(res);
    if (res.status) {
      setListData(res.data);
      setTotal(res.rawData.total);
      setPageSize(res.rawData.per_page);
    }

    dispatch(setSpinner(false));
  };

  // 4 - Xác nhận xoá nhà cung cấp
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deleteData(id, value);
  };

  // 5 - Xoá NCC
  const deleteData = async (id, input) => {
    dispatch(setSpinner(true));

    // console.log(id);
    let res;
    if (input == 1) {
      res = await deleteProductId({ id: id, deleted: true });
    } else {
      res = await deleteProductId({ id: id });
    }
    // console.log(res);
    if (res) {
      setKeySelected([]);
      // console.log("reload trang");
      setReload(true);
    }
    // Load lại từ trang 1
    dispatch(setSpinner(false));
  };

  // 6 - Hoàn tác xoá
  const undoDelete = async (id) => {
    dispatch(setSpinner(true));

    let res;
    res = await updateProduct({ id: id, deleted: false });
    // console.log(res);
    if (res) {
      setKeySelected([]);
      // console.log("reload trang");
      setReload(true);
    }

    dispatch(setSpinner(false));
  };

  // 7 - Kích hoạt sản phẩm
  const confirmUndo = (id) => {
    undoDelete(id);
  };

  // 8 - Lần đầu load trang => Kéo về danh sách user & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    loadPage();
    getListData(page);
  }, []);

  // 9 - Khi reload == true thì load lại API danh sách
  useEffect(() => {
    if (reload) {
      // loadPage();
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  // console.log(listData);

  const columns = [
    {
      title: "Mã SP",
      dataIndex: "sku",
      key: "sku",
      width: 100,
      fixed: "left",
      className: "text-center",
      render: (text, record) =>
        permission?.product?.update === 1 ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record.id);
            }}
            text={text}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Tên hàng",
      dataIndex: "name",
      key: "name",
      // width: 120,
      fixed: "left",
    },
    {
      title: "Ngày tạo",
      dataIndex: "created_at",
      key: "created_at",
      width: 120,
      render: (text, record) => {
        if (record.created_at) {
          return <p className="text-center">{stringToDate(text)}</p>;
        }
      },
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
      width: 130,
      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "ĐVT",
      dataIndex: "unit_name",
      key: "unit_name",
      width: 100,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },

    {
      title: "Loại hoá đơn",
      dataIndex: "vat_name",
      key: "vat_name",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Nhà cung cấp",
      dataIndex: "suppliers_name",
      key: "suppliers_name",
      width: 150,
    },
    {
      title: "Quy cách sản phẩm",
      dataIndex: "package_case",
      key: "package_case",
    },
    {
      title: "Quy cách đóng gói",
      dataIndex: "packaging",
      key: "packaging",
    },
    {
      title: "Người mua",
      dataIndex: "buyer_name",
      key: "buyer_name",
      width: 150,
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            {permission?.product?.update === 1 && (
              <FixBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  showModal(record.id);
                }}
              />
            )}
            {/* ======================= Undo ======================= */}
            {permission?.product?.update === 1 && (
              <UndoBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  confirmUndo(record.id);
                }}
                text={"sản phẩm"}
              />
            )}
            {/* ======================= Xoá ======================= */}
            {permission?.product?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => {
                  if (deletedPage) {
                    confirm(record.id, 1);
                  } else {
                    confirm(record.id, 0);
                  }
                }}
                text={"sản phẩm"}
              />
            )}
            {/* ======================= Lịch sử ======================= */}
            <Tooltip placement="bottom" title="Lịch sử chỉnh sửa">
              <button
                onClick={() => {
                  showHistoryProductModal(record.id);
                }}
              >
                <TbHistoryToggle className="text-blue-600" />
              </button>
            </Tooltip>
          </div>
        );
      },
    },
  ];

  return (
    <div className="table-render-API">
      <TableLength page={page} pageSize={pageSize} total={total} />
      <Table
        {...tableProps}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 600,
          x: 1600,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
