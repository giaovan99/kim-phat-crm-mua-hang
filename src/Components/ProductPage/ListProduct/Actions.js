import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
import DropdownOptions from "../../DropdownOptions/DropdownOptions";
import AddBtnComponent from "../../ButtonComponent/AddBtnComponent";
import { Select } from "antd";
import { normalize } from "../../../utils/others";

export default function Actions({
  showModal,
  setReload,
  deletedPage,
  keySelected,
  setKeySelected,
  permission,
  listNCC,
  setSearchFilter,
  listDepartment,
}) {
  console.log(listDepartment);

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      keywords: "",
      suppliers_name: "",
      package_case: "",
      department: "",
    },
    onSubmit: async (values) => {
      setSearchFilter(values);
      setReload(true);
    },
  });
  const onSearch = (value) => {
    // console.log("search:", value);
  };
  const filterOption = (input, option) =>
    normalize(option?.label ?? "").includes(normalize(input));

  const onChangeSelect = (value, field) => {
    formik.setFieldValue(field, value);
  };

  return (
    <div className="actions flex-col gap-5">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div
            className="flex flex-wrap-reverse gap-2"
            style={{ alignItems: "end" }}
          >
            {/* ======================= Khung tìm kiếm ======================= */}
            <div className="form-group">
              <div className="form-style items-end flex-wrap">
                {/* Tìm */}
                <div>
                  <label>Tìm</label>
                  <input
                    id="keywords"
                    name="keywords"
                    type="text"
                    placeholder="Tìm tên sản phẩm, NCC, ..."
                    value={formik.values.keywords}
                    onChange={formik.handleChange}
                    className="max-w-[200px]"
                  />
                </div>
                {/* NCC */}
                <div>
                  <label>NCC</label>
                  <input
                    id="suppliers_name"
                    name="suppliers_name"
                    type="text"
                    placeholder="Nhà cung cấp"
                    value={formik.values.suppliers_name}
                    onChange={formik.handleChange}
                    className="max-w-[200px]"
                  />
                </div>
                {/* Quy cách */}
                <div>
                  <label>Quy cách</label>
                  <input
                    id="package_case"
                    name="package_case"
                    type="text"
                    placeholder="Quy cách"
                    value={formik.values.package_case}
                    onChange={formik.handleChange}
                    className="max-w-[200px]"
                  />
                </div>
                {/* Bộ phận */}
                <div>
                  <label>Bộ phận</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "department")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listDepartment}
                    value={
                      formik.values.department != ""
                        ? formik.values.department
                        : null
                    }
                    className="w-[162px]"
                  />
                </div>
                <div className="flex gap-2">
                  <button className="btn-main-yl btn-actions" type="submit">
                    <HiOutlineSearch />
                    Tìm
                  </button>
                  <button
                    className="ant-btn btn-actions text-white bg-black ant-btn-default rounded-lg"
                    type="button"
                    onClick={formik.handleReset}
                  >
                    Reset
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      <div className="flex gap-2 items-end">
        {permission?.product?.update === 1 && (
          <DropdownOptions
            arrId={keySelected}
            deletedPage={deletedPage}
            page={"product"}
            setReload={setReload}
            setKeySelected={setKeySelected}
          />
        )}
        {!deletedPage && permission?.product?.create === 1 && (
          <div className="flex gap-3">
            <AddBtnComponent
              onClickF={() => showModal(null)}
              text={"Thêm sản phẩm"}
            />
          </div>
        )}
      </div>
    </div>
  );
}
