import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { updateTaxCode, vatList } from "../../../../../utils/product";
import RenderWithFixComponent from "../../../../ButtonComponent/RenderIdFix";
import FixBtnComponent from "../../../../ButtonComponent/FixBtnComponent";
import DeleteBtnItemComponent from "../../../../ButtonComponent/DeleteBtnItemComponent";
import { setSpinner } from "../../../../../redux/slice/spinnerSlice";

const tableProps = {
  bordered: true,
  size: "small",
};

export default function TableListTaxCode({
  showModal,
  reload,
  setReload,
  defineItem,
  rowSelection,
  permission,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const pageSize = 20;
  let keywords = useSelector((state) => state.keywordsSlice.keywords);
  let dispatch = useDispatch();

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list data
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (keywords) {
      keyParam = { keywords: keywords };
    }
    let res = await vatList(page, keyParam);
    if (res.status) {
      setListData(res.data);
      setTotal(res.data.length);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Xác nhận xoá loại hoá đơn
  const confirm = (record) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deleteTax(record);
  };

  // 6 - Xoá loại hoá đơn
  const deleteTax = async (record) => {
    dispatch(setSpinner(true));
    // console.log(record)
    let item = defineItem(record);
    let res = await updateTaxCode({
      ...item,
      deleted: true,
    });
    // console.log(res);
    if (res) {
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 7 - Gọi lại API khi chuyển tab
  useEffect(() => {
    if (reload) {
      getListData();
      setReload(false);
    }
  }, [reload]);

  // 8 - Lần đầu khi load trang
  useEffect(() => {
    getListData();
  }, []);

  const columns = [
    {
      title: "Loại hoá đơn",
      dataIndex: "label",
      key: "label",
      fixed: "left",
      className: "text-center",
      render: (text, record) =>
        permission["product/manage"]?.update === 1 ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record);
            }}
            text={text}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Giá trị VAT",
      dataIndex: "vat",
      key: "vat",
      fixed: "left",
      className: "text-center",
      render: (text, record) => <p>{text}%</p>,
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            {permission["product/manage"]?.update === 1 && (
              <FixBtnComponent
                onClickF={() => {
                  showModal(record);
                }}
              />
            )}
            {/* ======================= Xoá ======================= */}
            {permission["product/manage"]?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => confirm(record)}
                text={"loại hoá đơn"}
              />
            )}
          </div>
        );
      },
    },
  ];
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
          x: 240,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
