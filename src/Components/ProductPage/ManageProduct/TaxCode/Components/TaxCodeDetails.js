import React, { useEffect } from "react";
import { Modal } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import Swal from "sweetalert2";
import { updateTaxCode } from "../../../../../utils/product";
import { setSpinner } from "../../../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

export default function TaxCodeDetail({
  object,
  isModalOpen,
  setIsModalOpen,
  setObject,
  setReload,
  defineItem,
}) {
  const initial = {
    label: "",
    vat: "",
  };
  let dispatch = useDispatch();

  const resetForm = () => {
    formik.resetForm();
    setObject(null);
    formik.setValues(initial);
  };

  // Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      label: yup.string().required("Tên không để trống"),
      vat: yup
        .number()
        .required("Giá trị không để trống")
        .min(0, "Giá trị không hợp lệ"),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      // console.log(values);
      let item = defineItem(values);
      // console.log(item);
      let res = await updateTaxCode(item);
      // console.log(res);
      if (res) {
        // alert thông báo
        Swal.fire({
          title: object ? "Chỉnh sửa loại hoá đơn" : "Thêm loại hoá đơn",
          text: object
            ? "Chỉnh sửa loại hoá đơnthành công!"
            : "Thêm loại hoá đơn thành công",
          icon: "success",
          confirmButtonText: "Đóng",
        });
        // reset form
        resetForm();
        setReload(true);
        //  đóng modal
        setIsModalOpen(false);
      }
      dispatch(setSpinner(false));
    },
  });
  const { handleBlur, handleChange, touched, errors } = formik;

  // Chạy lần đầu khi Modal open -> check xem là thêm hay sửa
  useEffect(() => {
    if (object) {
      formik.setValues(object);
    } else {
      formik.setValues(initial);
    }
  }, [object]);

  //   Khi lưu
  const handleOk = () => {
    formik.handleSubmit();
  };
  //   Khi Huỷ
  const handleCancel = () => {
    resetForm();
    setIsModalOpen(false);
  };
  // console.log(formik.errors);

  return (
    <Modal
      title={object ? "Chỉnh sửa loại hoá đơn" : "Thêm loại hoá đơn"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form id="formAddTaxCode" onSubmit={formik.handleSubmit}>
        <div className="grid grid-cols-2 gap-2">
          {/* ======================= Đặt tên loại hoá đơn ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="label">
                Tên loại hoá đơn<span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên loại hoá đơn"
                name="label"
                id="label"
                value={formik.values.label}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.label && errors.label ? (
              <p className="mt-1 text-red-500 text-sm" id="label-warning">
                {errors.label}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Giá trị VAT ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="vat">
                Giá trị VAT(%)<span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="number"
                placeholder="Chỉ cần nhập số"
                name="vat"
                id="vat"
                value={formik.values.vat}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.vat && errors.vat ? (
              <p className="mt-1 text-red-500 text-sm" id="vat-warning">
                {errors.vat}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
      </form>
    </Modal>
  );
}
