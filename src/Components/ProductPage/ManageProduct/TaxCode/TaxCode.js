import React, { useEffect, useState } from "react";
import Actions from "./Components/Action";
import TableListTaxCode from "./Components/TableListTaxCode";
import TaxCodeDetail from "./Components/TaxCodeDetails";
import { useDispatch } from "react-redux";
import { resetKeywords } from "../../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../../redux/slice/pageSlice";

export default function TaxCode({ keyNumActive, keyTab, permission }) {
  // Khai báo state
  const [taxCodeChoosen, setTaxCodeChoosen] = useState();
  const [isModalOpen, setIsModalOpen] = useState(false); // Ẩn hiện modal thêm/sửa
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const dispatch = useDispatch();
  const [keySelected, setKeySelected] = useState([]); //

  // Hiển thị Modal
  const showModal = (record) => {
    setTaxCodeChoosen(record);
    setIsModalOpen(true);
  };

  useEffect(() => {
    if (keyNumActive == keyTab) {
      dispatch(resetKeywords());
      dispatch(resetPage());
      setReload(true);
    }
  }, [keyNumActive]);

  // 5 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    dispatch(resetKeywords());
    dispatch(resetPage());
  }, []);

  // 5 - format lại data
  const defineItem = (record) => {
    return {
      key: record?.key,
      name: record?.label,
      value: record?.vat,
    };
  };
  // rowSelection object indicates the need for row selection
  const rowSelection = {
    onChange: (selectedRows, info) => {
      setKeySelected(info);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };
  return (
    <div className="productTaxCode">
      {/* ======================= Hành động ======================= */}
      <Actions
        showModal={showModal}
        setReload={setReload}
        keyNumActive={keyNumActive}
        keyTab={keyTab}
        keySelected={keySelected}
        setKeySelected={setKeySelected}
        permission={permission}
      />
      {/* ======================= Danh sách đơn vị tính ======================= */}
      <TableListTaxCode
        showModal={showModal}
        reload={reload}
        setReload={setReload}
        defineItem={defineItem}
        rowSelection={rowSelection}
        permission={permission}
      />
      {/* ======================= Modals ======================= */}
      <div className="modals">
        <TaxCodeDetail
          object={taxCodeChoosen}
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          setObject={setTaxCodeChoosen}
          setReload={setReload}
          defineItem={defineItem}
        />
      </div>
    </div>
  );
}
