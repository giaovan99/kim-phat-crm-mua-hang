import React, { useEffect, useState } from "react";
import Actions from "./Components/Action";
import TableListUnit from "./Components/TableListUnit";
import Type1 from "../../../ManageUnits/Type1/Type1";
import { useDispatch } from "react-redux";
import { resetPage } from "../../../../redux/slice/pageSlice";
import { resetKeywords } from "../../../../redux/slice/keywordsSlice";

export default function Unit({ keyNumActive, keyTab, permission }) {
  // Khai báo state
  const [isModalOpen, setIsModalOpen] = useState(false); // Ẩn hiện modal thêm/sửa
  const [object, setObject] = useState(); // Id của đơn vị được chọn
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const dispatch = useDispatch();
  const [keySelected, setKeySelected] = useState([]); //

  // Hiển thị Modal
  let showModal = (id) => {
    setObject(id);
    setIsModalOpen(true);
  };

  useEffect(() => {
    if (keyNumActive == keyTab) {
      dispatch(resetKeywords());
      dispatch(resetPage());
      setReload(true);
    }
  }, [keyNumActive]);

  // 5 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    dispatch(resetKeywords());
    dispatch(resetPage());
  }, []);

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    onChange: (selectedRows, info) => {
      setKeySelected(info);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };
  return (
    <div className="productUnit">
      {/* ======================= Hành động ======================= */}
      <Actions
        showModal={showModal}
        setReload={setReload}
        keyNumActive={keyNumActive}
        keyTab={keyTab}
        keySelected={keySelected}
        setKeySelected={setKeySelected}
        permission={permission}
      />
      {/* ======================= Danh sách đơn vị tính ======================= */}
      <TableListUnit
        showModal={showModal}
        reload={reload}
        setReload={setReload}
        rowSelection={rowSelection}
        permission={permission}
      />
      {/* ======================= Modals ======================= */}
      <div className="modals">
        <Type1
          title={object?.value ? "Thêm đơn vị tính" : "Sửa đơn vị tính"}
          object={object}
          API="unit"
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          setObject={setObject}
          setReload={setReload}
        />
      </div>
    </div>
  );
}
