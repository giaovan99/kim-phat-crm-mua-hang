import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { setPage } from "../../redux/slice/pageSlice";
import { useDispatch, useSelector } from "react-redux";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import {
  reportProductList,
  reportProductPriceList,
} from "../../utils/reportAPI";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};
const TableListData = ({ formik, reload, setReload }) => {
  let page = useSelector((state) => state.pageSlice.value);
  const dispatch = useDispatch();
  const [pageSize, setPageSize] = useState(0);
  const [total, setTotal] = useState(0);
  const [listData, setListData] = useState([]);

  // 3 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list phiếu hoàn ứngtheo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = { ...formik.values };

    let res = await reportProductPriceList(page, keyParam);
    // console.log(res);
    if (res.status) {
      setListData(res.data);
      setTotal(res.data.length);
      setPageSize(res.data.length);
    }
    dispatch(setSpinner(false));
  };

  // 9 - Lần đầu load trang => Kéo về danh sách user & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    getListData(page);
  }, []);

  // 10 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      // console.log("run");
      // dispatch(resetPage());
      getListData(page);
      setReload(false);
    }
  }, [reload]);
  Table.SELECTION_COLUMN.fixed = "left";
  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      fixed: "left",
      width: 45,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Mã số NCC",
      dataIndex: "supplier_code",
      key: "supplier_code",
    },
    {
      title: "NCC",
      dataIndex: "supplier_name",
      key: "supplier_name",
    },
    {
      title: "Mã số đơn hàng",
      dataIndex: "po_code",
      key: "po_code",
    },
    {
      title: "Mã số sản phẩm",
      dataIndex: "sku",
      key: "sku",
    },
    {
      title: "Tên hàng hoá",
      dataIndex: "name",
      key: "name",
    },
    {
      title: "Thời gian đặt hàng",
      dataIndex: "create_at",
      key: "create_at",
    },
    {
      title: "Đơn giá (chưa VAT)",
      dataIndex: "price",
      key: "price",
    },
  ];
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 600,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
};

export default TableListData;
