import React, { useEffect, useState } from "react";
import "./sideBar.scss";
import {
  UserOutlined,
  HomeOutlined,
  BarcodeOutlined,
  ContactsOutlined,
  DollarOutlined,
  ShoppingCartOutlined,
  FileSyncOutlined,
  CreditCardOutlined,
  FormOutlined,
  SettingOutlined,
  FileTextOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
import { NavLink, useLocation } from "react-router-dom";
import { useSelector } from "react-redux";
import { extractLinkInfo } from "../../utils/localStorage";
const AntdIcons = {
  UserOutlined,
  HomeOutlined,
  BarcodeOutlined,
  ContactsOutlined,
  DollarOutlined,
  ShoppingCartOutlined,
  FileSyncOutlined,
  CreditCardOutlined,
  FormOutlined,
  SettingOutlined,
  FileTextOutlined,
};

const { Sider } = Layout;

export default function SideBar({ collapsed }) {
  const location = useLocation();
  const settingInfo = useSelector((state) => state.loginInfoSlice.settingInfo);
  const [logo, setLogo] = useState();
  const [title, setTitle] = useState();

  useEffect(() => {
    if (settingInfo.length > 0) {
      settingInfo.forEach((item) => {
        // Gắn giá trị cho logo
        if (item.keyword == "logo") {
          setLogo(item.url);
        }
        // Gắn giá trị cho title
        if (item.keyword == "title") {
          setTitle(item.value);
        }
      });
    }
  }, [settingInfo]);

  let listLink = useSelector(
    (state) => state.loginInfoSlice.loginInfo.side_bar
  );

  return (
    <section className="section section-sidebar">
      <Sider
        trigger={null}
        collapsible
        collapsed={collapsed}
        style={{
          overflow: "auto",
          height: "100vh",
          position: "fixed",
          left: 0,
          top: 0,
          bottom: 0,
        }}
        width={250}
        collapsedWidth="4.6rem"
      >
        {/* Logo */}
        <div className="logo py-[13px] px-[8px]">
          <div className="logo-icon flex items-center gap-4">
            <img
              src={logo ? logo : "../../../public/img/not-found.jpeg"}
              alt="Logo icon"
              width={33}
              className="ml-[13px]"
            />
            {collapsed ? (
              <></>
            ) : (
              <h1 className="text-3xl font-bold">{title}</h1>
            )}
          </div>
        </div>

        {/* Menu */}
        <Menu
          mode="inline"
          selectedKeys={
            location.pathname == "/purchase_order/add" ||
            location.pathname.includes("/purchase_order/fix")
              ? "purchase_order"
              : location.pathname == "/purchase_proposal/add" ||
                location.pathname.includes("/purchase_proposal/fix")
              ? "purchase_proposal"
              : location.pathname == "/stock_import/add" ||
                location.pathname.includes("/stock_import/fix")
              ? "stock_import"
              : location.pathname == "/payment_request/add" ||
                location.pathname.includes("/payment_request/fix")
              ? "payment_request"
              : location.pathname.replace("/", "")
          }
          defaultOpenKeys={["sub1"]}
          style={{
            height: "100%",
            borderRight: 0,
          }}
          items={listLink?.map((item) => {
            const newChildren = item.children?.map((child) => {
              const { to, content } = extractLinkInfo(child.label);
              return {
                ...child,
                label: <NavLink to={to}>{content}</NavLink>,
              };
            });
            // check xem label có <NavLink/> hay không? Nếu có thì ko có children (tabcon)
            const isValid = item.label.includes("NavLink");
            const IconElement = AntdIcons[item.icon];
            if (isValid) {
              const { to, content } = extractLinkInfo(item.label);
              return {
                ...item,
                icon: IconElement ? <IconElement /> : "",
                children: newChildren,
                label: <NavLink to={to}>{content}</NavLink>,
              };
            } else {
              return {
                ...item,
                icon: IconElement ? <IconElement /> : "",
                children: newChildren,
              };
            }
          })}
        />
      </Sider>
    </section>
  );
}
