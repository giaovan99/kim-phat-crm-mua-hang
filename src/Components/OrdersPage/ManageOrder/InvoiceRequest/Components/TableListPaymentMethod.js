import React, { useEffect, useState } from "react";
import { Popconfirm, Table, Tooltip, message } from "antd";
import { HiOutlinePencilSquare, HiOutlineTrash } from "react-icons/hi2";
import { useDispatch, useSelector } from "react-redux";
import {
  deletedInvoiceRequest,
  deletedPaymentMethod,
  getInvoiceRequestList,
  getPaymentMethodList,
} from "../../../../../utils/order";
import RenderWithFixComponent from "../../../../ButtonComponent/RenderIdFix";
import FixBtnComponent from "../../../../ButtonComponent/FixBtnComponent";
import DeleteBtnItemComponent from "../../../../ButtonComponent/DeleteBtnItemComponent";
import { setSpinner } from "../../../../../redux/slice/spinnerSlice";

const tableProps = {
  bordered: true,
  size: "small",
};

export default function TableListShippingMethod({
  showModal,
  reload,
  setReload,
  rowSelection,
  permission,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const pageSize = 20;
  const dispatch = useDispatch();
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list data
  const getListData = async (page) => {
    dispatch(setSpinner(true));

    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (keywords) {
      keyParam = { keywords: keywords };
    }
    // console.log(keyParam);
    let res = await getInvoiceRequestList(page, keyParam);
    if (res.status) {
      setListData(res.data);
      setTotal(res.data.length);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Xác nhận xoá đơn vị tính
  const confirm = (record) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deletePayment(record);
  };

  // 5 - Xoá đơn vị tính
  const deletePayment = async (record) => {
    dispatch(setSpinner(true));

    // console.log(record);
    let res = await deletedInvoiceRequest({
      ...record,
      deleted: true,
    });
    // console.log(res);
    if (res) {
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 6 - Gọi lại API khi chuyển tab
  useEffect(() => {
    if (reload) {
      getListData();
      setReload(false);
    }
  }, [reload]);

  // 7 - Lần đầu khi load trang
  useEffect(() => {
    getListData();
  }, []);

  const columns = [
    {
      title: "Yêu cầu về hoá đơn",
      dataIndex: "label",
      key: "label",
      // width: 100,
      fixed: "left",
      className: "text-center",
      render: (text, record) =>
        permission["purchase_order/manage"]?.update ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record);
            }}
            text={text}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            {permission["purchase_order/manage"]?.update === 1 && (
              <FixBtnComponent
                onClickF={() => {
                  showModal(record);
                }}
              />
            )}

            {/* ======================= Xoá ======================= */}
            {permission["purchase_order/manage"]?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => confirm(record)}
                text={"yêu cầu về hoá đơn"}
              />
            )}
          </div>
        );
      },
    },
  ];
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
          x: 240,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
