import React, { useEffect, useState } from "react";
import { Popconfirm, Table, Tooltip, message } from "antd";
import { HiOutlinePencilSquare, HiOutlineTrash } from "react-icons/hi2";
import { useDispatch, useSelector } from "react-redux";
import RenderWithFixComponent from "../../../ButtonComponent/RenderIdFix";
import FixBtnComponent from "../../../ButtonComponent/FixBtnComponent";
import DeleteBtnItemComponent from "../../../ButtonComponent/DeleteBtnItemComponent";
import { companyList, deleteCompanyId } from "../../../../utils/order";
import { setPage } from "../../../../redux/slice/pageSlice";
import { otherKeys } from "../../../../utils/user";
import { setSpinner } from "../../../../redux/slice/spinnerSlice";

const tableProps = {
  bordered: true,
  size: "small",
};

export default function TableListCompany({
  showModal,
  loadPage,
  reload,
  setReload,
  deletedPage,
  rowSelection,
  permission,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();
  let page = useSelector((state) => state.pageSlice.value);
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list data
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }
    if (keyParam || keywords) {
      keyParam = { ...keyParam, keywords };
    }

    // console.log(keyParam);

    let res = await companyList(page, keyParam);
    if (res.status) {
      setListData(res.data);
      setTotal(res.dataRaw.total);
      setPageSize(res.dataRaw.per_page);
    }
    dispatch(setSpinner(false));
  };

  // 6 - Xác nhận xoá cty
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deleteData(id, 1);
  };

  // 7 - Xoá cty
  const deleteData = async (id, input) => {
    dispatch(setSpinner(true));
    // console.log(id);
    let res;
    if (input == 1) {
      res = await deleteCompanyId({ id: id, deleted: true });
    } else {
      res = await deleteCompanyId({ id: id });
    }
    // console.log(res);
    if (res) {
      // console.log("reload trang");
      setReload(true);
    }
    // Load lại từ trang 1
    dispatch(setSpinner(false));
  };

  // 6 - Gọi lại API khi chuyển tab
  useEffect(() => {
    if (reload) {
      getListData();
      setReload(false);
    }
  }, [reload]);

  // 7 - Lần đầu khi load trang
  useEffect(() => {
    getListData();
  }, []);

  const columns = [
    {
      title: "Công ty",
      dataIndex: "name",
      key: "name",
      fixed: "left",
      className: "text-center",
      render: (text, record) =>
        permission["purchase_order/manage"]?.update ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record);
            }}
            text={text}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Tên pháp lý",
      dataIndex: "name_legal",
      key: "name_legal",
      fixed: "left",
      className: "text-center",
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "MST",
      dataIndex: "tax_no",
      key: "tax_no",
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            {permission["purchase_order/manage"]?.update === 1 && (
              <FixBtnComponent
                onClickF={() => {
                  showModal(record);
                }}
              />
            )}

            {/* ======================= Xoá ======================= */}
            {permission["purchase_order/manage"]?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => confirm(record.id)}
                text={"công ty"}
              />
            )}
          </div>
        );
      },
    },
  ];
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
