import { useFormik } from "formik";
import React, { useEffect } from "react";
import { useDispatch } from "react-redux";
import DropdownOptions from "../../../DropdownOptions/DropdownOptions";
import AddBtnComponent from "../../../ButtonComponent/AddBtnComponent";
import { setKeywords } from "../../../../redux/slice/keywordsSlice";
import { HiOutlineSearch } from "react-icons/hi";

export default function Actions({
  showModal,
  setReload,
  keyNumActive,
  keyTab,
  keySelected,
  setKeySelected,
  permission,
}) {
  const dispatch = useDispatch();
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: async (values) => {
      dispatch(setKeywords(values.search));
      setReload(true);
    },
  });
  useEffect(() => {
    if (keyNumActive == keyTab) {
      formik.resetForm();
    }
  }, [keyNumActive]);
  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div className="flex gap-1">
            {permission["purchase_order/manage"]?.update === 1 && (
              <DropdownOptions
                arrId={keySelected}
                page={"companyList"}
                setReload={setReload}
                setKeySelected={setKeySelected}
              />
            )}

            <div className="form-group">
              <div className="form-style">
                <input
                  id="search"
                  name="search"
                  type="text"
                  placeholder="Tìm kiếm công ty"
                  value={formik.values.search}
                  onChange={formik.handleChange}
                />
                <button className="btn-main-yl btn-actions">
                  <HiOutlineSearch />
                  Tìm kiếm
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      <div className="flex gap-3">
        {permission["purchase_order/manage"]?.create === 1 && (
          <AddBtnComponent
            onClickF={() => showModal(null)}
            text={"Thêm công ty"}
          />
        )}
      </div>
    </div>
  );
}
