import { useFormik } from "formik";
import React, { useEffect } from "react";
import * as yup from "yup";
import { Modal } from "antd";

import {
  companyUpdate,
  createCompany,
  getCompany,
} from "../../../../utils/order";
import { showError } from "../../../../utils/others";
export default function CompanyDetails({
  isModalOpen,
  setIsModalOpen,
  warehouse,
  setWarehouse,
  setReload,
  deletedPage,
}) {
  // Khai báo state
  const initial = {
    name_legal: "",
    name: "",
    address: "",
    tax_no: "",
  };

  // Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      name: yup.string().required("Tên rút gọn không để trống"),
      name_legal: yup.string().required("Tên pháp lý không để trống"),
    }),
    onSubmit: async (values) => {
      // console.log(values);
      if (warehouse) {
        // console.log("sửa");
        let res = await companyUpdate(values);
        if (res) {
          resetForm();
          //  đóng modal
          setIsModalOpen(false);
          //  Load lại trang 1
          setReload(true);
        }
      } else {
        let res = await createCompany(values, formik);
        if (res) {
          resetForm();
          //  đóng modal
          setIsModalOpen(false);
          //  Load lại trang 1
          setReload(true);
        }
      }
    },
  });
  const { handleBlur, handleChange, touched, errors } = formik;

  // 3 - Lấy thông tin kho
  const getDataInfo = async () => {
    // console.log(warehouse);
    let res = await getCompany(warehouse.id);
    if (res.data.success) {
      let data = res.data.data;
      // console.log(data);
      formik.setValues(data);
    } else {
      console.log(res);
      showError();
    }
  };

  //  4 - Lần đầu khi load trang => check xem là add hay fix
  useEffect(() => {
    if (warehouse) {
      // lấy thông tin nhà cung cấp trong danh sách nhà cung cấp & show ra màn hình
      getDataInfo();
    } else {
      formik.setValues(initial);
    }
  }, [warehouse]);

  // 5 - Khi lưu Form
  const handleOk = () => {
    formik.handleSubmit();
  };

  // 6 - Reset Form
  const resetForm = () => {
    formik.resetForm();
    formik.setValues(initial);
  };

  // 7 - Khi cancel form
  const handleCancel = () => {
    setWarehouse(null);
    resetForm();
    setIsModalOpen(false);
  };
  return (
    <Modal
      title={warehouse ? "Chỉnh sửa công ty" : "Thêm công ty"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: !deletedPage
          ? "btn-main-yl inline-flex items-center justify-center"
          : "hidden",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form
        id="formAddWarehouse"
        onSubmit={formik.handleSubmit}
        className={!deletedPage ? "" : "pointer-events-none"}
      >
        <div className="grid grid-cols-2 gap-2">
          {/* ======================= Tên pháp lý ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="name_legal">
                Tên pháp lý <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên công ty"
                name="name_legal"
                id="name_legal"
                value={formik.values.name_legal}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.name && errors.name ? (
              <p className="mt-1 text-red-500 text-sm" id="name-warning">
                {errors.name}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Tên công ty ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="name">
                Tên rút gọn <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên công ty"
                name="name"
                id="name"
                value={formik.values.name}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.name && errors.name ? (
              <p className="mt-1 text-red-500 text-sm" id="name-warning">
                {errors.name}
              </p>
            ) : (
              <></>
            )}
          </div>

          {/* ======================= Địa chỉ ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="address">Địa chỉ</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập địa chỉ"
                name="address"
                id="address"
                value={formik.values.address}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.address && errors.address ? (
              <p className="mt-1 text-red-500 text-sm" id="address-warning">
                {errors.address}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Mã số thuế (MST) ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="tax_no">Mã số thuế (MST)</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập mst"
                name="tax_no"
                id="tax_no"
                value={formik.values.tax_no}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.tax_no && errors.tax_no ? (
              <p className="mt-1 text-red-500 text-sm" id="tax_no-warning">
                {errors.tax_no}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
      </form>
    </Modal>
  );
}
