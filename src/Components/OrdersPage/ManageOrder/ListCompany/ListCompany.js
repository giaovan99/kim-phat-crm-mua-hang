import { useFormik } from "formik";
import React, { useEffect, useState } from "react";
import * as yup from "yup";
import { useDispatch } from "react-redux";
import Actions from "./Actions";
import { resetKeywords } from "../../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../../redux/slice/pageSlice";
import TableListCompany from "./TableListCompany";
import CompanyDetails from "./CompanyDetails";

export default function ListCompany({ keyNumActive, keyTab, permission }) {
  // Khai báo state
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [object, setObject] = useState();
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const dispatch = useDispatch();
  const [keySelected, setKeySelected] = useState([]); //
  // Hiển thị Modal
  let showModal = (id) => {
    setObject(id);
    setIsModalOpen(true);
  };

  useEffect(() => {
    if (keyNumActive == keyTab) {
      dispatch(resetKeywords());
      dispatch(resetPage());
      setReload(true);
    }
  }, [keyNumActive]);

  // 5 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    dispatch(resetKeywords());
    dispatch(resetPage());
  }, []);
  // rowSelection object indicates the need for row selection
  const rowSelection = {
    onChange: (selectedRows, info) => {
      setKeySelected(info);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };
  return (
    <>
      <div>
        {/* ======================= Actions ======================= */}
        <Actions
          showModal={showModal}
          setReload={setReload}
          keyNumActive={keyNumActive}
          keyTab={keyTab}
          keySelected={keySelected}
          setKeySelected={setKeySelected}
          permission={permission}
        />
        {/* ======================= Table ======================= */}
        <TableListCompany
          showModal={showModal}
          reload={reload}
          setReload={setReload}
          rowSelection={rowSelection}
          permission={permission}
        />
      </div>
      <div className="modals">
        <CompanyDetails
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          warehouse={object}
          setWarehouse={setObject}
          setReload={setReload}
        />
      </div>
    </>
  );
}
