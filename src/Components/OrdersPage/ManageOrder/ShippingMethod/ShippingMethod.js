import React, { useEffect, useState } from "react";
import Actions from "./Components/Actions";
import TableListShippingMethod from "./Components/TableListShippingMethod";
import Type1 from "../../../ManageUnits/Type1/Type1";
import { useDispatch } from "react-redux";
import { resetKeywords } from "../../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../../redux/slice/pageSlice";

export default function ShippingMethod({ keyNumActive, keyTab, permission }) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [object, setObject] = useState();
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const dispatch = useDispatch();
  const [keySelected, setKeySelected] = useState([]); //

  // Hiển thị Modal
  let showModal = (id) => {
    setObject(id);
    setIsModalOpen(true);
  };

  useEffect(() => {
    if (keyNumActive == keyTab) {
      dispatch(resetKeywords());
      dispatch(resetPage());
      setReload(true);
    }
  }, [keyNumActive]);

  // 5 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    dispatch(resetKeywords());
    dispatch(resetPage());
  }, []);

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    onChange: (selectedRows, info) => {
      setKeySelected(info);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  return (
    <div className="productShippingMethod">
      {/* ======================= Hành động ======================= */}
      <Actions
        showModal={showModal}
        setReload={setReload}
        keyNumActive={keyNumActive}
        keyTab={keyTab}
        keySelected={keySelected}
        setKeySelected={setKeySelected}
        permission={permission}
      />
      {/* ======================= Danh sách đơn vị tính ======================= */}
      <TableListShippingMethod
        showModal={showModal}
        reload={reload}
        setReload={setReload}
        rowSelection={rowSelection}
        permission={permission}
      />
      {/* ======================= Modals ======================= */}
      <div className="modals">
        <Type1
          title={
            object?.value
              ? "Sửa phương thức giao hàng"
              : "Thêm phương thức giao hàng"
          }
          object={object}
          API="shippingMethod"
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          setObject={setObject}
          setReload={setReload}
        />
      </div>
    </div>
  );
}
