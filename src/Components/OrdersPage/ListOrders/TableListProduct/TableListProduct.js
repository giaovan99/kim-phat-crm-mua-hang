import { Table } from "antd";
import React from "react";
const tableProps = {
  bordered: true,
  size: "small",
};
const paginationObj = {
  position: ["none", "bottomCenter"],
  hideOnSinglePage: true,
  pageSize: 20,
};
export default function TableListProduct({ cloneProduct, setCloneProduct }) {
  const columns = [
    {
      title: "Mã sản phẩm",
      dataIndex: "sku",
      key: "sku",
      width: 120,
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
      width: 200,
      fixed: "left",
    },
    {
      title: "Quy cách sản phẩm",
      dataIndex: "package_case",
      key: "package_case",
    },
    {
      title: "Quy cách đóng gói",
      dataIndex: "packaging",
      key: "packaging",
    },

    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Số lượng",
      dataIndex: "qty",
      key: "qty",
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Thành tiền",
      dataIndex: "sub_total",
      key: "sub_total",
      render: (text, record) => {
        return (
          <p className="text-right">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },
    {
      title: "Thuế GTGT",
      dataIndex: "vat_name",
      key: "vat_name",
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
  ];
  return (
    <Table
      {...tableProps}
      pagination={{
        ...paginationObj,
      }}
      scroll={{
        y: 600,
      }}
      columns={columns}
      dataSource={cloneProduct}
    />
  );
}
