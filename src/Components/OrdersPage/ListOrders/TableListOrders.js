import React, { useEffect, useState } from "react";
import { Tag, Table, Tooltip } from "antd";
import { useNavigate } from "react-router-dom";
import { showError, stringToDate } from "../../../utils/others";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteOrderId,
  getOrder,
  getOrderList,
  orderExport,
  updateOrder,
} from "../../../utils/order";
import { setPage } from "../../../redux/slice/pageSlice";
import { otherKeys } from "../../../utils/user";
import DeleteBtnItemComponent from "../../ButtonComponent/DeleteBtnItemComponent";
import FixBtnComponent from "../../ButtonComponent/FixBtnComponent";
import RenderWithViewComponent from "../../ButtonComponent/RenderIdView";
import ViewBtnComponent from "../../ButtonComponent/ViewBtnComponent";
import UndoBtnComponent from "../../ButtonComponent/UndoBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import PrintOrder from "../../ExportToPDF/PrintOrderFDF/PrintOrderFDF";
import ReactToPrint from "react-to-print";
import { FaFilePdf } from "react-icons/fa6";
import TableLength from "../../ButtonComponent/TableLength";
import ExportDetailsBtnComponent from "../../ButtonComponent/ExportDetailsBtnComponent";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListOrders({
  showViewDetailModal,
  reload,
  setReload,
  deletedPage,
  rowSelection,
  permission,
  setOptionSummary,
  searchFilter,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();
  let navigate = useNavigate();
  let page = useSelector((state) => state.pageSlice.value);
  let current = useSelector((state) => state.statusSlice.current);

  // 2 - Chuyển hướng sang trang chi tiết
  const goToFixOrder = (key) => {
    navigate(`/purchase_order/fix/${key}`);
  };

  // 3 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 4 - Kéo data list order theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));

    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }

    keyParam = {
      ...keyParam,
      ...searchFilter,
      status: current === 0 || current === 4 ? "" : current,
    };

    let res = await getOrderList(page, keyParam);
    if (res.status) {
      setListData(res.data.data);
      setTotal(res.data.total);
      setPageSize(res.data.per_page);
      if (res.option.summary) {
        setOptionSummary(res.option.summary);
      } else {
        setOptionSummary("");
      }
    }
    dispatch(setSpinner(false));
  };

  // 5 - Xác nhận xoá đơn hàng
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deleteData(id, value);
  };

  // 6 - Xoá đơn hàng
  const deleteData = async (id, input) => {
    dispatch(setSpinner(true));

    let res;
    if (input == 1) {
      res = await deleteOrderId({ id: id, deleted: true });
    } else {
      res = await deleteOrderId({ id: id });
    }
    if (res) {
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 7 - Hoàn tác xoá
  const undoDelete = async (id) => {
    dispatch(setSpinner(true));

    let res;
    res = await updateOrder({ id: id, deleted: false });
    // console.log(res);
    if (res) {
      // console.log("reload trang");
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 8 - Kích hoạt đơn hàng
  const confirmUndo = (id) => {
    undoDelete(id);
  };

  // 9 - Xuất file excel
  const exportFile = async (id) => {
    const res = await orderExport({ id, type: "items" });
    if (res) {
      // Tạo thẻ <a> để tải file
      const link = document.createElement("a");
      link.href = res;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  };

  // 10 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  Table.SELECTION_COLUMN.fixed = "left";
  const columns = [
    {
      title: "Mã đơn hàng",
      dataIndex: "code",
      key: "code",
      width: 110,
      fixed: "left",
      render: (text, record) => (
        <RenderWithViewComponent
          onClickF={() => {
            showViewDetailModal(record.id);
          }}
          text={text}
        />
      ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "created_at",
      key: "created_at",
      width: 100,
      render: (text, record) => {
        return <p className="text-center">{text ? stringToDate(text) : ""}</p>;
      },
    },
    ...(permission?.purchase_order?.full
      ? [
          {
            title: "Người tạo",
            dataIndex: "created_by_name",
            key: "created_by_name",
            render: (text, record) => {
              return <p className="text-center">{text ? text : ""}</p>;
            },
          },
        ]
      : []),
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      width: 150,
      render: (text, record) => {
        return (
          <p className="text-center">
            {record.status == 1 || record.status == 2 ? (
              <Tag color="green">{text}</Tag>
            ) : record.status == 3 || record.status == 4 ? (
              <Tag color="gold">{text}</Tag>
            ) : record.status == 5 || record.status == 6 ? (
              <Tag color="blue">{text}</Tag>
            ) : (
              <Tag color="red">{text}</Tag>
            )}
          </p>
        );
      },
    },

    {
      title: "NCC",
      dataIndex: "suppliers_short_name",
      key: "suppliers_short_name",
      width: 150,
    },
    {
      title: "Người đặt",
      dataIndex: "buyer_name",
      key: "buyer_name",
      width: 150,
    },

    {
      title: "Kho",
      children: [
        {
          title: "Kho nhận",
          dataIndex: "warehouse_name",
          key: "warehouse_name",
          width: 130,
        },
        {
          title: "Thủ kho",
          dataIndex: "storekeepers_name",
          key: "storekeepers_name",
          width: 150,
        },
      ],
    },
    {
      title: (
        <>
          Phương thức <br /> giao hàng
        </>
      ),
      dataIndex: "shippingMethod_name",
      key: "shippingMethod_name",
      width: 130,
    },
    {
      title: (
        <>
          Ngày giao
          <br /> dự kiến
        </>
      ),
      dataIndex: "delivery_date",
      key: "delivery_date",
      width: 100,
      render: (text, record) => {
        return <p className="text-center">{text ? stringToDate(text) : ""}</p>;
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 150,
      fixed: "right",
      render: (record) => {
        // console.log(record);
        return (
          <div className="flex gap-2">
            {/* ======================= Xem sản phẩm & chỉnh trạng thái  ======================= */}
            <ViewBtnComponent
              onClickF={() => {
                showViewDetailModal(record.id);
              }}
              text={"Xem sản phẩm & chỉnh trạng thái"}
            />
            {/* ======================= Undo ======================= */}
            {permission?.purchase_order?.update === 1 && (
              <UndoBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  confirmUndo(record.id);
                }}
                text={"đơn hàng"}
              />
            )}
            {/* ======================= Chỉnh sửa ======================= */}
            {permission?.purchase_order?.update === 1 && (
              <FixBtnComponent
                deleted={deletedPage}
                onClickF={() => goToFixOrder(record.id)}
              />
            )}
            {/* ======================= In file PDF ======================= */}
            {!deletedPage && permission?.purchase_order?.read === 1 && (
              <Tooltip placement="bottom" title="Xuất file PDF phiếu đã chọn">
                <div>
                  <ReactToPrint
                    content={() => componentRef.current}
                    documentTitle={
                      `ĐH_` + record.code + "_" + record.suppliers_short_name
                    }
                    onAfterPrint={handleAfterPrint}
                    onBeforeGetContent={() => {
                      return new Promise(async (resolve) => {
                        await handleOnBeforeGetContent(record.id);
                        onBeforeGetContentResolve.current = resolve;
                        setLoading(true);
                      });
                    }}
                    onBeforePrint={handleBeforePrint}
                    removeAfterPrint
                    trigger={reactToPrintTrigger}
                  />
                  <div className="hidden">
                    <PrintOrder
                      forwardedRef={componentRef}
                      details={PPDetails}
                    />
                  </div>
                </div>
              </Tooltip>
            )}
            {/* ======================= Xuất file Excel =======================  */}
            {permission?.purchase_order?.read === 1 && (
              <ExportDetailsBtnComponent
                onClickF={() => exportFile(record.id)}
                text={"Xuất file excel chi tiết"}
              />
            )}
            {/* ======================= Xoá ======================= */}
            {permission?.purchase_order?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => {
                  if (deletedPage) {
                    confirm(record.id, 1);
                  } else {
                    confirm(record.id, 0);
                  }
                }}
                text={"đơn hàng"}
              />
            )}
          </div>
        );
      },
    },
  ];

  {
    /* ======================= Start IN  ======================= */
  }
  // 2 - lấy chi tiết phiếu
  const getPurchaseRequestInfo = async (id) => {
    dispatch(setSpinner(true));
    let res = await getOrder(id);
    if (res.data.success) {
      setDetails(res.data.data);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  const componentRef = React.useRef(null);

  const onBeforeGetContentResolve = React.useRef(null);

  const [loading, setLoading] = React.useState(false);
  const [PPDetails, setDetails] = useState();

  const handleAfterPrint = () => {
    // console.log("`onAfterPrint` called");
    onBeforeGetContentResolve.current = null;
    setLoading(false);
  };

  const handleBeforePrint = React.useCallback(() => {
    // console.log("`onBeforePrint` called");
  }, []);

  const handleOnBeforeGetContent = async (id) => {
    // console.log("`onBeforeGetContent` called");
    await getPurchaseRequestInfo(id);
    // console.log("dont");
    // return componentRef.current;
  };

  React.useEffect(() => {
    if (loading && onBeforeGetContentResolve.current) {
      // Resolves the Promise, letting `react-to-print` know that the DOM updates are completed
      onBeforeGetContentResolve.current();
    }
  }, [loading]);

  const reactToPrintTrigger = React.useCallback(() => {
    // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
    // to the root node of the returned component as it will be overwritten.

    // Bad: the `onClick` here will be overwritten by `react-to-print`
    // return <button onClick={() => alert('This will not work')}>Print this out!</button>;

    // Good
    return (
      <button id="btnPdf">
        <FaFilePdf className="text-[#B6190D]" />
      </button>
    );
  }, []);

  {
    /* ======================= End IN  ======================= */
  }
  return (
    <div className="table-render-API">
      <TableLength page={page} pageSize={pageSize} total={total} />
      <Table
        {...tableProps}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 600,
          x: 1500,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
