import { Button, Popconfirm, Steps, message, Modal } from "antd";
import React, { useEffect, useState } from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { LuUndo } from "react-icons/lu";
import { MdDone } from "react-icons/md";
import { updateOrder } from "../../../../utils/order";
import { useLocation } from "react-router-dom";
import { useSelector } from "react-redux";

export default function SetStatusOrder({
  statusOptions,
  current,
  setCurrent,
  now = null,
  deletedPage,
  setReload = null,
  permissionUpdate = null,
}) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [choose, setChoose] = useState("");
  const [fix, setFix] = useState(false);
  const showModal = () => {
    setIsModalOpen(true);
  };
  const handleOk = () => {
    if (choose < current) {
      prev(choose + 1);
    } else {
      next(choose + 1);
    }
    setIsModalOpen();
  };
  const handleCancel = () => {
    setIsModalOpen(false);
  };
  const confirmNext = (e) => {
    // console.log(e);
    next();
  };
  const confirmPrev = (e) => {
    // console.log(e);
    prev();
  };
  const cancel = (e) => {
    // console.log(e);
    message.error("Huỷ");
  };
  const next = async (step = null) => {
    let res = true;
    document.getElementById("warning").innerText = "";
    if (!step) {
      step = current + 1;
    }
    // Nếu như now = true (cập nhật API ngay lập tức thì chạy API update status)
    if (now) {
      res = await updateOrder({ id: now, status: step });
    }
    if (res) {
      setCurrent(step);
      if (setReload) {
        setReload(true);
      }
    }
  };
  const prev = async (step = null) => {
    let res = true;
    if (current - 1 < 1 || current === 3 || (step && step < 3)) {
      document.getElementById("warning").innerText =
        "Không thể hoàn tác xuống trạng thái thấp hơn ";
      return;
    } else {
      if (!step) {
        step = current - 1;
      }
      if (now) {
        res = await updateOrder({ id: now, status: step });
      }
      if (res) {
        document.getElementById("warning").innerText = "";
        setCurrent(step);
        if (setReload) {
          setReload(true);
        }
      }
    }
  };
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  const location = useLocation();
  // Lấy phần path từ URL
  const pathName = location.pathname;
  useEffect(() => {
    if (permission) {
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      // console.log(pathSegments[1]);
      // Check xem có quyền chỉnh sửa hay ko mới hiển thị nút update trạng thái
      if (permission[pathSegments[1]].update) {
        setFix(true);
      }
    }
  }, [permission, pathName]);
  useEffect(() => {
    let stepsItem = document.querySelectorAll(".popUp .ant-steps-item");
    stepsItem.forEach((element, index) => {
      element.addEventListener("click", () => {
        setChoose(index);
        showModal();
      });
    });
  });
  return (
    <>
      <div className="flex justify-center popUp">
        {/* ======================= Btns ======================= */}
        <div className="flex gap-3">
          {/* ======================= Hoàn tác ======================= */}
          {current > 0 && fix && !deletedPage && permissionUpdate && (
            <Popconfirm
              placement="topRight"
              title="Hoàn tác trạng thái"
              description="Xác nhận hoàn tác trạng thái này?"
              onConfirm={confirmPrev}
              onCancel={cancel}
              okText="Xác nhận"
              cancelText="Huỷ"
              okButtonProps={{ className: "btn-main-yl" }}
              className="btn-undo shrink-0"
              icon={
                <ExclamationCircleOutlined className="warning-icon-popconfirm" />
              }
            >
              <Button>
                <LuUndo className="text-white" />
              </Button>
            </Popconfirm>
          )}
          <div className="shrink">
            {/* ======================= Steps ======================= */}
            {statusOptions && (
              <Steps
                current={current}
                items={statusOptions}
                labelPlacement="vertical"
                size="small"
                className={permissionUpdate ? "" : "pointer-events-none"}
              />
            )}
          </div>
          {/* ======================= Hoàn thành ======================= */}
          {current < statusOptions?.length &&
            fix &&
            !deletedPage &&
            permissionUpdate && (
              <Popconfirm
                placement="topRight"
                title="Hoàn thành trạng thái"
                description="Xác nhận hoàn thành trạng thái này?"
                onConfirm={confirmNext}
                onCancel={cancel}
                okText="Xác nhận"
                cancelText="Huỷ"
                okButtonProps={{ className: "btn-main-yl" }}
                className="btn-success shrink-0"
                icon={
                  <ExclamationCircleOutlined className="warning-icon-popconfirm" />
                }
              >
                <Button>
                  <MdDone className="text-white" />
                </Button>
              </Popconfirm>
            )}
        </div>
      </div>
      <p id="warning" className="text-red-600"></p>
      <Modal
        title="Xác nhận "
        open={isModalOpen}
        onOk={handleOk}
        onCancel={handleCancel}
        okButtonProps={{ className: "btn-main-yl-tst" }}
        okText="Xác nhận"
        cancelText="Huỷ"
      >
        <p>Cập nhật trạng thái đơn hàng?</p>
      </Modal>
    </>
  );
}
