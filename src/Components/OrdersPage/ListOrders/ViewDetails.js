import React, { useState } from "react";
import SetStatusOrder from "./SetStatusOrder/SetStatusOrder";
import TableListProduct from "./TableListProduct/TableListProduct";
import { Modal } from "antd";
import { useEffect } from "react";
import { getOrder } from "../../../utils/order";
import { showError } from "../../../utils/others";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

export default function ViewDetails({
  isModalOpen,
  setIsModalOpen,
  order,
  statusOptions,
  setOrder,
  deletedPage,
  setReload,
  permission,
}) {
  // 1 - Khai báo state
  const [orderDetail, setOrderDetail] = useState();
  const [cloneProduct, setCloneProduct] = useState();
  const [current, setCurrent] = useState(0);
  let dispatch = useDispatch();

  // 2 - Lấy thông tin đơn hàng
  const getOrderInfo = async () => {
    dispatch(setSpinner(true));
    let res = await getOrder(order);
    if (res.data.success) {
      let data = res.data.data;
      setOrderDetail(res.data.data);
      setCurrent(Number(data.status));
      setCloneProduct(data.items);
    } else {
      // console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  //  3 - Lần đầu khi load
  useEffect(() => {
    if (order) {
      // lấy thông tin người dùng trong danh sách người dùng & show ra màn hình
      getOrderInfo();
    }
  }, [order]);

  // 4 - Reset form
  const resetForm = () => {
    setOrder(null);
  };

  //  6 - Khi huỷ lưu form
  const handleCancel = () => {
    resetForm();
    setIsModalOpen(false);
  };
  console.log(orderDetail);

  return (
    <Modal
      title={`Xem sản phẩm và chỉnh trạng thái đơn hàng - ${
        orderDetail?.code || ""
      }`}
      centered
      open={isModalOpen}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Đóng"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center hidden",
      }}
      classNames={{ wrapper: "main-modal modal-view-detail" }}
    >
      <div className="tableListProducts">
        <div className={deletedPage ? "pointer-events-none mb-5" : "mb-5"}>
          {/* ======================= Title ======================= */}
          <h2 className="text-[1rem] font-medium mb-1 text-bl">
            Trạng thái đơn hàng
          </h2>
          {/* ======================= Step ======================= */}
          <SetStatusOrder
            statusOptions={statusOptions}
            current={current}
            setCurrent={setCurrent}
            now={order}
            deletedPage={deletedPage}
            setReload={setReload}
            permissionUpdate={permission?.purchase_order?.update}
          />
        </div>
        {cloneProduct && (
          <div>
            {/* ======================= Thông tin khác ======================= */}
            <div className="my-5 ">
              <h2 className="text-[1rem] font-medium mb-1 text-bl ">
                Thông tin khác
              </h2>
              <div className="grid grid-cols-5 gap-3 px-3">
                {/* Mã đơn hàng */}
                <div className=" ">
                  <p className="font-medium">Mã đơn hàng</p>
                  <p>{orderDetail?.code}</p>
                </div>
                {/* Mã đơn đề nghị mua hàng */}
                <div className=" ">
                  <p className="font-medium">Mã đơn đề nghị mua hàng</p>
                  <p>{orderDetail?.pp_code}</p>
                </div>
                {/* NCC */}
                <div className=" ">
                  <p className="font-medium">NCC</p>
                  <p>{orderDetail?.suppliers_name}</p>
                </div>
                {/* Địa chỉ NCC */}
                <div>
                  <p className="font-medium">Địa chỉ NCC</p>
                  <p>{orderDetail?.supplier_address}</p>
                </div>{" "}
                {/* Mã số NCC */}
                <div className=" ">
                  <p className="font-medium">Mã số NCC</p>
                  <p>{orderDetail?.supplier_code}</p>
                </div>
                {/* Người liên hệ */}
                <div className=" ">
                  <p className="font-medium">Người liên hệ</p>
                  <p>{orderDetail?.supplier_contact_name}</p>
                </div>
                {/* Sđt người liên hệ */}
                <div className=" ">
                  <p className="font-medium">Sđt người liên hệ</p>
                  <p>{orderDetail?.supplier_contact_phone}</p>
                </div>
                {/* Người đặt */}
                <div>
                  <p className="font-medium">Người đặt</p>
                  <p>{orderDetail?.buyer_name}</p>
                </div>
                {/* Sđt người đặt */}
                <div>
                  <p className="font-medium">Sđt người đặt</p>
                  <p>{orderDetail?.buyer_phone}</p>
                </div>
                {/* Email người đặt */}
                <div>
                  <p className="font-medium">Email người đặt</p>
                  <p>{orderDetail?.buyer_email}</p>
                </div>
                {/* Thủ kho */}
                <div>
                  <p className="font-medium">Thủ kho</p>
                  <p>{orderDetail?.storekeepers_name}</p>
                </div>
                {/* Sđt thủ kho */}
                <div>
                  <p className="font-medium">Sđt thủ kho</p>
                  <p>{orderDetail?.storekeepers_phone}</p>
                </div>
                {/* Yêu cầu về hoá đơn */}
                <div>
                  <p className="font-medium">Yêu cầu về hoá đơn</p>
                  <p>{orderDetail?.invoice_request}</p>
                </div>
                {/* Phương thức thanh toán */}
                <div>
                  <p className="font-medium">Phương thức thanh toán</p>
                  <p>{orderDetail?.paymentMethod}</p>
                </div>
              </div>
            </div>
            {/* ======================= Title ======================= */}
            <h2 className="text-[1rem] font-medium mb-1 text-bl">
              Danh sách sản phẩm
            </h2>
            {/* ======================= Table List Products ======================= */}
            <TableListProduct
              cloneProduct={cloneProduct}
              setCloneProduct={setCloneProduct}
            />
            {/* ======================= Summary ======================= */}
            <div>
              <div className="flex flex-col gap-3">
                <h2 className="text-[1rem] font-medium mb-1 text-bl mt-5 w-[340px]">
                  Summary
                </h2>
                {/* Tổng cộng*/}
                <div className="flex ">
                  <p className="font-medium w-[170px]">Tổng cộng</p>
                  <p className=" w-[170px] text-right font-medium">
                    {Number(orderDetail?.sub_total).toLocaleString("en-US")}
                  </p>
                </div>{" "}
                {/* Thuế GTGT theo từng mức VAT */}
                {typeof orderDetail?.tax_summary === "object" &&
                  orderDetail?.tax_summary?.map(({ vat, tax }, index) => {
                    if (vat !== "0%") {
                      return (
                        <div className="flex" key={vat}>
                          <p className=" w-[170px]">Thuế GTGT ({vat})</p>
                          <p className=" w-[170px] text-right ">
                            {Number(tax).toLocaleString("en-US")}
                          </p>
                        </div>
                      );
                    } else {
                      return;
                    }
                  })}
                {/* Tổng giá trị đơn hàng*/}
                <div className="flex ">
                  <p className="font-medium w-[170px]">Tổng giá trị đơn hàng</p>
                  <p className=" w-[170px] text-right font-medium">
                    {Number(orderDetail?.total).toLocaleString("en-US")}
                  </p>
                </div>{" "}
                {/* Đặt cọc*/}
                <div className="flex ">
                  <p className=" w-[170px]">Đặt cọc</p>
                  <p className="w-[170px] text-right">
                    {Number(orderDetail?.deposit).toLocaleString("en-US")}
                  </p>
                </div>{" "}
                {/* Số tiền cần thanh toán:*/}
                <div className="flex ">
                  <p className="font-medium w-[170px]">
                    Số tiền cần thanh toán
                  </p>
                  <p className=" w-[170px] text-right font-medium">
                    {Number(orderDetail?.total_paid).toLocaleString("en-US")}
                  </p>
                </div>{" "}
              </div>
            </div>
          </div>
        )}
      </div>
    </Modal>
  );
}
