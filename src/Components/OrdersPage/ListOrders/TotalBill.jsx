import { Col, Row, Statistic } from "antd";

const TotalBillOrder = ({ optionSummary }) => {
  const formatter = (value) => (
    <p className="text-[1.2vw] text-right p-2 text-black">
      {value ? Number(value).toLocaleString("en-US") : 0}
    </p>
  );

  return (
    <>
      {optionSummary && (
        <Row gutter={[16, 16]}>
          {Object.keys(optionSummary).map((key) => (
            <Col span={6} key={key} className="gutter-row">
              <Statistic
                title={
                  <span className="text-[1vw] line-clamp-2 text-left text-[#172C6A] p-2 bg-yellow-400">
                    {optionSummary[key].name}
                  </span>
                }
                value={Number(optionSummary[key].total)}
                formatter={formatter}
                className=" border  h-full rounded-lg text-right flex flex-col justify-between overflow-hidden	"
              />
            </Col>
          ))}
        </Row>
      )}
    </>
  );
};

export default TotalBillOrder;
