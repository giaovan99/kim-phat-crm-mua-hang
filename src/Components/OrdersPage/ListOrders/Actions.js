import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
import { useNavigate } from "react-router-dom";
import DropdownOptions from "../../DropdownOptions/DropdownOptions";
import AddBtnComponent from "../../ButtonComponent/AddBtnComponent";
import { DatePicker, Select } from "antd";
import { normalize } from "../../../utils/others";
const { RangePicker } = DatePicker;

export default function Actions({
  setReload,
  deletedPage,
  keySelected,
  listStatus,
  setKeySelected,
  itemSelected,
  setItemSelected,
  permission,
  setSearchFilter,
  listStaff,
}) {
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      keywords: "",
      startDate: "",
      endDate: "",
      buyer_id: "",
    },
    onSubmit: async (values) => {
      setSearchFilter(values);
      setReload(true);
    },
  });

  let navigate = useNavigate();
  const goToAddOrder = () => {
    navigate("/purchase_order/add");
  };
  const onSearch = (value) => {
    // console.log("search:", value);
  };
  const filterOption = (input, option) =>
    normalize(option?.label ?? "").includes(normalize(input));
  const onChangeSelect = (value, field) => {
    formik.setFieldValue(field, value);
  };

  return (
    <div className="actions flex-wrap gap-2">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div
            className="flex flex-wrap-reverse gap-2"
            style={{ alignItems: "end" }}
          >
            {permission?.purchase_order?.update === 1 && (
              <DropdownOptions
                arrId={keySelected}
                deletedPage={deletedPage}
                page={"order"}
                setReload={setReload}
                setKeySelected={setKeySelected}
                listStatus={listStatus}
                arrItem={itemSelected}
                setItemSelected={setItemSelected}
              />
            )}
            {/* ======================= Khung tìm kiếm ======================= */}
            <div className="form-group">
              <div className="form-style items-end flex-wrap">
                {/* Mã đơn hàng, nhà cung cấp,... */}
                <div>
                  <label>Tìm mã đơn hàng, nhà cung cấp...</label>
                  <input
                    id="keywords"
                    name="keywords"
                    type="text"
                    placeholder="Tìm mã đơn hàng, nhà cung cấp"
                    value={formik.values.keywords}
                    onChange={formik.handleChange}
                    className="max-w-[250px]"
                  />
                </div>
                {/* Người đặt */}
                <div>
                  <label>Người đặt</label>
                  <Select
                    mode="multiple"
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "buyer_id")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listStaff}
                    value={formik?.values?.buyer_id || []}
                    className="w-[250px]"
                    maxTagCount={1}
                  />
                </div>
                {/* Filter thời gian */}
                <div>
                  <label>Ngày giao dự kiến</label>
                  <RangePicker
                    size="large"
                    variant="borderless"
                    format="DD/MM/YYYY"
                    onChange={(value, dateString) => {
                      formik.setFieldValue("startDate", dateString[0]);
                      formik.setFieldValue("endDate", dateString[1]);
                    }}
                  />
                </div>
                <div className="flex gap-2">
                  <button className="btn-main-yl btn-actions" type="submit">
                    <HiOutlineSearch />
                    Tìm
                  </button>
                  <button
                    className="ant-btn btn-actions text-white bg-black ant-btn-default rounded-lg"
                    type="button"
                    onClick={formik.handleReset}
                  >
                    Reset
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      {!deletedPage && permission?.purchase_order?.create ? (
        <div className="flex gap-3 grow min-w-[220px] justify-end">
          <AddBtnComponent
            onClickF={() => goToAddOrder()}
            text={"Tạo đơn hàng"}
          />
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}
