import React from "react";

export default function FormUpdate({ formik }) {
  let { errors } = formik;
  // console.log(formik.touched);
  // console.log(errors);
  return (
    <div className="formUpdate">
      {/* ======================= Ngày giao thực tế ======================= */}
      <div className="form-group">
        <div className="form-style">
          <label htmlFor="deliveryDate">
            Ngày giao thực tế<span className="text-red-500">*</span>
          </label>
          <input
            className="grow"
            type="date"
            name="deliveryDate"
            id="deliveryDate"
            value={formik.values.deliveryDate}
            onChange={(event) => {
              formik.setFieldValue("deliveryDate", event.target.value);
            }}
            onBlur={formik.handleBlur}
          />
        </div>
        {errors.deliveryDate && formik.touched.deliveryDate ? (
          <p className="mt-1 text-red-500 text-sm" id="deliveryDate-warning">
            {errors.deliveryDate}
          </p>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}
