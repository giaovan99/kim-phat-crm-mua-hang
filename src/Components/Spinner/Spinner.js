import { Spin } from "antd";
import React, { useEffect } from "react";
import { useSelector } from "react-redux";

export default function Spinner() {
  let open = useSelector((state) => state.spinnerSlice.value);
  useEffect(() => {
    if (open) {
      document.body.classList.add("overlay-open");
    } else {
      document.body.classList.remove("overlay-open");
    }
  }, [open]);

  return (
    <>
      {open && (
        <div className="spinner w-[100vw] h-[100vh] bg-[#ffffffb2] top-0 left-0 z-[1000]  pointer-events-none fixed">
          <div className="flex justify-center items-center w-full h-full">
            <Spin size="large" fullscreen="true" />
          </div>
        </div>
      )}
    </>
  );
}
