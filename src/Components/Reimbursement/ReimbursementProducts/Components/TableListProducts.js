import React from "react";
import { Tag, Table } from "antd";

let data = [
  {
    key: "HU1",
    createdDate: "2023-10-11",
    keyDH: "abc1234",
    keyTU: "TU1",
    keyProduct: "SP1",
    product: "Bàn",
    unit: "Cái",
    unitPrice: 300000,
    actualQuantity: 2,
    quantity: 1,
    taxCode: "TT",
    value: 0,
    bill: 600000,
    billVAT: 600000,
    supplier: "CH Chiêu",
    status: "makePayment",
    advanceMoney: 300000,
    reimbursementMoney: 300000,
    paymentDeadline: "2023-10-20",
  },
];

// rowSelection object indicates the need for row selection
const rowSelection = {
  onChange: (selectedRowKeys, selectedRows) => {
    // console.log(
    //   `selectedRowKeys: ${selectedRowKeys}`,
    //   "selectedRows: ",
    //   selectedRows
    // );
  },
  getCheckboxProps: (record) => ({
    disabled: record.name === "Disabled User",
    // Column configuration not to be checked
    name: record.name,
  }),
  preserveSelectedRowKeys: true,
};

const tableProps = {
  bordered: true,
  size: "small",
};

export default function TableListProducts({ statusOptions, showModal }) {
  const stringToDate = (dateString) => {
    if (dateString) {
      const [yyyy, mm, dd] = dateString.split("-");
      return <>{dd + "/" + mm + "/" + yyyy}</>;
    }
  };
  let renderStatus = (x) => {
    let index = statusOptions.findIndex((item) => item.value == x);
    return statusOptions[index].label;
  };
  const columns = [
    {
      title: "Tên hàng hoá",
      dataIndex: "product",
      key: "product",
      fixed: "left",
      width: 150,
      render: (text, record) => (
        <p
          className="text-blue-500 cursor-pointer hover:text-blue-400"
          onClick={() => {
            showModal(record.key);
          }}
        >
          {text}
        </p>
      ),
    },
    {
      title: (
        <>
          Mã <br />
          hàng hoá
        </>
      ),
      dataIndex: "keyProduct",
      key: "keyProduct",
      fixed: "left",
      width: 80,
    },

    {
      title: "Trạng thái",
      dataIndex: "status",
      key: "status",
      fixed: "left",
      width: 130,
      render: (text, record) => {
        return <Tag color="processing">{renderStatus(text)}</Tag>;
      },
    },
    {
      title: (
        <>
          Mã phiếu
          <br />
          hoàn ứng
        </>
      ),
      dataIndex: "key",
      key: "key",
      width: 100,
      fixed: "left",
      className: "text-center",
    },
    {
      title: "Ngày tạo",
      dataIndex: "createdDate",
      key: "createdDate",
      width: 100,
      render: (text, record) => {
        return <p className="text-center">{stringToDate(text)}</p>;
      },
    },
    {
      title: (
        <>
          Mã
          <br />
          đơn hàng
        </>
      ),
      dataIndex: "keyDH",
      key: "keyDH",
      width: 100,
      className: "text-center text-bl",
    },
    {
      title: (
        <>
          Mã phiếu
          <br />
          tạm ứng
        </>
      ),
      dataIndex: "keyTU",
      key: "keyTU",
      width: 100,
      className: "text-center text-bl",
    },

    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 80,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Đơn giá",
      dataIndex: "unitPrice",
      key: "unitPrice",
      width: 80,
      render: (text, record) => {
        return <p className="text-center">{text.toLocaleString("en-US")}</p>;
      },
    },
    {
      title: "Số lượng thực tế",
      dataIndex: "actualQuantity",
      key: "actualQuantity",
      width: 100,
      className: "text-center",
    },

    {
      title: "Loại hoá đơn",
      dataIndex: "taxCode",
      key: "taxCode",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: (
        <>
          Thành tiền
          <br />
          sau VAT
        </>
      ),
      dataIndex: "billVAT",
      key: "billVAT",
      width: 120,
      render: (text, record) => {
        return (
          <p className="text-right text-green-700">
            {text.toLocaleString("en-US")}
          </p>
        );
      },
    },
    {
      title: "Tạm ứng",
      dataIndex: "advanceMoney",
      key: "advanceMoney",
      width: 120,
      render: (text, record) => {
        return (
          <p className="text-right font-medium text-bl">
            {text.toLocaleString("en-US")}
          </p>
        );
      },
    },
    {
      title: "Hoàn ứng",
      dataIndex: "reimbursementMoney",
      key: "reimbursementMoney",
      width: 120,
      render: (text, record) => {
        return (
          <p className="text-right font-medium text-red-700">
            {text.toLocaleString("en-US")}
          </p>
        );
      },
    },
    {
      title: "Hạn thanh toán",
      dataIndex: "paymentDeadline",
      key: "paymentDeadline",
      width: 100,
      render: (text, record) => {
        return <p className="text-center">{stringToDate(text)}</p>;
      },
    },
  ];
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        pagination={{
          position: ["none", "bottomCenter"],
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
          x: 1500,
        }}
        columns={columns}
        dataSource={data}
      />
    </div>
  );
}
