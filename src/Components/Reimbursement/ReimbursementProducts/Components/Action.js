import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch } from "react-icons/hi";

export default function Actions({}) {
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: async (values) => {
      formik.resetForm();
    },
  });
  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div className="form-group">
            <div className="form-style">
              <input
                id="search"
                name="search"
                type="text"
                placeholder="Tìm kiếm mã phiếu, mã sản phẩm"
                value={formik.values.search}
                onChange={formik.handleChange}
              />
              <button className="btn-main-yl btn-actions">
                <HiOutlineSearch />
                Tìm kiếm
              </button>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      {/* <div className="flex gap-3">
        <div className="icon-actions space-x-2">
          <Tooltip placement="bottom" title="Xuất file PDF">
            <button>
              <FaFilePdf className="text-[#B6190D]" />
            </button>
          </Tooltip>
        </div>
      </div> */}
    </div>
  );
}
