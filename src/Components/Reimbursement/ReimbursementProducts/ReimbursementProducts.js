import React, { useState } from "react";
import Actions from "./Components/Action";
import TableListProducts from "./Components/TableListProducts";
import ReimbursementSlipDetails from "../ReimbursementSlip/Components/ReimbursementSlipDetails";
const statusOptions = [
  { value: "makePayment", label: "Làm thanh toán" },
  { value: "approve", label: "Sếp duyệt" },
  { value: "banking", label: "Chuyển khoản" },
];

export default function ReimbursementProducts() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [reimbursementChoosen, setReimbursementChoosen] = useState();
  const showModal = (key) => {
    setReimbursementChoosen(key);
    setIsModalOpen(true);
  };

  return (
    <div className="reimbursementProducts">
      {/* ======================= Hành động ======================= */}
      <Actions />
      {/* ======================= Danh sách đơn vị tính ======================= */}
      <TableListProducts statusOptions={statusOptions} showModal={showModal} />
      {/* ======================= Add Reimbursement ======================= */}
      <ReimbursementSlipDetails
        isModalOpen={isModalOpen}
        setIsModalOpen={setIsModalOpen}
        reimbursement={reimbursementChoosen}
      />
    </div>
  );
}
