import React, { useEffect, useState } from "react";
import Actions from "./Components/Action";
import TableListReimbursements from "./Components/TableListReimbursements";
import ReimbursementSlipDetails from "./Components/ReimbursementSlipDetails";
import {
  getPOReimbursement,
  getReimbursementStatusList,
} from "../../../utils/reimbursement";
import { useDispatch, useSelector } from "react-redux";
import { resetKeywords } from "../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../redux/slice/pageSlice";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { getPaymentMethodList } from "../../../utils/order";
import SupplierListModal from "./Components/SupplierListModal";

export default function ReimbursementSlip({
  keyNumActive,
  keyTab,
  deletedPage,
}) {
  const [reimbursementChoosen, setReimbursementChoosen] = useState();
  const [supplierName, setSupplierName] = useState([]);
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isModalSupplierOpen, setIsModalSupplierOpen] = useState(false);
  const dispatch = useDispatch();
  const [keySelected, setKeySelected] = useState([]); //
  const [listStatus, setListStatus] = useState([]); //Danh sách trạng thái
  const [searchFilter, setSearchFilter] = useState({}); //Search filter
  const [listStaff, setListStaff] = useState([]); //hiển thị danh sách nhân viên
  const [listPaymentMethod, setListPaymentMethod] = useState([]); //Danh sách loại phương thức thanh toán
  const [itemSelected, setItemSelected] = useState([]); //Mảng chứa chi tiết các phiếu được chọn để xét status

  const [reload, setReload] = useState(false);

  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );

  // 2.1 - Hiển thị modal chi tiết phiếu
  const showModal = (id) => {
    setReimbursementChoosen(id);
    setIsModalOpen(true);
  };

  // 2.2 - Hiển thị modal NCC chi tiết phiếu
  const showModalSupplier = (id) => {
    setSupplierName(id);
    setIsModalSupplierOpen(true);
  };

  // 3 - Lấy danh sách mảng trạng thái
  const getArrs = async () => {
    dispatch(setSpinner(true));
    // Danh sách trạng thái
    let res = await getReimbursementStatusList();
    // console.log(res);
    if (res.status) {
      setListStatus(res.data);
    }

    // 5.7 - Danh sách nhân viên
    res = await getPOReimbursement();
    if (res.status) {
      setListStaff(res.options.staff);
    }

    // 5.4 - Phương thức thanh toán
    res = await getPaymentMethodList();
    if (res.status) {
      setListPaymentMethod(res.data);
    }
    dispatch(setSpinner(false));
  };

  // 3 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    dispatch(resetKeywords());
    dispatch(resetPage());
    getArrs();
  }, []);

  useEffect(() => {
    if (keyNumActive === keyTab) {
      setReload(true);
      dispatch(resetKeywords());
      dispatch(resetPage());
    }
  }, [keyNumActive]);

  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys, selectedRows) => {
      setKeySelected(selectedRowKeys);
      setItemSelected(selectedRows);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  useEffect(() => {
    setReload(true);
  }, [deletedPage]);

  return (
    <div className="reimbursementSlip">
      {/* ======================= Hành động ======================= */}
      <Actions
        setReload={setReload}
        deletedPage={deletedPage}
        keySelected={keySelected}
        setKeySelected={setKeySelected}
        keyNumActive={keyNumActive}
        keyTab={keyTab}
        permission={permission}
        listStaff={listStaff}
        listStatus={listStatus}
        listPaymentMethod={listPaymentMethod}
        setSearchFilter={setSearchFilter}
        itemSelected={itemSelected}
        setItemSelected={setItemSelected}
      />
      {/* ======================= Danh sách đơn vị tính ======================= */}
      <TableListReimbursements
        showModal={showModal}
        reload={reload}
        setReload={setReload}
        deletedPage={deletedPage}
        rowSelection={rowSelection}
        setKeySelected={setKeySelected}
        permission={permission}
        searchFilter={searchFilter}
        showModalSupplier={showModalSupplier}
      />
      {/* ======================= Modals ======================= */}
      <div className="modals">
        {/* ======================= Add Reimbursement ======================= */}
        <ReimbursementSlipDetails
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          reimbursement={reimbursementChoosen}
          statusOptions={listStatus}
          setReimbursement={setReimbursementChoosen}
          deletedPage={deletedPage}
          permission={permission}
        />

        {/* Modal Supplier */}
        <SupplierListModal
          isModalOpen={isModalSupplierOpen}
          setIsModalOpen={setIsModalSupplierOpen}
          supplierName={supplierName}
          setSupplierName={setSupplierName}
        />
      </div>
    </div>
  );
}
