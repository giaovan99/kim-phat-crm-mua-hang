import { useFormik } from "formik";
import React, { useEffect } from "react";
import { HiOutlineSearch } from "react-icons/hi";
import { useNavigate } from "react-router-dom";
import DropdownOptions from "../../../DropdownOptions/DropdownOptions";
import AddBtnComponent from "../../../ButtonComponent/AddBtnComponent";
import { Select } from "antd";
import { normalize } from "../../../../utils/others";

export default function Actions({
  deletedPage,
  setReload,
  keySelected,
  setKeySelected,
  keyNumActive,
  keyTab,
  permission,
  listStaff,
  listStatus,
  listPaymentMethod,
  setSearchFilter,
  itemSelected,
  setItemSelected,
}) {
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      keywords: "",
      status: "",
      staff_number: "",
      paymentMethod: "",
      supplier_name: "",
      product_name: "",
      product_code: "",
    },
    onSubmit: async (values) => {
      setSearchFilter(values);
      setReload(true);
    },
  });
  let navigate = useNavigate();
  const goToAdd = () => {
    navigate("/payment_request/add");
  };
  useEffect(() => {
    if (keyNumActive == keyTab) {
      formik.resetForm();
    }
  }, [keyNumActive]);

  const onSearch = (value) => {
    // console.log("search:", value);
  };
  const filterOption = (input, option) =>
    normalize(option?.label ?? "").includes(normalize(input));

  const onChangeSelect = (value, field) => {
    formik.setFieldValue(field, value);
  };
  return (
    <div className="actions flex-col gap-5">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div
            className="flex flex-wrap-reverse gap-2"
            style={{ alignItems: "end" }}
          >
            <div className="form-group">
              <div className="form-style items-end flex-wrap">
                {/* Mã phiếu */}
                <div>
                  <label>Tìm</label>
                  <input
                    id="keywords"
                    name="keywords"
                    type="text"
                    placeholder="Tìm mã phiếu"
                    value={formik.values.keywords}
                    onChange={formik.handleChange}
                    className="max-w-[120px]"
                  />
                </div>
                {/* NCC */}
                <div>
                  <label>NCC</label>
                  <input
                    id="supplier_name"
                    name="supplier_name"
                    type="text"
                    placeholder="Tìm nhà cung cấp"
                    value={formik.values.supplier_name}
                    onChange={formik.handleChange}
                    className="max-w-[150px]"
                  />
                </div>
                {/* Trạng thái */}
                <div>
                  <label>Trạng thái</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "status")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listStatus}
                    value={
                      formik.values.status != "" ? formik.values.status : null
                    }
                    className="w-[116px]"
                  />
                </div>
                {/* Người đề nghị */}
                <div>
                  <label>Người đề nghị</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "staff_number")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listStaff}
                    value={
                      formik.values.staff_number != ""
                        ? formik.values.staff_number
                        : null
                    }
                    className="w-[162px]"
                  />
                </div>

                {/* PTTT */}
                <div>
                  <label>PTTT</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "paymentMethod")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listPaymentMethod}
                    value={
                      formik.values.paymentMethod != ""
                        ? formik.values.paymentMethod
                        : null
                    }
                    className="w-[257px]"
                  />
                </div>
                {/* Tên sản phẩm */}
                <div>
                  <label>Tên sản phẩm</label>
                  <input
                    id="product_name"
                    name="product_name"
                    type="text"
                    placeholder="Tên sản phẩm"
                    value={formik.values.product_name}
                    onChange={formik.handleChange}
                    className="max-w-[120px]"
                  />
                </div>
                {/* Mã sản phẩm */}
                <div>
                  <label>Mã sản phẩm</label>
                  <input
                    id="product_code"
                    name="product_code"
                    type="text"
                    placeholder="Mã sản phẩm"
                    value={formik.values.product_code}
                    onChange={formik.handleChange}
                    className="max-w-[120px]"
                  />
                </div>
                <div className="flex gap-2">
                  <button className="btn-main-yl btn-actions">
                    <HiOutlineSearch />
                    Tìm
                  </button>
                  <button
                    className="ant-btn btn-actions text-white bg-black ant-btn-default rounded-lg"
                    type="button"
                    onClick={formik.handleReset}
                  >
                    Reset
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      <div className="flex gap-2 items-end">
        {permission?.payment_request?.update === 1 && (
          <DropdownOptions
            arrId={keySelected}
            deletedPage={deletedPage}
            page={"reimbursement"}
            setReload={setReload}
            setKeySelected={setKeySelected}
            listStatus={listStatus}
            arrItem={itemSelected}
            setItemSelected={setItemSelected}
          />
        )}
        {!deletedPage && permission?.payment_request?.create ? (
          <div className="flex gap-3 items-end">
            <AddBtnComponent
              onClickF={() => goToAdd()}
              text={"Tạo phiếu ĐNTT"}
            />
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}
