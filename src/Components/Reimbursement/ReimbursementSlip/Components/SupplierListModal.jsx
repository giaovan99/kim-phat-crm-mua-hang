import { Modal, Table } from "antd";
import React, { useState } from "react";
import { useDispatch } from "react-redux";

const SupplierListModal = ({
  isModalOpen,
  setSupplierName,
  setIsModalOpen,
  supplierName,
}) => {
  //  6 - Khi huỷ lưu form
  const handleCancel = () => {
    resetForm();
    setIsModalOpen(false);
  };
  // 4 - Reset form
  const resetForm = () => {
    setSupplierName([]);
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "key",
      key: "key",
      render: (text) => <div className="text-center">{text}</div>,
    },
    {
      title: "Tên NCC",
      dataIndex: "name",
      key: "name",
    },
  ];

  const dataSource = (supplierName) => {
    return supplierName.map((supplier, index) => ({
      key: index + 1,
      name: supplier,
    }));
  };

  return (
    <Modal
      title="Danh sách NCC"
      onCancel={handleCancel}
      open={isModalOpen}
      cancelText="Đóng"
      classNames={{ wrapper: "main-modal " }}
      centered
      okButtonProps={{
        className: "hidden",
      }}
    >
      <p className="mb-4 text-right">
        Có <strong>{supplierName.length}</strong> nhà cung cấp
      </p>
      <Table
        dataSource={dataSource(supplierName)}
        columns={columns}
        pagination={false}
        size="small"
      />
    </Modal>
  );
};

export default SupplierListModal;
