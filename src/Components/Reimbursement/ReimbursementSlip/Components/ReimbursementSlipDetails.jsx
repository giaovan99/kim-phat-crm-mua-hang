import React, { useEffect, useState } from "react";
import { Modal } from "antd";
import TableListProduct from "./TableListProduct/TableListProduct";
import { getReimbursemenId } from "../../../../utils/reimbursement";
import SetStatus2Step from "../../../SetStatus2Step/SetStatusProposal";
import { showError, stringToDate } from "../../../../utils/others";
import { setSpinner } from "../../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

export default function ReimbursementSlipDetails({
  isModalOpen,
  setIsModalOpen,
  reimbursement,
  statusOptions,
  setReimbursement,
  deletedPage,
  permission,
}) {
  // 1 - Khai báo state
  let [cloneProduct, setCloneProduct] = useState();
  const [current, setCurrent] = useState(0);
  const [importSlip, setImportSlip] = useState();
  // const [listFiles, setListFiles] = useState([]);
  let dispatch = useDispatch();

  // Lấy thông tin chi tiết phiếu hoàn ứng
  const APIGetReimbursement = async () => {
    dispatch(setSpinner(true));
    let res = await getReimbursemenId(reimbursement);
    if (res.data.success) {
      let data = res.data.data;
      setImportSlip(data);
      setCurrent(Number(data.status));
      setCloneProduct(data.items);
      // setListFiles(data?.documents ? data?.documents : initialFile);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  //  3 - Lần đầu khi load
  useEffect(() => {
    if (reimbursement) {
      // lấy thông tin người dùng trong danh sách người dùng & show ra màn hình
      APIGetReimbursement();
    }
  }, [reimbursement]);

  // 4 - Reset form
  const resetForm = () => {
    setReimbursement(null);
  };

  //  6 - Khi huỷ lưu form
  const handleCancel = () => {
    resetForm();
    setIsModalOpen(false);
  };

  // const initialFile = [
  //   {
  //     key: "paymentRequestFile",
  //     title: "Phiếu đề nghị thanh toán",
  //     file: "",
  //     fileName: "",
  //     require: true,
  //   },
  //   {
  //     key: "deliverySlip",
  //     title: "Phiếu giao hàng",
  //     file: "",
  //     fileName: "",
  //     require: false,
  //   },
  //   {
  //     key: "warehouseSlip",
  //     title: "Phiếu nhập kho",
  //     file: "",
  //     fileName: "",
  //     require: false,
  //   },
  //   {
  //     key: "purchaseProposal",
  //     title: "Đề nghị mua hàng",
  //     file: "",
  //     fileName: "",
  //     require: false,
  //   },
  //   {
  //     key: "receipt",
  //     title: "Phiếu thu đi theo hóa đơn VAT",
  //     file: "",
  //     fileName: "",
  //     require: false,
  //   },
  //   {
  //     key: "VATBill",
  //     title: "Hóa đơn VAT",
  //     file: "",
  //     fileName: "",
  //     require: false,
  //   },
  //   {
  //     key: "contractPrincipal",
  //     title: "HĐ nguyên tắc/ Thoả thuận mua bán",
  //     file: "",
  //     fileName: "",
  //     require: false,
  //   },
  //   {
  //     key: "priceReview",
  //     title: "Đề nghị duyệt giá/ Bảng so sánh giá",
  //     file: "",
  //     fileName: "",
  //     require: false,
  //   },
  // ];

  // Render List File
  // let renderListFile = () => {
  //   return (
  //     <div className="grid grid-cols-4 gap-2 ">
  //       {listFiles.map((item, index) => {
  //         return (
  //           <div className="form-group border-r pb-1 mb-1">
  //             <div className="form-style">
  //               <label>
  //                 {index + 1}.{item.title}{" "}
  //               </label>
  //               <div>
  //                 <div className="flex gap-2">
  //                   <p
  //                     id={item.key + "Name"}
  //                     className=" line-clamp-1 fileName"
  //                   >
  //                     {item.fileName}
  //                   </p>
  //                   {/* btns */}
  //                   <div
  //                     className={
  //                       item.fileName != "" ? "grow btns-file" : "hidden"
  //                     }
  //                   >
  //                     {/* xem */}
  //                     {item.url_preview && (
  //                       <a
  //                         href={`https://docs.google.com/gview?url=${item.url_preview}&embedded=true`}
  //                         target="_blank"
  //                         // type="button"
  //                         className="pointer-events-auto"
  //                       >
  //                         <FiEye />
  //                       </a>
  //                     )}
  //                     {item.url && (
  //                       <a
  //                         href={item.url}
  //                         type="button"
  //                         className="pointer-events-auto"
  //                         target="_blank"
  //                       >
  //                         <FiDownload />
  //                       </a>
  //                     )}
  //                   </div>
  //                 </div>
  //               </div>
  //             </div>
  //           </div>
  //         );
  //       })}
  //     </div>
  //   );
  // };

  return (
    <Modal
      title={"Xem chi tiết và chỉnh trạng thái đề nghị mua hàng"}
      centered
      open={isModalOpen}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Đóng"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center hidden",
      }}
      classNames={{ wrapper: "main-modal modal-view-detail" }}
    >
      <form id="formAddUser">
        <div className="mb-5">
          {/* ======================= Title ======================= */}
          <h2 className="text-[1rem] font-medium mb-1 text-bl">
            Trạng thái phiếu Đề nghị mua hàng
          </h2>
          {/* ======================= Step ======================= */}
          <SetStatus2Step
            statusOptions={statusOptions}
            current={current}
            setCurrent={setCurrent}
            now={reimbursement}
            page={"reimbursement"}
            deletedPage={deletedPage}
            permissionUpdate={permission?.payment_request?.update}
          />
        </div>
        <div className="pointer-events-none">
          {/* ======================= Thông tin phiếu Đề nghị thanh toán ======================= */}
          <div className="my-2">
            <h2 className="text-[1rem] font-medium mb-1 text-bl">
              Thông tin phiếu
            </h2>
            <div className="grid grid-cols-4 gap-2">
              {/* ======================= Mã phiếu Đề nghị thanh toán ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="code">Mã phiếu Đề nghị thanh toán</label>
                  <input
                    className="grow"
                    value={importSlip?.code ? importSlip.code : ""}
                    onChange={() => {}}
                  />
                </div>
              </div>
              {/* ======================= Ngày tạo ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="created_at">Ngày tạo</label>
                  <input
                    className="grow"
                    value={
                      importSlip?.created_at
                        ? stringToDate(importSlip.created_at)
                        : ""
                    }
                    onChange={() => {}}
                  />
                </div>
              </div>
              {/* ======================= Người đề nghị======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="staff_number_name">Người đề nghị</label>
                  <input
                    className="grow"
                    value={
                      importSlip?.staff_number_name
                        ? importSlip?.staff_number_name
                        : ""
                    }
                    onChange={() => {}}
                  />
                </div>
              </div>
              {/* ======================= Bộ phận ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="department_name">Bộ phận</label>
                  <input
                    className="grow"
                    type="text"
                    name="department_name"
                    id="department_name"
                    value={
                      importSlip?.department_name
                        ? importSlip?.department_name
                        : ""
                    }
                    onChange={() => {}}
                  />
                </div>
              </div>
              {/* ======================= Công ty ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="company_name">Công ty</label>
                  <input
                    className="grow"
                    type="text"
                    name="company_name"
                    id="company_name"
                    value={
                      importSlip?.company_name ? importSlip.company_name : ""
                    }
                    onChange={() => {}}
                  />
                </div>
              </div>
              {/* ======================= Đơn hàng cần thanh toán ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="po_detail.code">
                    Đơn hàng cần thanh toán
                  </label>
                  <input
                    id="po_detail.code"
                    name="po_detail.code"
                    value={importSlip?.po_no ? importSlip.po_no : ""}
                    onChange={() => {}}
                  />
                </div>
              </div>
            </div>
          </div>
          {/* ======================= Danh sách sản phẩm nhập kho ======================= */}
          <div className="listProductsChoosen col-span-4 form-group pointer-events-auto">
            <div className="form-style">
              <div className="flex justify-between">
                {/* ======================= Title ======================= */}
                <h2 className="text-[1rem] font-medium mb-1 text-bl">
                  Sản phẩm
                </h2>
              </div>
              {importSlip?.items && (
                <TableListProduct
                  cloneProduct={cloneProduct}
                  setCloneProduct={setCloneProduct}
                />
              )}
            </div>
          </div>
          {/* ======================= Summary ======================= */}
          <div className="listProductsChoosen col-span-4 form-group pointer-events-auto mt-2">
            <div className="form-style">
              <div className="flex justify-between">
                {/* ======================= Title ======================= */}
                <h2 className="text-[1rem] font-medium mb-1 text-bl">
                  Summary
                </h2>
              </div>
              <div className="space-y-2">
                {/* Tổng */}
                <div className="flex ">
                  <p className="font-medium w-[170px]">Tổng </p>
                  <p className=" w-[170px] text-right">
                    {Number(importSlip?.sub_total).toLocaleString("en-US")}
                  </p>
                </div>{" "}
                {/* Thuế*/}
                <div className="flex ">
                  <p className="font-medium w-[170px]">Thuế</p>
                  <p className=" w-[170px] text-right">
                    {Number(importSlip?.total_vat).toLocaleString("en-US")}
                  </p>
                </div>{" "}
                {/* Tổng cộng*/}
                <div className="flex ">
                  <p className="font-bold w-[170px]">Tổng cộng</p>
                  <p className=" w-[170px] text-right font-bold">
                    {Number(importSlip?.total).toLocaleString("en-US")}
                  </p>
                </div>{" "}
                {/* Tạm ứng lần 1*/}
                <div className="flex ">
                  <p className="font-medium w-[170px]">Tạm ứng lần 1</p>
                  <p className=" w-[170px] text-right">
                    {Number(importSlip?.advance_money_first).toLocaleString(
                      "en-US"
                    )}
                  </p>
                </div>{" "}
                {/* Tạm ứng lần 2*/}
                <div className="flex ">
                  <p className="font-medium w-[170px]">Tạm ứng lần 2</p>
                  <p className=" w-[170px] text-right">
                    {Number(importSlip?.advance_money_second).toLocaleString(
                      "en-US"
                    )}
                  </p>
                </div>
                {/* Còn lại*/}
                <div className="flex ">
                  <p className="font-bold w-[170px]">Còn lại</p>
                  <p className=" w-[170px] text-right font-bold">
                    {Number(importSlip?.remaining_amount).toLocaleString(
                      "en-US"
                    )}
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </form>
    </Modal>
  );
}
