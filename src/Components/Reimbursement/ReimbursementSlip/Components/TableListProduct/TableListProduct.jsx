import React from "react";
import { Table } from "antd";
import { stringToDate } from "../../../../../utils/others";
const tableProps = {
  bordered: true,
  size: "small",
};
const paginationObj = {
  position: ["none", "bottomCenter"],
  hideOnSinglePage: true,
  pageSize: 20,
};
export default function TableListProduct({ cloneProduct }) {
  const columns = [
    {
      title: (
        <>
          Mã <br />
          sản phẩm
        </>
      ),
      dataIndex: "sku",
      key: "sku",
      fixed: "left",
      width: 85,
      render: (text, record) => (
        <p className="text-center">{text ? text : ""}</p>
      ),
    },
    {
      title: "Chứng từ",
      children: [
        {
          title: "Ngày",
          dataIndex: "payment_date",
          key: "payment_date",

          render: (text) => {
            return (
              <p className="text-center">{text ? stringToDate(text) : ""}</p>
            );
          },
        },
        {
          title: "Số",
          dataIndex: "payment_no",
          key: "payment_no",
        },
      ],
    },
    {
      title: "Ngày giao hàng",
      dataIndex: "delivery_actual_date",
      key: "delivery_actual_date",
      render: (text, record) => (
        <p className="text-center">{text ? stringToDate(text) : ""}</p>
      ),
    },
    {
      title: "Nội dung",
      dataIndex: "product_name",
      key: "product_name",
      width: 200,
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 80,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "SL",
      dataIndex: "qty",
      key: "qty",
      width: 70,
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: (
        <>
          Thuế <br /> GTGT
        </>
      ),
      dataIndex: "vat_name",
      key: "vat_name",
      width: 70,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: <>Thành tiền</>,
      key: "sub_total",
      dataIndex: "sub_total",
      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: <>Ghi chú</>,
      key: "note",
      dataIndex: "note",
    },
  ];
  return (
    <Table
      {...tableProps}
      pagination={{
        ...paginationObj,
      }}
      scroll={{
        y: 600,
      }}
      columns={columns}
      dataSource={cloneProduct}
    />
  );
}
