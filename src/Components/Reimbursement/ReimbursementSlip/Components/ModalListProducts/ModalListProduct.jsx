import { Modal, Table } from "antd";
import React, { useState } from "react";
// rowSelection object indicates the need for row selection

const tableProps = {
  bordered: true,
  size: "small",
};
export default function ModalListProduct({
  handleCancelAddProducts,
  handleOkAddProducts,
  isModalOpen,
  data,
  productsChoosen,
}) {
  const [rowSelecteds, setRowSelecteds] = useState([]);
  const columns = [
    {
      title: "Mã sản phẩm",
      dataIndex: "key",
      key: "key",
      width: 100,
      fixed: "left",
      className: "text-center",
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product",
      key: "product",
      width: 100,
      fixed: "left",
    },
    {
      title: "Số lượng thực tế",
      dataIndex: "actualQuantity",
      key: "actualQuantity",
      width: 70,
      fixed: "left",
      className: "text-center",
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 70,
      fixed: "left",
      className: "text-center",
    },
    {
      title: "Đơn giá",
      dataIndex: "unitPrice",
      key: "unitPrice",
      width: 100,
      fixed: "left",
      className: "text-center",
      render: (text) => {
        return <p>{Number(text).toLocaleString("en-US")}</p>;
      },
    },
  ];
  const rowSelection = {
    selectedRowKeys: rowSelecteds,
    onChange: (selectedRowKeys, selectedRows) => {
      setRowSelecteds(selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User",
      // Column configuration not to be checked
      name: record.name,
    }),
    preserveSelectedRowKeys: true,
  };

  return (
    <Modal
      title="Chọn sản phẩm tạm ứng"
      centered
      open={isModalOpen}
      onOk={() => {
        handleOkAddProducts(rowSelecteds);
      }}
      onCancel={handleCancelAddProducts}
      okText="Thêm"
      cancelText="Huỷ"
      okButtonProps={{
        className:
          rowSelecteds.length > 0
            ? "btn-main-yl inline-flex items-center justify-center"
            : "hidden",
      }}
      classNames={{ wrapper: "main-modal " }}
    >
      <Table
        {...tableProps}
        pagination={{
          position: ["none", "bottomCenter"],
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
          x: 240,
        }}
        columns={columns}
        dataSource={data}
      />
    </Modal>
  );
}
