import React, { useEffect, useState } from "react";
import { Tag, Table, Tooltip } from "antd";
import {
  deleteReimbursementSlipId,
  getReimbursemenId,
  getReimbursementList,
  reimbursementExport,
  updateReimbursementSlip,
} from "../../../../utils/reimbursement";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setPage } from "../../../../redux/slice/pageSlice";
import { otherKeys } from "../../../../utils/user";
import { showError, stringToDate } from "../../../../utils/others";
import DeleteBtnItemComponent from "../../../ButtonComponent/DeleteBtnItemComponent";
import RenderWithFixComponent from "../../../ButtonComponent/RenderIdView";
import { FaFilePdf } from "react-icons/fa6";
import ReactToPrint from "react-to-print";
import PrintPR from "../../../ExportToPDF/PaymentRequestPDF/PrintPR";
import FixBtnComponent from "../../../ButtonComponent/FixBtnComponent";
import ViewBtnComponent from "../../../ButtonComponent/ViewBtnComponent";
import UndoBtnComponent from "../../../ButtonComponent/UndoBtnComponent";
import { setSpinner } from "../../../../redux/slice/spinnerSlice";
import TableLength from "../../../ButtonComponent/TableLength";
import ExportDetailsBtnComponent from "../../../ButtonComponent/ExportDetailsBtnComponent";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListReimbursements({
  showModal,
  reload,
  setReload,
  deletedPage,
  rowSelection,
  setKeySelected,
  searchFilter,
  showModalSupplier,
}) {
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );

  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();
  let navigate = useNavigate();
  let page = useSelector((state) => state.pageSlice.value);

  // 2 - Chuyển hướng sang trang chi tiết
  const goToFix = (key) => {
    navigate(`/payment_request/fix/${key}`);
  };

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list phiếu hoàn ứngtheo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }
    keyParam = { ...keyParam, ...searchFilter };
    let res = await getReimbursementList(page, keyParam);

    if (res.status) {
      setListData(res.data.data);
      setTotal(res.data.total);
      setPageSize(res.data.per_page);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Xác nhận xoá phiếu hoàn ứng
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deleteData(id, value);
  };

  // 5 - Xoá phiếu hoàn ứng
  const deleteData = async (id, input) => {
    dispatch(setSpinner(true));
    let res;
    if (input == 1) {
      res = await deleteReimbursementSlipId({ id: id, deleted: true });
    } else {
      res = await deleteReimbursementSlipId({ id: id });
    }
    if (res) {
      setKeySelected([]);
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 6 - Hoàn tác xoá phiếu hoàn ứng
  const undoDelete = async (id) => {
    dispatch(setSpinner(true));
    let res;
    res = await updateReimbursementSlip({ id: id, deleted: false });
    // console.log(res);
    if (res) {
      setKeySelected([]);
      // console.log("reload trang");
      setReload(true);
    }
    dispatch(setSpinner(true));
  };

  // 7 - Kích hoạt phiếu
  const confirmUndo = (id) => {
    undoDelete(id);
  };

  // 9 - Lần đầu load trang => Kéo về danh sách user & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    getListData(page);
  }, []);

  // 10 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      // console.log("run");
      // dispatch(resetPage());
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  {
    /* ======================= Start IN  ======================= */
  }
  // 2 - lấy chi tiết phiếu
  const getSlipInfo = async (id) => {
    dispatch(setSpinner(true));
    let res = await getReimbursemenId(id);
    if (res.data.success) {
      setDetails(res.data.data);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  const componentRef = React.useRef(null);

  const onBeforeGetContentResolve = React.useRef(null);

  const [loading, setLoading] = React.useState(false);
  const [PPDetails, setDetails] = useState();

  const handleAfterPrint = () => {
    // console.log("`onAfterPrint` called");
    onBeforeGetContentResolve.current = null;
    setLoading(false);
  };

  const handleBeforePrint = React.useCallback(() => {
    // console.log("`onBeforePrint` called");
  }, []);

  const handleOnBeforeGetContent = async (id) => {
    // console.log("`onBeforeGetContent` called");
    await getSlipInfo(id);
    // console.log("dont");
    // return componentRef.current;
  };

  React.useEffect(() => {
    if (loading && onBeforeGetContentResolve.current) {
      // Resolves the Promise, letting `react-to-print` know that the DOM updates are completed
      onBeforeGetContentResolve.current();
    }
  }, [loading]);

  const reactToPrintTrigger = React.useCallback(() => {
    // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
    // to the root node of the returned component as it will be overwritten.

    // Bad: the `onClick` here will be overwritten by `react-to-print`
    // return <button onClick={() => alert('This will not work')}>Print this out!</button>;

    // Good
    return (
      <button id="btnPdf">
        <FaFilePdf className="text-[#B6190D]" />
      </button>
    );
  }, []);

  {
    /* ======================= End IN  ======================= */
  }

  Table.SELECTION_COLUMN.fixed = "left";
  const columns = [
    {
      title: <>Mã phiếu</>,
      dataIndex: "code",
      key: "code",
      width: 100,
      fixed: "left",
      className: "text-center",
      render: (text, record) => (
        <RenderWithFixComponent
          onClickF={() => {
            showModal(record.id);
          }}
          text={text}
          textAlign={"left"}
        />
      ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "created_at",
      key: "created_at",
      width: 100,
      render: (text, record) => {
        return <p className="text-center">{text ? stringToDate(text) : ""}</p>;
      },
    },
    {
      title: <>Trạng thái</>,
      dataIndex: "status_name",
      key: "status_name",
      width: 120,
      className: "text-center",
      render: (text, record) => {
        return text ? <Tag color="processing">{text}</Tag> : "";
      },
    },

    {
      title: "Người yêu cầu",
      dataIndex: "staff_number_name",
      key: "staff_number_name",
    },
    {
      title: "NCC",
      dataIndex: "supplier_list",
      key: "supplier_list",
      render: (text, record) => {
        return (
          <RenderWithFixComponent
            onClickF={() => {
              showModalSupplier(
                record?.suppliers_short_name ? record?.suppliers_short_name : []
              );
            }}
            text={
              record?.suppliers_short_name
                ? displaySuppliers(record?.suppliers_short_name)
                : []
            }
            textAlign={"left"}
          />
        );
      },
    },
    {
      title: "PTTT",
      dataIndex: "paymentMethod_name",
      key: "paymentMethod_name",
    },
    {
      title: (
        <>
          Số tiền <br /> thanh toán
        </>
      ),
      dataIndex: "total",
      key: "total",
      width: 120,
      render: (text, record) => {
        return (
          <p className="text-right">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 150,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Xem chi tiết ======================= */}
            {permission?.payment_request?.read === 1 && (
              <ViewBtnComponent
                onClickF={() => {
                  showModal(record.id);
                }}
                text={"Xem chi tiết & chỉnh trạng thái"}
              />
            )}
            {/* ======================= Chỉnh sửa ======================= */}
            {permission?.payment_request?.update === 1 && (
              <FixBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  goToFix(record.id);
                }}
              />
            )}
            {/* ======================= Undo ======================= */}
            {permission?.payment_request?.update === 1 && (
              <UndoBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  confirmUndo(record.id);
                }}
                text={"phiếu"}
              />
            )}
            {/* ======================= In file PDF ======================= */}
            {!deletedPage && permission.payment_request?.read === 1 && (
              <Tooltip placement="bottom" title="Xuất file PDF phiếu đã chọn">
                <div>
                  <ReactToPrint
                    content={() => componentRef.current}
                    documentTitle="Xuất phiếu hoàn ứng"
                    onAfterPrint={handleAfterPrint}
                    onBeforeGetContent={() => {
                      return new Promise(async (resolve) => {
                        await handleOnBeforeGetContent(record.id);
                        onBeforeGetContentResolve.current = resolve;
                        setLoading(true);
                      });
                    }}
                    onBeforePrint={handleBeforePrint}
                    removeAfterPrint
                    trigger={reactToPrintTrigger}
                  />
                  <div className="hidden">
                    <PrintPR forwardedRef={componentRef} details={PPDetails} />
                  </div>
                </div>
              </Tooltip>
            )}
            {/* ======================= Xuất file Excel =======================  */}
            {permission?.payment_request?.read === 1 && (
              <ExportDetailsBtnComponent
                onClickF={() => exportFile(record.id)}
                text={"Xuất file excel chi tiết"}
              />
            )}
            {/* ======================= Xoá ======================= */}
            <DeleteBtnItemComponent
              onClickF={() => {
                if (deletedPage) {
                  confirm(record.id, 1);
                } else {
                  confirm(record.id, 0);
                }
              }}
              text={"phiếu"}
            />
          </div>
        );
      },
    },
  ];

  const exportFile = async (id) => {
    const res = await reimbursementExport({ id, type: "items" });
    if (res) {
      // Tạo thẻ <a> để tải file
      const link = document.createElement("a");
      link.href = res;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  };

  const displaySuppliers = (suppliers) => {
    if (suppliers.length > 2) {
      return `${suppliers[0]}, ${suppliers[1]}...`;
    } else {
      return suppliers.join(", ");
    }
  };
  return (
    <div className="table-render-API">
      <TableLength page={page} pageSize={pageSize} total={total} />
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
          x: 1000,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
