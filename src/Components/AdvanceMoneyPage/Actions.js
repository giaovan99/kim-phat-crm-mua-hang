import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
import DropdownOptions from "../DropdownOptions/DropdownOptions";
import AddBtnComponent from "../ButtonComponent/AddBtnComponent";
import { Select } from "antd";
import { normalize } from "../../utils/others";
export default function Actions({
  showModal,
  deletedPage,
  setReload,
  keySelected,
  setKeySelected,
  permission,
  listStaff,
  listStatus,
  typeList,
  setSearchFilter,
  itemSelected,
  setItemSelected,
}) {
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      keywords: "",
      status: "",
      staff_number: "",
      type: "",
    },

    onSubmit: async (values) => {
      setSearchFilter(values);
      setReload(true);
    },
  });
  const onSearch = (value) => {
    // console.log("search:", value);
  };
  const filterOption = (input, option) =>
    normalize(option?.label ?? "").includes(normalize(input));

  const onChangeSelect = (value, field) => {
    formik.setFieldValue(field, value);
  };
  return (
    <div className="actions gap-3 flex-wrap-reverse">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div
            className="flex flex-wrap-reverse gap-2"
            style={{ alignItems: "end" }}
          >
            {permission?.requests_for_advance?.update === 1 && (
              <DropdownOptions
                arrId={keySelected}
                deletedPage={deletedPage}
                page={"advanceMoney"}
                setReload={setReload}
                listStatus={listStatus}
                setKeySelected={setKeySelected}
                arrItem={itemSelected}
                setItemSelected={setItemSelected}
              />
            )}
            <div className="form-group">
              <div className="form-style items-end flex-wrap">
                <div>
                  <label>Tìm</label>
                  <input
                    id="keywords"
                    name="keywords"
                    type="text"
                    placeholder="Tìm mã phiếu"
                    value={formik.values.keywords}
                    onChange={formik.handleChange}
                    className="max-w-[150px]"
                  />
                </div>
                {/* Trạng thái */}
                <div>
                  <label>Trạng thái</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "status")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listStatus}
                    value={
                      formik.values.status != "" ? formik.values.status : null
                    }
                    className="w-[116px]"
                  />
                </div>
                {/* Người đề nghị */}
                <div>
                  <label>Người đề nghị</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "staff_number")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listStaff}
                    value={formik?.values?.staff_number || null}
                    className="w-[162px]"
                  />
                </div>

                {/* Loại phiếu */}
                <div>
                  <label>Loại phiếu</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "type")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={typeList}
                    value={formik.values.type != "" ? formik.values.type : null}
                    className="w-[117px]"
                  />
                </div>

                <button className="btn-main-yl btn-actions">
                  <HiOutlineSearch />
                  Tìm
                </button>
                <button
                  className="ant-btn btn-actions text-white bg-black ant-btn-default rounded-lg"
                  type="button"
                  onClick={formik.handleReset}
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      {!deletedPage && permission?.requests_for_advance?.create ? (
        <div className="flex gap-3 items-end">
          <AddBtnComponent
            onClickF={() => showModal(null)}
            text={"Tạo phiếu tạm ứng"}
          />
        </div>
      ) : (
        <></>
      )}
    </div>
  );
}
