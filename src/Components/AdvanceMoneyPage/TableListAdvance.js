import React, { useEffect, useState } from "react";
import { Tag, Table, Tooltip } from "antd";
import { showError, stringToDate } from "../../utils/others";
import { useDispatch, useSelector } from "react-redux";
import { otherKeys } from "../../utils/user";
import {
  deleteAdvanceSlipId,
  getAdvanceList,
  getAdvanceSlip,
  updateAdvanceSlip,
} from "../../utils/advance";
import { setPage } from "../../redux/slice/pageSlice";
import { FaFilePdf } from "react-icons/fa6";
import ReactToPrint from "react-to-print";
import PrintAM from "../ExportToPDF/AdvanceMoneyPDF/PrintAM";
import DeleteBtnItemComponent from "../ButtonComponent/DeleteBtnItemComponent";
import FixBtnComponent from "../ButtonComponent/FixBtnComponent";
import RenderWithFixComponent from "../ButtonComponent/RenderIdFix";
import UndoBtnComponent from "../ButtonComponent/UndoBtnComponent";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import TableLength from "../ButtonComponent/TableLength";
import { companyList } from "../../utils/order";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListAdvance({
  showModal,
  reload,
  setReload,
  deletedPage,
  rowSelection,
  setListStaff,
  setListStatus,
  permission,
  searchFilter,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const [companyInfo, setCompanyInfo] = useState("");
  const dispatch = useDispatch();
  let page = useSelector((state) => state.pageSlice.value);

  // 3 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 4 - Kéo data list phiếu tạm ứng theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }

    keyParam = { ...keyParam, ...searchFilter };

    // console.log(keyParam);
    let res = await getAdvanceList(page, keyParam);
    if (res.status) {
      setListData(res.data.data);
      setTotal(res.data.total);
      setPageSize(res.data.per_page);
      setListStaff(res.options.staff);
      setListStatus(res.options.status);
    }
    dispatch(setSpinner(false));
  };

  // 5 - Xác nhận xoá phiếu
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    deleteData(id, value);
  };

  // 6 - Xoá phiếu
  const deleteData = async (id, input) => {
    dispatch(setSpinner(true));
    let res;
    if (input == 1) {
      res = await deleteAdvanceSlipId({ id: id, deleted: true });
    } else {
      res = await deleteAdvanceSlipId({ id: id });
    }
    if (res) {
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 7 - Hoàn tác xoá
  const undoDelete = async (id) => {
    dispatch(setSpinner(true));
    let res;
    res = await updateAdvanceSlip({ id: id, deleted: false });
    // console.log(res);
    if (res) {
      // console.log("reload trang");
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 8 - Kích hoạt phiếu
  const confirmUndo = (id) => {
    undoDelete(id);
  };

  // 10 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    getListData(page);
    if (reload) {
      setReload(false);
    }
  }, [reload, page]);

  const getCompanyInfo = async () => {
    let res = await companyList();
    if (res.data) {
      setCompanyInfo(res.data);
    }
  };
  useEffect(() => {
    //  Lấy danh sách thông tin công ty
    getCompanyInfo();
  }, []);

  {
    /* ======================= Start IN  ======================= */
  }
  // Lấy thông tin chi tiết phiếu tạm ứng
  const getDetailId = async (id) => {
    dispatch(setSpinner(true));
    let res = await getAdvanceSlip(id);
    if (res.data.success) {
      setDetails(res.data.data);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  const componentRef = React.useRef(null);

  const onBeforeGetContentResolve = React.useRef(null);

  const [loading, setLoading] = React.useState(false);
  const [PPDetails, setDetails] = useState("");

  const handleAfterPrint = () => {
    // console.log("`onAfterPrint` called");
    onBeforeGetContentResolve.current = null;
    setLoading(false);
  };

  const handleBeforePrint = React.useCallback(() => {
    // console.log("`onBeforePrint` called");
  }, []);

  const handleOnBeforeGetContent = async (id) => {
    // console.log("`onBeforeGetContent` called");
    await getDetailId(id);
    // console.log("dont");
    // return componentRef.current;
  };

  React.useEffect(() => {
    if (loading && onBeforeGetContentResolve.current) {
      // Resolves the Promise, letting `react-to-print` know that the DOM updates are completed
      onBeforeGetContentResolve.current();
    }
  }, [loading]);

  const reactToPrintTrigger = React.useCallback(() => {
    // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
    // to the root node of the returned component as it will be overwritten.

    // Bad: the `onClick` here will be overwritten by `react-to-print`
    // return <button onClick={() => alert('This will not work')}>Print this out!</button>;

    // Good
    return (
      <button id="btnPdf">
        <FaFilePdf className="text-[#B6190D]" />
      </button>
    );
  }, []);

  {
    /* ======================= End IN  ======================= */
  }

  Table.SELECTION_COLUMN.fixed = "left";
  const columns = [
    {
      title: "Mã phiếu ",
      dataIndex: "code",
      key: "code",
      width: 100,
      fixed: "left",
      render: (text, record) =>
        permission?.requests_for_advance?.update === 1 ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record.id);
            }}
            text={text}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",

      render: (text, record) => {
        return (
          <p className="text-center">
            {text === "Chưa duyệt" ? (
              <Tag color="success">{text}</Tag>
            ) : (
              <Tag color="processing">{text}</Tag>
            )}
          </p>
        );
      },
    },
    {
      title: "Ngày tạo",
      dataIndex: "created_at",
      key: "created_at",
      render: (text, record) => {
        return <p className="text-center">{text ? stringToDate(text) : ""}</p>;
      },
    },
    {
      title: "Công ty",
      dataIndex: "company_name",
      key: "company_name",
    },
    {
      title: "Người đề nghị",
      dataIndex: "staff_number_name",
      key: "staff_number_name",
      width: 130,
    },
    {
      title: "Loại phiếu",
      dataIndex: "type_name",
      key: "type_name",
      width: 130,
    },
    {
      title: "Số tiền tạm ứng",
      dataIndex: "total",
      key: "total",
      width: 150,
      render: (text, record) => {
        return (
          <p className="text-right">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },
    {
      title: "Hạn thanh toán",
      dataIndex: "puchase_date",
      key: "puchase_date",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Lý do tạm ứng",
      dataIndex: "description",
      key: "description",
      width: 250,
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            {permission?.requests_for_advance?.update === 1 && (
              <FixBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  showModal(record.id);
                }}
              />
            )}

            {/* ======================= Undo ======================= */}
            {permission?.requests_for_advance?.update === 1 && (
              <UndoBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  confirmUndo(record.id);
                }}
                text={"phiếu"}
              />
            )}

            {/* ======================= In file PDF ======================= */}
            {!deletedPage && permission?.requests_for_advance?.read === 1 && (
              <Tooltip placement="bottom" title="Xuất file PDF phiếu đã chọn">
                <div>
                  <ReactToPrint
                    content={() => componentRef.current}
                    documentTitle="Xuất phiếu tạm ứng"
                    onAfterPrint={handleAfterPrint}
                    onBeforeGetContent={() => {
                      return new Promise(async (resolve) => {
                        await handleOnBeforeGetContent(record.id);
                        onBeforeGetContentResolve.current = resolve;
                        setLoading(true);
                      });
                    }}
                    onBeforePrint={handleBeforePrint}
                    removeAfterPrint
                    trigger={reactToPrintTrigger}
                  />
                  <div className="hidden">
                    <PrintAM
                      forwardedRef={componentRef}
                      details={PPDetails}
                      companyInfo={companyInfo}
                    />
                  </div>
                </div>
              </Tooltip>
            )}
            {/* ======================= Xoá ======================= */}
            {permission?.requests_for_advance?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => {
                  if (deletedPage) {
                    confirm(record.id, 1);
                  } else {
                    confirm(record.id, 0);
                  }
                }}
                text={"phiếu"}
              />
            )}
          </div>
        );
      },
    },
  ];
  return (
    <div className="table-render-API">
      {" "}
      <TableLength page={page} pageSize={pageSize} total={total} />
      <Table
        {...tableProps}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 600,
          x: 1400,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
