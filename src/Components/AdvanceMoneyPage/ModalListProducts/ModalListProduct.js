import { Modal, Table } from "antd";
import React, { useState } from "react";
// rowSelection object indicates the need for row selection

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "product_id",
};
const paginationObj = {
  position: ["none", "bottomCenter"],
  hideOnSinglePage: true,
  pageSize: 20,
};
export default function ModalListProduct({
  handleCancelAddProducts,
  handleOkAddProducts,
  isModalOpen,
  data,
}) {
  const [rowSelecteds, setRowSelecteds] = useState([]);
  const columns = [
    {
      title: "Mã sản phẩm",
      dataIndex: "sku",
      key: "sku",
      width: 120,
      fixed: "left",
      className: "text-center",
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
      // width: 100,
      fixed: "left",
    },
    {
      title: "Số lượng ",
      dataIndex: "qty",
      key: "qty",
      width: 120,
      fixed: "left",
      render: (text, record) => {
        return (
          <p className="text-center">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },
    {
      title: (
        <>
          Số lượng <br /> thực tế
        </>
      ),
      dataIndex: "qty_actual",
      key: "qty_actual",
      width: 120,
      fixed: "left",
      render: (text, record) => {
        return (
          <p className="text-center">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },
  ];
  const rowSelection = {
    selectedRowKeys: rowSelecteds,
    onChange: (selectedRowKeys, selectedRows) => {
      setRowSelecteds(selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User",
      // Column configuration not to be checked
      name: record.name,
    }),
    preserveSelectedRowKeys: true,
  };
  // console.log(rowSelecteds.length);
  return (
    <Modal
      title="Chọn sản phẩm tạm ứng"
      centered
      open={isModalOpen}
      onOk={() => {
        handleOkAddProducts(rowSelecteds);
      }}
      onCancel={handleCancelAddProducts}
      okText="Thêm"
      cancelText="Huỷ"
      okButtonProps={{
        className:
          rowSelecteds.length > 0
            ? "btn-main-yl inline-flex items-center justify-center"
            : "hidden",
      }}
      classNames={{ wrapper: "main-modal " }}
    >
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
          x: 240,
        }}
        columns={columns}
        dataSource={data}
      />
    </Modal>
  );
}
