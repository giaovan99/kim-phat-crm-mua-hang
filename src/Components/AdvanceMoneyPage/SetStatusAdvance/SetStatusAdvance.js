import { Button, Popconfirm, Steps, message } from "antd";
import React from "react";
import { ExclamationCircleOutlined } from "@ant-design/icons";
import { LuUndo } from "react-icons/lu";
import { MdDone } from "react-icons/md";
export default function SetStatusAdvance({
  statusOptions,
  current,
  setCurrent,
  now = null,
}) {
  const confirmNext = (e) => {
    // console.log(e);
    next();
  };
  const confirmPrev = (e) => {
    // console.log(e);
    prev();
  };
  const cancel = (e) => {
    // console.log(e);
    message.error("Huỷ");
  };

  const next = async () => {
    document.getElementById("warning").innerText = "";
    let step = current + 1;
    setCurrent(step);
  };

  const prev = async () => {
    if (current - 1 < 1) {
      document.getElementById("warning").innerText =
        "Không thể hoàn tác xuống trạng thái thấp hơn ";
      return;
    } else {
      let step = current - 1;
      document.getElementById("warning").innerText = "";
      setCurrent(step);
    }
  };

  return (
    <div className="flex justify-center flex-col items-center">
      {/* ======================= Btns ======================= */}
      <div className="flex gap-3">
        {/* ======================= Hoàn tác ======================= */}
        {current > 0 && (
          <Popconfirm
            placement="topRight"
            title="Hoàn tác trạng thái"
            description="Xác nhận hoàn tác trạng thái này?"
            onConfirm={confirmPrev}
            onCancel={cancel}
            okText="Xác nhận"
            cancelText="Huỷ"
            okButtonProps={{ className: "btn-main-yl" }}
            className="btn-undo shrink-0"
            icon={
              <ExclamationCircleOutlined className="warning-icon-popconfirm" />
            }
          >
            <Button>
              <LuUndo className="text-white" />
            </Button>
          </Popconfirm>
        )}
        <div className="shrink">
          {/* ======================= Steps ======================= */}
          <Steps
            current={current}
            items={statusOptions}
            labelPlacement="vertical"
            size="small"
          />
        </div>
        {/* ======================= Hoàn thành ======================= */}
        {current < statusOptions.length && (
          <Popconfirm
            placement="topRight"
            title="Hoàn thành trạng thái"
            description="Xác nhận hoàn thành trạng thái này?"
            onConfirm={confirmNext}
            onCancel={cancel}
            okText="Xác nhận"
            cancelText="Huỷ"
            okButtonProps={{ className: "btn-main-yl" }}
            className="btn-success shrink-0"
            icon={
              <ExclamationCircleOutlined className="warning-icon-popconfirm" />
            }
          >
            <Button>
              <MdDone className="text-white" />
            </Button>
          </Popconfirm>
        )}
      </div>
      <p id="warning" className="text-red-600"></p>
    </div>
  );
}
