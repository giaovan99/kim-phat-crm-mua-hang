import React from "react";
import { Popconfirm, Table, Tooltip, message } from "antd";
import { HiOutlineTrash } from "react-icons/hi2";
const tableProps = {
  bordered: true,
  size: "small",
};
const paginationObj = {
  position: ["none", "bottomCenter"],
  hideOnSinglePage: true,
  pageSize: 20,
};
export default function TableListProduct({
  cloneProduct,
  confirmDelete,
  setCloneProduct,
}) {
  const cancel = (e) => {
    // console.log(e);
    message.error("Đã huỷ");
  };
  const columns = [
    {
      title: <>Mã SP</>,
      dataIndex: "product_id",
      key: "product_id",
      width: 120,
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
      // width: 140,
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
      width: 150,
      render: (text, record) => {
        return (
          <p className="text-center">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 80,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Số lượng",
      dataIndex: "qty",
      key: "qty",
      width: 120,
      render: (text, record) => {
        return (
          <p className="text-center">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },
    {
      title: (
        <>
          Số lượng <br /> thực tế
        </>
      ),
      dataIndex: "qty_actual",
      key: "qty_actual",
      width: 120,
      render: (text, record) => {
        return (
          <p className="text-center">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },

    {
      title: "Hoá đơn",
      dataIndex: "vat",
      key: "vat",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },

    {
      title: (
        <>
          Số tiền <br />
          cần tạm ứng
        </>
      ),
      dataIndex: "advance_money",
      key: "advance_money",
      width: 150,
      render: (text, record) => {
        return (
          <input
            className="text-right w-full"
            value={record.advance_money}
            type="number"
            onChange={(event) => {
              let arr = [...cloneProduct];
              let index = arr.findIndex(
                (element) => element.product_id == record.product_id
              );
              arr[index].advance_money = Math.abs(event.target.value);
              setCloneProduct([...arr]);
            }}
          />
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 80,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Xoá ======================= */}
            <Popconfirm
              placement="topRight"
              title="Xoá sản phẩm"
              description="Bạn chắc chắn muốn xoá sản phẩm này?"
              onConfirm={() => confirmDelete(record.product_id)}
              onCancel={cancel}
              okText="Xoá"
              cancelText="Huỷ"
              okButtonProps={{ className: "btn-main-yl" }}
              className="popover-danger"
            >
              <Tooltip placement="bottom" title="Xoá">
                <button type="button">
                  <HiOutlineTrash className="text-rose-600" />
                </button>
              </Tooltip>
            </Popconfirm>
          </div>
        );
      },
    },
  ];
  return (
    <Table
      {...tableProps}
      pagination={{
        ...paginationObj,
      }}
      scroll={{
        y: 600,
        x: 1000,
      }}
      columns={columns}
      dataSource={cloneProduct}
    />
  );
}
