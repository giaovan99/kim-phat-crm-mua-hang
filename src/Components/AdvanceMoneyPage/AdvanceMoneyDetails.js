import React, { useEffect, useState } from "react";
import { Modal } from "antd";
import { Field, FormikProvider, useFormik } from "formik";
import * as yup from "yup";
import { Select } from "antd";
import moment from "moment";
import {
  createAdvanceSlip,
  getAdvanceSlip,
  getTypeAM,
  updateAdvanceSlip,
} from "../../utils/advance";
import InputPriceNotTable from "../Input/InpurPriceNotTable";
import SetStatus2Step from "../SetStatus2Step/SetStatusProposal";
import { normalize, stringToDate } from "../../utils/others";
import { companyList } from "../../utils/order";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";
// Filter `option.key` match the user type `input`
const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

export default function AdvanceMoneyDetails({
  isModalOpen,
  setIsModalOpen,
  advance,
  setAdvance,
  setReload,
  deletedPage,
  statusOptions,
  listStaff,
  setListStaff,
  permission,
  setTypeList,
  typeList,
}) {
  // 1 - Khai báo state
  let [current, setCurrent] = useState(1); //vị trí của trạng thái phiếu tạm ứng
  const [listCompany, setCompany] = useState(); //Danh sách cty

  const initial = {
    staff_number: "",
    type: "",
    request_date: moment().format("yyyy-MM-DD"),
    puchase_date: "",
    description: "",
    total: "",
    status: 1,
    company: "",
  };
  let dispatch = useDispatch();

  // 2 - Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      staff_number: yup.string().required("Chọn người đề nghị"),
      type: yup.string().required("Chọn loại phiếu"),
      puchase_date: yup.string().required("Chọn thời gian cần thanh toán"),
      total: yup.string().required("Nhập tổng số tiền tạm ứng"),
      description: yup.string().required("Nhập lí do"),
      company: yup.string().required("Chọn công ty"),
    }),
    onSubmit: async (values) => {
      console.log(values);
      dispatch(setSpinner(true));
      let res;
      if (!advance) {
        // console.log("API tạo");
        res = await createAdvanceSlip(values, formik);
      } else {
        // console.log("API sửa");
        res = await updateAdvanceSlip(values);
      }
      if (res) {
        resetForm();
        setReload(true);
      }
      dispatch(setSpinner(false));
    },
  });

  const { handleBlur, touched, errors } = formik;
  // console.log(formik.errors);

  // 3 - Lấy tất cả các mảng cố định
  let getArr = async (page) => {
    dispatch(setSpinner(true));
    let res;

    // 5.10 - Danh sách loại phiếu
    res = await getTypeAM();
    if (res.status) {
      setTypeList(res.data);
    }
    // 5.3 - Công ty
    res = await companyList(page);
    if (res.status) {
      setCompany(res.data);
    }
    dispatch(setSpinner(false));
  };

  // Lấy thông tin chi tiết phiếu tạm ứng
  const getDetailId = async () => {
    let res = await getAdvanceSlip(advance);
    if (res.data.success) {
      formik.setValues(res.data.data);
      setCurrent(res.data.data.status * 1);
    }
  };

  // 4 - Lần đầu khi load trang => check xem là add hay fix phiếu tạm ứng
  useEffect(() => {
    // Lấy danh sách bộ phận
    if (advance) {
      // console.log("sửa ");
      // API lấy thông tin chi tiết của phiếu tạm ứng
      getDetailId();
    } else {
      // console.log("thêm đơn hàng");
      formik.setValues(initial);
    }
  }, [advance]);

  useEffect(() => {
    getArr();
  }, []);

  // 6 - Reset Form
  const resetForm = () => {
    setAdvance(null);
    formik.setValues(initial);
    formik.resetForm();
    setIsModalOpen(false);
  };

  // 7 - Khi lưu form
  const handleOk = () => {
    formik.handleSubmit();
  };

  // 8 - Khi cancel form
  const handleCancel = () => {
    resetForm();
  };

  // 16 - Khi search
  const onSearch = (value) => {
    // console.log("search:", value);
  };

  //  9 - Khi current thay đổi
  useEffect(() => {
    formik.values.status = current;
  }, [current]);

  // 5 - Khi select thay đổi -> cập nhật formik
  const onChangeSelect = (value, field) => {
    formik.setFieldValue(field, value);
  };

  return (
    <Modal
      title={advance ? `Chỉnh sửa phiếu tạm ứng` : "Tạo phiếu tạm ứng"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: !deletedPage
          ? "btn-main-yl inline-flex items-center justify-center"
          : "hidden",
      }}
      classNames={{ wrapper: "main-modal modal-view-detail" }}
    >
      <FormikProvider value={formik}>
        <form id="formAddUser" onSubmit={formik.handleSubmit}>
          {/* ======================= Trạng thái ======================= */}
          {advance && (
            <div className={deletedPage ? "pointer-events-none mb-5" : "mb-5"}>
              {/* ======================= Title ======================= */}
              <h2 className="text-[1rem] font-medium mb-1 text-bl">
                Trạng thái phiếu tạm ứng
              </h2>
              {/* ======================= Step ======================= */}
              <SetStatus2Step
                statusOptions={statusOptions}
                current={current}
                setCurrent={setCurrent}
                page={"advancedMoney"}
                deletedPage={deletedPage}
                permissionUpdate={permission?.requests_for_advance?.update}
              />
            </div>
          )}
          {/* ======================= Thông tin phiếu, khi trạng thái đã duyệt -> không cho sửa ======================= */}
          <div
            className={current > 1 || deletedPage ? "pointer-events-none" : ""}
          >
            <div className="grid grid-cols-2 xl:grid-cols-4 gap-2">
              {/* ======================= Mã phiếu tạm ứng ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="code">Mã phiếu tạm ứng</label>
                  <input
                    className="grow"
                    type="text"
                    placeholder="Hệ thống tự tạo"
                    name="code"
                    id="code"
                    value={formik.values.code ? formik.values.code : ""}
                    disabled
                  />
                </div>
                {touched.code && errors.code ? (
                  <p className="mt-1 text-red-500 text-sm" id="code-warning">
                    {errors.code}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Ngày tạo ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="request_date">Ngày tạo</label>
                  <input
                    className="grow"
                    type="text"
                    placeholder="Hệ thống tự tạo"
                    name="request_date"
                    id="request_date"
                    value={
                      formik.values.request_date
                        ? stringToDate(formik.values.request_date)
                        : ""
                    }
                    disabled
                  />
                </div>
                {touched.request_date && errors.request_date ? (
                  <p
                    className="mt-1 text-red-500 text-sm"
                    id="request_date-warning"
                  >
                    {errors.request_date}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* =======================  Công ty ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="company">
                    Công ty <span className="text-red-500">*</span>
                  </label>
                  <Field name="company">
                    {({ field }) => {
                      return (
                        <Select
                          {...field}
                          showSearch
                          placeholder="Chọn công ty"
                          optionFilterProp="children"
                          onChange={(value) =>
                            formik.setFieldValue("company", value)
                          }
                          onSearch={onSearch}
                          filterOption={filterOption}
                          options={listCompany}
                          onBlur={handleBlur}
                          value={
                            formik.values.company ? formik.values.company : null
                          }
                        />
                      );
                    }}
                  </Field>
                </div>
                {touched.company && errors.company ? (
                  <p className="mt-1 text-red-500 text-sm" id="company-warning">
                    {errors.company}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Người đề nghị ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="staff_number">
                    Người đề nghị<span className="text-red-500">*</span>
                  </label>
                  <Field name="staff_number">
                    {({ field }) => {
                      return (
                        <Select
                          showSearch
                          placeholder="Chọn người đề nghị"
                          optionFilterProp="children"
                          onChange={(value) =>
                            onChangeSelect(value, "staff_number")
                          }
                          onSearch={onSearch}
                          filterOption={filterOption}
                          options={listStaff}
                          value={
                            formik.values.staff_number != ""
                              ? formik.values.staff_number
                              : null
                          }
                          onBlur={handleBlur}
                        />
                      );
                    }}
                  </Field>
                </div>
                {touched.staff_number && errors.staff_number ? (
                  <p
                    className="mt-1 text-red-500 text-sm"
                    id="staff_number-warning"
                  >
                    {errors.staff_number}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= loại phiếu ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="type">
                    Loại phiếu <span className="text-red-500">*</span>
                  </label>
                  <Field name="type">
                    {({ field }) => {
                      return (
                        <Select
                          id="type"
                          name="type"
                          showSearch
                          placeholder="Chọn loại phiếu"
                          optionFilterProp="children"
                          onChange={(value) => onChangeSelect(value, "type")}
                          onSearch={onSearch}
                          filterOption={filterOption}
                          options={typeList}
                          value={
                            formik.values.type != "" ? formik.values.type : null
                          }
                          onBlur={handleBlur}
                        />
                      );
                    }}
                  </Field>
                </div>
                {touched.type && errors.type ? (
                  <p className="mt-1 text-red-500 text-sm" id="type-warning">
                    {errors.type}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Tổng số tiền tạm ứng ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="total">
                    Tổng số tiền tạm ứng<span className="text-red-500">*</span>
                  </label>
                  <InputPriceNotTable
                    className={"grow"}
                    name={"total"}
                    formik={formik}
                    onChange={onChangeSelect}
                    value={formik.values.total}
                  />
                </div>
                {touched.total && errors.total ? (
                  <p className="mt-1 text-red-500 text-sm" id="total-warning">
                    {errors.total}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Thời hạn thanh toán ======================= */}
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="puchase_date">
                    Thời hạn thanh toán <span className="text-red-500">*</span>
                  </label>
                  <input
                    className="grow"
                    type="text"
                    placeholder="Nhập thời hạn thanh toán"
                    name="puchase_date"
                    id="puchase_date"
                    value={formik.values.puchase_date}
                    onChange={formik.handleChange}
                  />
                </div>
                {touched.puchase_date && errors.puchase_date ? (
                  <p
                    className="mt-1 text-red-500 text-sm"
                    id="puchase_date-warning"
                  >
                    {errors.puchase_date}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Lý do tạm ứng ======================= */}
              <div className="form-group col-span-full">
                <div className="form-style">
                  <label htmlFor="description">
                    Lý do tạm ứng
                    <span className="text-red-500">*</span>
                  </label>
                  <textarea
                    id="description"
                    name="description"
                    value={formik.values.description}
                    type="text"
                    onChange={formik.handleChange}
                    rows={1}
                  ></textarea>
                </div>
                {touched.description && errors.description ? (
                  <p
                    className="mt-1 text-red-500 text-sm"
                    id="description-warning"
                  >
                    {errors.description}
                  </p>
                ) : (
                  <></>
                )}
              </div>
            </div>
          </div>
        </form>
      </FormikProvider>
    </Modal>
  );
}
