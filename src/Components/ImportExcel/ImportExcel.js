import React, { useState } from "react";
import UploadExcel from "./UploadExcel/UploadExcel";
import ConfirmField from "./ConfirmField/ConfirmField";
import { Modal } from "antd";

export default function ImportExcel({ isModalOpen, handleOk, handleCancel }) {
  const [upload, setUpload] = useState(true);
  const [confirm, setConfirm] = useState(false);
  return (
    <Modal
      title="Nhập file Excel"
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Xác nhận"
      okButtonProps={!confirm ? "hidden" : "btn-main-yl"}
      cancelText="Huỷ"
      centered
      wrapClassName=""
    >
      {upload && <UploadExcel />}
      {confirm && <ConfirmField />}
    </Modal>
  );
}
