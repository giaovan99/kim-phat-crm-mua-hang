import React, { useEffect, useState } from "react";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import Products from "./Products/Products";
import { Collapse } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { IoSaveOutline } from "react-icons/io5";
import { LiaUndoSolid } from "react-icons/lia";
import { CaretRightOutlined } from "@ant-design/icons";
import ProposalInfo from "./Proposalnfo/ProposalInfo";
import {
  createPurchaseRequest,
  getPurchaseRequest,
  getPurchaseRequestStatusList,
  getStaffPurchaseRequest,
  getTypePP,
  updatePurchaseRequest,
} from "../../utils/purchaseRequest";
import { getListProduct, getUnit, vatList } from "../../utils/product";
import { getListUser } from "../../utils/user";
import SetStatus2Step from "../SetStatus2Step/SetStatusProposal";
import { getListSupplier } from "../../utils/supplier";
import ModalProductDetails from "../ProductPage/ListProduct/ModalProductDetails";
import { goToLogin, normalize } from "../../utils/others";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import { useDispatch, useSelector } from "react-redux";

const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

export default function PurchaseProposalDetail() {
  // 1 - Lấy giá trị từ params
  let { proposalKey } = useParams();
  // Khai báo useNavigate để chuyển hướng trang
  let navigate = useNavigate();
  let dispatch = useDispatch();
  const [searchParams] = useSearchParams();

  // 2 - Khai báo state
  const [productsSelected, setProductsSelected] = useState([]); // Quản lý danh sách sản phẩm được chọn
  const [current, setCurrent] = useState(1); // Quản lý vị trí của trạng thái
  const [listStaff, setListStaff] = useState([]); //Danh sách staff bth
  const [listStaffProduct, setListStaffProduct] = useState([]); //Danh sách staff modal sản phẩm
  const [listProducts, setListProducts] = useState([]); //Danh sách sản phẩm
  const [purchaseStatus, setPurchaseStatus] = useState(); //Danh sách trạng thái
  const [listUnit, setListUnit] = useState([]); //Danh sách đơn vị tính
  const [listType, setTypePP] = useState([]); //hiển thị danh sách loại phiếu
  const [reloadNCC, setReloadNCC] = useState(false); //Sản phẩm
  const [reloadProduct, setReloadProduct] = useState(false); //Sản phẩm
  const [listSuppliers, setListSuppliers] = useState([]); //Danh sách nhà cung cấp
  const [isAddProductModalOpen, setIsAddProductModalOpen] = useState(false); // Quản lý ẩn hiện Modal thêm Nhà cung cấp
  const [listTaxCode, setListTaxCode] = useState([]); //Danh sách loại hoá đơn
  const [loadingSelect, setLoadingSelect] = useState(false);

  const initial = {
    items: [],
    proposer: "",
    department: "",
    intended_use: "",
    status: 1,
    type: "",
  };

  // Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      items: yup
        .array()
        .min(1, "Sản phẩm không để trống")
        .test("check-qty", "Số lượng đề nghị không bỏ trống", function (value) {
          if (!value || !value.length) {
            return false; // Mảng sản phẩm trống
          }
          // Kiểm tra từng sản phẩm trong mảng
          for (const product of value) {
            if (!product.qty || product.qty <= 0) {
              return false; // Số lượng thực tế không được để trống
            }
          }
          return true;
        }),
      proposer: yup.string().required("Người đề nghị không để trống"),
      intended_use: yup.string().required("Mục đích sử dụng không để trống"),
      type: yup.string().required("Loại hình không để trống"),
      // purchasing_group: yup.string(),
      // receiving_destination: yup.string(),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      // console.log(values);
      // alert thông báo
      let res;
      let debug = searchParams.get("debug");
      if (proposalKey) {
        // Sửa đơn hàng
        res = await updatePurchaseRequest(values);
        if (res && !debug) {
          navigate(0);
        }
      } else {
        // Tạo đơn hàng
        res = await createPurchaseRequest(values, formik);
        // console.log(res);
        if (res && !debug) {
          navigate(0);
        }
      }
      dispatch(setSpinner(false));
    },
  });

  // 4 - Lấy thông tin phiếu đề nghị mua hàng
  const APIGetPurchaseRequest = async () => {
    dispatch(setSpinner(true));
    let res = await getPurchaseRequest(proposalKey);
    // console.log(res);
    if (res.data.success == true) {
      if (res.data.data.deleted == 0) {
        formik.setValues(res.data.data);
        setProductsSelected(res.data.data.items);
        setCurrent(res.data.data.status * 1);
      } else {
        goToLogin();
      }
    }
    dispatch(setSpinner(false));
  };

  // 5 - Lần đầu khi load trang, kiểm tra params để xác định trang tạo/ sửa phiếu đề nghị mua hàng, lấy danh sách các mảng cố định
  useEffect(() => {
    // Lấy danh sách Stt , staff normal, staff đặt hàng, sản phẩm
    getArr();
    if (proposalKey) {
      // console.log("sửa phiếu đề nghị mua hàng");
      APIGetPurchaseRequest();
    } else {
      // console.log("thêm phiếu đề nghị mua hàng");
      formik.setValues(initial);
    }
  }, []);

  //  6 - Lấy danh sách SP
  const getProduct = async (page = 1, keywords = "") => {
    setLoadingSelect(true);
    let res = await getListProduct(page, {
      keywords: keywords ? keywords : "",
    });
    if (res) {
      setListProducts(res.data);
      setReloadProduct(false);
    }
    setLoadingSelect(false);
  };

  //  6 - Lấy danh sách NCC
  const getSupplier = async (page = 1, keywords = "") => {
    setLoadingSelect(true);
    if (listSuppliers.length === 0) {
      // Lấy danh sách NCC
      let res = await getListSupplier(page, {
        keywords: keywords ? keywords : "",
      });
      if (res) {
        setListSuppliers(res.data);
        setReloadNCC(false);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách Các loại phiếu
  const getListType = async (page) => {
    setLoadingSelect(true);
    if (listType.length === 0) {
      let res = await getTypePP();
      // console.log(res);
      if (res.status) {
        setTypePP(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách Các loại VAT
  const getListTaxCode = async (page) => {
    setLoadingSelect(true);
    if (listTaxCode.length === 0) {
      let res = await vatList();
      if (res.status) {
        setListTaxCode(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách Các loại ĐVT
  const getListUnit = async (page) => {
    setLoadingSelect(true);
    if (listUnit.length === 0) {
      let res = await getUnit();
      if (res.status) {
        setListUnit(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách nhân viên mua hàng
  const getListStaffProduct = async (page) => {
    setLoadingSelect(true);
    if (listStaffProduct.length === 0) {
      let res = await getListUser(1, { department: 4 });
      if (res.status) {
        const transformedArray = res.data.data.map((item) => ({
          value: item.id,
          label: item.full_name,
        }));
        setListStaffProduct(transformedArray);
      }
    }
    setLoadingSelect(false);
  };

  //  6 - Lấy tất cả các mảng cố định
  let getArr = async (page) => {
    dispatch(setSpinner(true));
    let res;

    // 5.6 - Danh sách nhân viên đề nghị
    res = await getStaffPurchaseRequest();
    if (res.status) {
      setListStaff(res.data);
    }

    // 5.10 - Danh sách trạng thái
    res = await getPurchaseRequestStatusList();
    // console.log(res);
    if (res.status) {
      setPurchaseStatus(res.data);
    }
    dispatch(setSpinner(false));
  };

  //  8 -  Hiển thị modal thêm sp
  const showAddProductModal = () => {
    setIsAddProductModalOpen(true);
  };

  // 10 - Tải lại Product khi reloadProduct == true
  useEffect(() => {
    if (reloadProduct) {
      getProduct();
    }
  }, [reloadProduct]);

  //  7 -  Trở về trang Đề nghị mua hàng
  const goToProposalPurchase = () => {
    navigate("/purchase_proposal");
  };

  // 8 - Khi danh sách sản phẩm đc chọn thay đổi thì cập nhật [productSelected]
  useEffect(() => {
    formik.setFieldValue("items", productsSelected);
  }, [productsSelected]);

  //  9 - Khi current thay đổi
  useEffect(() => {
    formik.values.status = current;
  }, [current]);

  // Khi loại phiếu thay đổi thì tất cả item reset "PO, SKU, Master Cast Part" = null
  useEffect(() => {
    let arr = [...productsSelected];
    arr.forEach((item) => {
      item.po_export_code = null;
      item.po_export_sku = null;
      item.po_export_pack = null;
    });
    setProductsSelected(arr);
  }, [formik.values.type]);

  // 10 - Danh sách các Tabs
  const getItems = [
    {
      key: "orderInfo",
      label: (
        <h2 className="text-[1rem] font-medium mb-1">
          Thông tin phiếu đề nghị
        </h2>
      ),
      children: (
        <ProposalInfo
          staffOptions={listStaff}
          formik={formik}
          filterOption={filterOption}
          current={current}
          proposalKey={proposalKey}
          listType={listType}
          getListType={getListType}
          loadingSelect={loadingSelect}
        />
      ),
    },
    {
      key: "products",
      label: <h2 className="text-[1rem] font-medium mb-1">Sản phẩm</h2>,
      children: (
        <Products
          productOptions={listProducts}
          formik={formik}
          filterOption={filterOption}
          productsSelected={productsSelected}
          setProductsSelected={setProductsSelected}
          unitOptions={listUnit}
          buyerOptions={listStaff}
          current={current}
          proposalKey={proposalKey}
          showAddProductModal={showAddProductModal}
          getProduct={getProduct}
          loadingSelect={loadingSelect}
          getListUnit={getListUnit}
        />
      ),
    },
  ];
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );

  return (
    <>
      {/* ======================= Content ======================= */}
      <div className="page-content">
        {/* ======================= Trạng thái  ======================= */}
        {proposalKey && (
          <div className="form-group col-span-3 mb-5">
            <div className="form-style">
              <label htmlFor="contractSigningDate" className="mb-2">
                Trạng thái
              </label>
              <SetStatus2Step
                statusOptions={purchaseStatus}
                current={current}
                setCurrent={setCurrent}
                page={"purchaseProposal"}
                permissionUpdate={permission?.purchase_proposal?.update}
              />
            </div>
          </div>
        )}
        {/* ======================= Tabs lựa chọn ======================= */}
        <Collapse
          bordered={true}
          defaultActiveKey={["orderInfo", "products"]}
          expandIcon={({ isActive }) => (
            <CaretRightOutlined rotate={isActive ? 90 : 0} />
          )}
          items={getItems}
        />
        {/* ======================= warning ======================= */}
        <div className="mt-5">
          <p className="text-[#17a2b8]" id="warning">
            {Object.keys(formik.errors).length > 0
              ? "Biểu mẫu chưa hoàn chỉnh thông tin."
              : ""}
          </p>
        </div>
        {/* ======================= btns ======================= */}
        <div className="flex gap-3 mt-5 justify-between">
          <button
            className="btn-actions btn-main-bl"
            onClick={() => {
              formik.handleSubmit();
            }}
          >
            <IoSaveOutline />
            Lưu
          </button>
          <button
            className="btn-actions btn-main-dark"
            onClick={goToProposalPurchase}
            type="button"
          >
            <LiaUndoSolid />
            Huỷ
          </button>
        </div>
        {/* ======================= Modals ======================= */}
        {/* Model thêm sản phẩm mới */}
        <div className="modals">
          <ModalProductDetails
            isModalOpen={isAddProductModalOpen}
            setIsModalOpen={setIsAddProductModalOpen}
            product={null}
            listUnit={listUnit}
            listStaff={listStaffProduct}
            listVAT={listTaxCode}
            listNCC={listSuppliers}
            setProduct={setReloadProduct}
            setReload={setReloadProduct}
            deletedPage={false}
            getListTaxCode={getListTaxCode}
            loadingSelect={loadingSelect}
            getSupplier={getSupplier}
            getListUnit={getListUnit}
            getListStaffProduct={getListStaffProduct}
          />
        </div>
      </div>
    </>
  );
}
