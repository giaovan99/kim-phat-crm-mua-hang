import { Select } from "antd";
import React, { useState } from "react";
import { getOrder } from "../../../utils/order";
import ModalListProduct from "../../Warehouse/ImportWarehouse/ListOrders/Components/ModalListProducts/ModalListProduct";

export default function OrderPP({
  filterOption,
  productsSelected,
  setProductsSelected,
  current,
  orderOptions,
}) {
  const [orderChoosen, setOrderChoosen] = useState();
  let [listProductOrder, setListProductOrder] = useState([]); //danh sách sản phẩm được chọn
  let [isAddProductsModal, setIsAddProductsModal] = useState(false); //hiển thị Modal danh sách sản phẩm của đơn hàng được chọn

  const onSearch = (value) => {
    // console.log("search:", value);
  };

  // Khi chọn sản phẩm
  const handleOkAddProducts = (value) => {
    setIsAddProductsModal(false);
    let arr = [...productsSelected];
    // console.log(value);
    value.map((pro) => {
      // Check xem sản phẩm đã có trong danh sách chọn chưa = cách tìm id trùng
      let index = productsSelected.findIndex(
        (item) => item.product_id == pro.product_id
      );
      //  Nếu chưa có trong danh sách đã chọn, thì tạo 1 object sản phẩm có thêm key số lượng + số lượng thực tế
      if (index == -1) {
        let newOption = { ...pro };
        arr.push(newOption);
      }
    });
    setProductsSelected(arr);
  };
  // console.log(productsSelected);

  // Khi huỷ chọn sản phẩm
  const handleCancelAddProducts = () => {
    setIsAddProductsModal(false);
  };

  // 10 - Lấy thông tin chi tiết đơn hàng
  const getOrderInfo = async (id) => {
    let res = await getOrder(id);
    if (res.data.success) {
      return res.data.data;
    } else {
      return false;
    }
  };

  // 11 - Lọc lại các key cần thiết của
  const getItemsArr = (arr, order) => {
    let newArr = [];
    arr.map((item) => {
      newArr.push({
        product_id: item.product_id,
        product_name: item.product_name,
        sku: item.sku,
        unit: item.unit,
        buyer_id: null,
        qty: 1,
        specifications: null,
        did: null,
        po_id: order.id,
        po_code: order.code,
        purchase_date: null,
        qty_inventory: null,
        qty_purchase: 1,
      });
    });
    return newArr;
  };

  // 12 - Khi mã đơn hàng thay đổi => kéo api chi tiết đơn để lấy đc danh sách sản phẩm
  const onChangeOrder = async (value) => {
    // console.log(value);
    // // Lấy thông tin chi tiết của đơn hàng
    setOrderChoosen(value);
    let order = await getOrderInfo(value);
    if (order) {
      let items = [...getItemsArr(order.items, order)];
      setListProductOrder(items);
    }
  };

  return (
    <div className="productsAddPage">
      <label htmlFor="items" className="font-medium">
        Chọn đơn hàng <span className="text-red-500">*</span>
      </label>
      {/* ======================= Sản phẩm ======================= */}
      <div
        className={
          current > 1
            ? "pointer-events-none form-group mb-2"
            : "form-group mb-2 flex gap-2 items-center"
        }
      >
        <div className="form-style">
          <Select
            showSearch
            placeholder="Chọn đơn hàng"
            optionFilterProp="children"
            onChange={onChangeOrder}
            onSearch={onSearch}
            filterOption={filterOption}
            options={orderOptions}
          />
        </div>
        {orderChoosen && (
          <button
            type="button"
            className="btn-actions btn-main-bl"
            onClick={() => setIsAddProductsModal(true)}
          >
            Chọn sản phẩm từ đơn hàng
          </button>
        )}
      </div>
      {/* ======================= Modals ======================= */}
      {isAddProductsModal && (
        <ModalListProduct
          handleOkAddProducts={handleOkAddProducts}
          handleCancelAddProducts={handleCancelAddProducts}
          data={listProductOrder}
          isModalOpen={isAddProductsModal}
          price={false}
        />
      )}
    </div>
  );
}
