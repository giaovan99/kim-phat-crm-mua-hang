import { Select, Spin } from "antd";
import React from "react";
import { Field, FormikProvider } from "formik";
import moment from "moment";
import { stringToDate } from "../../../utils/others";
import { getUser } from "../../../utils/user";

export default function ProposalInfo({
  staffOptions,
  formik,
  filterOption,
  current,
  departmentOptions,
  proposalKey,
  listType,
  getDepartment,
  getListType,
  loadingSelect,
}) {
  const { handleBlur, handleChange, touched, errors } = formik;
  const onSearch = (value) => {
    // console.log("search:", value);
  };

  const onChangeSelect = async (tag, value) => {
    formik.setFieldValue(tag, value);
    if (tag === "proposer") {
      let res = await getUser(value);
      if (res.data.success) {
        formik.setFieldValue(
          "department",
          res.data.data?.department ? res.data.data.department : ""
        );
        formik.setFieldValue(
          "department_name",
          res.data.data?.position ? res.data.data.position : ""
        );
      }
    }
  };

  return (
    <div className="orderInfo">
      <FormikProvider value={formik}>
        <div
          className={
            current > 1
              ? "pointer-events-none grid md:grid-cols-2 xl:grid-cols-4 gap-2"
              : "grid md:grid-cols-2 xl:grid-cols-4 gap-2"
          }
        >
          {/* ======================= Mã phiếu đề nghị ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="code">Mã phiếu đề nghị</label>
              <input
                className="grow"
                type="text"
                placeholder="Hệ thống tự tạo"
                name="code"
                id="code"
                value={formik.values.code ? formik.values.code : ""}
                disabled
              />
            </div>
            {touched.code && errors.code ? (
              <p className="mt-1 text-red-500 text-sm" id="code-warning">
                {errors.code}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Ngày tạo ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="created_at">Ngày tạo</label>
              <input
                className="grow"
                type="text"
                placeholder="Hệ thống tự tạo"
                name="created_at"
                id="created_at"
                value={
                  proposalKey
                    ? stringToDate(formik.values.created_at)
                    : moment().format("yyyy-MM-DD")
                }
                disabled
              />
            </div>
            {touched.created_at && errors.created_at ? (
              <p className="mt-1 text-red-500 text-sm" id="created_at-warning">
                {errors.created_at}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Người đề nghị ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="proposer">
                Người đề nghị <span className="text-red-500">*</span>
              </label>
              <Field name="proposer">
                {({ field }) => {
                  return (
                    <Select
                      id="proposer"
                      showSearch
                      placeholder="Chọn người đề nghị"
                      optionFilterProp="children"
                      onChange={(value) => {
                        onChangeSelect("proposer", value);
                      }}
                      onSearch={onSearch}
                      filterOption={filterOption}
                      options={staffOptions}
                      value={
                        formik.values.proposer != ""
                          ? formik.values.proposer
                          : null
                      }
                      onBlur={handleBlur}
                    />
                  );
                }}
              </Field>
            </div>
            {touched.proposer && errors.proposer ? (
              <p className="mt-1 text-red-500 text-sm" id="proposer-warning">
                {errors.proposer}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Bộ phận ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="department">Bộ phận</label>
              <input
                className="grow"
                type="text"
                placeholder="Hệ thống tự render"
                name="department_name"
                id="department_name"
                value={
                  formik.values.department_name
                    ? formik.values.department_name
                    : ""
                }
                disabled
              />
            </div>
            {touched.department && errors.department ? (
              <p className="mt-1 text-red-500 text-sm">{errors.department}</p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Loại phiếu ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="type">
                Loại phiếu <span className="text-red-500">*</span>
              </label>
              <Field name="type">
                {({ field }) => {
                  return (
                    <Select
                      showSearch
                      placeholder="Chọn người đề nghị"
                      optionFilterProp="children"
                      onChange={(value) => onChangeSelect("type", value)}
                      onSearch={onSearch}
                      filterOption={filterOption}
                      options={listType}
                      value={
                        listType.length <= 0 && formik.values.type_name
                          ? formik.values.type_name
                          : formik.values.type != ""
                          ? formik.values.type
                          : null
                      }
                      onBlur={handleBlur}
                      onDropdownVisibleChange={getListType}
                      notFoundContent={
                        loadingSelect ? <Spin size="small" /> : null
                      }
                    />
                  );
                }}
              </Field>
            </div>
            {touched.type && errors.type ? (
              <p className="mt-1 text-red-500 text-sm">{errors.type}</p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Mục đích sử dụng ======================= */}
          <div className="form-group col-span-full">
            <div className="form-style">
              <label htmlFor="intended_use">
                Mục đích sử dụng <span className="text-red-500">*</span>
              </label>
              <textarea
                className="grow"
                type="text"
                placeholder="Nhập mục đích sử dụng"
                name="intended_use"
                id="intended_use"
                value={formik.values.intended_use}
                onChange={handleChange}
                onBlur={handleBlur}
              ></textarea>
            </div>
            {touched.intended_use && errors.intended_use ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="intended_use-warning"
              >
                {errors.intended_use}
              </p>
            ) : (
              <></>
            )}
          </div>
          <div className="col-span-full grid grid-cols-2 gap-2">
            {/* ======================= Nhóm hàng hoá ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="type">Nhóm hàng hoá</label>
                <textarea
                  className="form-control "
                  name="purchasing_group"
                  type="text"
                  value={
                    formik.values.purchasing_group
                      ? formik.values.purchasing_group
                      : ""
                  }
                  onChange={handleChange}
                ></textarea>
              </div>
              {touched.type && errors.type ? (
                <p className="mt-1 text-red-500 text-sm">
                  {errors.purchasing_group}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Nơi nhận hàng ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="type">Nơi nhận hàng</label>
                <textarea
                  className="form-control "
                  type="text"
                  name="receiving_destination"
                  value={
                    formik.values.receiving_destination
                      ? formik.values.receiving_destination
                      : ""
                  }
                  onChange={handleChange}
                ></textarea>
              </div>
              {touched.type && errors.type ? (
                <p className="mt-1 text-red-500 text-sm">
                  {errors.receiving_destination}
                </p>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </FormikProvider>
    </div>
  );
}
