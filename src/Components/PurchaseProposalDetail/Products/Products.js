import { HiOutlineTrash } from "react-icons/hi2";
import { RxUpdate } from "react-icons/rx";

import { Popconfirm, Select, Spin, Table, Tooltip, message } from "antd";
import React, { useCallback, useEffect, useState } from "react";
import Swal from "sweetalert2";
import { checkNumber, debounce } from "../../../utils/others";
import InputNumberFormat from "../../Input/InputNumberFormat";
import { HiOutlinePlusCircle } from "react-icons/hi";
import ImportFilesComponent from "../../ImportFilesComponents/ImportFilesComponents";
import ImportExcelBtnComponent from "../../ButtonComponent/ImportExcelBtnComponent";
import { getProductDetails } from "../../../utils/product";
import { useDispatch } from "react-redux";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import InputDatePicker from "../../Input/InputDatePicker";

export default function Products({
  productOptions,
  formik,
  filterOption,
  productsSelected,
  setProductsSelected,
  unitOptions,
  buyerOptions,
  current,
  proposalKey,
  showAddProductModal,
  getProduct,
  loadingSelect,
  getListUnit,
}) {
  const [isViewImportFile, setIsViewImportFile] = useState(false);
  const [reload, setReload] = useState(false);
  const [addProductExcel, setAddProductExcel] = useState([]);
  let dispatch = useDispatch();

  const tableProps = {
    bordered: true,
    size: "small",
  };
  const { handleBlur, touched, errors } = formik;

  const onSearch = (value) => {};

  const debouncedFetchOptions = useCallback(
    debounce(async (page, keywords) => {
      getProduct(1, keywords);
    }, 300),
    []
  );

  const confirm = (index) => {
    let arr = [...productsSelected];
    arr.splice(index, 1);
    setProductsSelected([...arr]);
    // message.success("Xoá thành công");
    Swal.fire({
      title: "Xoá",
      text: "Xoá thành công",
      icon: "success",
      confirmButtonText: "Đóng",
    });
  };

  const confirmUpdate = async (index) => {
    dispatch(setSpinner(true));
    let arr = [...productsSelected];

    // Gọi API lấy thông tin sp (tên, dvt)
    let res = await getProductDetails(arr[index].product_id);
    // Cập nhật tên + dvt + quy cách sp của sp vào mảng
    if (res.data.success) {
      arr[index].product_name = res.data.data?.name ? res.data.data.name : "";
      arr[index].unit = res.data.data?.unit ? res.data.data.unit : "";
      arr[index].specifications = res.data.data?.package_case
        ? res.data.data.package_case
        : "";
    }
    setProductsSelected([...arr]);
    dispatch(setSpinner(false));
    message.success("Làm mới thành công");
    Swal.fire({
      title: "Làm mới thông tin sản phẩm",
      text: "Thành công",
      icon: "success",
      confirmButtonText: "Đóng",
    });
  };

  const cancel = (e) => {
    // console.log(e);
    message.error("Đã huỷ");
  };

  // Khi chọn sản phẩm
  const onChangeProductSelected = (value, option) => {
    let newOption = {
      product_id: option.id,
      sku: option.sku,
      product_name: option.name,
      qty: 0,
      unit: option.unit,
      buyer_id: "",
      specifications: option.package_case,
      did: "",
      po_id: "",
      po_code: "",
      purchase_date: "",
      delivery_date: null,
      qty_inventory: 0,
      qty_purchase: "",
      po_export_code: "",
      po_export_sku: "",
      po_export_pack: "",
      note: "",
    };
    setProductsSelected([...productsSelected, newOption]);
  };

  // Hàm tìm vị trí
  const fFindIndex = (record, arr) => {
    let index = arr.findIndex(
      (element) => element.product_id == record.product_id
    );
    return index;
  };

  // Hàm xử lý thay đổi số lượng đề nghị phần tử của mảng
  const fChangeNumberQty = (value, index) => {
    let arr = [...productsSelected];
    arr[index].qty = checkNumber(value);
    fChangeNumberPurchase(index);
    setProductsSelected([...arr]);
  };
  // Hàm xử lý thay đổi số lượng tồn kho phần tử của mảng
  const fChangeNumberInventory = (value, index) => {
    let arr = [...productsSelected];
    arr[index].qty_inventory = Math.abs(value);
    fChangeNumberPurchase(index);
    setProductsSelected([...arr]);
  };
  // Hàm xử lý thay đổi số lượng cần đặt phần tử của mảng
  const fChangeNumberPurchase = (index) => {
    let arr = [...productsSelected];
    let number = arr[index]?.qty - arr[index]?.qty_inventory;
    arr[index].qty_purchase = number;
    setProductsSelected([...arr]);
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      width: 50,
      fixed: "left",
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Mã sản phẩm",
      dataIndex: "sku",
      key: "sku",
      width: 110,
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
      fixed: "left",
      width: 250,
    },
    {
      title: "Quy cách",
      dataIndex: "specifications",
      key: "specifications",
      render: (text, record, index) => {
        return (
          <div className="form-group">
            <div className="form-style">
              <textarea
                className="text-left w-full"
                id={record.key}
                name={record.specifications}
                value={record.specifications}
                type="text"
                rows={1}
                disabled={current > 1 ? true : false}
                onChange={(event) => {
                  let arr = [...productsSelected];
                  // Gắn giá trị mới vào mảng
                  arr[index].specifications = event.target.value;
                  setProductsSelected([...arr]);
                }}
              ></textarea>
            </div>
          </div>
        );
      },
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 120,
      render: (text, record, index) => {
        // console.log(record);
        return (
          <Select
            name="unit"
            showSearch
            placeholder="Chọn đơn vị tính"
            optionFilterProp="children"
            className={current > 1 ? "pointer-events-none w-full" : "w-full"}
            onChange={(value, event) => {
              let arr = [...productsSelected];
              arr[index].unit = value;
              setProductsSelected([...arr]);
            }}
            disabled={current > 1 ? true : false}
            value={record.unit}
            onSearch={onSearch}
            filterOption={filterOption}
            options={unitOptions}
            onDropdownVisibleChange={getListUnit}
            notFoundContent={loadingSelect ? <Spin size="small" /> : null}
          />
        );
      },
    },
    {
      title: "Đơn hàng",
      children: [
        {
          title: "PO",
          dataIndex: "po_export_code",
          key: "po_export_code",
          width: formik.values.type == 2 ? 120 : 0,
          render: (text, record, index) => {
            return (
              <div className="form-group ">
                <div className="form-style">
                  <input
                    id="po_export_code"
                    name="po_export_code"
                    type="text"
                    className="w-full"
                    disabled={current > 1 ? true : false}
                    onChange={(event) => {
                      let arr = [...productsSelected];
                      arr[index].po_export_code = event.target.value;
                      setProductsSelected([...arr]);
                    }}
                    value={record.po_export_code}
                  />
                </div>
              </div>
            );
          },
        },
        {
          title: "SKU",
          dataIndex: "po_export_sku",
          key: "po_export_sku",
          width: formik.values.type == 2 ? 120 : 0,
          render: (text, record, index) => {
            return (
              <div className="form-group ">
                <div className="form-style">
                  <input
                    id="po_export_sku"
                    name="po_export_sku"
                    type="text"
                    disabled={current > 1 ? true : false}
                    className="w-full"
                    onChange={(event) => {
                      let arr = [...productsSelected];
                      arr[index].po_export_sku = event.target.value;
                      setProductsSelected([...arr]);
                    }}
                    value={record.po_export_sku}
                  />
                </div>
              </div>
            );
          },
        },
        {
          title: "Master Case Pack",
          dataIndex: "po_export_pack",
          key: "po_export_pack",
          width: formik.values.type == 2 ? 120 : 0,
          render: (text, record, index) => {
            return (
              <div className="form-group ">
                <div className="form-style">
                  <input
                    id="po_export_pack"
                    name="po_export_pack"
                    type="text"
                    className="w-full"
                    disabled={current > 1 ? true : false}
                    onChange={(event) => {
                      let arr = [...productsSelected];
                      arr[index].po_export_pack = event.target.value;
                      setProductsSelected([...arr]);
                    }}
                    value={record.po_export_pack}
                  />
                </div>
              </div>
            );
          },
        },
      ],
    },
    {
      title: "Số lượng",
      children: [
        {
          title: "Đề nghị",
          dataIndex: "qty",
          key: "qty",
          width: 130,
          render: (text, record, index) => {
            return (
              <InputNumberFormat
                className="text-center w-full"
                value={record.qty}
                onChange={fChangeNumberQty}
                index={index}
                disable={current > 1 ? true : false}
              />
            );
          },
        },
        {
          title: "Tồn kho",
          dataIndex: "qty_inventory",
          key: "qty_inventory",
          width: 130,
          render: (text, record, index) => {
            return (
              <InputNumberFormat
                className="text-center w-full"
                value={record.qty_inventory}
                onChange={fChangeNumberInventory}
                index={index}
                disable={current > 1 ? true : false}
              />
            );
          },
        },
        {
          title: "Cần đặt",
          dataIndex: "qty_purchase",
          key: "qty_purchase",
          width: 130,
          render: (text, record, index) => {
            return (
              <InputNumberFormat
                className="text-center w-full"
                value={record.qty_purchase}
                onChange={() => {}}
                index={index}
                disable={true}
                allowNegative={true}
              />
            );
          },
        },
      ],
    },
    {
      title: "Chỉ định nhân viên mua hàng",
      dataIndex: "buyer_id",
      key: "buyer_id",
      width: 170,
      render: (text, record, index) => {
        return (
          <Select
            disabled={current > 1 ? true : false}
            id="buyer_id"
            name="buyer_id"
            showSearch
            placeholder="Chọn nhân viên mua hàng"
            optionFilterProp="children"
            className="w-full"
            onChange={(value, event) => {
              let arr = [...productsSelected];
              arr[index].buyer_id = value;
              setProductsSelected([...arr]);
            }}
            value={record.buyer_id}
            onSearch={onSearch}
            filterOption={filterOption}
            options={buyerOptions}
          />
        );
      },
    },
    {
      title: "Ngày cần giao",
      dataIndex: "delivery_date",
      key: "delivery_date",
      width: 170,
      render: (text, record, index) => {
        return (
          <div className="form-group ">
            <div className="form-style">
              <InputDatePicker
                field={record.delivery_date}
                onChange={(dateString) => {
                  let arr = [...productsSelected];
                  arr[index].delivery_date = dateString;
                  setProductsSelected([...arr]);
                }}
                disabled={current > 1 ? true : false}
              />
            </div>
          </div>
        );
      },
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      // width: 170,
      render: (text, record, index) => {
        return (
          <div className="form-group ">
            <div className="form-style">
              <textarea
                className="w-full"
                name={record.note}
                value={record.note}
                type="text"
                rows={1}
                onChange={(event) => {
                  let arr = [...productsSelected];
                  // Gắn giá trị mới vào mảng
                  arr[index].note = event.target.value;
                  setProductsSelected([...arr]);
                }}
                disabled={current > 1 ? true : false}
              ></textarea>
            </div>
          </div>
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 80,
      fixed: "right",
      render: (text, record, index) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Xoá ======================= */}
            <Popconfirm
              placement="topRight"
              title="Xoá sản phẩm"
              description="Bạn chắc chắn muốn xoá sản phẩm này?"
              onConfirm={() => confirm(index)}
              onCancel={cancel}
              okText="Xoá"
              cancelText="Huỷ"
              okButtonProps={{ className: "btn-main-yl" }}
              className="popover-danger"
            >
              <Tooltip placement="bottom" title="Xoá">
                <button>
                  <HiOutlineTrash className="text-rose-600" />
                </button>
              </Tooltip>
            </Popconfirm>
            {/* ======================= Refresh sản phẩm ======================= */}
            {proposalKey && (
              <Popconfirm
                placement="topRight"
                title="Làm mới thông tin"
                description="Bạn chắc chắn muốn làm mới thông tin sản phẩm này?"
                onConfirm={() => confirmUpdate(index)}
                onCancel={cancel}
                okText="Làm mới"
                cancelText="Huỷ"
                okButtonProps={{ className: "btn-main-yl" }}
                className="popover-danger"
              >
                <Tooltip placement="bottom" title="Làm mới thông tin">
                  <button>
                    <RxUpdate className="text-blue-600" />
                  </button>
                </Tooltip>
              </Popconfirm>
            )}
          </div>
        );
      },
    },
  ];

  // Thêm sản phẩm từ Excel
  // Nhập file sản phẩm từ Excel
  const showImportFile = () => {
    setIsViewImportFile(true);
  };

  // Khi danh sách sản phẩm từ file excel thay đổi thì cộng mảng vô danh sách sp chính
  useEffect(() => {
    let oldArr = [...productsSelected];
    let newwArr = [];
    // console.log("addProductExcel", addProductExcel);
    addProductExcel.forEach((product) => {
      const data = {
        product_id: product?.product_id ? product.product_id : "",
        sku: product?.sku ? product.sku : "",
        product_name: product?.product_name ? product.product_name : "",
        specifications: product?.specifications ? product.specifications : "",
        unit: product?.unit ? product.unit : "",
        po_export_code: product?.po_export_code ? product.po_export_code : "",
        po_export_sku: product?.po_export_sku ? product.po_export_sku : "",
        po_export_pack: product?.po_export_pack ? product.po_export_pack : "",
        qty: product?.qty ? product.qty : "",
        qty_inventory: product?.qty_inventory ? product.qty_inventory : "",
        qty_purchase: product?.qty_purchase ? product.qty_purchase : "",
        buyer_id: product?.buyer_id ? product.buyer_id : "",
        purchase_date: product?.purchase_date ? product.purchase_date : "",
        delivery_date: product?.delivery_date ? product.delivery_date : "",
        note: product?.note ? product.note : "",
      };
      newwArr.push(data);
    });

    setProductsSelected([...oldArr, ...newwArr]);
  }, [addProductExcel]);

  return (
    <div className="productsAddPage">
      <label htmlFor="items" className="w-full font-medium">
        Chọn sản phẩm <span className="text-red-500">*</span>
      </label>
      {/* ======================= Sản phẩm ======================= */}
      <div
        className={
          current > 1
            ? " pointer-events-noneform-group flex items-start gap-2  mb-2"
            : "form-group flex items-start gap-2  mb-2"
        }
      >
        <div className="flex-grow">
          <div className="form-style">
            <Select
              id="items"
              name="items"
              showSearch
              placeholder="Chọn sản phẩm"
              optionFilterProp="children"
              onChange={onChangeProductSelected}
              onSearch={(value) => value && debouncedFetchOptions(1, value)}
              filterOption={filterOption}
              options={productOptions}
              value={formik.values.items !== "" ? formik.values.items : null}
              onBlur={handleBlur}
              onDropdownVisibleChange={(open) =>
                open && getProduct(1, "", true)
              }
              notFoundContent={loadingSelect ? <Spin size="small" /> : null}
            />
          </div>
          {touched.items && errors.items ? (
            <p className="mt-1 text-red-500 text-sm" id="items-warning">
              {errors.items}
            </p>
          ) : (
            <></>
          )}
        </div>
        <div className="flex flex-shirk gap-2">
          {/* ======================= Btn thêm nhanh sản phẩm ======================= */}
          <Tooltip placement="bottom" title="Thêm nhanh sản phẩm">
            <button
              className="btn-actions btn-main-bl"
              onClick={showAddProductModal}
            >
              <HiOutlinePlusCircle />
            </button>
          </Tooltip>
          {/* ======================= Btn import từ file excel ======================= */}
          <ImportExcelBtnComponent
            deleted={false}
            onClickF={() => showImportFile()}
            text={"Thêm sản phẩm từ file Excel"}
          />
        </div>
      </div>
      {/* ======================= Danh sách sản phẩm ======================= */}
      <Table
        {...tableProps}
        pagination={{
          position: ["none", "bottomCenter"],
          pageSize: 20,
          hideOnSinglePage: true,
        }}
        scroll={{
          y: 600,
          x: 1800,
        }}
        columns={columns}
        dataSource={formik.values.items}
      />
      {isViewImportFile && (
        <ImportFilesComponent
          isModalOpen={isViewImportFile}
          setIsModalOpen={setIsViewImportFile}
          confirmFileStyle={2}
          title={"Import file sản phẩm"}
          module={"purchase_proposal_details"}
          setReload={setReload}
          setAddProductExcel={setAddProductExcel}
        />
      )}
    </div>
  );
}
