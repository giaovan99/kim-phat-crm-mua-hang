import { Select, Spin, Tooltip } from "antd";
import React, { useCallback } from "react";
import { HiOutlinePlusCircle } from "react-icons/hi";
import moment from "moment";
import { Field, FormikProvider } from "formik";
import { debounce, stringToDate } from "../../../utils/others";
import InputPhone from "../../Input/InputPhone";
import { getUser } from "../../../utils/user";
import { getWarehouse } from "../../../utils/warehouse";
import InputDatePicker from "../../Input/InputDatePicker";

export default function OrderInfo({
  supplierOptions,
  userOptions,
  paymentMethodOptions,
  shippingMethodOptions,
  companyOptions,
  formik,
  filterOption,
  showAddSupplierModal,
  warehouseOptions,
  current,
  listInvoiceRequest,
  setN,
  onChangePP,
  listPurchaseProposal,
  getSupplier,
  loadingSelect,
  getCompany,
  getListWarehouse,
  getListShippingMethod,
  getListPaymentMethod,
  getListInvoiceRequest,
}) {
  const { handleBlur, handleChange, touched, errors } = formik;
  const onSearch = (value) => {};

  const debouncedFetchOptions = useCallback(
    debounce(async (page, keywords) => {
      getSupplier(1, keywords);
    }, 300),
    []
  );

  const onChangeSelect = async (value, tag) => {
    // console.log(value);
    formik.setFieldValue(tag, value);
    let index;
    // Nếu tag == "supplier_id" thì check để gắn địa chỉ & người liên hệ & sdt người liên hệ tự động
    if (tag === "supplier_id") {
      index = supplierOptions.findIndex((supplier) => supplier.id == value);
      formik.setFieldValue("supplier_address", supplierOptions[index].address);
      formik.setFieldValue("supplier_code", supplierOptions[index].code);
      formik.setFieldValue(
        "supplier_contact_name",
        supplierOptions[index].contact_name
      );
      formik.setFieldValue(
        "supplier_contact_phone",
        supplierOptions[index].contact_phone
      );
    } else if (tag === "storekeepers") {
      let res = await getUser(value);
      if (res.data.success) {
        formik.setFieldValue("storekeepers_phone", res.data.data.phone);
      }
    } else if (tag === "warehouse") {
      let res = await getWarehouse(value);
      if (res.data.success) {
        formik.setFieldValue("storekeepers", res.data.data.storekeeper_name);
        formik.setFieldValue(
          "storekeepers_phone",
          res.data.data.storekeeper_phone
        );
      }
    }
  };
  console.log(formik.values.items);

  return (
    <div className="orderInfo">
      <FormikProvider value={formik}>
        <div
          className={
            current > 1
              ? "pointer-events-none grid md:grid-cols-2 xl:grid-cols-3 gap-2"
              : "grid md:grid-cols-2 xl:grid-cols-3 gap-2"
          }
        >
          {/* ======================= Mã đơn hàng ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="code">Mã đơn hàng</label>
              <input
                className="grow "
                type="text"
                placeholder="Hệ thống tự tạo"
                name="code"
                id="id"
                value={formik.values.code ? formik.values.code : ""}
                disabled
              />
            </div>
            {touched.code && errors.code ? (
              <p className="mt-1 text-red-500 text-sm" id="id-warning">
                {errors.code}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Ngày tạo ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="created_at">Ngày tạo</label>
              <input
                className="grow"
                type="text"
                placeholder="Hệ thống tự tạo"
                name="created_at"
                id="created_at"
                value={
                  formik.values.created_at
                    ? stringToDate(formik.values.created_at)
                    : moment().format("yyyy-MM-DD")
                }
                disabled
              />
            </div>
            {touched.created_at && errors.created_at ? (
              <p className="mt-1 text-red-500 text-sm" id="created_at-warning">
                {errors.created_at}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Người tạo ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="creator">Người tạo</label>
              <input
                className="grow"
                type="text"
                placeholder="Hệ thống tự tạo"
                name="creator"
                id="creator"
                value={formik.values?.created_by_name || ""}
                disabled
              />
            </div>
          </div>
          {/* ======================= Bên mua ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="company_id">
                Bên mua <span className="text-red-500">*</span>
              </label>
              <Field name="company_id">
                {({ field }) => {
                  return (
                    <Select
                      id="company_id"
                      name="company_id"
                      showSearch
                      placeholder="Chọn công ty mua hàng"
                      optionFilterProp="children"
                      onChange={(value) => onChangeSelect(value, "company_id")}
                      onSearch={onSearch}
                      filterOption={filterOption}
                      options={companyOptions}
                      value={
                        companyOptions.length <= 0 && formik.values.company_name
                          ? formik.values.company_name
                          : formik.values.company_id != ""
                          ? formik.values.company_id
                          : ""
                      }
                      onBlur={handleBlur}
                      onDropdownVisibleChange={getCompany}
                      notFoundContent={
                        loadingSelect ? <Spin size="small" /> : null
                      }
                    />
                  );
                }}
              </Field>
            </div>

            {touched.company_id && errors.company_id ? (
              <p className="mt-1 text-red-500 text-sm" id="company_id-warning">
                {errors.company_id}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Mã phiếu đề nghị mua hàng ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="pp_id">Mã phiếu đề nghị mua hàng </label>
              <Field name="pp_id">
                {({ field }) => {
                  return (
                    <Select
                      {...field}
                      name="pp_id"
                      showSearch
                      placeholder="Chọn phiếu đề nghị mua hàng"
                      optionFilterProp="children"
                      onChange={(value) => {
                        setN(1);
                        onChangePP(value);
                      }}
                      onSearch={onSearch}
                      filterOption={filterOption}
                      options={listPurchaseProposal}
                      onBlur={handleBlur}
                      value={formik.values.pp_id || ""}
                    />
                  );
                }}
              </Field>
            </div>
            {touched.pp_id && errors.pp_id ? (
              <p className="mt-1 text-red-500 text-sm" id="pp_id-warning">
                {errors.pp_id}
              </p>
            ) : (
              <></>
            )}
          </div>
          <div className="md:hidden xl:block"></div>
          {/* ======================= Nhà cung cấp ======================= */}
          <div className="form-group">
            <div className="form-group ">
              <div className="form-style">
                <label htmlFor="supplier_id">
                  Nhà cung cấp <span className="text-red-500">*</span>
                </label>
                <div className="flex gap-2 items-end">
                  <Field name="supplier_id">
                    {({ field }) => {
                      return (
                        <Select
                          id="supplier_id"
                          showSearch
                          placeholder="Chọn nhà cung cấp"
                          optionFilterProp="children"
                          onChange={(value) => {
                            onChangeSelect(value, "supplier_id");
                          }}
                          onSearch={(value) =>
                            value && debouncedFetchOptions(1, value)
                          }
                          filterOption={filterOption}
                          options={supplierOptions}
                          value={
                            supplierOptions.length <= 0 &&
                            formik.values.suppliers_name
                              ? formik.values.suppliers_name
                              : formik.values.supplier_id !== ""
                              ? formik.values.supplier_id
                              : ""
                          }
                          onBlur={handleBlur}
                          className="flex-grow "
                          onDropdownVisibleChange={(open) =>
                            open && getSupplier(1, "", true)
                          }
                          notFoundContent={
                            loadingSelect ? <Spin size="small" /> : null
                          }
                        />
                      );
                    }}
                  </Field>
                  {/* ======================= Btn thêm nhanh nhà cung cấp ======================= */}
                  <Tooltip placement="bottom" title="Thêm nhanh nhà cung cấp">
                    <button
                      className="btn-actions btn-main-bl shrink-0"
                      onClick={showAddSupplierModal}
                    >
                      <HiOutlinePlusCircle />
                    </button>
                  </Tooltip>
                </div>
              </div>
              {touched.supplier_id && errors.supplier_id ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="supplier_id-warning"
                >
                  {errors.supplier_id}
                </p>
              ) : (
                <></>
              )}
            </div>
          </div>
          {/* ======================= Địa chỉ NCC ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="supplier_address">Địa chỉ NCC</label>
              <input
                className="grow"
                type="text"
                placeholder="Hệ thống tự render hoặc Nhập để thay đổi"
                name="supplier_address"
                value={formik.values.supplier_address || ""}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.supplier_address && errors.supplier_address ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="supplier_address-warning"
              >
                {errors.supplier_address}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Mã số NCC ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="supplier_code">Mã số NCC</label>
              <input
                className="grow"
                type="text"
                placeholder="Hệ thống tự render"
                name="supplier_code"
                value={formik.values.supplier_code || ""}
                disabled
              />
            </div>
            {touched.supplier_code && errors.supplier_code ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="supplier_code-warning"
              >
                {errors.supplier_code}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Kho nhận ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="warehouse">
                Kho nhận <span className="text-red-500">*</span>
              </label>
              <Select
                id="warehouse"
                name="warehouse"
                showSearch
                placeholder="Chọn kho nhận"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "warehouse")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={warehouseOptions}
                value={
                  warehouseOptions.length <= 0 && formik.values.warehouse_name
                    ? formik.values.warehouse_name
                    : formik.values.warehouse != ""
                    ? formik.values.warehouse
                    : ""
                }
                onBlur={handleBlur}
                onDropdownVisibleChange={getListWarehouse}
                notFoundContent={loadingSelect ? <Spin size="small" /> : null}
              />
            </div>
            {touched.warehouse && errors.warehouse ? (
              <p className="mt-1 text-red-500 text-sm" id="warehouse-warning">
                {errors.warehouse}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Tên thủ kho ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="storekeepers">
                Tên thủ kho <span className="text-red-500">*</span>
              </label>
              <Select
                id="storekeepers"
                name="storekeepers"
                showSearch
                placeholder="Hệ thống tự render hoặc chọn để thay đổi"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "storekeepers")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={userOptions}
                value={
                  formik.values.storekeepers != ""
                    ? formik.values.storekeepers * 1
                    : ""
                }
                onBlur={handleBlur}
              />
            </div>
            {touched.storekeepers && errors.storekeepers ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="storekeepers-warning"
              >
                {errors.storekeepers}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= SDT thủ kho ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="storekeepers_phone">
                Sđt thủ kho<span className="text-red-500">*</span>
              </label>
              <InputPhone
                className="grow"
                placeholder="Hệ thống tự render hoặc Nhập để thay đổi"
                name="storekeepers_phone"
                value={formik.values.storekeepers_phone || ""}
                formik={formik}
              />
            </div>
            {touched.storekeepers_phone && errors.storekeepers_phone ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="storekeepers_phone-warning"
              >
                {errors.storekeepers_phone}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Phương thức vận chuyển ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="shippingMethod">
                Phương thức vận chuyển <span className="text-red-500">*</span>
              </label>
              <Select
                id="shippingMethod"
                name="shippingMethod"
                showSearch
                placeholder="Chọn phương thức vận chuyển"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "shippingMethod")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={shippingMethodOptions}
                value={
                  shippingMethodOptions.length <= 0 &&
                  formik.values.shippingMethod_name
                    ? formik.values.shippingMethod_name
                    : formik.values.shippingMethod != ""
                    ? formik.values.shippingMethod
                    : ""
                }
                onBlur={handleBlur}
                onDropdownVisibleChange={getListShippingMethod}
                notFoundContent={loadingSelect ? <Spin size="small" /> : null}
              />
            </div>
            {touched.shippingMethod && errors.shippingMethod ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="shippingMethod-warning"
              >
                {errors.shippingMethod}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Phương thức thanh toán ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="paymentMethod">
                Phương thức thanh toán <span className="text-red-500">*</span>
              </label>
              <Select
                id="paymentMethod"
                name="paymentMethod"
                showSearch
                placeholder="Chọn phương thức thanh toán"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "paymentMethod")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={paymentMethodOptions}
                value={
                  paymentMethodOptions.length <= 0 &&
                  formik.values.paymentMethod_name
                    ? formik.values.paymentMethod_name
                    : formik.values.paymentMethod != ""
                    ? formik.values.paymentMethod
                    : ""
                }
                onBlur={handleBlur}
                onDropdownVisibleChange={getListPaymentMethod}
                notFoundContent={loadingSelect ? <Spin size="small" /> : null}
              />
            </div>
            {touched.paymentMethod && errors.paymentMethod ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="paymentMethod-warning"
              >
                {errors.paymentMethod}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Ngày giao dự kiến ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="delivery_date">Ngày giao dự kiến</label>
              <InputDatePicker
                field={formik?.values?.delivery_date || ""}
                onChange={(dateString) => {
                  formik.setFieldValue("delivery_date", dateString);
                }}
              />
            </div>
            {touched.delivery_date && errors.delivery_date ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="delivery_date-warning"
              >
                {errors.delivery_date}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Yêu cầu về hoá đơn ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="invoice_request">
                Yêu cầu về hoá đơn <span className="text-red-500">*</span>
              </label>
              <Select
                id="invoice_request"
                name="invoice_request"
                showSearch
                placeholder="Chọn yêu cầu về hoá đơn"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "invoice_request")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={listInvoiceRequest}
                value={
                  formik.values.invoice_request != ""
                    ? formik.values.invoice_request
                    : ""
                }
                onBlur={handleBlur}
                onDropdownVisibleChange={getListInvoiceRequest}
                notFoundContent={loadingSelect ? <Spin size="small" /> : null}
              />
            </div>
            {touched.invoice_request && errors.invoice_request ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="invoice_request-warning"
              >
                {errors.invoice_request}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Ghi chú ======================= */}
          <div className="form-group	">
            <div className="form-style">
              <label htmlFor="note">Ghi chú</label>
              <textarea
                className="grow"
                placeholder="Nhập ghi chú"
                name="note"
                id="note"
                value={formik.values.note || ""}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              ></textarea>
            </div>
            {touched.note && errors.note ? (
              <p className="mt-1 text-red-500 text-sm" id="note-warning">
                {errors.note}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Điều khoản giao hàng ======================= */}
          {/* <div className="form-group	">
            <div className="form-style">
              <label htmlFor="delivery_terms">Điều khoản giao hàng</label>
              <textarea
                className="grow"
                placeholder="Nhập điều khoản giao hàng"
                name="delivery_terms"
                id="delivery_terms"
                value={formik.values.delivery_terms}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              ></textarea>
            </div>
            {touched.delivery_terms && errors.delivery_terms ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="delivery_terms-warning"
              >
                {errors.delivery_terms}
              </p>
            ) : (
              <></>
            )}
          </div> */}

          {/* ======================= Điều khoản về kỹ thuật & chất lượng ======================= */}
          {/* <div className="form-group	">
            <div className="form-style">
              <label htmlFor="technical_terms">
                Điều khoản về kỹ thuật & chất lượng
              </label>
              <textarea
                className="grow"
                placeholder="Nhập điều khoản về kỹ thuật & chất lượn"
                name="technical_terms"
                id="technical_terms"
                value={formik.values.technical_terms}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              ></textarea>
            </div>
            {touched.technical_terms && errors.technical_terms ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="technical_terms-warning"
              >
                {errors.technical_terms}
              </p>
            ) : (
              <></>
            )}
          </div> */}
          {/* ======================= Yêu cầu về giấy tờ ======================= */}
          <div className="form-group	">
            <div className="form-style">
              <label htmlFor="document_required">Yêu cầu về giấy tờ</label>
              <textarea
                className="grow"
                placeholder="Nhập yêu cầu về giấy tờ"
                name="document_required"
                id="document_required"
                value={formik.values.document_required || ""}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              ></textarea>
            </div>
            {touched.document_required && errors.document_required ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="document_required-warning"
              >
                {errors.document_required}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
      </FormikProvider>
    </div>
  );
}
