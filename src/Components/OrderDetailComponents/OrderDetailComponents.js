import React, { useEffect, useState } from "react";
import OrderInfo from "./OrderInfo/OrderInfo";
import BuyerReceiver from "./BuyerReceiver/BuyerReceiver";
import Products from "./Products/Products";
import Summary from "./Summary/Summary";
import * as yup from "yup";
import { useFormik } from "formik";
import { Collapse } from "antd";
import { CaretRightOutlined } from "@ant-design/icons";
import { IoSaveOutline } from "react-icons/io5";
import { LiaUndoSolid } from "react-icons/lia";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import SetStatusOrder from "../../Components/OrdersPage/ListOrders/SetStatusOrder/SetStatusOrder";
import ModalSupplierDetail from "../SuppliersPage/ModalSupplierDetail";
import { getListSupplier } from "../../utils/supplier";
import {
  companyList,
  createOrder,
  getInvoiceRequestList,
  getOrder,
  getOrderStatusList,
  getPaymentMethodList,
  getShippingMethodList,
  getStaffOrder,
  updateOrder,
} from "../../utils/order";
import { warehouseList } from "../../utils/warehouse";
import {
  getListProduct,
  getProductDetails,
  getUnit,
  vatList,
} from "../../utils/product";
import ModalProductDetails from "../ProductPage/ListProduct/ModalProductDetails";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";
import { getLocalStorage } from "../../utils/localStorage";
import {
  getPurchaseRequest,
  getPurchaseRequestList,
} from "../../utils/purchaseRequest";
import {
  convertArr,
  convertObjectToArray,
  normalize,
  showError,
} from "../../utils/others";
import { getListUser } from "../../utils/user";

// Filter khi search
const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

export default function OrderDetailComponents() {
  // 1 - Lấy giá trị từ params
  let { orderKey } = useParams();
  // Khai báo useNavigate để chuyển hướng trang
  let navigate = useNavigate();
  const [searchParams] = useSearchParams();
  const dispatch = useDispatch();

  // 2 - Khai báo state
  const [productsSelected, setProductsSelected] = useState([]); // Quản lý danh sách sản phẩm được chọn
  const [current, setCurrent] = useState(1); // Quản lý vị trí của trạng thái
  const [isAddSupplierModalOpen, setIsAddSupplierModalOpen] = useState(false); // Quản lý ẩn hiện Modal thêm Nhà cung cấp
  const [isAddProductModalOpen, setIsAddProductModalOpen] = useState(false); // Quản lý ẩn hiện Modal thêm Nhà cung cấp
  const [listSuppliers, setListSuppliers] = useState([]); //Danh sách nhà cung cấp
  const [listUsers, setListUsers] = useState([]); //Danh sách người dùng
  const [listShippingMethod, setListShippingMethod] = useState([]); //Danh sách phương thức vận chuyển
  const [listPaymentMethod, setlistPaymentMethod] = useState([]); //Danh sách phương thức thanh toán
  const [listInvoiceRequest, setlistInvoiceRequest] = useState([]); //Danh sách yêu cầu về hoá đơn
  const [company, setCompany] = useState([]); //Danh sách cty
  const [listWarehouse, setListWarehouse] = useState([]); //Danh sách kho
  const [listProducts, setListProducts] = useState([]); //Danh sách sản phẩm
  const [listTaxCode, setListTaxCode] = useState([]); //Danh sách loại hoá đơn
  const [listUnit, setListUnit] = useState([]); //Danh sách loại hoá đơn
  const [orderStatus, setOrderStatus] = useState([]); //Danh sách loại hoá đơn
  const [reloadNCC, setReloadNCC] = useState(false); //Người tạo
  const [reloadProduct, setReloadProduct] = useState(false); //Người tạo
  const [listPurchaseProposal, setListPurchaseProposal] = useState([]); //hiển thị danh sách phiếu đề nghị mua hàng
  const [n, setN] = useState(0); //hiển thị Modal danh sách sản phẩm của đơn hàng được chọn
  const [ppChoosen, setPPChoosen] = useState(); //mã phiếu đề nghị + danh sách sản phẩm của phiếu đề nghị
  const [isAddProductsModal, setIsAddProductsModal] = useState(false); //hiển thị Modal danh sách sản phẩm của đơn hàng được chọn
  const [loadingSelect, setLoadingSelect] = useState(false);
  const [listStaffProduct, setListStaffProduct] = useState([]); //Danh sách staff modal sản phẩm
  const [update, setUpdate] = useState(0); //Cờ quản lý việc chỉnh sửa

  const initial = {
    supplier_id: "",
    paymentMethod: "",
    shippingMethod: "",
    company_id: "",
    buyer_id: getLocalStorage("profile")?.full_name,
    buyer_phone: getLocalStorage("profile")?.phone,
    buyer_email: getLocalStorage("profile")?.email,
    supplier_contact_name: "",
    supplier_contact_phone: "",
    items: [],
    delivery_date: "",
    warehouse: "",
    supplier_address: "",
    supplier_code: "",
    storekeepers: "",
    storekeepers_phone: "",
    tax_summary: [],
    deposit: 0,
    invoice_request: "",
    note: "",
    delivery_terms: "",
    technical_terms: "",
    document_required: "",
    created_by: getLocalStorage("profile")?.full_name,
    total_paid: 0,
    pp_id: "",
    total_vat: 0,
    sub_total_items_rounded: true,
  };

  // 3 - Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      pp_id: yup.string().nullable(),
      supplier_id: yup.string().required("Nhà cung cấp không để trống"),
      paymentMethod: yup
        .string()
        .required("Phương thức thanh toán không để trống"),
      shippingMethod: yup
        .string()
        .required("Phương thức vận chuyển không để trống"),
      invoice_request: yup
        .string()
        .required("Yêu cầu về hoá đơn không để trống"),
      company_id: yup.string().required("Bên mua không để trống"),
      buyer_id: yup.string().required("Người đặt hàng không để trống"),
      buyer_phone: yup
        .string()
        .required("Sđt người đặt hàng không để trống")
        .matches(
          /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
          "Số điện thoại không hợp lệ"
        ),
      buyer_email: yup.string().email("Email không hợp lệ"),
      supplier_contact_phone: yup
        .string()
        .required("Sđt người liên hệ không để trống"),
      supplier_contact_name: yup
        .string()
        .required("Người liên hệ không để trống"),
      items: yup
        .array()
        .min(1, "Sản phẩm không để trống")
        .test("check-qty", "Số lượng không hợp lệ", function (value) {
          if (!value || !value.length) {
            return false; // Mảng sản phẩm trống
          }
          // Kiểm tra từng sản phẩm trong mảng
          for (const product of value) {
            if (!product.qty || product.qty <= 0) {
              return false; // Số lượng thực tế không được để trống hoặc <=0
            }
          }
          return true;
        })
        .test("check-price", "Số tiền không hợp lệ", function (value) {
          if (!value || !value.length) {
            return false; // Mảng sản phẩm trống
          }
          // Kiểm tra từng sản phẩm trong mảng
          for (const product of value) {
            if (!product.price || product.price <= 0) {
              return false; // Số tiền không được để trống hoặc <=0
            }
          }
          return true;
        })
        .test("check-vat", "Có sản phẩm chưa chọn vat", function (value) {
          if (!value || !value.length) {
            return false; // Mảng sản phẩm trống
          }
          // Kiểm tra từng sản phẩm trong mảng
          for (const product of value) {
            if (!product.vat) {
              return false; // Số VAT để trống
            }
          }
          return true;
        }),
      warehouse: yup.string().required("Kho nhận không để trống"),
      storekeepers: yup.string().required("Thủ kho không để trống"),
      storekeepers_phone: yup
        .string()
        .required("Sdt thủ kho không để trống")
        .matches(
          /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
          "Số điện thoại không hợp lệ"
        ),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));

      let res;
      // alert thông báo
      if (orderKey) {
        // Sửa đơn hàng
        res = await updateOrder(values);
      } else {
        // Tạo đơn hàng
        let obj = setValuesAdd(values);
        res = await createOrder(obj, formik);
      }
      if (res) {
        let debug = searchParams.get("debug");
        if (!debug && orderKey) {
          navigate(0);
        } else if (!debug && !orderKey) {
          navigate("/purchase_order");
        }
      }

      dispatch(setSpinner(false));
    },
  });

  const setValuesAdd = (values) => {
    let item = [];
    values.items.map((pro) => {
      //Xử lí danh sách sản phẩm
      item.push({
        did: pro.did,
        package_case: pro.package_case,
        packaging: pro.packaging,
        product_id: pro.product_id,
        qty: pro.qty,
        price: pro.price,
        vat: pro.vat,
        unit: pro.unit,
        sub_total: pro.sub_total,
        pp_did: pro.pp_did,
      });
    });
    //Xử lý các key đẩy lên sever
    let obj = {
      supplier_id: values.supplier_id,
      supplier_code: values.supplier_code,
      supplier_address: values.supplier_address,
      company_id: values.company_id,
      buyer_id: values.buyer_id,
      buyer_phone: values.buyer_phone,
      buyer_email: values.buyer_email,
      supplier_contact_name: values.supplier_contact_name,
      supplier_contact_phone: values.supplier_contact_phone,
      items: item,
      warehouse: values.warehouse,
      storekeepers: values.storekeepers,
      storekeepers_phone: values.storekeepers_phone,
      vat: values.vat,
      deposit: values.deposit,
      invoice_request: values.invoice_request,
      note: values.note,
      delivery_terms: values.delivery_terms,
      technical_terms: values.technical_terms,
      document_required: values.document_required,
      delivery_date: values.delivery_date,
      paymentMethod: values.paymentMethod,
      shippingMethod: values.shippingMethod,
      pp_id: values.pp_id,
      sub_total: values.sub_total,
      total: values.total,
      total_paid: values.total_paid,
      total_vat: values.total_vat,
      sub_total_items_rounded: values.sub_total_items_rounded,
      tax_summary: values.tax_summary,
    };
    return obj;
  };

  // 4 - Lấy thông tin đơn hàng
  const APIGetOrder = async () => {
    dispatch(setSpinner(true));
    setUpdate(1); // Set update = 1 để không tính lại các phần ở summary
    let res = await getOrder(orderKey);
    if (res.data.success) {
      formik.setValues(res.data.data);
      formik.setFieldValue("sub_total_items_rounded", true);
      setProductsSelected(res.data.data.items);
      setCurrent(res.data.data.status * 1);
      onChangePP(res.data.data.pp_id);
    }
    dispatch(setSpinner(false));
  };

  // 5 - Lần đầu khi load trang, kiểm tra params để xác định trang tạo/ sửa đơn hàng, lấy danh sách các mảng cố định
  useEffect(() => {
    // Lấy danh sách nhà cung cấp, phương thức vận chuyển, phương thức thanh toán, kho, danh sách nhân viên, danh sách sản phẩm
    getArr();
    if (orderKey) {
      APIGetOrder();
    } else {
      formik.setValues(initial);
    }
  }, []);

  //  6 - Lấy danh sách NCC
  const getSupplier = async (page = 1, keywords = "") => {
    setLoadingSelect(true);
    let res = await getListSupplier(page, {
      keywords: keywords ? keywords : "",
    });
    if (res) {
      setListSuppliers(res.data);
      setReloadNCC(false);
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách Sản phẩm
  const getProduct = async (page = 1, keywords = "") => {
    setLoadingSelect(true);
    let res = await getListProduct(page, {
      keywords: keywords ? keywords : "",
    });
    if (res) {
      setListProducts(res.data);
      setReloadProduct(false);
    }
    setLoadingSelect(false);
  };

  //  6 - Lấy danh sách các bên mua (công ty)
  const getCompany = async (page) => {
    setLoadingSelect(true);
    if (company.length <= 0) {
      let res = await companyList(page);
      if (res.status) {
        setCompany(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách các kho
  const getListWarehouse = async (page) => {
    setLoadingSelect(true);
    if (listWarehouse.length <= 0) {
      let res = await warehouseList(page, { show_all: true });
      if (res.status) {
        setListWarehouse(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách các Phương thức vận chuyển
  const getListShippingMethod = async (page) => {
    setLoadingSelect(true);
    if (listShippingMethod.length <= 0) {
      let res = await getShippingMethodList(page);
      if (res.status) {
        setListShippingMethod(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách các PTTT
  const getListPaymentMethod = async (page) => {
    setLoadingSelect(true);
    if (listPaymentMethod.length <= 0) {
      let res = await getPaymentMethodList(page);
      if (res.status) {
        setlistPaymentMethod(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách các Yêu cầu về hoá đơn
  const getListInvoiceRequest = async (page) => {
    setLoadingSelect(true);
    if (listInvoiceRequest.length <= 0) {
      let res = await getInvoiceRequestList(page);
      if (res.status) {
        setlistInvoiceRequest(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách nhân viên mua hàng
  const getListStaffProduct = async (page) => {
    setLoadingSelect(true);
    if (listStaffProduct.length === 0) {
      let res = await getListUser(1, { department: 4 });
      if (res.status) {
        const transformedArray = res.data.data.map((item) => ({
          value: item.id,
          label: item.full_name,
        }));
        setListStaffProduct(transformedArray);
      }
    }
    setLoadingSelect(false);
  };

  //  6 - Lấy danh sách Các loại ĐVT
  const getListUnit = async (page) => {
    setLoadingSelect(true);
    if (listUnit.length <= 0) {
      let res = await getUnit();
      if (res.status) {
        setListUnit(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách Các loại VAT
  const getListTaxCode = async (page) => {
    setLoadingSelect(true);
    if (listTaxCode.length <= 0) {
      let res = await vatList();
      if (res.status) {
        setListTaxCode(res.data);
      }
    }
    setLoadingSelect(false);
  };

  //  7 - Lấy tất cả các mảng cố định
  let getArr = async (page) => {
    dispatch(setSpinner(true));

    let res;
    // 5.10 - Danh sách trạng thái
    res = await getOrderStatusList();
    if (res.status) {
      setOrderStatus(res.data);
    }
    // 5.10 - Danh sách phiếu đề nghị mua hàng
    res = await getPurchaseRequestList(1, { show_all: true });
    if (res.status) {
      setListPurchaseProposal(convertArr(res.data));
      setListUsers(convertObjectToArray(res.options.staff));
    }
    getListTaxCode();
    dispatch(setSpinner(false));
  };
  //  8 -  Hiển thị modal thêm Nhà cung cấp
  const showAddSupplierModal = () => {
    setIsAddSupplierModalOpen(true);
  };
  //  8 -  Hiển thị modal thêm sp
  const showAddProductModal = () => {
    setIsAddProductModalOpen(true);
  };

  //  9 -  Trở về trang đơn hàng
  const goToOrder = () => {
    navigate("/purchase_order");
  };

  // 10 - Tải lại NCC khi reloadNCC == true
  useEffect(() => {
    if (reloadNCC) {
      getSupplier();
    }
  }, [reloadNCC]);

  // 10 - Tải lại Product khi reloadProduct == true
  useEffect(() => {
    if (reloadProduct) {
      getProduct();
    }
  }, [reloadProduct]);

  // 11 - Khi danh sách sản phẩm đc chọn thay đổi thì cập nhật [productSelected]
  useEffect(() => {
    formik.setFieldValue("items", productsSelected);
  }, [productsSelected]);

  //  12 - Khi current thay đổi
  useEffect(() => {
    formik.values.status = current;
  }, [current]);

  // 11 - Lọc lại các key cần thiết của sản phẩm
  const getItemsArr = (arr) => {
    let newArr = [];
    arr.forEach((item) => {
      newArr.push({
        did: null,
        product_id: item.product_id,
        sku: item.sku,
        product_name: item.product_name,
        unit: item.unit,
        total: item.total,
        price: item.price,
        package_case: item.specifications,
        qty: item.qty_purchase,
        pp_did: item.did,
      });
    });
    return newArr;
  };

  // 12 - Khi mã phiếu tạm ứng thay đổi => kéo api chi tiết phiếu tạm ứng để lấy đc danh sách sản phẩm
  const onChangePP = async (value, open = true) => {
    dispatch(setSpinner(true));
    // Lấy thông tin chi tiết của phiếu tạm ứng
    let order = await getPurchaseRequest(value);

    if (order.data.success) {
      let data = order.data.data;
      let obj = {
        pp_id: data.id,
        pp_no: data.code,
        items: [...getItemsArr(data.items)],
      };
      setPPChoosen(obj);
    }
    dispatch(setSpinner(false));
  };

  useEffect(() => {
    if (n > 0 && ppChoosen) {
      formik.setFieldValue("pp_id", ppChoosen.pp_id);
      formik.setFieldValue("pp_no", ppChoosen.pp_no);
    }
  }, [ppChoosen]);

  // Function check giá sản phẩm
  let checkPrice = async (id) => {
    let obj = {
      price: "",
      packaging: "",
      vat: "",
      vat_name: "",
    };
    let res = await getProductDetails(id);

    if (res.data.success) {
      let data = res.data.data;
      obj.price = data?.price ? data.price * 1 : 0;
      obj.packaging = data?.packaging ? data.packaging * 1 : 0;
      obj.vat = data?.vat ? data.vat : "";
      obj.vat_name = data?.vat_name ? data.vat_name : "";
    } else {
      console.log(res);
      showError();
    }
    return obj;
  };

  // 13 - Khi xác nhận thêm sản phẩm từ phiếu đề nghị mua hàng
  const handleOkAddProducts = async (value) => {
    dispatch(setSpinner(true));
    setUpdate(0);
    let arr = [...productsSelected];

    // Xử lý tuần tự các hàm checkPrice
    for (const item of value) {
      let index = productsSelected.findIndex(
        (product) =>
          product.product_id === item.product_id && "pp_did" in product
      );

      //  Cho phép add thêm nếu sản phẩm thoả mãn: sản phẩm tại vị trí index được add từ dnmh
      if (index === -1) {
        // Sử dụng await ở đây để đợi cho hàm checkPrice được thực thi và trả về giá trị thực
        let moreDetail = await checkPrice(item.product_id);

        arr.push({
          ...item,
          total_payment: 0,
          price: moreDetail?.price || "",
          packaging: moreDetail?.packaging || "",
          vat: moreDetail?.vat || "",
          vat_name: moreDetail?.vat_name || "",
          sub_total: item.qty * 1 * (moreDetail.price * 1),
        });
      }
    }

    setProductsSelected(arr);
    formik.setFieldValue("items", arr);
    setIsAddProductsModal(false);
    dispatch(setSpinner(false));
  };

  // Khi huỷ chọn sản phẩm
  const handleCancelAddProducts = () => {
    setIsAddProductsModal(false);
  };

  // 13 - Danh sách các Tabs
  const getItems = [
    {
      key: "orderInfo",
      label: (
        <h2 className="text-[1rem] font-medium mb-1">Thông tin đơn hàng</h2>
      ),
      children: (
        <OrderInfo
          supplierOptions={listSuppliers}
          userOptions={listUsers}
          paymentMethodOptions={listPaymentMethod}
          shippingMethodOptions={listShippingMethod}
          companyOptions={company}
          formik={formik}
          filterOption={filterOption}
          showAddSupplierModal={showAddSupplierModal}
          warehouseOptions={listWarehouse}
          current={current}
          listInvoiceRequest={listInvoiceRequest}
          setN={setN}
          onChangePP={onChangePP}
          listPurchaseProposal={listPurchaseProposal}
          getSupplier={getSupplier}
          loadingSelect={loadingSelect}
          getCompany={getCompany}
          getListWarehouse={getListWarehouse}
          getListShippingMethod={getListShippingMethod}
          getListPaymentMethod={getListPaymentMethod}
          getListInvoiceRequest={getListInvoiceRequest}
        />
      ),
    },
    {
      key: "buyerReceiver",
      label: (
        <h2 className="text-[1rem] font-medium mb-1">
          Người đặt hàng - Người liên hệ
        </h2>
      ),
      children: (
        <BuyerReceiver
          userOptions={listUsers}
          formik={formik}
          filterOption={filterOption}
          current={current}
          orderKey={orderKey}
        />
      ),
    },
    {
      key: "products",
      label: <h2 className="text-[1rem] font-medium mb-1">Sản phẩm</h2>,
      children: (
        <Products
          taxCode={listTaxCode}
          productOptions={listProducts}
          formik={formik}
          filterOption={filterOption}
          productsSelected={productsSelected}
          setProductsSelected={setProductsSelected}
          unitOptions={listUnit}
          current={current}
          orderKey={orderKey}
          showAddProductModal={showAddProductModal}
          ppChoosen={ppChoosen}
          setIsAddProductsModal={setIsAddProductsModal}
          handleOkAddProducts={handleOkAddProducts}
          handleCancelAddProducts={handleCancelAddProducts}
          isAddProductsModal={isAddProductsModal}
          getProduct={getProduct}
          loadingSelect={loadingSelect}
          getListUnit={getListUnit}
          listTaxCode={listTaxCode}
          update={update}
          setUpdate={setUpdate}
        />
      ),
    },
    {
      key: "summary",
      label: <h2 className="text-[1rem] font-medium mb-1">Summary</h2>,
      children: (
        <Summary
          productsSelected={productsSelected}
          taxCode={listTaxCode}
          formik={formik}
          orderKey={orderKey}
          current={current}
          listTaxCode={listTaxCode}
          filterOption={filterOption}
          loadingSelect={loadingSelect}
          getListUnit={getListUnit}
          getListTaxCode={getListTaxCode}
          update={update}
          setUpdate={setUpdate}
        />
      ),
    },
  ];

  return (
    <>
      {/* ======================= Content ======================= */}
      <div className="page-content">
        {orderKey && (
          <div className="form-group col-span-3 mb-5">
            <div className="form-style">
              <label className="mb-2">Trạng thái</label>
              <SetStatusOrder
                statusOptions={orderStatus}
                current={current}
                setCurrent={setCurrent}
                permissionUpdate={1}
              />
            </div>
          </div>
        )}

        {/* ======================= Các tabs  ======================= */}
        <Collapse
          bordered={true}
          defaultActiveKey={[
            "orderInfo",
            "buyerReceiver",
            "products",
            "summary",
          ]}
          expandIcon={({ isActive }) => (
            <CaretRightOutlined rotate={isActive ? 90 : 0} />
          )}
          items={getItems}
        />
        {/* ======================= warning ======================= */}
        <div className="mt-5">
          <p className="text-[#17a2b8]" id="warning">
            {Object.keys(formik.errors).length > 0
              ? "Biểu mẫu chưa hoàn chỉnh thông tin."
              : ""}
          </p>
        </div>
        {/* ======================= btns ======================= */}
        <div className="flex gap-3 mt-5 justify-between">
          <button
            className="btn-actions btn-main-bl"
            onClick={() => {
              formik.handleSubmit();
            }}
          >
            <IoSaveOutline />
            Lưu
          </button>
          <button
            className="btn-actions btn-main-dark"
            onClick={goToOrder}
            type="button"
          >
            <LiaUndoSolid />
            Huỷ
          </button>
        </div>
      </div>
      {/* ======================= Modals ======================= */}
      <div className="modals">
        <ModalSupplierDetail
          isModalOpen={isAddSupplierModalOpen}
          setIsModalOpen={setIsAddSupplierModalOpen}
          supplier={null}
          setReload={setReloadNCC}
          setSupplier={setReloadNCC}
        />
        <ModalProductDetails
          isModalOpen={isAddProductModalOpen}
          setIsModalOpen={setIsAddProductModalOpen}
          product={null}
          listUnit={listUnit}
          listStaff={listStaffProduct}
          listVAT={listTaxCode}
          listNCC={listSuppliers}
          setProduct={setReloadProduct}
          setReload={setReloadProduct}
          deletedPage={false}
          getSupplier={getSupplier}
          loadingSelect={loadingSelect}
          getListUnit={getListUnit}
          getListTaxCode={getListTaxCode}
          getListStaffProduct={getListStaffProduct}
        />
      </div>
    </>
  );
}
