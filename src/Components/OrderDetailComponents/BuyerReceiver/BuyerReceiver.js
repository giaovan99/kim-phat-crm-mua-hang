import { Select } from "antd";
import React from "react";
import InputPhone from "../../Input/InputPhone";
import { getLocalStorage } from "../../../utils/localStorage";
import { getUser } from "../../../utils/user";

export default function BuyerReceiver({
  userOptions,
  formik,
  filterOption,
  orderKey,
  current,
}) {
  const { handleBlur, handleChange, touched, errors } = formik;
  const onSearch = (value) => {
    // console.log("search:", value);
  };

  // Khi select user thay đổi
  const onChangeSelect = async (tag, value) => {
    formik.setFieldValue(tag, value);
    if (tag === "buyer_id") {
      let res = await getUser(value);
      if (res.data.success) {
        formik.setFieldValue(
          "buyer_phone",
          res.data.data?.phone ? res.data.data.phone : ""
        );
        formik.setFieldValue(
          "buyer_email",
          res.data.data?.email ? res.data.data.email : ""
        );
      }
    }
  };

  return (
    <div className="BuyerReceiver">
      <div
        className={
          current > 1
            ? "pointer-events-none grid md:grid-cols-2 xl:grid-cols-3 gap-2"
            : "grid md:grid-cols-2 xl:grid-cols-3 gap-2"
        }
      >
        {/* ======================= Người đặt hàng ======================= */}
        <div className="form-group">
          <div className="form-style">
            <label htmlFor="buyer_id">
              Người đặt hàng <span className="text-red-500">*</span>
            </label>
            <Select
              id="buyer_id"
              name="buyer_id"
              showSearch
              placeholder="Chọn người đặt hàng"
              optionFilterProp="children"
              onChange={(value) => onChangeSelect("buyer_id", value)}
              onSearch={onSearch}
              filterOption={filterOption}
              options={userOptions}
              value={
                formik.values.buyer_id != ""
                  ? formik.values.buyer_id
                  : getLocalStorage("profile")?.full_name
              }
              onBlur={handleBlur}
            />
          </div>
          {touched.buyer_id && errors.buyer_id ? (
            <p className="mt-1 text-red-500 text-sm" id="buyer_id-warning">
              {errors.buyer_id}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Sđt người đặt ======================= */}
        <div className="form-group">
          <div className="form-style">
            <label htmlFor="buyer_phone">
              Sđt người đặt hàng <span className="text-red-500">*</span>
            </label>
            <InputPhone
              className="grow"
              placeholder="Hệ thống tự render hoặc Nhập để thay đổi"
              name="buyer_phone"
              value={formik.values.buyer_phone || ""}
              formik={formik}
            />
          </div>
          {touched.buyer_phone && errors.buyer_phone ? (
            <p className="mt-1 text-red-500 text-sm" id="orderer-warning">
              {errors.buyer_phone}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Email người đặt hàng ======================= */}
        <div className="form-group">
          <div className="form-style">
            <label htmlFor="buyer_email">Email người đặt hàng</label>
            <input
              className="grow"
              placeholder="Hệ thống tự render hoặc Nhập để thay đổi"
              name="buyer_email"
              onChange={handleChange}
              value={formik.values.buyer_email || ""}
              formik={formik}
            />
          </div>
          {touched.buyer_email && errors.buyer_email ? (
            <p className="mt-1 text-red-500 text-sm" id="orderer-warning">
              {errors.buyer_email}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Người liên hệ  ======================= */}
        <div className="form-group">
          <div className="form-style">
            <label htmlFor="supplier_contact_name">
              Người liên hệ <span className="text-red-500">*</span>
            </label>
            <input
              className="grow"
              type="text"
              placeholder="Hệ thống tự render theo NCC hoặc Nhập để thay đổi"
              name="supplier_contact_name"
              value={
                formik.values?.supplier_contact_name
                  ? formik.values.supplier_contact_name
                  : ""
              }
              onChange={handleChange}
              onBlur={handleBlur}
            />
          </div>
          {touched.supplier_contact_name && errors.supplier_contact_name ? (
            <p
              className="mt-1 text-red-500 text-sm"
              id="supplier_contact_name-warning"
            >
              {errors.supplier_contact_name}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Sđt người liên hệ ======================= */}
        <div className="form-group">
          <div className="form-style">
            <label htmlFor="supplier_contact_phone">
              Sđt người liên hệ <span className="text-red-500">*</span>
            </label>
            <InputPhone
              className="grow"
              placeholder="Hệ thống tự render theo NCC hoặc Nhập để thay đổi"
              name="supplier_contact_phone"
              value={
                formik.values?.supplier_contact_phone
                  ? formik.values.supplier_contact_phone
                  : ""
              }
              formik={formik}
            />
          </div>
          {touched.supplier_contact_phone && errors.supplier_contact_phone ? (
            <p className="mt-1 text-red-500 text-sm" id="receiver-warning">
              {errors.supplier_contact_phone}
            </p>
          ) : (
            <></>
          )}
        </div>
      </div>
      <div className="grid grid-cols-4 gap-2"></div>
    </div>
  );
}
