import React, { useEffect } from "react";
import "./summary.scss";
import { FormikProvider } from "formik";
import InputPriceNotTable from "../../Input/InpurPriceNotTable";
import { NumericFormat } from "react-number-format";

export default function Summary({
  productsSelected,
  taxCode,
  formik,
  orderKey,
  current,
  listTaxCode,
  filterOption,
  getListTaxCode,
  loadingSelect,
  update,
  setUpdate,
}) {
  const { handleBlur, handleChange, touched, errors } = formik;

  const onSearch = (value) => {
    // console.log("search:", value);
  };
  const onChangeValue = (value, field) => {
    setUpdate(0);
    formik.setFieldValue(field, value);
  };

  //   Tính và setState cho summary
  let calSummary = () => {
    if (update === 0) {
      let totalQty = 0; // Tổng số lượng sp
      let totalWithoutTax = 0; // Thành tiền trước VAT
      let taxSummary = {}; // Lưu trữ thuế GTGT cho các mức VAT khác nhau

      // Hàm lấy phần trăm VAT từ listTaxCode dựa vào vat_id
      const getVatPercentage = (vatId) => {
        const tax = listTaxCode.find((taxCode) => taxCode.key == vatId);
        return tax ? parseFloat(tax.vat) : 0;
      };

      productsSelected.forEach((product) => {
        const qty = product.qty * 1;
        const price = product.price * 1;
        const vatId = product.vat * 1; // Đây là vat_id

        // Tính tổng số lượng
        totalQty += qty;

        let productTotalWithoutTax = 0;
        // Nếu sản phẩm có làm tròn thành tiền (key sub_total_rounded = true )
        if (formik.values?.sub_total_items_rounded) {
          productTotalWithoutTax = Math.round(qty * price);
        } else {
          productTotalWithoutTax = qty * price;
        }
        // Tính tổng giá trị chưa bao gồm thuế
        totalWithoutTax += productTotalWithoutTax;

        // Tính thuế cho sản phẩm dựa trên vat
        const vatPercentage = getVatPercentage(vatId);

        // Tính thuế và lưu trữ trong taxSummary theo VAT cụ thể
        if (!taxSummary[vatPercentage]) {
          taxSummary[vatPercentage] = 0;
        }
        taxSummary[vatPercentage] +=
          productTotalWithoutTax * (vatPercentage / 100);
      });

      // Trả về kết quả, bao gồm các loại thuế khác nhau
      return {
        totalQty: totalQty,
        totalWithoutTax: totalWithoutTax,
        taxSummary: Object.entries(taxSummary).map(([vat, tax]) => ({
          vat: `${vat}%`,
          tax: tax,
        })),
      };
    } else {
      return;
    }
  };

  let calLastSummary = () => {
    if (update === 0) {
      let totalWithTax = Number(formik.values.sub_total);
      let totalVat = 0;
      if (
        formik?.values?.tax_summary &&
        typeof formik?.values?.tax_summary === "object"
      ) {
        // Tính tổng giá trị đơn hàng (đã bao gồm thuế)
        formik?.values?.tax_summary.forEach((item) => {
          totalWithTax += Number(item.tax);
          // Tỉnh tổng thuế
          totalVat += Number(item.tax);
        });
      }

      formik.setFieldValue("total", totalWithTax);
      formik.setFieldValue("total_vat", totalVat);

      // Tính số tiền cần trả
      let paymentAmount = totalWithTax - Number(formik.values.deposit);
      formik.setFieldValue("total_paid", paymentAmount);
    }
  };

  let calSummaryChangeDeposit = () => {
    if (update === 0) {
      // Tính số tiền cần trả
      let paymentAmount =
        Number(formik.values.total) - Number(formik.values.deposit);
      formik.setFieldValue("total_paid", paymentAmount);
    }
  };

  //   Tính và setState cho summary tự động khi danh sách sản phẩm được chọn thay đổi
  useEffect(() => {
    if (update === 0) {
      let summary = calSummary();
      formik.setFieldValue("qty", summary.totalQty);
      formik.setFieldValue("sub_total", summary.totalWithoutTax);
      formik.setFieldValue("tax_summary", summary.taxSummary);
    }
  }, [productsSelected, formik.values.items, listTaxCode]);

  // Khi user đổi giá trị tax_summary
  useEffect(() => {
    calLastSummary();
  }, [formik.values.tax_summary]);

  // Khi user đổi giá trị đặt cọc
  useEffect(() => {
    calSummaryChangeDeposit();
  }, [formik.values.deposit]);

  return (
    <FormikProvider value={formik}>
      <div
        className={
          current > 1
            ? "summary max-w-[400px] pointer-events-none"
            : "summary max-w-[400px]"
        }
      >
        {/* ======================= Tổng số lượng ======================= */}
        <div className="item">
          <p>Tổng số lượng:</p>
          <p className="text-right grow">
            {Number(formik?.values?.qty).toLocaleString("en-US")}
          </p>
        </div>

        {/* ======================= Tổng cộng = tổng thành tiền của các mã sp (ko vat) ======================= */}
        <div className="item">
          <p className="font-bold">
            <b>Tổng cộng:</b>
          </p>
          <p className="text-right grow font-bold">
            {Number(formik?.values?.sub_total).toLocaleString("en-US")}
          </p>
        </div>
        {/* ======================= Thuế GTGT theo từng mức VAT ======================= */}
        {typeof formik?.values?.tax_summary === "object" &&
          formik?.values?.tax_summary?.map(({ vat, tax }, index) => {
            if (vat !== "0%") {
              return (
                <div className="item form-group" key={vat}>
                  <label className="w-[50%]">Thuế GTGT ({vat}):</label>
                  <NumericFormat
                    thousandSeparator=","
                    allowNegative={false}
                    displayType="input"
                    placeholder=""
                    min={1}
                    className="grow text-right"
                    value={tax ? Number(tax).toLocaleString("en-US") : 0}
                    onValueChange={(value) => {
                      setUpdate(0);
                      //  Cho thay đổi giá trị thuế GTGT
                      let clone_tax_summary = [...formik.values.tax_summary];
                      if (!value.value) {
                        clone_tax_summary[index].tax = 0;
                      } else {
                        clone_tax_summary[index].tax = value.floatValue;
                      }
                      formik.setFieldValue("tax_summary", clone_tax_summary);
                    }}
                  />
                </div>
              );
            }
          })}
        {/* ======================= Tổng giá trị đơn hàng = tổng cộng (ko vat) + thuế GTGT 10% + thuế GTGT 8% +..... ======================= */}
        <div className="item">
          <p>Tổng giá trị đơn hàng</p>
          <p className="text-right grow font-bold">
            {Number(formik?.values?.total).toLocaleString("en-US")}
          </p>
        </div>
        {/* ======================= Đặt cọc ======================= */}
        <div className="form-group">
          <div className="form-style item" style={{ flexDirection: "row" }}>
            <label htmlFor="deposit" className="w-[50%]">
              Đặt cọc
            </label>
            <InputPriceNotTable
              className="grow text-right"
              placeholder="Nhập số tiền đặt cọc"
              name="deposit"
              id="deposit"
              value={formik.values.deposit}
              onChange={onChangeValue}
              onBlur={handleBlur}
            />
          </div>
          {touched.deposit && errors.deposit ? (
            <p className="mt-1 text-red-500 text-sm" id="deposit-warning">
              {errors.deposit}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Số tiền cần thanh toán ======================= */}
        <div className="item">
          <p>Số tiền cần thanh toán:</p>
          <p className="text-right grow font-bold">
            {Number(formik?.values?.total_paid).toLocaleString("en-US")}
          </p>
        </div>
      </div>
    </FormikProvider>
  );
}
