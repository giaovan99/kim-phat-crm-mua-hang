import { HiOutlinePlusCircle, HiOutlineTrash } from "react-icons/hi2";
import { Popconfirm, Select, Spin, Table, Tooltip, message } from "antd";
import React, { useCallback, useEffect, useState } from "react";
import Swal from "sweetalert2";
import {
  calBeforeVAT,
  checkNumber,
  checkPrice,
  debounce,
} from "../../../utils/others";
import InputPriceFormat from "../../Input/InputPriceFormat";
import InputNumberFormat from "../../Input/InputNumberFormat";
import ImportExcelBtnComponent from "../../ButtonComponent/ImportExcelBtnComponent";
import ImportFilesComponent from "../../ImportFilesComponents/ImportFilesComponents";
import ModalListProduct from "../../Warehouse/ImportWarehouse/ListOrders/Components/ModalListProducts/ModalListProduct";
import { useDispatch } from "react-redux";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { getProductDetails } from "../../../utils/product";
import { RxUpdate } from "react-icons/rx";

export default function Products({
  taxCode,
  productOptions,
  formik,
  setProductsSelected,
  productsSelected,
  unitOptions,
  filterOption,
  current,
  orderKey,
  showAddProductModal,
  ppChoosen,
  setIsAddProductsModal,
  isAddProductsModal,
  handleOkAddProducts,
  handleCancelAddProducts,
  getProduct,
  loadingSelect,
  getListUnit,
  listTaxCode,
  setUpdate,
}) {
  const [isViewImportFile, setIsViewImportFile] = useState(false);
  const [reload, setReload] = useState(false);
  const [addProductExcel, setAddProductExcel] = useState([]);

  let dispatch = useDispatch();

  const tableProps = {
    bordered: true,
    size: "small",
  };
  const { handleBlur, handleChange, touched, errors } = formik;

  const onSearch = (value) => {};

  const debouncedFetchOptions = useCallback(
    debounce(async (page, keywords) => {
      getProduct(1, keywords);
    }, 300),
    []
  );
  const confirm = (index) => {
    setUpdate(0);
    let arr = [...productsSelected];
    arr.splice(index, 1);
    setProductsSelected([...arr]);
    Swal.fire({
      title: "Xoá",
      text: "Xoá thành công",
      icon: "success",
      confirmButtonText: "Đóng",
    });
  };

  const cancel = (e) => {
    message.error("Đã huỷ");
  };

  // Khi chọn sản phẩm
  const onChangeProductSelected = (value, option) => {
    let newOption = {
      did: null,
      product_id: option.id,
      sku: option.sku,
      product_name: option.name,
      qty: 1,
      unit: option.unit,
      price: option.price,
      vat: option.vat,
      vat_name: option.vat_name,
      package_case: option.package_case,
      packaging: option.packaging,
      sub_total: 1 * option.price,
    };
    setUpdate(0);
    setProductsSelected([...productsSelected, newOption]);
    // }
  };
  // Hàm tính lại thành tiền
  const fSubTotal = (index, record) => {
    let arr = [...productsSelected];
    let subTotal = calBeforeVAT(record, formik.values.sub_total);
    arr[index].sub_total = subTotal;
    setProductsSelected([...arr]);
  };

  // Hàm tìm vị trí
  const fFindIndex = (record, arr) => {
    let index = arr.findIndex(
      (element) => element.product_id == record.product_id
    );
    return index;
  };

  // Hàm xử lý thay đổi giá phần tử của mảng
  const fChangeItem = (value, index) => {
    let arr = [...productsSelected];

    // Update giá của sp tại index trong mảng
    arr[index].price = checkPrice(value);
    setProductsSelected([...arr]);
    setUpdate(0);
    // Tính lại thành tiền
    fSubTotal(index, arr[index]);
  };

  // Hàm xử lý thay đổi số lượng phần tử của mảng
  const fChangeNumber = (value, index) => {
    let arr = [...productsSelected];
    // Update số lượng của sp tại index trong mảng
    arr[index].qty = checkNumber(value);
    setProductsSelected([...arr]);
    setUpdate(0);
    // Tính lại thành tiền
    fSubTotal(index, arr[index]);
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      key: "stt",
      fixed: "left",
      width: 50,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Mã SP",
      dataIndex: "sku",
      key: "sku",
      width: 100,
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
      width: 250,
      fixed: "left",
    },
    {
      title: "Quy cách sản phẩm",
      dataIndex: "package_case",
      key: "package_case",
      render: (text, record, index) => {
        return (
          <div className="form-group">
            <div className="form-style">
              <textarea
                className={
                  current > 1
                    ? "pointer-events-none text-right w-full"
                    : "text-right w-full"
                }
                value={record.package_case || ""}
                onChange={(e) => {
                  let arr = [...productsSelected];
                  arr[index].package_case = e.target.value;
                  setProductsSelected([...arr]);
                }}
              ></textarea>
            </div>
          </div>
        );
      },
    },
    {
      title: "Quy cách đóng gói",
      dataIndex: "packaging",
      key: "packaging",
      render: (text, record, index) => {
        return (
          <div className="form-group">
            <div className="form-style">
              <textarea
                className={
                  current > 1
                    ? "pointer-events-none text-right w-full"
                    : "text-right w-full"
                }
                value={record.packaging || ""}
                onChange={(e) => {
                  let arr = [...productsSelected];
                  arr[index].packaging = e.target.value;
                  setProductsSelected([...arr]);
                }}
              ></textarea>
            </div>
          </div>
        );
      },
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 120,
      render: (text, record, index) => {
        {
          /* ======================= Đơn vị tính ======================= */
        }
        return (
          <Select
            name="unit"
            showSearch
            placeholder="Chọn đơn vị tính"
            optionFilterProp="children"
            className={current > 1 ? "pointer-events-none w-full" : "w-full"}
            onChange={(value, event) => {
              let arr = [...productsSelected];
              arr[index].unit = value;
              setProductsSelected([...arr]);
            }}
            value={record.unit || ""}
            onSearch={onSearch}
            filterOption={filterOption}
            options={unitOptions}
            onDropdownVisibleChange={getListUnit}
            notFoundContent={loadingSelect ? <Spin size="small" /> : null}
          />
        );
      },
    },
    {
      title: "Số lượng",
      dataIndex: "qty",
      key: "qty",
      width: 100,
      render: (text, record, index) => {
        return (
          <InputNumberFormat
            className={
              current > 1
                ? "pointer-events-none text-center w-full"
                : "text-center w-full"
            }
            value={record.qty || ""}
            index={index}
            onChange={fChangeNumber}
          />
        );
      },
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
      width: 130,
      render: (text, record, index) => {
        return (
          <InputPriceFormat
            className={
              current > 1
                ? "pointer-events-none text-right w-full"
                : "text-right w-full"
            }
            value={record.price || ""}
            onChange={fChangeItem}
            index={index}
          />
        );
      },
    },
    {
      title: "Thành tiền",
      width: 140,
      dataIndex: "sub_total",
      key: "sub_total",
      render: (text, record) => {
        return (
          <p className="text-right">
            {orderKey && current > 1
              ? Number(text).toLocaleString("en-US")
              : calBeforeVAT(record, formik.values.sub_total).toLocaleString(
                  "en-US"
                )}
          </p>
        );
      },
    },
    {
      title: "Thuế GTGT",
      width: 100,
      dataIndex: "sub_total",
      key: "sub_total",
      render: (text, record, index) => {
        return (
          <Select
            name="vat"
            showSearch
            placeholder="Chọn đơn vị tính"
            optionFilterProp="children"
            className={current > 1 ? "pointer-events-none w-full" : "w-full"}
            onChange={(value, event) => {
              setUpdate(0);
              let arr = [...productsSelected];
              arr[index].vat = value;
              setProductsSelected([...arr]);
            }}
            value={record.vat || ""}
            onSearch={onSearch}
            filterOption={filterOption}
            options={listTaxCode}
            notFoundContent={loadingSelect ? <Spin size="small" /> : null}
          />
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 80,
      fixed: "right",
      render: (text, record, index) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Refresh sản phẩm ======================= */}
            {orderKey && (
              <Popconfirm
                placement="topRight"
                title="Làm mới thông tin"
                description="Bạn chắc chắn muốn làm mới thông tin sản phẩm này?"
                onConfirm={() => confirmUpdate(index)}
                onCancel={cancel}
                okText="Làm mới"
                cancelText="Huỷ"
                okButtonProps={{ className: "btn-main-yl" }}
                className="popover-danger"
              >
                <Tooltip placement="bottom" title="Làm mới thông tin">
                  <button>
                    <RxUpdate className="text-blue-600" />
                  </button>
                </Tooltip>
              </Popconfirm>
            )}
            {/* ======================= Xoá ======================= */}
            <Popconfirm
              placement="topRight"
              title="Xoá sản phẩm"
              description="Bạn chắc chắn muốn xoá sản phẩm này?"
              onConfirm={() => confirm(index)}
              onCancel={cancel}
              okText="Xoá"
              cancelText="Huỷ"
              okButtonProps={{ className: "btn-main-yl" }}
              className={
                current > 1
                  ? "pointer-events-none popover-danger"
                  : "popover-danger"
              }
            >
              <Tooltip placement="bottom" title="Xoá">
                <button>
                  <HiOutlineTrash className="text-rose-600" />
                </button>
              </Tooltip>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  // Thêm sản phẩm từ Excel
  const showImportFile = () => {
    setIsViewImportFile(true);
  };

  useEffect(() => {
    let oldArr = [...productsSelected];
    let newArr = [];
    addProductExcel.forEach((product) => {
      const data = {
        sku: product?.sku ? product.sku : "",
        product_name: product?.product_name ? product.product_name : "",
        package_case: product?.package_case ? product.package_case : "",
        packaging: product?.packaging ? product.packaging : "",
        price: product?.price ? product.price : "",
        unit: product?.unit ? product.unit : "",
        qty: product?.qty ? product.qty : "",
        sub_total: product?.sub_total ? product.sub_total : "",
      };
      newArr.push(data);
    });
    setProductsSelected([...oldArr, ...newArr]);
  }, [addProductExcel]);

  // Khi làm mới sản phẩm
  const confirmUpdate = async (index) => {
    setUpdate(0);
    dispatch(setSpinner(true));
    let arr = [...productsSelected];
    // Gọi API lấy thông tin sp (tên, dvt) -> Nếu sp ko có pp_did thì lấy thôgn tin sp mới
    let res = await getProductDetails(arr[index].product_id);
    // // Cập nhật tên + dvt + quy cách sp + quy cách đóng gói + đơn giá + sku của sp vào mảng, giữ nguyên số lượng
    if (res.data.success) {
      arr[index].product_name = res.data.data?.name ? res.data.data.name : "";
      arr[index].unit = res.data.data?.unit ? res.data.data.unit : "";
      arr[index].package_case = res.data.data?.package_case
        ? res.data.data.package_case
        : "";
      arr[index].packaging = res.data.data?.packaging
        ? res.data.data.packaging
        : "";
      arr[index].price = res.data.data?.price ? res.data.data.price : "";
      arr[index].sku = res.data.data?.sku ? res.data.data.sku : "";
      arr[index].sub_total = res.data.data.price * 1 * (arr[index].qty * 1);
      setProductsSelected([...arr]);
      dispatch(setSpinner(false));
      message.success("Làm mới thành công");
      Swal.fire({
        title: "Làm mới thông tin sản phẩm",
        text: "Thành công",
        icon: "success",
        confirmButtonText: "Đóng",
      });
    }
    // Nếu sp có pp_did thì cần lấy thông tin từ phiếu đề nghị mua hàng
  };
  return (
    <div className="productsAddPage">
      <label htmlFor="items" className="w-full font-medium">
        Chọn sản phẩm <span className="text-red-500">*</span>
      </label>
      {/* ======================= Sản phẩm ======================= */}
      <div
        className={
          current > 1
            ? "pointer-events-none flex items-start gap-2 form-group mb-2"
            : "flex items-start gap-2 form-group mb-2"
        }
      >
        <div className="flex-grow">
          <div className="form-style">
            <Select
              id="items"
              name="items"
              showSearch
              placeholder="Chọn sản phẩm"
              optionFilterProp="children"
              onChange={onChangeProductSelected}
              onSearch={(value) => value && debouncedFetchOptions(1, value)}
              filterOption={filterOption}
              options={productOptions}
              value={formik.values.items != "" ? formik.values.items : null}
              onBlur={handleBlur}
              onDropdownVisibleChange={(open) =>
                open && getProduct(1, "", true)
              }
              notFoundContent={loadingSelect ? <Spin size="small" /> : null}
            />
          </div>
          {touched.items && errors.items ? (
            <p className="mt-1 text-red-500 text-sm" id="items-warning">
              {errors.items}
            </p>
          ) : (
            <></>
          )}
        </div>
        {ppChoosen && (
          <div
            className={
              current > 1
                ? "pointer-events-none form-group mb-2"
                : "form-group mb-2"
            }
          >
            <button
              type="button"
              className="btn-actions btn-main-bl"
              onClick={() => {
                setIsAddProductsModal(true);
              }}
            >
              Chọn SP từ phiếu đề nghị
            </button>
          </div>
        )}
        <div className="flex flex-shirk gap-2">
          {/* ======================= Btn thêm nhanh sản phẩm ======================= */}
          <Tooltip placement="bottom" title="Thêm nhanh sản phẩm">
            <button
              className="btn-actions btn-main-bl"
              onClick={showAddProductModal}
            >
              <HiOutlinePlusCircle />
            </button>
          </Tooltip>
          {/* ======================= Btn import từ file excel ======================= */}
          <ImportExcelBtnComponent
            deleted={false}
            onClickF={() => showImportFile()}
            text={"Thêm sản phẩm từ file Excel"}
          />
        </div>
      </div>
      {/* ======================= Danh sách sản phẩm ======================= */}
      <Table
        {...tableProps}
        pagination={{
          position: ["none", "bottomCenter"],
          pageSize: 20,
          hideOnSinglePage: true,
        }}
        scroll={{
          y: 600,
          x: 1400,
        }}
        columns={columns}
        dataSource={formik.values?.items || []}
      />
      {isViewImportFile && (
        <ImportFilesComponent
          isModalOpen={isViewImportFile}
          setIsModalOpen={setIsViewImportFile}
          confirmFileStyle={2}
          title={"Import file sản phẩm"}
          module={"purchase_order_details"}
          setReload={setReload}
          setAddProductExcel={setAddProductExcel}
        />
      )}
      {/* ======================= Modals ======================= */}
      {isAddProductsModal && (
        <ModalListProduct
          handleOkAddProducts={handleOkAddProducts}
          handleCancelAddProducts={handleCancelAddProducts}
          data={ppChoosen.items}
          setProductChoosen={setProductsSelected}
          isModalOpen={isAddProductsModal}
          orderChoosen={ppChoosen}
          productsChoosen={productsSelected}
        />
      )}
    </div>
  );
}
