import React from "react";
import { NumericFormat } from "react-number-format";

export default function InputPriceFormat({
  className,
  name,
  value,
  onChange,
  record,
}) {
  return (
    <div className="form-group ">
      <div className="form-style">
        <NumericFormat
          thousandSeparator=","
          allowNegative={false}
          displayType="input"
          min={1}
          className={className}
          name={name}
          value={value ? Number(value) : ""}
          onValueChange={(values) => {
            onChange(values.floatValue, record);
          }}
        />
      </div>
    </div>
  );
}
