import React from "react";
import { NumericFormat } from "react-number-format";

export default function InputPriceNotTable({
  className,
  placeholder,
  name,
  id,
  value,
  onChange,
  onBlur,
  min = 1,
}) {
  return (
    <NumericFormat
      thousandSeparator=","
      allowNegative={false}
      displayType="input"
      placeholder={placeholder}
      min={min}
      className={className}
      name={name}
      id={id}
      value={value ? Number(value) : 0}
      onValueChange={(value) => {
        onChange(value.floatValue, name);
      }}
      onBlur={onBlur}
    />
  );
}
