import React from "react";
import { NumericFormat } from "react-number-format";

export default function InputNumberFormat({
  className,
  name,
  value,
  onChange,
  record = false,
  index = false,
  disable = false,
  allowNegative = false,
}) {
  return (
    <>
      <div className="form-group ">
        <div className="form-style">
          <NumericFormat
            thousandSeparator=","
            allowNegative={allowNegative}
            displayType="input"
            className={className}
            decimalScale={2}
            name={name}
            value={value ? Number(value) : 0}
            onValueChange={(values) => {
              if (record) {
                onChange(values.floatValue);
              } else {
                onChange(values.floatValue, index);
              }
            }}
            disabled={disable}
          />
        </div>
      </div>
    </>
  );
}
