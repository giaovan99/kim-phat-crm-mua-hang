import React from "react";

export default function InputPhone({
  placeholder,
  className,
  name,
  value,
  formik,
}) {
  const { handleBlur } = formik;
  const handleInputChange = (e) => {
    // Lọc chỉ giữ lại các ký tự số
    const numericValue = e.target.value.replace(/[^0-9]/g, "");
    return numericValue;
  };
  return (
    <div className="form-group ">
      <div className="form-style">
        <input
          type="text"
          placeholder={placeholder}
          className={className}
          name={name}
          onChange={(event) => {
            let check = handleInputChange(event);
            formik.setFieldValue(`${name}`, check);
          }}
          onBlur={handleBlur}
          value={value || ""}
        />
      </div>
    </div>
  );
}
