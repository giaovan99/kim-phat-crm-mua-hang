import { DatePicker } from "antd";
import React from "react";
import { formatDatePicker, stringToDateBE } from "../../utils/others";
import dayjs from "dayjs";
import "dayjs/locale/vi"; // Import locale nếu cần
import localizedFormat from "dayjs/plugin/localizedFormat";

// Cấu hình dayjs với các plugin
dayjs.extend(localizedFormat);
dayjs.locale("vi"); // Thiết lập locale nếu cần

const dateFormat = "DD/MM/YYYY"; // Chọn định dạng cụ thể

const InputDatePicker = ({ field, onChange, disabled = false }) => {
  return (
    <DatePicker
      value={field ? dayjs(formatDatePicker(field), dateFormat) : null}
      format={dateFormat}
      onChange={(date, dateString) => {
        onChange(stringToDateBE(dateString));
      }}
      locale={dayjs.locale() === "vi" ? "vi_VN" : undefined}
      className="h-full"
      disabled={disabled}
    />
  );
};

export default InputDatePicker;
