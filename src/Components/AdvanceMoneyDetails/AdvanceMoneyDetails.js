import React, { useEffect, useState } from "react";
import { useNavigate, useParams } from "react-router-dom";
import { Collapse } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { IoSaveOutline } from "react-icons/io5";
import { LiaUndoSolid } from "react-icons/lia";
import { CaretRightOutlined } from "@ant-design/icons";
import { getOrder, getOrderList } from "../../utils/order";
import SetStatus2Step from "../SetStatus2Step/SetStatusProposal";
import ImportInfo from "./ImportInfo/ImportInfo";
import Products from "./Products/Products";
import {
  createImportWarehouse,
  getImportWarehouse,
  getImportWarehouseStatusList,
  updateImportWarehouse,
} from "../../utils/warehouse";
import { normalize } from "../../utils/others";

const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

export default function AdvanceMoneyDetails() {
  // 1 - Lấy giá trị từ params
  let { advanceKey } = useParams();
  // Khai báo useNavigate để chuyển hướng trang
  let navigate = useNavigate();

  // 2 - Khai báo state
  const [productsSelected, setProductsSelected] = useState([]); // Quản lý danh sách sản phẩm được chọn
  const [current, setCurrent] = useState(1); // Quản lý vị trí của trạng thái
  const [importStatus, setImportStatus] = useState(); //Danh sách trạng thái
  let [orderChoosen, setOrderChoosen] = useState(); //mã đơn + danh sách sản phẩm của đơn
  let [listOrder, setListOrder] = useState(false); //hiển thị danh sách đơn hàng
  let [isAddProductsModal, setIsAddProductsModal] = useState(false); //hiển thị Modal danh sách sản phẩm của đơn hàng được chọn
  let [n, setN] = useState(0); //hiển thị Modal danh sách sản phẩm của đơn hàng được chọn

  // 2 - Khai báo giá trị ban đầu
  const initial = {
    po_id: "",
    delivery_actual_date: "",
    items: [],
    status: 1,
  };

  // 3 - Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      po_id: yup.string().required("Chọn mã đơn hàng"),
      delivery_actual_date: yup
        .string()
        .required("Ngày giao thực tế không để trống"),
      items: yup
        .array()
        .min(1, "Chọn sản phẩm ")
        .test(
          "check-actual-quantity",
          "Số lượng thực tế của sản phẩm phải lớn hơn 0",
          function (value) {
            if (!value || !value.length) {
              return false; // Mảng sản phẩm trống
            }
            // Kiểm tra từng sản phẩm trong mảng
            for (const product of value) {
              if (!product.qty_actual || product.qty_actual <= 0) {
                return false; // Số lượng thực tế không được để trống
              }
            }
            return true;
          }
        ),
    }),
    onSubmit: async (values) => {
      // console.log(values);
      let res;
      // alert thông báo
      if (advanceKey) {
        // Sửa đơn hàng
        res = await updateImportWarehouse(values);
        if (res) {
          navigate(0);
        }
      } else {
        // Tạo đơn hàng
        let obj = setValuesAdd(values);
        res = await createImportWarehouse(obj, formik);
      }
    },
  });

  const setValuesAdd = (values) => {
    let item = [];
    values.items.forEach((pro) => {
      item.push({
        did: null,
        product_id: pro.product_id,
        qty_actual: pro.qty_actual,
      });
    });
    let obj = {
      po_id: values.po_id,
      delivery_actual_date: values.delivery_actual_date,
      items: item,
    };
    return obj;
  };

  //  6 - Lấy tất cả các mảng cố định
  let getArr = async (page) => {
    let res;
    // 5.10 - Danh sách trạng thái
    res = await getImportWarehouseStatusList();
    // console.log(res);
    if (res.status) {
      setImportStatus(res.data);
    }
    // 5.7 - Danh sách đơn hàng
    res = await getOrderList(page, { show_all: true });
    if (res.status) {
      setListOrder(res.data);
    }
  };

  // 4 - Lấy thông tin chi tiết phiếu nhập kho
  const APIGetImportwarehouse = async () => {
    // console.log(importWarehouse);
    let res = await getImportWarehouse(advanceKey);
    if (res.data.success) {
      onChangeOrder(res.data.data.po_id, false);
      setProductsSelected(res.data.data.items);
      setCurrent(res.data.data.status * 1);
      formik.setValues(res.data.data);
    }
  };

  // Render lần đầu khi load trang check xem đang Thêm hay Xoá Phiếu nhập kho
  useEffect(() => {
    getArr();
    if (advanceKey) {
      APIGetImportwarehouse();
    } else {
      formik.setValues(initial);
    }
  }, []);

  // 10 - Lấy thông tin chi tiết đơn hàng
  const getOrderInfo = async (id) => {
    let res = await getOrder(id);
    // console.log(res);
    if (res.data.success) {
      return res.data.data;
    } else {
      return false;
    }
  };

  // 11 - Lọc lại các key cần thiết của
  const getItemsArr = (arr) => {
    let newArr = [];
    arr.forEach((item) => {
      newArr.push({
        product_id: item.product_id,
        sku: item.sku,
        product_name: item.product_name,
        unit: item.unit,
        vat: item.vat,
        vat_name: item.vat_name,
        qty: item.qty,
        qty_actual: 0,
        total: item.total,
        price: item.price,
        sub_total: item.sub_total,
        did: null,
      });
    });
    return newArr;
  };

  // 12 - Khi mã đơn hàng thay đổi => kéo api chi tiết đơn để lấy đc danh sách sản phẩm
  const onChangeOrder = async (value, open = true) => {
    // Lấy thông tin chi tiết của đơn hàng
    let order = await getOrderInfo(value);
    if (order) {
      let obj = {
        po_id: order.id,
        po_no: order.code,
        delivery_date: order.delivery_date,
        suppliers_name: order.suppliers_name,
        po_status_name: order.status_name,
        shippingMethod_name: order.shippingMethod_name,
        warehouse_name: order.warehouse_name,
        buyer_name: order.buyer_name,
        buyer_phone: order.buyer_phone,
        receiver_name: order.receiver_name,
        receiver_phone: order.receiver_phone,
        items: [...getItemsArr(order.items)],
      };
      setOrderChoosen(obj);
      // if()
      if (open) {
        setProductsSelected([]);
      }
      setIsAddProductsModal(open);
    }
  };

  useEffect(() => {
    if (n > 0) {
      formik.setFieldValue("po_id", orderChoosen.po_id);
      formik.setFieldValue("po_no", orderChoosen.po_no);
      formik.setFieldValue("delivery_date", orderChoosen.delivery_date);
      formik.setFieldValue("suppliers_name", orderChoosen.suppliers_name);
      formik.setFieldValue("po_status_name", orderChoosen.po_status_name);
      formik.setFieldValue(
        "shippingMethod_name",
        orderChoosen.shippingMethod_name
      );
      formik.setFieldValue("warehouse_name", orderChoosen.warehouse_name);
      formik.setFieldValue("buyer_name", orderChoosen.buyer_name);
      formik.setFieldValue("buyer_phone", orderChoosen.buyer_phone);
      formik.setFieldValue("receiver_name", orderChoosen.receiver_name);
      formik.setFieldValue("receiver_phone", orderChoosen.receiver_phone);
    }
  }, [orderChoosen]);

  // 13 - Khi xác nhận thêm sản phẩm
  const handleOkAddProducts = (value) => {
    // console.log("first");
    //  value: mảng sản phẩm được chọn, check xem items thuộc value đã có trong mảng chưa, nếu chưa thì thêm vào mảng
    let arr = [...productsSelected];
    value.forEach((item) => {
      let index = productsSelected.findIndex(
        (product) => product.product_id == item.product_id
      );
      if (index === -1) {
        arr.push({ ...item, advance_money: 0 });
      }
    });
    setProductsSelected(arr);
    formik.setFieldValue("items", arr);
    setIsAddProductsModal(false);
  };

  // console.log(productsSelected);
  // Khi huỷ chọn sản phẩm
  const handleCancelAddProducts = () => {
    setIsAddProductsModal(false);
  };

  // Khi productsChoosen thay đổi thì gắn giá trị cho formik products
  useEffect(() => {
    if (productsSelected) {
      formik.setFieldValue("items", productsSelected);
    }
  }, [productsSelected]);

  //  7 -  Trở về trang Đề nghị mua hàng
  const goToWarehouseImport = () => {
    navigate("/import-warehouse");
  };

  //  9 - Khi current thay đổi
  useEffect(() => {
    formik.values.status = current;
  }, [current]);

  // 10 - Danh sách các Tabs
  const getItems = [
    {
      key: "info",
      label: (
        <h2 className="text-[1rem] font-medium mb-1">
          Thông tin phiếu nhập kho
        </h2>
      ),
      children: (
        <ImportInfo
          formik={formik}
          filterOption={filterOption}
          current={current}
          importKey={advanceKey}
          onChangeOrder={onChangeOrder}
          listOrder={listOrder}
          orderChoosen={orderChoosen}
          setN={setN}
        />
      ),
    },
    {
      key: "products",
      label: <h2 className="text-[1rem] font-medium mb-1">Sản phẩm</h2>,
      children: (
        <Products
          formik={formik}
          filterOption={filterOption}
          productsSelected={productsSelected}
          setProductsSelected={setProductsSelected}
          current={current}
          importKey={advanceKey}
          setIsAddProductsModal={setIsAddProductsModal}
          handleOkAddProducts={handleOkAddProducts}
          handleCancelAddProducts={handleCancelAddProducts}
          orderChoosen={orderChoosen}
          isAddProductsModal={isAddProductsModal}
        />
      ),
    },
  ];

  return (
    <>
      {/* ======================= Content ======================= */}
      <div className="page-content">
        {/* ======================= Trạng thái  ======================= */}
        {advanceKey && (
          <div className="form-group col-span-3 mb-5">
            <div className="form-style">
              <label htmlFor="contractSigningDate" className="mb-2">
                Trạng thái
              </label>
              <SetStatus2Step
                statusOptions={importStatus}
                current={current}
                setCurrent={setCurrent}
                page={"importWarehouse"}
              />
            </div>
          </div>
        )}
        {/* ======================= Tabs lựa chọn ======================= */}
        <Collapse
          bordered={true}
          defaultActiveKey={["info", "products"]}
          expandIcon={({ isActive }) => (
            <CaretRightOutlined rotate={isActive ? 90 : 0} />
          )}
          items={getItems}
        />
        {/* ======================= warning ======================= */}
        <div className="mt-5">
          <p className="text-[#17a2b8]" id="warning">
            {Object.keys(formik.errors).length > 0
              ? "Biểu mẫu chưa hoàn chỉnh thông tin."
              : ""}
          </p>
        </div>
        {/* ======================= btns ======================= */}
        <div className="flex gap-3 mt-5 justify-between">
          <button
            className="btn-actions btn-main-bl"
            onClick={() => {
              formik.handleSubmit();
            }}
          >
            <IoSaveOutline />
            Lưu
          </button>
          <button
            className="btn-actions btn-main-dark"
            onClick={goToWarehouseImport}
            type="button"
          >
            <LiaUndoSolid />
            Huỷ
          </button>
        </div>
      </div>
    </>
  );
}
