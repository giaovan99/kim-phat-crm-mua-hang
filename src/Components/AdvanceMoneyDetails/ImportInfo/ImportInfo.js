import { Select } from "antd";
import React from "react";
import { Field, FormikProvider } from "formik";

export default function ImportInfo({
  formik,
  filterOption,
  current,
  onChangeOrder,
  listOrder,
  setN,
}) {
  const { handleBlur, handleChange, touched, errors } = formik;
  const onSearch = (value) => {
    // console.log("search:", value);
  };

  return (
    <div className="orderInfo">
      <FormikProvider value={formik}>
        <div
          className={
            current > 1
              ? "pointer-events-none grid grid-cols-4 gap-2"
              : "grid grid-cols-4 gap-2"
          }
        >
          {/* ======================= Mã phiếu nhập kho ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="code">Mã phiếu nhập kho</label>
              <input
                className="grow"
                type="string"
                placeholder="Hệ thống tự tạo"
                name="code"
                id="code"
                value={formik.values.code || ""}
                disabled
              />
            </div>
            {touched.code && errors.code ? (
              <p className="mt-1 text-red-500 text-sm" id="code-warning">
                {errors.code}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Mã đơn hàng ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="po_id">
                Mã đơn hàng <span className="text-red-500">*</span>
              </label>
              <Field name="po_id">
                {({ field }) => {
                  return (
                    <Select
                      {...field}
                      showSearch
                      placeholder="Chọn đơn hàng"
                      optionFilterProp="children"
                      onChange={(value) => {
                        setN(1);
                        onChangeOrder(value);
                      }}
                      onSearch={onSearch}
                      filterOption={filterOption}
                      options={listOrder}
                      onBlur={handleBlur}
                      value={formik.values.po_id || ""}
                    />
                  );
                }}
              </Field>
            </div>
            {touched.po_id && errors.po_id ? (
              <p className="mt-1 text-red-500 text-sm" id="po_id-warning">
                {errors.po_id}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Ngày giao thực tế ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="delivery_actual_date">
                Ngày giao thực tế<span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="date"
                name="delivery_actual_date"
                id="delivery_actual_date"
                value={formik.values.delivery_actual_date || ""}
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
            {errors.delivery_actual_date && touched.delivery_actual_date ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="delivery_actual_date-warning"
              >
                {errors.delivery_actual_date}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Ngày giao dự kiến ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="delivery_date">Ngày giao dự kiến</label>
                <input
                  className="grow"
                  type="date"
                  name="delivery_date"
                  id="delivery_date"
                  value={formik.values?.delivery_date || ""}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= Nhà cung cấp ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="suppliers_name">Nhà cung cấp</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="Nhập nhà cung cấp"
                  name="suppliers_name"
                  id="suppliers_name"
                  value={formik.values.suppliers_name || ""}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= Trạng thái ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="po_status_name">Trạng thái đơn hàng</label>
                <input
                  className="grow"
                  type="string"
                  name="po_status_name"
                  value={formik.values?.po_status_name || ""}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= Phương thức giao hàng ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="shippingMethod_name">
                  Phương thức giao hàng{" "}
                </label>
                <input
                  className="grow"
                  type="string"
                  name="shippingMethod_name"
                  id="shippingMethod_name"
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                  value={formik.values?.shippingMethod_name || ""}
                />
              </div>
            </div>
          )}
          {/* ======================= Kho nhận ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="warehouse_name">Kho nhận </label>
                <input
                  className="grow"
                  type="string"
                  name="warehouse_name"
                  id="warehouse_name"
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                  value={
                    formik.values?.warehouse_name
                      ? formik.values?.warehouse_name
                      : ""
                  }
                />
              </div>
            </div>
          )}
          {/* ======================= Người mua ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="buyer_name">Người mua</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="Người mua"
                  name="buyer_name"
                  id="buyer_name"
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                  value={formik.values?.buyer_name || ""}
                />
              </div>
            </div>
          )}
          {/* ======================= SĐT người mua ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="buyer_phone">SĐT người mua</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="SĐT người mua"
                  name="buyer_phone"
                  id="buyer_phone"
                  value={formik.values?.buyer_phone || ""}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= Người nhận ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="receiver_name">Người nhận</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="Người nhận"
                  name="receiver_name"
                  id="receiver_name"
                  value={
                    formik.values?.receiver_name
                      ? formik.values?.receiver_name
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= SĐT người nhận ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="receiver_phone">SĐT người nhận</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="SĐT người nhận"
                  name="receiver_phone"
                  id="receiver_phone"
                  value={formik.values?.receiver_phone || ""}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
        </div>
      </FormikProvider>
    </div>
  );
}
