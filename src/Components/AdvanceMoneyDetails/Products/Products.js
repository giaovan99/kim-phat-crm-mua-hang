import { HiOutlineTrash } from "react-icons/hi2";
import { Popconfirm, Table, Tooltip, message } from "antd";
import React from "react";
import Swal from "sweetalert2";
import { checkNumber } from "../../../utils/others";
import InputNumberFormat from "../../Input/InputNumberFormat";
import ModalListProduct from "../../Warehouse/ImportWarehouse/ListOrders/Components/ModalListProducts/ModalListProduct";

export default function Products({
  formik,
  setProductsSelected,
  current,
  setIsAddProductsModal,
  handleOkAddProducts,
  handleCancelAddProducts,
  orderChoosen,
  productsSelected,
  isAddProductsModal,
}) {
  const tableProps = {
    bordered: true,
    size: "small",
  };
  const { touched, errors } = formik;

  const confirm = (record) => {
    // console.log(record);
    let arr = [...productsSelected];
    let index = arr.findIndex((item) => item.product_id == record.id);
    arr.splice(index, 1);
    setProductsSelected([...arr]);
    // message.success("Xoá thành công");
    Swal.fire({
      title: "Xoá",
      text: "Xoá thành công",
      icon: "success",
      confirmButtonText: "Đóng",
    });
  };

  const cancel = (e) => {
    // console.log(e);
    message.error("Đã huỷ");
  };

  // Hàm tìm vị trí
  const fFindIndex = (record, arr) => {
    let index = arr.findIndex(
      (element) => element.product_id == record.product_id
    );
    return index;
  };

  // Hàm xử lý thay đổi số lượng phần tử của mảng
  const fChangeNumber = (value, record) => {
    let arr = [...productsSelected];
    let index = fFindIndex(record, arr);
    arr[index].qty_actual = checkNumber(value);
    setProductsSelected([...arr]);
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      fixed: "left",
      width: 50,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Mã sản phẩm",
      dataIndex: "sku",
      key: "sku",
      width: 120,
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
      width: 80,
      render: (text, record) => {
        return (
          <p className="text-center">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 80,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },

    {
      title: "Số lượng",
      dataIndex: "qty",
      key: "qty",
      width: 80,
      render: (text, record) => {
        return (
          <p className="text-center">{Number(text).toLocaleString("en-US")}</p>
        );
      },
    },
    {
      title: "Số lượng thực tế",
      dataIndex: "qty_actual",
      key: "qty_actual",
      width: 80,
      render: (text, record, index) => {
        return (
          <InputNumberFormat
            className={"text-center w-full"}
            value={record?.qty_actual ? record?.qty_actual : ""}
            onChange={fChangeNumber}
            record={record}
            name={"qty_actual"}
          />
        );
      },
    },
    {
      title: "Hoá đơn",
      dataIndex: "vat_name",
      key: "vat_name",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Thành tiền",
      children: [
        {
          title: "Trước VAT",
          dataIndex: "sub_total",
          key: "sub_total",
          width: 150,
          render: (text, record) => {
            return (
              <p className="text-right">
                {Number(text).toLocaleString("en-US")}
              </p>
            );
          },
        },
        {
          title: "Sau VAT",
          dataIndex: "total",
          key: "total",
          width: 150,
          render: (text, record) => {
            return (
              <p className="text-right">
                {Number(text).toLocaleString("en-US")}
              </p>
            );
          },
        },
      ],
    },
    {
      title: "Hành động",
      key: "action",
      width: 80,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Xoá ======================= */}
            <Popconfirm
              placement="topRight"
              title="Xoá sản phẩm"
              description="Bạn chắc chắn muốn xoá sản phẩm này?"
              onConfirm={() => confirm(record.product_id)}
              onCancel={cancel}
              okText="Xoá"
              cancelText="Huỷ"
              okButtonProps={{ className: "btn-main-yl" }}
              className="popover-danger"
            >
              <Tooltip placement="bottom" title="Xoá">
                <button type="button">
                  <HiOutlineTrash className="text-rose-600" />
                </button>
              </Tooltip>
            </Popconfirm>
          </div>
        );
      },
    },
  ];
  return (
    <div className="productsAddPage">
      {/* ======================= Sản phẩm ======================= */}
      <div
        className={
          current > 1
            ? "pointer-events-none form-group mb-2"
            : "form-group mb-2"
        }
      >
        <button
          type="button"
          className="btn-actions btn-main-bl"
          onClick={() => {
            setIsAddProductsModal(true);
          }}
        >
          Chọn sản phẩm từ đơn hàng
        </button>
        {touched.items && errors.items ? (
          <p className="mt-1 text-red-500 text-sm" id="items-warning">
            {errors.items}
          </p>
        ) : (
          <></>
        )}
      </div>
      {/* ======================= Danh sách sản phẩm ======================= */}
      <Table
        {...tableProps}
        pagination={{
          position: ["none", "bottomCenter"],
          pageSize: 20,
          hideOnSinglePage: true,
        }}
        scroll={{
          y: 600,
          x: 1300,
        }}
        columns={columns}
        dataSource={formik.values.items}
      />

      {/* ======================= Modals ======================= */}
      {isAddProductsModal && (
        <ModalListProduct
          handleOkAddProducts={handleOkAddProducts}
          handleCancelAddProducts={handleCancelAddProducts}
          data={orderChoosen.items}
          setProductChoosen={setProductsSelected}
          isModalOpen={isAddProductsModal}
          orderChoosen={orderChoosen}
          productsChoosen={productsSelected}
        />
      )}
    </div>
  );
}
