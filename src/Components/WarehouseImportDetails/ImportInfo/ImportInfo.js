import { Select, Spin } from "antd";
import React from "react";
import { Field, FormikProvider } from "formik";

export default function ImportInfo({
  formik,
  filterOption,
  current,
  importKey,
  onChangeOrder,
  listOrder,
  listStaff,
  orderChoosen,
  setN,
  getListStaffProduct,
  loadingSelect,
  onChangeStaffNumber,
  getListDepartments,
  getListWarehouse,
  listWarehouse,
  listDepartment,
}) {
  const { handleBlur, handleChange, touched, errors } = formik;
  const onSearch = (value) => {};
  const onChangeSelect = async (value, tag) => {
    // console.log(value);
    formik.setFieldValue(tag, value);
  };
  return (
    <div className="orderInfo">
      <FormikProvider value={formik}>
        <div
          className={
            current > 2
              ? "pointer-events-none grid md:grid-cols-2 xl:grid-cols-4 gap-2"
              : "grid md:grid-cols-2 xl:grid-cols-4 gap-2"
          }
        >
          {/* ======================= Mã phiếu nhập kho ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="code">Mã phiếu nhập kho</label>
              <input
                className="grow"
                type="string"
                placeholder="Hệ thống tự tạo"
                name="code"
                id="code"
                value={formik.values.code}
                disabled
              />
            </div>
            {touched.code && errors.code ? (
              <p className="mt-1 text-red-500 text-sm" id="code-warning">
                {errors.code}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Người nhập kho ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="staff_number">
                Người nhập kho <span className="text-red-500">*</span>
              </label>
              <Field name="staff_number">
                {({ field }) => {
                  return (
                    <Select
                      {...field}
                      id="staff_number"
                      name="staff_number"
                      showSearch
                      placeholder="Chọn người nhập kho"
                      optionFilterProp="children"
                      onChange={(value) => {
                        formik.setFieldValue("staff_number", value);
                        onChangeStaffNumber(value);
                      }}
                      filterOption={filterOption}
                      options={listStaff}
                      value={
                        formik.values?.staff_number_name
                          ? formik.values?.staff_number_name
                          : formik.values.staff_number
                          ? formik.values.staff_number
                          : null
                      }
                      onBlur={handleBlur}
                      onDropdownVisibleChange={getListStaffProduct}
                      notFoundContent={
                        loadingSelect ? <Spin size="small" /> : null
                      }
                    />
                  );
                }}
              </Field>
            </div>
            {touched.staff_number && errors.staff_number ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="staff_number-warning"
              >
                {errors.staff_number}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Mã đơn hàng ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="po_id">
                Mã đơn hàng <span className="text-red-500">*</span>
              </label>
              <Field name="po_id">
                {({ field }) => {
                  return (
                    <Select
                      {...field}
                      showSearch
                      placeholder="Chọn đơn hàng"
                      optionFilterProp="children"
                      onChange={(value) => {
                        setN(1);
                        onChangeOrder(value);
                      }}
                      onSearch={onSearch}
                      filterOption={filterOption}
                      options={listOrder}
                      onBlur={handleBlur}
                      value={formik.values?.po_detail?.code || ""}
                    />
                  );
                }}
              </Field>
            </div>
            {touched.po_id && errors.po_id ? (
              <p className="mt-1 text-red-500 text-sm" id="po_id-warning">
                {errors.po_id}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Ngày giao thực tế ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="delivery_actual_date">
                Ngày giao thực tế<span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="date"
                name="delivery_actual_date"
                id="delivery_actual_date"
                value={
                  formik.values.delivery_actual_date
                    ? formik.values.delivery_actual_date
                    : ""
                }
                onChange={handleChange}
                onBlur={handleBlur}
              />
            </div>
            {errors.delivery_actual_date && touched.delivery_actual_date ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="delivery_actual_date-warning"
              >
                {errors.delivery_actual_date}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Ngày giao dự kiến ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="delivery_date">Ngày giao dự kiến</label>
                <input
                  className="grow"
                  type="date"
                  name="delivery_date"
                  id="delivery_date"
                  value={
                    formik.values?.delivery_date
                      ? formik.values?.delivery_date
                      : ""
                  }
                  onChange={handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= Nhà cung cấp ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="suppliers_name">Nhà cung cấp</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="Hệ thống tự render"
                  name="suppliers_name"
                  id="suppliers_name"
                  value={
                    formik.values?.po_detail?.suppliers_short_name
                      ? formik.values?.po_detail?.suppliers_short_name
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= Trạng thái ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="po_status_name">Trạng thái đơn hàng</label>
                <input
                  className="grow"
                  type="string"
                  name="status_name"
                  value={
                    formik.values?.po_detail?.status_name
                      ? formik.values?.po_detail?.status_name
                      : ""
                  }
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= Phương thức giao hàng ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="shippingMethod_name">
                  Phương thức giao hàng{" "}
                </label>
                <input
                  className="grow"
                  type="string"
                  name="shippingMethod_name"
                  id="shippingMethod_name"
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                  // value={
                  //   formik.values?.po_detail?.shippingMethod_name
                  //     ? formik.values?.po_detail?.shippingMethod_name
                  //     : ""
                  // }
                  value={
                    formik.values?.shippingMethod_name
                      ? formik.values?.shippingMethod_name
                      : ""
                  }
                />
              </div>
            </div>
          )}

          {/* ======================= Người mua ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="buyer_name">Người mua</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="Người mua"
                  name="buyer_name"
                  id="buyer_name"
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                  value={
                    formik.values?.po_detail?.buyer_name
                      ? formik.values?.po_detail?.buyer_name
                      : ""
                  }
                />
              </div>
            </div>
          )}
          {/* ======================= SĐT người mua ======================= */}
          {formik.values.po_id && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="buyer_phone">SĐT người mua</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="SĐT người mua"
                  name="buyer_phone"
                  id="buyer_phone"
                  value={
                    formik.values?.po_detail?.buyer_phone
                      ? formik.values?.po_detail?.buyer_phone
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= Người nhận ======================= */}
          {formik.values.staff_number && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="receiver_name">Người nhận</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="Người nhận"
                  name="receiver_name"
                  id="receiver_name"
                  value={
                    formik.values?.receiver_name
                      ? formik.values?.receiver_name
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= SĐT người nhận ======================= */}
          {formik.values.staff_number && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="receiver_phone">SĐT người nhận</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="SĐT người nhận"
                  name="receiver_phone"
                  id="receiver_phone"
                  value={
                    formik.values?.receiver_phone
                      ? formik.values?.receiver_phone
                      : ""
                  }
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                  disabled
                />
              </div>
            </div>
          )}
          {/* ======================= Nhập tại kho ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="warehouse_import">Nhập tại kho</label>
              <Select
                id="warehouse"
                name="warehouse"
                showSearch
                placeholder="Chọn kho nhận"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "warehouse")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={listWarehouse}
                value={
                  listWarehouse.length <= 0 && formik.values.warehouse_name
                    ? formik.values.warehouse_name
                    : formik.values.warehouse != ""
                    ? formik.values.warehouse
                    : ""
                }
                onBlur={handleBlur}
                onDropdownVisibleChange={getListWarehouse}
                notFoundContent={loadingSelect ? <Spin size="small" /> : null}
              />
            </div>
          </div>
          {/* ======================= Bộ phận sử dụng ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="department">Bộ phận sử dụng</label>
              <Select
                id="department"
                name="department"
                showSearch
                placeholder="Chọn bộ nhận"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "department")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={listDepartment}
                value={
                  listDepartment.length <= 0 && formik.values.department_name
                    ? formik.values.department_name
                    : formik.values.department != ""
                    ? formik.values.department
                    : ""
                }
                onBlur={handleBlur}
                onDropdownVisibleChange={getListDepartments}
                notFoundContent={loadingSelect ? <Spin size="small" /> : null}
              />
            </div>
          </div>
        </div>
      </FormikProvider>
    </div>
  );
}
