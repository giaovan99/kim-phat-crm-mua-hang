import { HiOutlineTrash } from "react-icons/hi2";
import { Popconfirm, Table, Tooltip, message } from "antd";
import React from "react";
import Swal from "sweetalert2";
import { checkNumber } from "../../../utils/others";
import InputNumberFormat from "../../Input/InputNumberFormat";
import ModalListProduct from "../../Warehouse/ImportWarehouse/ListOrders/Components/ModalListProducts/ModalListProduct";

export default function Products({
  formik,
  setProductsSelected,
  current,
  setIsAddProductsModal,
  handleOkAddProducts,
  handleCancelAddProducts,
  orderChoosen,
  productsSelected,
  isAddProductsModal,
}) {
  const tableProps = {
    bordered: true,
    size: "small",
  };
  const { handleBlur, handleChange, touched, errors } = formik;

  const confirm = (index) => {
    let arr = [...productsSelected];
    arr.splice(index, 1);
    setProductsSelected([...arr]);
    // message.success("Xoá thành công");
    Swal.fire({
      title: "Xoá",
      text: "Xoá thành công",
      icon: "success",
      confirmButtonText: "Đóng",
    });
  };

  const cancel = (e) => {
    message.error("Đã huỷ");
  };

  // Hàm xử lý thay đổi số lượng phần tử của mảng
  const fChangeNumber = (value, index) => {
    let arr = [...productsSelected];
    arr[index].qty_actual = checkNumber(value);
    setProductsSelected([...arr]);
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      fixed: "left",
      width: 50,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Mã SP",
      dataIndex: "sku",
      key: "sku",
      width: 120,
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },

    {
      title: "Tên SP",
      dataIndex: "product_name",
      key: "product_name",
    },
    {
      title: (
        <>
          Mã số <br />
          (Quy cách)
        </>
      ),
      dataIndex: "package_case",
      key: "package_case",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Số lượng",
      children: [
        {
          title: "Theo chứng từ",
          dataIndex: "qty",
          key: "qty",
          width: 120,
          render: (text, record) => {
            return (
              <p className="text-center">
                {Number(text).toLocaleString("en-US")}
              </p>
            );
          },
        },
        {
          title: "Thực nhập",
          dataIndex: "qty_actual",
          key: "qty_actual",
          width: 120,
          render: (text, record, index) => {
            return (
              <InputNumberFormat
                className={"text-center w-full"}
                value={record?.qty_actual ? record?.qty_actual : ""}
                onChange={fChangeNumber}
                index={index}
                name={"qty_actual"}
                disable={current > 2 ? true : false}
              />
            );
          },
        },
      ],
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      render: (text, record, index) => {
        return (
          <div className="form-group ">
            <div className="form-style">
              <textarea
                className="w-full"
                name={record.note}
                value={record.note}
                type="text"
                rows={1}
                onChange={(event) => {
                  let arr = [...productsSelected];
                  // Gắn giá trị mới vào mảng
                  arr[index].note = event.target.value;
                  setProductsSelected([...arr]);
                }}
                disabled={current > 2 ? true : false}
              ></textarea>
            </div>
          </div>
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (text, record, index) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Xoá ======================= */}
            <Popconfirm
              placement="topRight"
              title="Xoá sản phẩm"
              description="Bạn chắc chắn muốn xoá sản phẩm này?"
              onConfirm={() => confirm(index)}
              onCancel={cancel}
              okText="Xoá"
              cancelText="Huỷ"
              okButtonProps={{ className: "btn-main-yl" }}
              className="popover-danger"
              disabled={current > 2 ? true : false}
            >
              <Tooltip placement="bottom" title="Xoá">
                <button type="button">
                  <HiOutlineTrash className="text-rose-600" />
                </button>
              </Tooltip>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  return (
    <div className="productsAddPage">
      {/* ======================= Sản phẩm ======================= */}
      {orderChoosen && (
        <div
          className={
            current > 2
              ? "pointer-events-none form-group mb-2"
              : "form-group mb-2"
          }
        >
          <button
            type="button"
            className="btn-actions btn-main-bl"
            onClick={() => {
              setIsAddProductsModal(true);
            }}
          >
            Chọn sản phẩm từ đơn hàng
          </button>
          {touched.items && errors.items ? (
            <p className="mt-1 text-red-500 text-sm" id="items-warning">
              {errors.items}
            </p>
          ) : (
            <></>
          )}
        </div>
      )}

      {/* ======================= Danh sách sản phẩm ======================= */}

      <Table
        {...tableProps}
        pagination={{
          position: ["none", "bottomCenter"],
          pageSize: 20,
          hideOnSinglePage: true,
        }}
        scroll={{
          y: 600,
          x: 1300,
        }}
        columns={columns}
        dataSource={formik.values.items}
      />

      {/* ======================= Modals ======================= */}
      {isAddProductsModal && (
        <ModalListProduct
          handleOkAddProducts={handleOkAddProducts}
          handleCancelAddProducts={handleCancelAddProducts}
          data={orderChoosen.items}
          setProductChoosen={setProductsSelected}
          isModalOpen={isAddProductsModal}
          orderChoosen={orderChoosen}
          productsChoosen={productsSelected}
          page="order"
        />
      )}
    </div>
  );
}
