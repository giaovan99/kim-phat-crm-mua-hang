import React, { useEffect, useState } from "react";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import { Collapse } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { IoSaveOutline } from "react-icons/io5";
import { LiaUndoSolid } from "react-icons/lia";
import { CaretRightOutlined } from "@ant-design/icons";
import { getOrder } from "../../utils/order";
import SetStatus2Step from "../SetStatus2Step/SetStatusProposal";
import ImportInfo from "./ImportInfo/ImportInfo";
import Products from "./Products/Products";
import {
  createImportWarehouse,
  getImportWarehouse,
  getImportWarehouseStatusList,
  getPOWarehouse,
  getStaffWarehouse,
  updateImportWarehouse,
  warehouseList,
} from "../../utils/warehouse";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import { useDispatch, useSelector } from "react-redux";
import { normalize } from "../../utils/others";
import { getListDepartment, getListUser, getUser } from "../../utils/user";

// Lọc filter
const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

export default function WarehouseImportDetails() {
  // 1 - Lấy giá trị từ params
  let { importKey } = useParams();
  // Khai báo useNavigate để chuyển hướng trang
  let navigate = useNavigate();
  const [searchParams] = useSearchParams();

  // 2 - Khai báo state
  const [productsSelected, setProductsSelected] = useState([]); // Quản lý danh sách sản phẩm được chọn
  const [current, setCurrent] = useState(1); // Quản lý vị trí của trạng thái
  const [importStatus, setImportStatus] = useState(); //Danh sách trạng thái
  let [orderChoosen, setOrderChoosen] = useState(); //mã đơn + danh sách sản phẩm của đơn
  let [listOrder, setListOrder] = useState(false); //hiển thị danh sách đơn hàng
  let [listStaff, setListStaff] = useState([]); //hiển thị danh sách nhân viên cóp thể làm phiếu nhập kho
  let [isAddProductsModal, setIsAddProductsModal] = useState(false); //hiển thị Modal danh sách sản phẩm của đơn hàng được chọn
  let [n, setN] = useState(0); //hiển thị Modal danh sách sản phẩm của đơn hàng được chọn
  const [loadingSelect, setLoadingSelect] = useState(false);
  const [listDepartment, setListDepartment] = useState([]); //Danh sách bộ phận
  const [listWarehouse, setListWarehouse] = useState([]); //Danh sách kho

  let dispatch = useDispatch();

  // 2 - Khai báo giá trị ban đầu
  const initial = {
    po_id: "",
    delivery_actual_date: "",
    items: [],
    status: 1,
    warehouse: "",
    department: "",
    po_detail: {},
    created_by: "",
    staff_number: "",
    receiver_phone: "",
    receiver_name: "",
  };

  // 3 - Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      po_id: yup.string().required("Chọn mã đơn hàng"),
      delivery_actual_date: yup
        .string()
        .required("Ngày giao thực tế không để trống"),
      staff_number: yup.string().required("Người đề nghị không để trống"),
      items: yup
        .array()
        .min(1, "Chọn sản phẩm nhập kho")
        .test(
          "check-actual-quantity",
          "Số lượng thực tế của sản phẩm phải lớn hơn 0",
          function (value) {
            if (!value || !value.length) {
              return false; // Mảng sản phẩm trống
            }
            // Kiểm tra từng sản phẩm trong mảng
            for (const product of value) {
              if (!product.qty_actual || product.qty_actual <= 0) {
                return false; // Số lượng thực tế không được để trống
              }
            }
            return true;
          }
        ),
      warehouse: yup.string().nullable(),
      department: yup.string().nullable(),
      warehouse_address: yup.string().nullable(),
      receiver_phone: yup.string().nullable(),
      receiver_name: yup.string().nullable(),
      po_detail: yup.object().nullable(),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      let res;
      let obj = setValuesAdd(values);
      // alert thông báo
      if (importKey) {
        // Sửa phiếu nhập kho
        res = await updateImportWarehouse({ ...obj, id: importKey });
      } else {
        // Tạo phiếu nhập kho
        res = await createImportWarehouse(obj, formik);
      }
      if (res) {
        let debug = searchParams.get("debug");
        if (!debug) {
          navigate(0);
        }
      }
      dispatch(setSpinner(false));
    },
  });

  const setValuesAdd = (values) => {
    let obj = {
      po_id: values.po_id,
      delivery_actual_date: values.delivery_actual_date,
      items: values.items,
      staff_number: values.staff_number,
      warehouse: values.warehouse,
      department: values.department,
      receiver_phone: values.receiver_phone,
      receiver_name: values.receiver_name,
      status: values.status,
      shippingMethod: values.po_detail.shippingMethod,
      shippingMethod_name: values.po_detail.shippingMethod_name,
    };
    return obj;
  };

  //  6 - Lấy tất cả các mảng cố định
  let getArr = async (page) => {
    dispatch(setSpinner(true));
    let res;
    // 5.10 - Danh sách trạng thái
    res = await getImportWarehouseStatusList();
    if (res.status) {
      setImportStatus(res.data);
    }
    // 5.7 - Danh sách đơn hàng lấy từ options
    // res = await getOrderList(page, { show_all: true });
    res = await getPOWarehouse();
    if (res.status) {
      setListOrder(res.data);
    }

    dispatch(setSpinner(false));
  };

  //  6 - Lấy danh sách người đề nghị
  const getListStaffProduct = async (page) => {
    setLoadingSelect(true);
    if (listStaff.length === 0) {
      let res = await getListUser(1, { department: 12 });
      if (res.status) {
        const transformedArray = res.data.data.map((item) => ({
          value: item.id,
          label: item.full_name,
        }));
        setListStaff(transformedArray);
      }
    }
    setLoadingSelect(false);
  };

  // Khi người đề nghị thay đổi thì update thông tin người nhận & sdt người nhận
  const onChangeStaffNumber = async (value) => {
    let res = await getUser(value);
    if (res.data.success) {
      formik.setFieldValue(
        "receiver_phone",
        res.data.data?.phone ? res.data.data.phone : ""
      );
      formik.setFieldValue(
        "receiver_name",
        res.data.data?.full_name ? res.data.data.full_name : ""
      );
    }
  };

  // 4 - Lấy thông tin chi tiết phiếu nhập kho
  const APIGetImportwarehouse = async () => {
    dispatch(setSpinner(true));
    let res = await getImportWarehouse(importKey);
    if (res.data.success) {
      onChangeOrder(res.data.data.po_id, false);
      setProductsSelected(res.data.data.items);
      setCurrent(res.data.data.status * 1);
      formik.setValues(res.data.data);
    }
    dispatch(setSpinner(false));
  };

  // Render lần đầu khi load trang check xem đang Thêm hay Xoá Phiếu nhập kho
  useEffect(() => {
    getArr();
    if (importKey) {
      APIGetImportwarehouse();
    } else {
      formik.setValues(initial);
    }
  }, []);

  //  6 - Lấy danh sách các kho
  const getListWarehouse = async (page) => {
    setLoadingSelect(true);
    if (listWarehouse.length <= 0) {
      let res = await warehouseList(page, { show_all: true });
      if (res.status) {
        setListWarehouse(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách các bộ phận
  const getListDepartments = async (page) => {
    setLoadingSelect(true);
    if (listDepartment.length <= 0) {
      let res = await getListDepartment();
      if (res.status) {
        setListDepartment(res.data);
      }
    }
    setLoadingSelect(false);
  };

  // 10 - Lấy thông tin chi tiết đơn hàng
  const getOrderInfo = async (id) => {
    let res = await getOrder(id);
    if (res.data.success) {
      return res.data.data;
    } else {
      return false;
    }
  };

  // 11 - Lọc lại các key cần thiết của
  const getItemsArr = (arr) => {
    let newArr = [];
    arr.forEach((item) => {
      newArr.push({
        sku: item.sku, //Mã sp
        product_name: item.product_name, // Tên sp
        unit: item.unit, // DVT
        qty: item.qty, // Số lượng theo chứng từ
        qty_actual: 0, // Thực nhập
        note: "", // Ghi chú
        package_case: item.package_case, //Quy cách
        did: null,
        po_did: item.did,
        product_id: item.product_id,
      });
    });
    return newArr;
  };

  // 12 - Khi mã đơn hàng thay đổi => kéo api chi tiết đơn để lấy đc danh sách sản phẩm
  const onChangeOrder = async (value, open = true) => {
    // Lấy thông tin chi tiết của đơn hàng
    dispatch(setSpinner(true));
    let order = await getOrderInfo(value);
    if (order) {
      let obj = {
        ...order,
        items: [...getItemsArr(order.items)],
      };
      setOrderChoosen(obj);
    }
    dispatch(setSpinner(false));
  };

  useEffect(() => {
    if (n > 0) {
      formik.setFieldValue("po_detail", orderChoosen);
      formik.setFieldValue("po_id", orderChoosen.id);
      formik.setFieldValue("shippingMethod", orderChoosen.shippingMethod);
      formik.setFieldValue(
        "shippingMethod_name",
        orderChoosen.shippingMethod_name
      );
    }
  }, [orderChoosen]);

  // 13 - Khi xác nhận thêm sản phẩm
  const handleOkAddProducts = (value) => {
    //  value: mảng sản phẩm được chọn, check xem items thuộc value đã có trong mảng chưa, nếu chưa thì thêm vào mảng
    let arr = [...productsSelected];

    value.forEach((item) => {
      let index = productsSelected.findIndex(
        (product) => product.po_did == item.po_did
      );
      if (index === -1) {
        arr.push({ ...item, advance_money: 0 });
      }
    });
    setProductsSelected(arr);
    formik.setFieldValue("items", arr);
    setIsAddProductsModal(false);
  };

  // Khi huỷ chọn sản phẩm
  const handleCancelAddProducts = () => {
    setIsAddProductsModal(false);
  };

  // Khi productsChoosen thay đổi thì gắn giá trị cho formik products
  useEffect(() => {
    if (productsSelected) {
      formik.setFieldValue("items", productsSelected);
    }
  }, [productsSelected]);

  //  7 -  Trở về trang Đề nghị mua hàng
  const goToWarehouseImport = () => {
    navigate("/stock_import");
  };

  //  9 - Khi current thay đổi
  useEffect(() => {
    formik.values.status = current;
  }, [current]);

  // 10 - Danh sách các Tabs
  const getItems = [
    {
      key: "info",
      label: (
        <h2 className="text-[1rem] font-medium mb-1">
          Thông tin phiếu nhập kho
        </h2>
      ),
      children: (
        <ImportInfo
          formik={formik}
          filterOption={filterOption}
          current={current}
          importKey={importKey}
          onChangeOrder={onChangeOrder}
          listOrder={listOrder}
          listStaff={listStaff}
          orderChoosen={orderChoosen}
          setN={setN}
          getListStaffProduct={getListStaffProduct}
          loadingSelect={loadingSelect}
          onChangeStaffNumber={onChangeStaffNumber}
          getListDepartments={getListDepartments}
          getListWarehouse={getListWarehouse}
          listWarehouse={listWarehouse}
          listDepartment={listDepartment}
        />
      ),
    },
    {
      key: "products",
      label: <h2 className="text-[1rem] font-medium mb-1">Sản phẩm</h2>,
      children: (
        <Products
          formik={formik}
          filterOption={filterOption}
          productsSelected={productsSelected}
          setProductsSelected={setProductsSelected}
          current={current}
          importKey={importKey}
          setIsAddProductsModal={setIsAddProductsModal}
          handleOkAddProducts={handleOkAddProducts}
          handleCancelAddProducts={handleCancelAddProducts}
          orderChoosen={orderChoosen}
          isAddProductsModal={isAddProductsModal}
        />
      ),
    },
  ];
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );

  return (
    <>
      {/* ======================= Content ======================= */}
      <div className="page-content">
        {/* ======================= Trạng thái  ======================= */}
        {importKey && (
          <div className="form-group col-span-3 mb-5">
            <div className="form-style">
              <label htmlFor="contractSigningDate" className="mb-2">
                Trạng thái
              </label>
              <SetStatus2Step
                statusOptions={importStatus}
                current={current}
                setCurrent={setCurrent}
                page={"importWarehouse"}
                permissionUpdate={permission?.stock_import?.update}
              />
            </div>
          </div>
        )}
        {/* ======================= Tabs lựa chọn ======================= */}
        <Collapse
          bordered={true}
          defaultActiveKey={["info", "products"]}
          expandIcon={({ isActive }) => (
            <CaretRightOutlined rotate={isActive ? 90 : 0} />
          )}
          items={getItems}
        />
        {/* ======================= warning ======================= */}
        <div className="mt-5">
          <p className="text-[#17a2b8]" id="warning">
            {Object.keys(formik.errors).length > 0
              ? "Biểu mẫu chưa hoàn chỉnh thông tin."
              : ""}
          </p>
        </div>
        {/* ======================= btns ======================= */}
        <div className="flex gap-3 mt-5 justify-between">
          <button
            className="btn-actions btn-main-bl"
            onClick={() => {
              formik.handleSubmit();
            }}
          >
            <IoSaveOutline />
            Lưu
          </button>
          <button
            className="btn-actions btn-main-dark"
            onClick={goToWarehouseImport}
            type="button"
          >
            <LiaUndoSolid />
            Huỷ
          </button>
        </div>
      </div>
    </>
  );
}
