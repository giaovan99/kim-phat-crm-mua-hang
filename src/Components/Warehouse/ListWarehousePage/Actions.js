import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch, HiOutlinePlusCircle } from "react-icons/hi";
import { setKeywords } from "../../../redux/slice/keywordsSlice";
import { useDispatch } from "react-redux";
import DropdownOptions from "../../DropdownOptions/DropdownOptions";
import AddBtnComponent from "../../ButtonComponent/AddBtnComponent";

export default function Actions({
  showModal,
  setReload,
  keySelected,
  setKeySelected,
}) {
  const dispatch = useDispatch();
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: async (values) => {
      // console.log(values);
      dispatch(setKeywords(values.search));
      setReload(true);
    },
  });
  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div className="flex gap-1">
            <DropdownOptions
              arrId={keySelected}
              page={"warehouse"}
              setReload={setReload}
              setKeySelected={setKeySelected}
            />
            <div className="form-group">
              <div className="form-style">
                <input
                  id="search"
                  name="search"
                  type="text"
                  placeholder="Tìm kiếm kho"
                  value={formik.values.search}
                  onChange={formik.handleChange}
                />
                <button className="btn-main-yl btn-actions" type="submit">
                  <HiOutlineSearch />
                  Tìm kiếm
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      <div className="flex gap-3">
        <AddBtnComponent
          onClickF={() => showModal(null)}
          text={"Thêm kho hàng"}
        />
      </div>
    </div>
  );
}
