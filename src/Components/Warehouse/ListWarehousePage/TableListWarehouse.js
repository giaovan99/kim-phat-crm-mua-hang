import React, { useEffect, useState } from "react";
import { Popconfirm, Table, Tooltip, message } from "antd";
import { HiOutlinePencilSquare, HiOutlineTrash } from "react-icons/hi2";
import { useDispatch, useSelector } from "react-redux";
import {
  deleteWarehouseId,
  warehouseList,
  warehouseUpdate,
} from "../../../utils/warehouse";
import { setPage } from "../../../redux/slice/pageSlice";
import { otherKeys } from "../../../utils/user";
import RenderWithFixComponent from "../../ButtonComponent/RenderIdFix";
import FixBtnComponent from "../../ButtonComponent/FixBtnComponent";
import DeleteBtnItemComponent from "../../ButtonComponent/DeleteBtnItemComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListWareHouse({
  showModal,
  loadPage,
  reload,
  setReload,
  deletedPage,
  rowSelection,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();
  let page = useSelector((state) => state.pageSlice.value);
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list nhà cung cấp theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }
    if (keyParam || keywords) {
      keyParam = { ...keyParam, keywords };
    }

    // console.log(keyParam);

    let res = await warehouseList(page, keyParam);
    if (res.status) {
      setListData(res.data);
      setTotal(res.dataRaw.total);
      setPageSize(res.dataRaw.per_page);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Lần đầu load trang => Kéo về danh sách kho & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    loadPage();
    getListData(page);
  }, []);

  // 5 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      // loadPage();
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  // 6 - Xác nhận xoá kho
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deleteData(id, value);
  };

  // 7 - Xoá kho
  const deleteData = async (id, input) => {
    dispatch(setSpinner(true));
    // console.log(id);
    let res;
    if (input == 1) {
      res = await deleteWarehouseId({ id: id, deleted: true });
    } else {
      res = await deleteWarehouseId({ id: id });
    }
    // console.log(res);
    if (res) {
      // console.log("reload trang");
      setReload(true);
    }
    // Load lại từ trang 1
    dispatch(setSpinner(false));
  };

  // 8 - Định nghĩa các cột trong table
  const columns = [
    {
      title: "Kho",
      dataIndex: "name",
      key: "name",
      // width: 200,
      fixed: "left",
      render: (text, record) => (
        <RenderWithFixComponent
          onClickF={() => {
            showModal(record.id);
          }}
          text={text}
        />
      ),
    },
    {
      title: "Thủ kho",
      dataIndex: "storekeeper_name",
      key: "storekeeper_name",
      // width: 200,
      fixed: "left",
    },
    {
      title: "Sdt thủ kho",
      dataIndex: "storekeeper_phone",
      key: "storekeeper_phone",
      // width: 200,
      fixed: "center",
    },
    {
      title: "Địa chỉ",
      dataIndex: "address",
      key: "address",
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            <FixBtnComponent
              onClickF={() => {
                showModal(record.id);
              }}
            />
            {/* ======================= Xoá ======================= */}
            <DeleteBtnItemComponent
              onClickF={() => {
                if (deletedPage) {
                  confirm(record.id, 1);
                } else {
                  confirm(record.id, 0);
                }
              }}
              text={"kho hàng"}
            />
          </div>
        );
      },
    },
  ];
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 600,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
