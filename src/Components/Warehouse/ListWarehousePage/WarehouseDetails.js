import { FormikProvider, useFormik } from "formik";
import React, { useEffect } from "react";
import * as yup from "yup";
import { Modal, Select } from "antd";
import {
  createWarehouse,
  getWarehouse,
  warehouseUpdate,
} from "../../../utils/warehouse";
import { normalize, showError } from "../../../utils/others";
import InputPhone from "../../Input/InputPhone";
import { getUser } from "../../../utils/user";
const onSearch = (value) => {
  // console.log("search:", value);
};
// Filter khi search
const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

export default function WarehouseDetails({
  isModalOpen,
  setIsModalOpen,
  warehouse,
  setWarehouse,
  setReload,
  deletedPage,
  userOptions,
}) {
  // Khai báo state
  const initial = {
    name: "",
    address: "",
    storekeeper_name: "",
    storekeeper_phone: "",
  };

  // Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      name: yup.string().required("Tên kho không để trống"),
      storekeeper_name: yup.string().required("Thủ kho không để trống"),
      storekeeper_phone: yup
        .string()
        .required("Sdt thủ kho không để trống")
        .matches(
          /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
          "Số điện thoại không hợp lệ"
        ),
    }),
    onSubmit: async (values) => {
      // console.log(values);
      if (warehouse) {
        // console.log("sửa");
        let res = await warehouseUpdate(values);
        if (res) {
          resetForm();
          //  đóng modal
          setIsModalOpen(false);
          //  Load lại trang 1
          setReload(true);
        }
      } else {
        let res = await createWarehouse(values, formik);
        if (res) {
          resetForm();
          //  đóng modal
          setIsModalOpen(false);
          //  Load lại trang 1
          setReload(true);
        }
      }
    },
  });
  const { handleBlur, handleChange, touched, errors } = formik;

  // 3 - Lấy thông tin kho
  const getDataInfo = async () => {
    let res = await getWarehouse(warehouse);
    if (res.data.success) {
      let data = res.data.data;
      // console.log(data);
      formik.setValues(data);
    } else {
      console.log(res);
      showError();
    }
  };

  //  4 - Lần đầu khi load trang => check xem là add hay fix
  useEffect(() => {
    if (warehouse) {
      // lấy thông tin nhà cung cấp trong danh sách nhà cung cấp & show ra màn hình
      getDataInfo();
    } else {
      formik.setValues(initial);
    }
  }, [warehouse]);

  // 5 - Khi lưu Form
  const handleOk = () => {
    formik.handleSubmit();
  };

  // 6 - Reset Form
  const resetForm = () => {
    formik.resetForm();
    formik.setValues(initial);
  };

  // 7 - Khi cancel form
  const handleCancel = () => {
    setWarehouse(null);
    resetForm();
    setIsModalOpen(false);
  };

  const onChangeSelect = async (value, tag) => {
    // console.log(value);
    formik.setFieldValue(tag, value);

    // Nếu tag == "supplier_id" thì check để gắn địa chỉ & người liên hệ & sdt người liên hệ tự động
    if (tag === "storekeeper_name") {
      let res = await getUser(value);
      if (res.data.success) {
        formik.setFieldValue("storekeeper_phone", res.data.data.phone);
      }
    }
  };
  return (
    <Modal
      title={warehouse ? "Chỉnh sửa kho" : "Thêm kho"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: !deletedPage
          ? "btn-main-yl inline-flex items-center justify-center"
          : "hidden",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <FormikProvider value={formik}>
        <form
          id="formAddWarehouse"
          onSubmit={formik.handleSubmit}
          className={!deletedPage ? "" : "pointer-events-none"}
        >
          <div className="grid grid-cols-2 gap-2">
            {/* ======================= Tên kho ======================= */}
            <div className="form-group col-span-2">
              <div className="form-style">
                <label htmlFor="name">
                  Tên kho <span className="text-red-500">*</span>
                </label>
                <input
                  className="grow"
                  type="text"
                  placeholder="Nhập tên kho"
                  name="name"
                  id="name"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                />
              </div>
              {touched.name && errors.name ? (
                <p className="mt-1 text-red-500 text-sm" id="name-warning">
                  {errors.name}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Tên thủ kho ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="storekeeper_name">
                  Tên thủ kho <span className="text-red-500">*</span>
                </label>
                <Select
                  id="storekeeper_name"
                  name="storekeeper_name"
                  showSearch
                  placeholder="Chọn thủ kho"
                  optionFilterProp="children"
                  onChange={(value) =>
                    onChangeSelect(value, "storekeeper_name")
                  }
                  onSearch={onSearch}
                  filterOption={filterOption}
                  options={userOptions}
                  value={
                    formik.values.storekeeper_name != ""
                      ? formik.values.storekeeper_name
                      : null
                  }
                  onBlur={handleBlur}
                />
              </div>
              {touched.storekeeper_name && errors.storekeeper_name ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="storekeeper_name-warning"
                >
                  {errors.storekeeper_name}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= SDT thủ kho ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="storekeeper_phone">
                  Sđt thủ kho<span className="text-red-500">*</span>
                </label>
                <InputPhone
                  className="grow"
                  placeholder="Hệ thống tự render hoặc Nhập để thay đổi"
                  name="storekeeper_phone"
                  value={formik.values.storekeeper_phone}
                  formik={formik}
                />
              </div>
              {touched.storekeeper_phone && errors.storekeeper_phone ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="storekeeper_phone-warning"
                >
                  {errors.storekeeper_phone}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Địa chỉ ======================= */}
            <div className="form-group col-span-2">
              <div className="form-style">
                <label htmlFor="address">Địa chỉ</label>
                <input
                  className="grow"
                  type="text"
                  placeholder="Nhập địa chỉ"
                  name="address"
                  id="address"
                  value={formik.values.address}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                />
              </div>
              {touched.address && errors.address ? (
                <p className="mt-1 text-red-500 text-sm" id="address-warning">
                  {errors.address}
                </p>
              ) : (
                <></>
              )}
            </div>
          </div>
        </form>
      </FormikProvider>
    </Modal>
  );
}
