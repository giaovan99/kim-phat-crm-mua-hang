import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { convertSlipToArray, stringToDate } from "../../../../../utils/others";
import { setPage } from "../../../../../redux/slice/pageSlice";
import { setSpinner } from "../../../../../redux/slice/spinnerSlice";
import { stockImportPartListPO } from "../../../../../utils/order";
import TableLength from "../../../../ButtonComponent/TableLength";
import { stockImportPartList } from "../../../../../utils/warehouse";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "did",
};

export default function TableListProducts({
  reload,
  setReload,
  rowSelection,
  searchFilter,
  setListCodeDH,
  setListCodeNK,
}) {
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();
  let page = useSelector((state) => state.pageSlice.value);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list phiếu nhập kho theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = { ...searchFilter, si_status: [2, 3] };
    let res = await stockImportPartList(page, keyParam);
    if (res.status) {
      setListData(res.data.data);
      setTotal(res.data.total);
      setPageSize(res.data.per_page);
      setListCodeDH(
        res.options?.purchaser_order
          ? convertSlipToArray(res.options?.purchaser_order)
          : []
      );
      setListCodeNK(
        res.options?.stock_import
          ? convertSlipToArray(res.options?.stock_import)
          : []
      );
    }
    dispatch(setSpinner(false));
  };

  // 10 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  Table.SELECTION_COLUMN.fixed = "left";
  const columns = [
    {
      title: "Mã SP",
      dataIndex: "sku",
      key: "sku",
      fixed: "left",
      width: 80,
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
      className: "text-left",
      fixed: "left",
      width: 150,
    },
    {
      title: "Mã phiếu nhập kho",
      dataIndex: "nk_code",
      key: "nk_code",
      width: 120,
      className: "text-center",
    },
    {
      title: "Mã đơn hàng",
      dataIndex: "hd_code",
      key: "hd_code",
      width: 100,
      className: "text-center",
    },
    {
      title: (
        <>
          Ngày giao <br />
          thực tế
        </>
      ),
      dataIndex: "delivery_actual_date",
      key: "delivery_actual_date",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text ? stringToDate(text) : ""}</p>;
      },
    },
    {
      title: (
        <>
          Ngày giao <br />
          dự kiến
        </>
      ),
      dataIndex: "delivery_date",
      key: "delivery_date",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text ? stringToDate(text) : ""}</p>;
      },
    },

    {
      title: "Quy cách",
      dataIndex: "package_case",
      key: "package_case",
    },

    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 80,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",
      width: 120,
      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: (
        <>
          Số lượng
          <br /> thực tế
        </>
      ),
      dataIndex: "qty_actual",
      key: "qty_actual",
      width: 100,
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Số lượng",
      dataIndex: "qty",
      key: "qty",
      width: 100,
      render: (text, record) => {
        return (
          <p className="text-center">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Hoá đơn",
      dataIndex: "vat_name",
      key: "vat_name",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Thành tiền",
      children: [
        {
          title: "Trước VAT",
          dataIndex: "sub_total",
          key: "sub_total",
          width: 120,
          render: (text, record) => {
            return (
              <p className="text-right">
                {text ? Number(text).toLocaleString("en-US") : ""}
              </p>
            );
          },
        },
        {
          title: "Sau VAT",
          dataIndex: "total",
          key: "total",
          width: 120,
          render: (text, record) => {
            return (
              <p className="text-right">
                {text ? Number(text).toLocaleString("en-US") : ""}
              </p>
            );
          },
        },
      ],
    },
    {
      title: "Nơi nhận",
      dataIndex: "warehouse_name",
      key: "warehouse_name",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: (
        <>
          Phương thức <br /> giao hàng
        </>
      ),
      dataIndex: "shippingMethod_name",
      key: "shippingMethod_name",
      width: 110,
    },
  ];
  return (
    <div className="table-render-API">
      <TableLength page={page} pageSize={pageSize} total={total} />
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          x: 2000,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
