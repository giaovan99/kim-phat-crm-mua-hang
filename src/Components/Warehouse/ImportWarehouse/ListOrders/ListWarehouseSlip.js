import React, { useEffect, useState } from "react";
import Actions from "./Components/Action";
import TableListImportWarehouses from "./Components/TableListImportWarehouses";
import ModalImportWarhouseDetail from "./Components/ModalImportWarhouseDetail";
import { useDispatch, useSelector } from "react-redux";
import { resetKeywords } from "../../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../../redux/slice/pageSlice";
import {
  getImportWarehouseStatusList,
  warehouseList,
} from "../../../../utils/warehouse";
import { setSpinner } from "../../../../redux/slice/spinnerSlice";
import { getListDepartment } from "../../../../utils/user";
import { getListSupplier } from "../../../../utils/supplier";
import { getShippingMethodList } from "../../../../utils/order";

export default function ListWarehouseSlip({
  keyNumActive,
  keyTab,
  changeStateAPI,
  deletedPage,
  setDeletedPage,
}) {
  // 1 - Khai báo state
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [importWarehouseChoosen, setImportWarehouseChoosen] = useState();
  const dispatch = useDispatch();
  const [keySelected, setKeySelected] = useState([]); //
  const [listStatus, setListStatus] = useState([]); //Danh sách phân quyền
  const [reload, setReload] = useState(false);
  const [itemSelected, setItemSelected] = useState([]); //Mảng chứa chi tiết các phiếu được chọn để xét status
  const [searchFilter, setSearchFilter] = useState({}); //Search filter
  const [listDepartment, setListDepartment] = useState([]); //Danh sách bộ phận
  const [listShippingMethod, setListShippingMethod] = useState([]); //Danh sách phương thức vận chuyển
  const [listWarehouse, setListWarehouse] = useState([]); //Danh sách kho
  const [listPO, setListPO] = useState([]); //Danh sách kho
  const [listNK, setListNK] = useState([]); //Danh sách kho
  const [loadingSelect, setLoadingSelect] = useState(false);

  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  // 2 - Hiển thị modal chi tiết phiếu
  const showModal = (id) => {
    setImportWarehouseChoosen(id);
    setIsModalOpen(true);
  };

  // 3 - Lấy danh sách mảng trạng thái
  const getArrs = async () => {
    dispatch(setSpinner(true));
    let res = await getImportWarehouseStatusList();
    if (res.status) {
      setListStatus(res.data);
    }
    dispatch(setSpinner(false));
  };

  // 3 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    dispatch(resetKeywords());
    dispatch(resetPage());
    getArrs();
  }, []);

  //  6 - Lấy danh sách các Phương thức vận chuyển
  const getListShippingMethod = async (page) => {
    setLoadingSelect(true);
    if (listShippingMethod.length <= 0) {
      let res = await getShippingMethodList(page);
      if (res.status) {
        setListShippingMethod(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách các kho
  const getListWarehouse = async (page) => {
    setLoadingSelect(true);
    if (listWarehouse.length <= 0) {
      let res = await warehouseList(page, { show_all: true });
      if (res.status) {
        setListWarehouse(res.data);
      }
    }
    setLoadingSelect(false);
  };
  //  6 - Lấy danh sách các bộ phận
  const getListDepartments = async (page) => {
    setLoadingSelect(true);
    if (listDepartment.length <= 0) {
      let res = await getListDepartment();
      if (res.status) {
        setListDepartment(res.data);
      }
    }
    setLoadingSelect(false);
  };

  useEffect(() => {
    if (keyNumActive == keyTab) {
      setReload(true);
      dispatch(resetKeywords());
      dispatch(resetPage());
    }
  }, [keyNumActive]);

  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys, selectedRows) => {
      setKeySelected(selectedRowKeys);
      setItemSelected(selectedRows);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  useEffect(() => {
    setReload(true);
  }, [deletedPage]);

  return (
    <div className="listImportWarehouses">
      {/* ======================= Hành động ======================= */}
      <Actions
        setReload={setReload}
        deletedPage={deletedPage}
        keySelected={keySelected}
        setKeySelected={setKeySelected}
        keyNumActive={keyNumActive}
        keyTab={keyTab}
        listStatus={listStatus}
        permission={permission}
        itemSelected={itemSelected}
        setItemSelected={setItemSelected}
        getListDepartments={getListDepartments}
        getListShippingMethod={getListShippingMethod}
        getListWarehouse={getListWarehouse}
        setSearchFilter={setSearchFilter}
        loadingSelect={loadingSelect}
        listDepartment={listDepartment}
        listShippingMethod={listShippingMethod}
        listWarehouse={listWarehouse}
        listPO={listPO}
        listNK={listNK}
      />

      {/* ======================= Danh sách phiếu nhập kho ======================= */}
      <TableListImportWarehouses
        showModal={showModal}
        reload={reload}
        setReload={setReload}
        deletedPage={deletedPage}
        rowSelection={rowSelection}
        setKeySelected={setKeySelected}
        permission={permission}
        searchFilter={searchFilter}
        setListPO={setListPO}
        setListNK={setListNK}
      />
      {/* ======================= Modals ======================= */}
      <div className="modals">
        {/* ======================= Fix ImportWarehouse ======================= */}
        <ModalImportWarhouseDetail
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          importWarehouse={importWarehouseChoosen}
          statusOptions={listStatus}
          setImportWarehouse={setImportWarehouseChoosen}
          deletedPage={deletedPage}
          permission={permission}
        />
      </div>
    </div>
  );
}
