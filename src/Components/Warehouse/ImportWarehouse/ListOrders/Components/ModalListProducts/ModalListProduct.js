import { Modal, Table } from "antd";
import React, { useState } from "react";
// rowSelection object indicates the need for row selection

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: (record, index) => `${record.po_did}`, // Sử dụng po_id và product_id với index để tạo key duy nhất
};

export default function ModalListProduct({
  handleCancelAddProducts,
  handleOkAddProducts,
  isModalOpen,
  data,
  price = true,
}) {
  const [rowSelecteds, setRowSelecteds] = useState([]);

  const columns = [
    ...(data.some((item) => item.po_no)
      ? [
          {
            title: "Đơn hàng",
            dataIndex: "po_no",
            key: "po_no",
            className: "text-center",
          },
        ]
      : []),
    {
      title: "Mã sản phẩm",
      dataIndex: "sku",
      key: "sku",
      className: "text-center",
      width: 100,
    },
    {
      title: "Tên sản phẩm",
      dataIndex: "product_name",
      key: "product_name",
      width: 200,
      className: "text-center",
    },
  ];
  const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
      setRowSelecteds(selectedRows);
    },
    getCheckboxProps: (record) => ({
      disabled: record.name === "Disabled User",
      name: record.name,
    }),
    preserveSelectedRowKeys: true,
  };
  return (
    <Modal
      title="Chọn sản phẩm "
      centered
      open={isModalOpen}
      onOk={() => {
        handleOkAddProducts(rowSelecteds);
      }}
      onCancel={handleCancelAddProducts}
      okText="Thêm"
      cancelText="Huỷ"
      okButtonProps={{
        className:
          rowSelecteds.length > 0
            ? "btn-main-yl inline-flex items-center justify-center"
            : "hidden",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <Table
        {...tableProps}
        rowKey={(record, index) => record.pp_did || record.po_did || index}
        pagination={{
          position: ["none", "bottomCenter"],
          hideOnSinglePage: true,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
          x: 240,
        }}
        columns={columns}
        dataSource={data}
      />
    </Modal>
  );
}
