import React, { useEffect, useState } from "react";
import { Table, Tag, Tooltip } from "antd";
import {
  convertSlipToArray,
  showError,
  stringToDate,
} from "../../../../../utils/others";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import {
  deleteImportWarehouseId,
  getImportWarehouse,
  getImportWarehouseList,
  updateImportWarehouse,
} from "../../../../../utils/warehouse";
import { setPage } from "../../../../../redux/slice/pageSlice";
import { otherKeys } from "../../../../../utils/user";
import DeleteBtnItemComponent from "../../../../ButtonComponent/DeleteBtnItemComponent";
import FixBtnComponent from "../../../../ButtonComponent/FixBtnComponent";
import RenderWithViewComponent from "../../../../ButtonComponent/RenderIdView";
import ViewBtnComponent from "../../../../ButtonComponent/ViewBtnComponent";
import UndoBtnComponent from "../../../../ButtonComponent/UndoBtnComponent";
import { setSpinner } from "../../../../../redux/slice/spinnerSlice";
import { FaFilePdf } from "react-icons/fa6";
import ReactToPrint from "react-to-print";
import PrintImportWarehouse from "../../../../ExportToPDF/ImportWarehousePDF/ImportWarehousePDF";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListImportWarehouses({
  showModal,
  reload,
  setReload,
  deletedPage,
  rowSelection,
  setKeySelected,
  permission,
  setListPO,
  setListNK,
  searchFilter,
}) {
  // 2 - Chuyển hướng sang trang chi tiết
  const goToFix = (key) => {
    navigate(`/stock_import/fix/${key}`);
  };

  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();
  let navigate = useNavigate();
  let page = useSelector((state) => state.pageSlice.value);
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list phiếu nhập kho theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }
    // Check xem cần truyền lên APIs key deleted không
    keyParam = { ...keyParam, ...searchFilter };
    let res = await getImportWarehouseList(page, keyParam);

    // console.log(res);
    if (res.status) {
      setListData(res.data.data);
      setTotal(res.data.total);
      setPageSize(res.data.per_page);
      setListPO(
        res?.options?.purchase_order
          ? convertSlipToArray(res?.options?.purchase_order)
          : []
      );
      setListNK(
        res?.options?.stock_import
          ? convertSlipToArray(res?.options?.stock_import)
          : []
      );
    }
    dispatch(setSpinner(false));
  };

  // 4 - Xác nhận xoá phiếu nhập kho
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    deleteData(id, value);
  };

  // 5 - Xoá phiếu nhập kho
  const deleteData = async (id, input) => {
    dispatch(setSpinner(true));
    let res;
    if (input == 1) {
      res = await deleteImportWarehouseId({ id: id, deleted: true });
    } else {
      res = await deleteImportWarehouseId({ id: id });
    }
    if (res) {
      setKeySelected([]);
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 6 - Hoàn tác xoá phiếu nhập kho
  const undoDelete = async (id) => {
    dispatch(setSpinner(true));
    let res;
    res = await updateImportWarehouse({ id: id, deleted: false });
    if (res) {
      setKeySelected([]);
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 7 - Kích hoạt đơn hàng
  const confirmUndo = (id) => {
    undoDelete(id);
  };

  // 10 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  Table.SELECTION_COLUMN.fixed = "left";
  const columns = [
    {
      title: (
        <>
          Mã phiếu <br /> nhập kho
        </>
      ),
      dataIndex: "code",
      key: "code",
      width: 95,
      fixed: "left",
      className: "text-center",
      render: (text, record) => (
        <RenderWithViewComponent
          onClickF={() => {
            showModal(record.id);
          }}
          text={text}
        />
      ),
    },
    {
      title: "Ngày tạo",
      dataIndex: "created_at",
      key: "created_at",
      width: 80,
      render: (text, record) => (
        <p className="text-center">{text ? stringToDate(text) : ""}</p>
      ),
    },
    {
      title: "Trạng thái",
      dataIndex: "status_name",
      key: "status_name",
      width: 140,
      render: (text, record) => {
        return (
          <p className="text-center">
            {text === "Khởi tạo" ? (
              <Tag color="green">{text}</Tag>
            ) : text === "Đã nhập kho" ? (
              <Tag color="red">{text}</Tag>
            ) : (
              <Tag color="blue">{text}</Tag>
            )}
          </p>
        );
      },
    },
    {
      title: "Người nhập kho",
      dataIndex: "staff_number_name",
      key: "staff_number_name",
      width: 120,
    },
    {
      title: (
        <>
          Mã <br /> đơn hàng
        </>
      ),
      dataIndex: "po_no",
      key: "po_no",
      width: 80,
      className: "text-center",
      render: (text, record) => <p>{record?.po_detail?.code}</p>,
    },
    {
      title: "NCC",
      dataIndex: "suppliers_short_name",
      key: "suppliers_short_name",
      width: 120,
      render: (text, record) => (
        <p>{record?.po_detail?.suppliers_short_name}</p>
      ),
    },
    {
      title: (
        <>
          Phương thức <br /> giao hàng
        </>
      ),
      dataIndex: "shippingMethod_name",
      key: "shippingMethod_name",
      width: 110,
      render: (text, record) => <p>{record?.po_detail?.shippingMethod_name}</p>,
    },
    {
      title: "Ngày giao",
      children: [
        {
          title: "Dự kiến",
          dataIndex: "delivery_date",
          key: "delivery_date",
          width: 80,
          render: (text, record) => (
            <p className="text-center">
              {record?.po_detail?.delivery_date
                ? stringToDate(record?.po_detail?.delivery_date)
                : ""}
            </p>
          ),
        },
        {
          title: "Thực tế",
          dataIndex: "delivery_actual_date",
          key: "delivery_actual_date",
          width: 80,
          render: (text, record) => (
            <p className="text-center">{text ? stringToDate(text) : ""}</p>
          ),
        },
      ],
    },
    {
      title: "Nhập tại kho",
      dataIndex: "warehouse_name",
      key: "warehouse_name",
      width: 120,
    },
    {
      title: "Bộ phận sử dụng",
      dataIndex: "department_name",
      key: "department_name",
      width: 120,
    },
    {
      title: "Hành động",
      key: "action",
      width: 100,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Xem sản phẩm & chỉnh trạng thái  ======================= */}
            <ViewBtnComponent
              onClickF={() => {
                showModal(record.id);
              }}
              text={"Xem chi tiết & chỉnh trạng thái"}
            />
            {/* ======================= Chỉnh sửa ======================= */}
            <FixBtnComponent
              deleted={deletedPage}
              onClickF={() => goToFix(record.id)}
            />
            {/* ======================= In file PDF ======================= */}
            {!deletedPage && permission?.stock_import?.read === 1 && (
              <Tooltip placement="bottom" title="Xuất file PDF phiếu đã chọn">
                <div>
                  <ReactToPrint
                    content={() => componentRef.current}
                    documentTitle="Xuất file pdf"
                    onAfterPrint={handleAfterPrint}
                    onBeforeGetContent={() => {
                      return new Promise(async (resolve) => {
                        await handleOnBeforeGetContent(record.id);
                        onBeforeGetContentResolve.current = resolve;
                        setLoading(true);
                      });
                    }}
                    onBeforePrint={handleBeforePrint}
                    removeAfterPrint
                    trigger={reactToPrintTrigger}
                  />
                  <div className="hidden">
                    <PrintImportWarehouse
                      forwardedRef={componentRef}
                      details={PPDetails}
                    />
                  </div>
                </div>
              </Tooltip>
            )}
            {/* ======================= Undo ======================= */}
            <UndoBtnComponent
              deleted={deletedPage}
              onClickF={() => {
                confirmUndo(record.id);
              }}
              text={"phiếu"}
            />
            {/* ======================= Xoá ======================= */}
            <DeleteBtnItemComponent
              onClickF={() => {
                if (deletedPage) {
                  confirm(record.id, 1);
                } else {
                  confirm(record.id, 0);
                }
              }}
              text={"phiếu"}
            />
          </div>
        );
      },
    },
  ];
  {
    /* ======================= Start IN  ======================= */
  }
  // 2 - lấy chi tiết phiếu
  const getPurchaseRequestInfo = async (id) => {
    dispatch(setSpinner(true));
    let res = await getImportWarehouse(id);
    if (res.data.success) {
      setDetails(res.data.data);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  const componentRef = React.useRef(null);

  const onBeforeGetContentResolve = React.useRef(null);

  const [loading, setLoading] = React.useState(false);
  const [PPDetails, setDetails] = useState();

  const handleAfterPrint = () => {
    // console.log("`onAfterPrint` called");
    onBeforeGetContentResolve.current = null;
    setLoading(false);
  };

  const handleBeforePrint = React.useCallback(() => {
    // console.log("`onBeforePrint` called");
  }, []);

  const handleOnBeforeGetContent = async (id) => {
    // console.log("`onBeforeGetContent` called");
    await getPurchaseRequestInfo(id);
    // console.log("dont");
    // return componentRef.current;
  };

  React.useEffect(() => {
    if (loading && onBeforeGetContentResolve.current) {
      // Resolves the Promise, letting `react-to-print` know that the DOM updates are completed
      onBeforeGetContentResolve.current();
    }
  }, [loading]);

  const reactToPrintTrigger = React.useCallback(() => {
    // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
    // to the root node of the returned component as it will be overwritten.

    // Bad: the `onClick` here will be overwritten by `react-to-print`
    // return <button onClick={() => alert('This will not work')}>Print this out!</button>;

    // Good
    return (
      <button id="btnPdf">
        <FaFilePdf className="text-[#B6190D]" />
      </button>
    );
  }, []);

  {
    /* ======================= End IN  ======================= */
  }
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
          x: 1500,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
