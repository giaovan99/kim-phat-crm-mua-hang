import React, { useEffect, useState } from "react";
import { Modal } from "antd";

import TableListProduct from "./TableListProduct/TableListProduct";
import { getImportWarehouse } from "../../../../../utils/warehouse";
import { showError, stringToDate } from "../../../../../utils/others";
import SetStatus2Step from "../../../../SetStatus2Step/SetStatusProposal";
import { setSpinner } from "../../../../../redux/slice/spinnerSlice";
import { useDispatch, useSelector } from "react-redux";

export default function ModalImportWarhouseDetail({
  isModalOpen,
  setIsModalOpen,
  importWarehouse,
  setImportWarehouse,
  reload,
  setReload,
  deletedPage,
  statusOptions,
  permission,
}) {
  // 1 - Khai báo state
  let [cloneProduct, setCloneProduct] = useState();
  const [current, setCurrent] = useState(0);
  const [importSlip, setImportSlip] = useState();
  let dispatch = useDispatch();

  // Lấy thông tin chi tiết phiếu nhập kho
  const APIGetImportwarehouse = async () => {
    dispatch(setSpinner(true));
    // console.log(importWarehouse);
    let res = await getImportWarehouse(importWarehouse);
    if (res.data.success) {
      let data = res.data.data;
      setImportSlip(data);
      setCurrent(Number(data.status));
      setCloneProduct(data.items);
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  //  3 - Lần đầu khi load
  useEffect(() => {
    if (importWarehouse) {
      // lấy thông tin người dùng trong danh sách người dùng & show ra màn hình
      APIGetImportwarehouse();
    }
  }, [importWarehouse]);

  // 4 - Reset form
  const resetForm = () => {
    setImportWarehouse(null);
  };

  //  6 - Khi huỷ lưu form
  const handleCancel = () => {
    resetForm();
    setIsModalOpen(false);
  };

  return (
    <Modal
      title={"Xem chi tiết và chỉnh trạng thái phiếu nhập kho"}
      centered
      open={isModalOpen}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Đóng"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center hidden",
      }}
      classNames={{ wrapper: "main-modal modal-view-detail" }}
    >
      <div className="tableListProducts">
        <form
          id="formUpdate"
          // className={deletedPage ? "pointer-events-none" : ""}
        >
          <div className="mb-5">
            {/* ======================= Title ======================= */}
            <h2 className="text-[1rem] font-medium mb-1 text-bl">
              Trạng thái phiếu nhập kho
            </h2>
            {/* ======================= Step ======================= */}
            <SetStatus2Step
              statusOptions={statusOptions}
              current={current}
              setCurrent={setCurrent}
              now={importWarehouse}
              page={"importWarehouse"}
              deletedPage={deletedPage}
              permissionUpdate={permission?.stock_import?.update}
            />
          </div>
          {cloneProduct && (
            <>
              <div>
                {/* ======================= Title ======================= */}
                <h2 className="text-[1rem] font-medium mb-1 text-bl">
                  Thông tin phiếu nhập kho
                </h2>
                <div className="grid grid-cols-4 gap-2 gap-y-3">
                  {/* ======================= Mã phiếu nhập kho ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="code">Mã phiếu nhập kho</label>
                      <p>{importSlip.code ? importSlip.code : ""}</p>
                    </div>
                  </div>
                  {/* ======================= Người đề nghị ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="staff_number_name">Người đề nghị</label>
                      <p>
                        {importSlip.staff_number_name
                          ? importSlip.staff_number_name
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= Mã đơn hàng ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="po_no">Mã đơn hàng</label>
                      <p>
                        {importSlip.po_detail.code
                          ? importSlip.po_detail.code
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= Ngày giao thực tế ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="delivery_actual_date">
                        Ngày giao thực tế
                      </label>
                      <p>
                        {importSlip.delivery_actual_date
                          ? stringToDate(importSlip.delivery_actual_date)
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= Ngày giao dự kiến ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="delivery_date">Ngày giao dự kiến</label>
                      <p>
                        {importSlip.po_detail.delivery_date
                          ? stringToDate(importSlip.po_detail.delivery_date)
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= Nhà cung cấp ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="suppliers_name">Nhà cung cấp</label>
                      <p>
                        {importSlip.po_detail.suppliers_short_name
                          ? importSlip.po_detail.suppliers_short_name
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= Trạng thái ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="po_status_name">
                        Trạng thái đơn hàng
                      </label>
                      <p>
                        {importSlip.po_detail.status_name
                          ? importSlip.po_detail.status_name
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= Phương thức giao hàng ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="shippingMethod_name">
                        Phương thức giao hàng{" "}
                      </label>
                      <p>
                        {importSlip.po_detail.shippingMethod_name
                          ? importSlip.po_detail.shippingMethod_name
                          : ""}
                      </p>
                    </div>
                  </div>

                  {/* ======================= Người mua ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="buyer_name">Người mua</label>
                      <p>
                        {importSlip.po_detail.buyer_name
                          ? importSlip.po_detail.buyer_name
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= SĐT người mua ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="buyer_phone">SĐT người mua</label>
                      <p>
                        {importSlip.po_detail.buyer_phone
                          ? importSlip.po_detail.buyer_phone
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= Người nhận ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="receiver_name">Người nhận</label>
                      <p>
                        {importSlip.receiver_name
                          ? importSlip.receiver_name
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= SĐT người nhận ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="receiver_phone">SĐT người nhận</label>
                      <p>
                        {importSlip.receiver_phone
                          ? importSlip.receiver_phone
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= Nhập tại kho ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor=".warehouse_name">Nhập tại kho</label>
                      <p>
                        {importSlip.warehouse_name
                          ? importSlip.warehouse_name
                          : ""}
                      </p>
                    </div>
                  </div>
                  {/* ======================= Bộ phận sử dụng ======================= */}
                  <div className="form-group">
                    <div className="form-style">
                      <label htmlFor="warehouse_address">Bộ phận sử dụng</label>
                      <p>
                        {importSlip.warehouse_address
                          ? importSlip.warehouse_address
                          : ""}
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="mt-2">
                {/* ======================= Title ======================= */}
                <h2 className="text-[1rem] font-medium mb-1 text-bl">
                  Danh sách sản phẩm
                </h2>
                {/* ======================= Table List Products ======================= */}
                <TableListProduct
                  cloneProduct={cloneProduct}
                  setCloneProduct={setCloneProduct}
                />
              </div>
            </>
          )}
        </form>
      </div>
    </Modal>
  );
}
