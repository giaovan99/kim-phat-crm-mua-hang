import { useFormik } from "formik";
import React, { useEffect } from "react";
import { HiOutlineSearch } from "react-icons/hi";
import DropdownOptions from "../../../../DropdownOptions/DropdownOptions";
import { useNavigate } from "react-router-dom";
import AddBtnComponent from "../../../../ButtonComponent/AddBtnComponent";
import { Select, Spin } from "antd";
import { normalize } from "../../../../../utils/others";

export default function Actions({
  deletedPage,
  setReload,
  keySelected,
  setKeySelected,
  keyNumActive,
  keyTab,
  permission,
  listStatus,
  itemSelected,
  setItemSelected,
  getListDepartments,
  getListShippingMethod,
  getListWarehouse,
  setSearchFilter,
  loadingSelect,
  listDepartment,
  listShippingMethod,
  listWarehouse,
  listPO,
  listNK,
}) {
  const onSearch = (value) => {
    // console.log("search:", value);
  };
  const filterOption = (input, option) =>
    normalize(option?.label ?? "").includes(normalize(input));

  const onChangeSelect = (value, field) => {
    formik.setFieldValue(field, value);
  };

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      po_ids: [],
      nk_ids: [],
      department: "",
      supplier_name: "",
      shippingMethod: "",
      warehouse: "",
    },
    onSubmit: async (values) => {
      setSearchFilter(values);
      setReload(true);
    },
  });

  console.log(formik.values);
  let navigate = useNavigate();
  const goToAdd = () => {
    navigate("/stock_import/add");
  };

  useEffect(() => {
    if (keyNumActive == keyTab) {
      formik.resetForm();
    }
  }, [keyNumActive]);
  const { handleBlur, handleChange, touched, errors } = formik;

  return (
    <div className="actions flex-col gap-5">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div
            className="flex flex-wrap-reverse gap-2"
            style={{ alignItems: "end" }}
          >
            {/* ======================= Tìm kiếm ======================= */}
            <div className="form-group">
              <div className="form-style flex-wrap">
                {/* Mã phiếu nhập kho */}
                <div>
                  <label>Mã phiếu nhập kho</label>
                  <Select
                    mode="multiple"
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "nk_ids")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listNK}
                    value={formik.values.nk_ids || []}
                    className="w-[170px]"
                    maxTagCount={1} // Giới hạn hiển thị tối đa 1 tags
                  />
                </div>
                {/* Mã đơn hàng */}
                <div>
                  <label>Mã đơn hàng</label>
                  <Select
                    mode="multiple"
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "po_ids")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listPO}
                    value={formik.values.po_ids || []}
                    className="w-[150px]"
                    maxTagCount={1} // Giới hạn hiển thị tối đa 1 tags
                  />
                </div>

                {/* NCC */}
                <div>
                  <label>NCC</label>
                  <input
                    id="supplier_name"
                    name="supplier_name"
                    type="text"
                    placeholder="Tìm nhà cung cấp"
                    value={formik.values.supplier_name}
                    onChange={formik.handleChange}
                    className="max-w-[150px]"
                  />
                </div>
                {/* Bộ phận sử dụng */}
                <div>
                  <label>Bộ phận sử dụng</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) =>
                      formik.setFieldValue("department", value)
                    }
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listDepartment}
                    value={formik.values.department || null}
                    className="w-[170px]"
                    onBlur={handleBlur}
                    onDropdownVisibleChange={getListDepartments}
                    notFoundContent={
                      loadingSelect ? <Spin size="small" /> : null
                    }
                  />
                </div>
                {/* PTGH */}
                <div>
                  <label>PTGH</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) =>
                      onChangeSelect(value, "shippingMethod")
                    }
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listShippingMethod}
                    value={formik.values.shippingMethod || null}
                    className="w-[170px]"
                    onBlur={handleBlur}
                    onDropdownVisibleChange={getListShippingMethod}
                    notFoundContent={
                      loadingSelect ? <Spin size="small" /> : null
                    }
                  />
                </div>
                {/* Kho nhận */}
                <div>
                  <label>Kho nhận</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "warehouse")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listWarehouse}
                    value={formik.values.warehouse || null}
                    className="w-[170px]"
                    onBlur={handleBlur}
                    onDropdownVisibleChange={getListWarehouse}
                    notFoundContent={
                      loadingSelect ? <Spin size="small" /> : null
                    }
                  />
                </div>
                <div className="flex gap-2 w-full flex-shrink-0">
                  <button className="btn-main-yl btn-actions">
                    <HiOutlineSearch />
                    Tìm
                  </button>
                  <button
                    className="ant-btn btn-actions text-white bg-black ant-btn-default rounded-lg"
                    type="button"
                    onClick={formik.handleReset}
                  >
                    Reset
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>

      {/* ======================= btns ======================= */}
      <div className="flex gap-2 items-end">
        {permission?.stock_import?.update === 1 && (
          <DropdownOptions
            arrId={keySelected}
            deletedPage={deletedPage}
            page={"warehouseSlip"}
            setReload={setReload}
            setKeySelected={setKeySelected}
            listStatus={listStatus}
            arrItem={itemSelected}
            setItemSelected={setItemSelected}
          />
        )}
        {!deletedPage && permission?.stock_import?.create ? (
          <div className="flex gap-3 items-end">
            <AddBtnComponent
              onClickF={() => goToAdd()}
              text={"Tạo phiếu nhập kho"}
            />
          </div>
        ) : (
          <></>
        )}
      </div>
    </div>
  );
}
