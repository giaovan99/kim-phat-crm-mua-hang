import React from "react";
import { Table } from "antd";
const tableProps = {
  bordered: true,
  size: "small",
};
export default function TableListProduct({ cloneProduct }) {
  const columns = [
    {
      title: "Mã SP",
      dataIndex: "sku",
      key: "sku",
      width: 120,
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Tên SP",
      dataIndex: "product_name",
      key: "product_name",
    },
    {
      title: (
        <>
          Mã số <br />
          (Quy cách)
        </>
      ),
      dataIndex: "package_case",
      key: "package_case",
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 120,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Số lượng",
      children: [
        {
          title: "Theo chứng từ",
          dataIndex: "qty",
          key: "qty",
          width: 120,
          render: (text, record) => {
            return (
              <p className="text-center">
                {Number(text).toLocaleString("en-US")}
              </p>
            );
          },
        },
        {
          title: "Thực nhập",
          dataIndex: "qty_actual",
          key: "qty_actual",
          width: 120,
          render: (text, record) => {
            return (
              <p className="text-center">
                {Number(text).toLocaleString("en-US")}
              </p>
            );
          },
        },
      ],
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
    },
  ];
  return (
    <Table
      {...tableProps}
      pagination={{
        position: ["none", "bottomCenter"],
        pageSize: 20,
        hideOnSinglePage: true,
      }}
      scroll={{
        y: 600,
        x: 1000,
      }}
      columns={columns}
      dataSource={cloneProduct}
    />
  );
}
