import { Modal } from "antd";
import { useFormik } from "formik";
import React, { useEffect } from "react";
import Swal from "sweetalert2";
import * as yup from "yup";
import { updateUnit } from "../../../utils/product";
import { updateDepartment, updatePosition } from "../../../utils/user";
import {
  updateInvoiceRequest,
  updatePaymentMethod,
  updateShippingMethod,
} from "../../../utils/order";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

export default function Type1({
  title,
  object,
  API,
  isModalOpen,
  setIsModalOpen,
  setObject,
  setReload,
}) {
  const initial1 = {
    name: "",
    value: "",
    shortName: "",
  };
  const initial2 = {
    name: "",
  };
  let dispatch = useDispatch();

  const checkAPI = async (values) => {
    dispatch(setSpinner(true));
    let res;
    switch (API) {
      // Đơn vị tính
      case "unit": {
        res = await updateUnit({
          ...values,
          deleted: 0,
        });
        return res;
      }
      // Chức vụ
      case "position": {
        res = await updatePosition({
          ...values,
          deleted: 0,
        });
        return res;
      }
      // Bộ phận
      case "department": {
        res = await updateDepartment({
          ...values,
          deleted: 0,
        });
        return res;
      }
      // Phương thức giao hàng
      case "shippingMethod": {
        res = await updateShippingMethod({
          ...values,
          deleted: 0,
        });
        return res;
      }
      // Phương thức giao hàng
      case "paymentMethod": {
        res = await updatePaymentMethod({
          ...values,
          deleted: 0,
        });
        return res;
      }
      // Yêu cầu về hoá đơn
      case "invoiceRequest": {
        res = await updateInvoiceRequest({
          ...values,
          deleted: 0,
        });
        return res;
      }
    }
    dispatch(setSpinner(false));
  };

  const resetForm = () => {
    formik.resetForm();
    setObject(null);
    if (API == "department") {
      formik.setValues(initial1);
    } else {
      formik.setValues(initial2);
    }
  };

  const validation1 = {
    name: yup.string().required("Tên không để trống"),
    shortName: yup.string().required("Tên viết tắt không để trống"),
  };
  const validation2 = {
    name: yup.string().required("Tên không để trống"),
  };

  // Chuyển đổi values
  const convertValues = (values) => {
    if (API == "department" && object) {
      return {
        key: values.key,
        name: values.name,
        value: values.shortName,
        type: "value",
      };
    } else if (API == "department" && !object) {
      return {
        name: values.name,
        value: values.shortName,
        type: "value",
      };
    } else if (object) {
      return {
        name: values.name,
        key: values.key,
      };
    } else {
      return {
        name: values.name,
      };
    }
  };

  // Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object(
      API == "department"
        ? {
            ...validation1,
          }
        : {
            ...validation2,
          }
    ),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      let convert = convertValues(values);
      let res = await checkAPI(convert);
      if (res) {
        // alert thông báo
        Swal.fire({
          title: title,
          text: `${title} thành công!`,
          icon: "success",
          confirmButtonText: "Đóng",
        });
        setObject(null);
        // reset form
        resetForm();
        setReload(true);
        //  đóng modal
        setIsModalOpen(false);
      }
      dispatch(setSpinner(false));
    },
  });

  // Chạy lần đầu khi Modal open -> check xem là thêm hay sửa
  useEffect(() => {
    if (object) {
      formik.setValues(object);
    } else {
      if (API == "department") {
        formik.setValues(initial1);
      } else {
        formik.setValues(initial2);
      }
    }
  }, [object]);

  //   Khi lưu
  const handleOk = () => {
    formik.handleSubmit();
  };
  //   Khi Huỷ
  const handleCancel = () => {
    resetForm();
    setIsModalOpen(false);
  };

  const { handleBlur, handleChange, touched, errors } = formik;
  return (
    <Modal
      title={title}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form onSubmit={formik.handleSubmit}>
        <div className="grid grid-cols-2 gap-2">
          {/* ======================= Đặt tên phương thức giao hàng ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="name">
                Tên <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên"
                name="name"
                id="name"
                value={formik.values.name}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.name && errors.name ? (
              <p className="mt-1 text-red-500 text-sm" id="name-warning">
                {errors.name}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Tên viết tắt ======================= */}
          {API == "department" && (
            <div className="form-group col-span-2">
              <div className="form-style">
                <label htmlFor="shortName">
                  Tên viết tắt <span className="text-red-500">*</span>
                </label>
                <input
                  className="grow"
                  type="text"
                  placeholder="Nhập tên viết tắt"
                  name="shortName"
                  id="shortName"
                  value={formik.values.shortName}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                />
              </div>
              {touched.shortName && errors.shortName ? (
                <p className="mt-1 text-red-500 text-sm" id="name-warning">
                  {errors.shortName}
                </p>
              ) : (
                <></>
              )}
            </div>
          )}
        </div>
      </form>
    </Modal>
  );
}
