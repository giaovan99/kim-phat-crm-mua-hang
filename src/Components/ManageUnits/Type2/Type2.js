import { Modal } from "antd";
import { useFormik } from "formik";
import React, { useEffect } from "react";
import Swal from "sweetalert2";
import * as yup from "yup";
// Option API
//[
// 1 - Đơn vị tính
// 2 - Loại hoá đơn
//]

export default function Type1({
  title,
  object,
  API,
  isModalOpen,
  setIsModalOpen,
}) {
  // khai báo formik
  const formik = useFormik({
    initialValues: { value: "", label: "" },
    validationSchema: yup.object({
      label: yup.string().required("Tên không để trống"),
    }),
    onSubmit: async (values) => {
      // alert thông báo
      Swal.fire({
        title: title,
        text: `${title} thành công!`,
        icon: "success",
        confirmButtonText: "Đóng",
      });
      // reset form
      formik.resetForm();
      //  đóng modal
      setIsModalOpen(false);
    },
  });
  // Chạy lần đầu khi Modal open -> check xem là thêm hay sửa
  useEffect(() => {
    if (object) {
      formik.setValues(object);
    }
  }, [object]);
  //   Khi lưu
  const handleOk = () => {
    formik.handleSubmit();
  };
  //   Khi Huỷ
  const handleCancel = () => {
    formik.resetForm();
    setIsModalOpen(false);
  };

  const { handleBlur, handleChange, touched, errors } = formik;
  return (
    <Modal
      title={title}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form onSubmit={formik.handleSubmit}>
        <div className="grid grid-cols-2 gap-2">
          {/* ======================= Đặt tên ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="label">
                Tên <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên"
                name="label"
                id="label"
                value={formik.values.label}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.label && errors.label ? (
              <p className="mt-1 text-red-500 text-sm" id="label-warning">
                {errors.label}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Giá trị ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="label">
                Tên <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên"
                name="label"
                id="label"
                value={formik.values.label}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.label && errors.label ? (
              <p className="mt-1 text-red-500 text-sm" id="label-warning">
                {errors.label}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
      </form>
    </Modal>
  );
}
