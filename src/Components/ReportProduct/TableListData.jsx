import { Table } from "antd";
import React, { useEffect, useState } from "react";
import { setPage } from "../../redux/slice/pageSlice";
import { useDispatch, useSelector } from "react-redux";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import { reportProductList } from "../../utils/reportAPI";
import { stringToDate } from "../../utils/others";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};
const TableListData = ({ formik, reload, setReload }) => {
  let page = useSelector((state) => state.pageSlice.value);
  const dispatch = useDispatch();
  const [pageSize, setPageSize] = useState(0);
  const [total, setTotal] = useState(0);
  const [listData, setListData] = useState([]);

  // 3 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListData(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list phiếu hoàn ứngtheo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = { ...formik.values };

    let res = await reportProductList(page, keyParam);

    // console.log(res);
    if (res.status) {
      setListData(res.data);
      setTotal(res.data.length);
      setPageSize(res.data.length);
    }
    dispatch(setSpinner(false));
  };

  // 9 - Lần đầu load trang => Kéo về danh sách user & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    getListData(page);
  }, []);

  // 10 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      // console.log("run");
      // dispatch(resetPage());
      getListData(page);
      setReload(false);
    }
  }, [reload]);
  Table.SELECTION_COLUMN.fixed = "left";
  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      fixed: "left",
      width: 45,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Thời gian đề nghị mua hàng",
      dataIndex: "ngay_tao",
      key: "ngay_tao",
      render: (text) => <p className="text-center">{stringToDate(text)}</p>, //
    },
    {
      title: "Thời gian giao hàng",
      dataIndex: "delivery_date",
      key: "delivery_date",
    },
    {
      title: "Bộ phận yêu cầu",
      dataIndex: "department_name",
      key: "department_name",
    },
    {
      title: "Mã sản phẩm",
      dataIndex: "sku",
      className: "text-center",
      key: "sku",
    },
    {
      title: "Tên hàng hoá",
      dataIndex: "po_product_name",
      key: "po_product_name",
      width: 150,
    },
    {
      title: "Quy cách",
      dataIndex: "po_package_case",
      key: "po_package_case",
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      className: "text-center",
    },
    {
      title: "Đơn giá",
      dataIndex: "po_product_price",
      key: "po_product_price",
      render: (text) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
    },
  ];
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 650,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
};

export default TableListData;
