import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
import { DatePicker, Select } from "antd";
import { normalize } from "../../utils/others";
const { RangePicker } = DatePicker;
export default function Actions({ listDepartment, formik }) {
  // Filter `option.key` match the user type `input`
  const filterOption = (input, option) =>
    normalize(option?.label ?? "").includes(normalize(input));

  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div className="flex gap-4 items-end">
            {/* Filter thời gian */}
            <div className="form-group relative">
              <div className="form-style form-style-block ">
                <label>Thời gian</label>
                <RangePicker
                  size="medium"
                  variant="borderless"
                  format="DD/MM/YYYY"
                  onChange={(value, dateString) => {
                    formik.setFieldValue("start_date", dateString[0]);
                    formik.setFieldValue("end_date", dateString[1]);
                  }}
                  className="h-[38px]"
                />
              </div>
            </div>
            {/* Filter bộ phận */}
            <div className="form-group">
              <div className="form-style form-style-block">
                <label htmlFor="department">Bộ phận</label>
                <Select
                  showSearch
                  placeholder="Chọn bộ phận"
                  optionFilterProp="children"
                  onChange={(value) => {
                    formik.setFieldValue("department", value);
                  }}
                  filterOption={filterOption}
                  options={listDepartment}
                  value={
                    formik.values.department ? formik.values.department : null
                  }
                  className="min-w-[140px]"
                />
              </div>
            </div>
            {/* Lọc */}
            <button className="btn-main-yl btn-actions" type="submit">
              <HiOutlineSearch />
              Lọc
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
