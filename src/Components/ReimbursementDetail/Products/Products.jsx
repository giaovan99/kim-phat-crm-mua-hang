import { HiOutlineTrash } from "react-icons/hi2";
import { Popconfirm, Select, Table, Tooltip, message } from "antd";
import React, { useState } from "react";
import Swal from "sweetalert2";
import ModalListProduct from "../../Warehouse/ImportWarehouse/ListOrders/Components/ModalListProducts/ModalListProduct";
import InputDatePicker from "../../Input/InputDatePicker";
import InputNumberFormat from "../../Input/InputNumberFormat";
import { actualIndex, calBeforeVAT } from "../../../utils/others";
import { RxUpdate } from "react-icons/rx";
// import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";
import { setSpinner } from "../../../redux/slice/spinnerSlice";

export default function Products({
  formik,
  setProductsSelected,
  current,
  setIsAddProductsModal,
  handleOkAddProducts,
  handleCancelAddProducts,
  orderChoosen,
  setOrderChoosen,
  productsSelected,
  isAddProductsModal,
  filterOption,
  listTaxCode,
  importKey,
  setUpdate,
  getListOrderProduct,
  getOrderInfo,
}) {
  const [currentPage, setCurrentPage] = useState(1); // Mặc định là trang 1
  let dispatch = useDispatch();

  const tableProps = {
    bordered: true,
    size: "small",
  };
  const { touched, errors } = formik;

  const confirm = (record) => {
    let arr = [...productsSelected];
    let index = arr.findIndex((item) => item.product_id === record.product_id);
    if (index > -1) {
      arr.splice(index, 1);
      setProductsSelected([...arr]);
      Swal.fire({
        title: "Xoá",
        text: "Xoá thành công",
        icon: "success",
        confirmButtonText: "Đóng",
      });
    }
    setUpdate(0);
  };

  const cancel = (e) => {
    message.error("Đã huỷ");
  };

  // Hàm tính lại thành tiền
  const fSubTotal = (index, record) => {
    let arr = [...productsSelected];
    let subTotal = calBeforeVAT(record, true);
    arr[index].sub_total = subTotal;
    arr[index].total = subTotal;
    setProductsSelected([...arr]);
  };

  // Hàm xử lý thay đổi số lượng phần tử của mảng
  const fChangeNumber = (value, index) => {
    let cloneArr = [...productsSelected];
    cloneArr[index].qty = value;
    setProductsSelected([...cloneArr]);
  };

  // Khi làm mới sản phẩm
  const confirmUpdate = async (record, index) => {
    setUpdate(0);
    dispatch(setSpinner(true));
    let arr = [...productsSelected];

    // Lấy thông tin đơn hàng chứa sản phẩm đó
    let order = await getOrderInfo(record.po_id);

    let i = order?.items.findIndex(
      (item) => item.product_id == record.product_id
    );

    if (i !== -1) {
      arr[index].product_name = order.items[i].product_name; //Tên
      arr[index].unit = order.items[i].unit; // Đơn vị tính
      arr[index].price = order.items[i].price; // Đơn giá
      arr[index].total = order.items[i].total; //Thành tiền
      arr[index].vat = order.items[i].vat; // VAT id
      arr[index].vat_name = order.items[i].vat_name; // VAT name
    }

    setProductsSelected([...arr]);
    dispatch(setSpinner(false));
    message.success("Làm mới thành công");
    Swal.fire({
      title: "Làm mới thông tin sản phẩm",
      text: "Thành công",
      icon: "success",
      confirmButtonText: "Đóng",
    });
  };

  const columns = [
    {
      title: "STT",
      dataIndex: "index",
      rowScope: "row",
      key: "stt",
      fixed: "left",
      width: 50,
      render: (_, __, index) => index + 1, // Tăng lên 1 để bắt đầu từ 1 thay vì 0
    },
    {
      title: "Mã SP",
      dataIndex: "sku",
      key: "sku",
      fixed: "left",
      width: 80,
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Đơn hàng",
      dataIndex: "po_no",
      key: "po_no",
      fixed: "left",
      render: (text, record) => <p className="text-center">{text}</p>,
    },
    {
      title: "Chứng từ",
      children: [
        {
          title: "Ngày",
          dataIndex: "payment_date",
          key: "payment_date",
          width: 150,
          render: (text, record, index) => {
            return (
              <div className="form-group">
                <div className="form-style">
                  <InputDatePicker
                    field={record.payment_date}
                    onChange={(dateString) => {
                      let arr = [...productsSelected];
                      let actual = actualIndex(currentPage, 20, index);
                      arr[actual].payment_date = dateString;
                      setProductsSelected([...arr]);
                    }}
                  />
                </div>
              </div>
            );
          },
        },
        {
          title: "Số",
          dataIndex: "payment_no",
          key: "payment_no",
          width: 150,
          render: (text, record, index) => {
            return (
              <div className="form-group">
                <div className="form-style">
                  <input
                    className="grow"
                    type="text"
                    value={record?.payment_no ? record?.payment_no : ""}
                    onChange={(e) => {
                      let actual = actualIndex(currentPage, 20, index);
                      let arr = [...productsSelected];
                      arr[actual].payment_no = e.target.value;
                      setProductsSelected([...arr]);
                    }}
                  />
                </div>
              </div>
            );
          },
        },
      ],
    },
    {
      title: "Ngày giao hàng",
      dataIndex: "delivery_actual_date",
      key: "delivery_actual_date",
      width: 150,
      render: (text, record, index) => {
        return (
          <div className="form-group">
            <div className="form-style">
              <InputDatePicker
                field={record.delivery_actual_date}
                onChange={(dateString) => {
                  let arr = [...productsSelected];
                  let actual = actualIndex(currentPage, 20, index);
                  arr[actual].delivery_actual_date = dateString;
                  setProductsSelected([...arr]);
                }}
              />
            </div>
          </div>
        );
      },
    },
    {
      title: "Nội dung",
      dataIndex: "product_name",
      key: "product_name",
      width: 200,
    },
    {
      title: "ĐVT",
      dataIndex: "unit",
      key: "unit",
      width: 80,
      render: (text, record) => {
        return <p className="text-center">{text}</p>;
      },
    },
    {
      title: "Số lượng",
      dataIndex: "qty",
      key: "qty",
      render: (text, record, index) => {
        return (
          <InputNumberFormat
            className={
              current > 1
                ? "pointer-events-none text-center w-full"
                : "text-center w-full"
            }
            value={record.qty || ""}
            onChange={(value) => {
              let actual = actualIndex(currentPage, 20, index);
              fChangeNumber(value, actual, record);
              setUpdate(0);
              // Tính lại thành tiền
              fSubTotal(actual, record);
            }}
            record={record}
          />
        );
      },
    },
    {
      title: "Đơn giá",
      dataIndex: "price",
      key: "price",

      render: (text, record) => {
        return (
          <p className="text-right">
            {text ? Number(text).toLocaleString("en-US") : ""}
          </p>
        );
      },
    },
    {
      title: "Thuế GTGT",
      width: 100,
      dataIndex: "total",
      key: "total",
      render: (text, record, index) => {
        return (
          <Select
            name="vat"
            placeholder="Chọn đơn vị tính"
            optionFilterProp="children"
            className={current > 1 ? "pointer-events-none w-full" : "w-full"}
            onChange={(value, event) => {
              let arr = [...productsSelected];
              let actual = actualIndex(currentPage, 20, index);
              arr[actual].vat = value;
              setProductsSelected([...arr]);
              setUpdate(0);
            }}
            value={record.vat || ""}
            filterOption={filterOption}
            options={listTaxCode}
          />
        );
      },
    },
    {
      title: "Thành tiền",
      width: 140,
      dataIndex: "sub_total",
      key: "sub_total",
      render: (text, record) => {
        return (
          <p className="text-right">
            {current > 1
              ? Number(text).toLocaleString("en-US")
              : calBeforeVAT(record, formik.values.sub_total).toLocaleString(
                  "en-US"
                )}
          </p>
        );
      },
    },
    {
      title: "Ghi chú",
      dataIndex: "note",
      key: "note",
      width: 200,
      className: "text-center",
      render: (text, record, index) => {
        return (
          <div className="form-group">
            <div className="form-style">
              <input
                className="grow"
                type="text"
                value={record?.note ? record?.note : ""}
                onChange={(e) => {
                  let actual = actualIndex(currentPage, 20, index);
                  let arr = [...productsSelected];
                  arr[actual].note = e.target.value;
                  setProductsSelected([...arr]);
                }}
              />
            </div>
          </div>
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 80,
      fixed: "right",
      render: (_, record, index) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Refresh sản phẩm ======================= */}
            {importKey && (
              <Popconfirm
                placement="topRight"
                title="Làm mới thông tin"
                description="Bạn chắc chắn muốn làm mới thông tin sản phẩm này?"
                onConfirm={() => confirmUpdate(record, index)}
                onCancel={cancel}
                okText="Làm mới"
                cancelText="Huỷ"
                okButtonProps={{ className: "btn-main-yl" }}
                className="popover-danger"
              >
                <Tooltip placement="bottom" title="Làm mới thông tin">
                  <button type="button">
                    <RxUpdate className="text-blue-600" />
                  </button>
                </Tooltip>
              </Popconfirm>
            )}
            {/* ======================= Xoá ======================= */}
            <Popconfirm
              placement="topRight"
              title="Xoá sản phẩm"
              description="Bạn chắc chắn muốn xoá sản phẩm này?"
              onConfirm={() => confirm(record)}
              onCancel={cancel}
              okText="Xoá"
              cancelText="Huỷ"
              okButtonProps={{ className: "btn-main-yl" }}
              className="popover-danger"
            >
              <Tooltip placement="bottom" title="Xoá">
                <button type="button">
                  <HiOutlineTrash className="text-rose-600" />
                </button>
              </Tooltip>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  return (
    <div className="productsAddPage">
      {/* ======================= Sản phẩm ======================= */}
      {orderChoosen && formik.values?.po_no?.length && (
        <div
          className={
            current > 1
              ? "pointer-events-none form-group mb-2"
              : "form-group mb-2"
          }
        >
          <button
            type="button"
            className="btn-actions btn-main-bl"
            onClick={async () => {
              if (importKey && orderChoosen.items.length === 0) {
                // Đảm bảo getListOrderProduct() chạy và hoàn tất trước khi mở modal
                let items = await getListOrderProduct();
                setOrderChoosen({ ...orderChoosen, items });
              }
              // Mở modal sau khi getListOrderProduct hoàn thành
              setIsAddProductsModal(true);
            }}
          >
            Chọn sản phẩm từ đơn hàng
          </button>
          {touched.items && errors.items ? (
            <p className="mt-1 text-red-500 text-sm" id="items-warning">
              {errors.items}
            </p>
          ) : (
            <></>
          )}
        </div>
      )}

      {/* ======================= Danh sách sản phẩm ======================= */}
      <Table
        {...tableProps}
        pagination={{
          position: ["none", "bottomCenter"],
          pageSize: 20,
          hideOnSinglePage: true,
          onChange: (page) => {
            setCurrentPage(page);
          },
        }}
        scroll={{
          y: 600,
          x: 1700,
        }}
        columns={columns}
        dataSource={formik.values.items}
      />

      {/* ======================= Modals ======================= */}
      {isAddProductsModal && (
        <ModalListProduct
          handleOkAddProducts={handleOkAddProducts}
          handleCancelAddProducts={handleCancelAddProducts}
          data={orderChoosen.items}
          setProductChoosen={setProductsSelected}
          isModalOpen={isAddProductsModal}
          orderChoosen={orderChoosen}
          productsChoosen={productsSelected}
        />
      )}
    </div>
  );
}
