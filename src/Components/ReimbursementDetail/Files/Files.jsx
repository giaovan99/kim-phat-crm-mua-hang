import React from "react";

export default function Files({ renderListFile, formik, current }) {
  return (
    <div
      className={
        current > 1 ? "pointer-events-none my-2" : " pointer-events-auto my-2"
      }
    >
      {/* ======================= Title ======================= */}
      <h2 className="text-[1rem] font-medium text-bl col-span-4 flex justify-between">
        <span>Files đính kèm</span>
      </h2>
      <p className=" mb-1 ">
        {formik.errors.documents && (
          <p
            className="mt-1 text-red-500 text-sm font-normal italic"
            id="files-warning"
          >
            {formik.errors.documents}
          </p>
        )}
      </p>
      {renderListFile()}
    </div>
  );
}
