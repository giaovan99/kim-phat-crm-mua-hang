import React, { useEffect, useState } from "react";
import { useNavigate, useParams, useSearchParams } from "react-router-dom";
import { Collapse } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { IoSaveOutline } from "react-icons/io5";
import { LiaUndoSolid } from "react-icons/lia";
import { CaretRightOutlined } from "@ant-design/icons";
import {
  companyList,
  getOrder,
  getOrderList2,
  getPaymentMethodList,
} from "../../utils/order";
import SetStatus2Step from "../SetStatus2Step/SetStatusProposal";
import ImportInfo from "./ImportInfo/ImportInfo";
import Products from "./Products/Products";
import {
  createReimbursementSlip,
  getPOReimbursement,
  getReimbursemenId,
  getReimbursementStatusList,
  updateReimbursementSlip,
} from "../../utils/reimbursement";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import { useDispatch, useSelector } from "react-redux";
import Summary from "./Summary/Summary";
import { getListDepartment } from "../../utils/user";
import { vatList } from "../../utils/product";
import { normalize } from "../../utils/others";

// Lọc filter
const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

export default function ReimbursementDetail() {
  // 1 - Lấy giá trị từ params
  let { importKey } = useParams();
  // Khai báo useNavigate để chuyển hướng trang
  let navigate = useNavigate();
  const [searchParams] = useSearchParams();
  let dispatch = useDispatch();

  // 2 - Khai báo state
  const [productsSelected, setProductsSelected] = useState([]); // Quản lý danh sách sản phẩm được chọn
  const [current, setCurrent] = useState(1); // Quản lý vị trí của trạng thái
  const [importStatus, setImportStatus] = useState([]); //Danh sách trạng thái
  const [orderChoosen, setOrderChoosen] = useState({
    po_id: [],
    list_po_code: [],
    items: [],
  }); //mã đơn + danh sách sản phẩm của đơn
  const [listOrder, setListOrder] = useState([]); //hiển thị danh sách đơn hàng
  const [listStaff, setListStaff] = useState([]); //hiển thị danh sách nhân viên cóp thể làm phiếu nhập kho
  const [isAddProductsModal, setIsAddProductsModal] = useState(false); //hiển thị Modal danh sách sản phẩm của đơn hàng được chọn
  const [n, setN] = useState(0); //hiển thị Modal danh sách sản phẩm của đơn hàng được chọn
  const [departmentList, setDepartmentList] = useState([]); //Danh sách loại hoá đơn
  const [listTaxCode, setListTaxCode] = useState([]); //Danh sách loại hoá đơn
  const [listPaymentMethod, setListPaymentMethod] = useState([]); //Danh sách phương thức thanh toán
  const [company, setCompany] = useState([]); //Danh sách cty
  const [update, setUpdate] = useState(0); //Cờ quản lý việc chỉnh sửa
  const [loadingSelect, setLoadingSelect] = useState(false); //Loading Spinner

  // 2 - Khai báo giá trị ban đầu
  const initial = {
    po_id: [],
    staff_number: "",
    items: [],
    status: 1,
    vat_name: "",
    vat: "",
    company: "",
    total: 0,
    sub_total: 0,
    paymentMethod: "",
    paymentMethod_name: "",
    department_name: "",
    department: "",
    advance_money_first: 0,
    advance_money_second: 0,
    remaining_amount: 0,
    total_vat: 0, // Tổng thuế của tất cả sp
    sub_total_items_rounded: true, // Đang setup mặc định true khi nào khách phân trường hợp, phiếu cần làm tròn hay ko làm tròn thì mới update trên dtb
    print_suppliers: 0,
    print_date: 0,
  };

  // 3 - Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      po_no: yup.array().required("Chọn đơn hàng cần hoàn ứng"),
      staff_number: yup.string().required("Chọn người yêu cầu"),
      company: yup.string().required("Chọn công ty"),
      paymentMethod: yup.string().required("Chọn phương thức thanh toán"),
      paymentMethod_name: yup.string().nullable(),
      department_name: yup.string().nullable(),
      department: yup.string().nullable(),
      items: yup
        .array()
        .min(1, "Chọn sản phẩm từ đơn hàng")
        .test("check-qty", "Số lượng không hợp lệ", function (value) {
          if (!value || !value.length) {
            return false; // Mảng sản phẩm trống
          }
          // Kiểm tra từng sản phẩm trong mảng
          for (const product of value) {
            if (!product.qty || product.qty <= 0) {
              return false; // Số lượng thực tế không được để trống hoặc <=0
            }
          }
          return true;
        })
        .test("check-vat", "Có sản phẩm chưa chọn vat", function (value) {
          if (!value || !value.length) {
            return false; // Mảng sản phẩm trống
          }
          // Kiểm tra từng sản phẩm trong mảng
          for (const product of value) {
            if (!product.vat) {
              return false; // Số VAT để trống
            }
          }
          return true;
        }),
      total_vat: yup.string().nullable(),
      advance_money_first: yup.string().nullable(),
      advance_money_second: yup.string().nullable(),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      let obj = valuesFilter(values);
      let res;
      // lọc lại các key từ values
      if (importKey) {
        // Sửa đơn hàng
        res = await updateReimbursementSlip({ ...obj, id: importKey });
      } else {
        // Tạo đơn hàng
        res = await createReimbursementSlip(obj, formik);
      }
      if (res) {
        let debug = searchParams.get("debug");
        if (!debug) {
          navigate(0);
        }
      }
      dispatch(setSpinner(false));
    },
  });

  let valuesFilter = (values) => {
    return {
      items: values?.items,
      company: values?.company,
      po_id: values?.po_id,
      po_no: values?.po_no,
      staff_number: values?.staff_number,
      status: values?.status,
      vat: values?.vat,
      vat_name: values?.vat_name,
      total: values?.total,
      sub_total: values?.sub_total,
      paymentMethod: values?.paymentMethod,
      paymentMethod_name: values?.paymentMethod_name,
      department_name: values?.department_name,
      department: values?.department,
      advance_money_first: values?.advance_money_first || 0,
      advance_money_second: values?.advance_money_second || 0,
      total_vat: values?.total_vat || 0,
      remaining_amount: values?.remaining_amount || 0,
      print_suppliers: values.print_suppliers,
      print_date: values.print_date,
    };
  };

  //  6 - Lấy tất cả các mảng cố định
  let getArr = async (page) => {
    dispatch(setSpinner(true));
    let res;
    // 5.10 - Danh sách trạng thái
    res = await getReimbursementStatusList();
    if (res.status) {
      setImportStatus(res.data);
    }
    // 5.10 - Danh sách bộ phận
    res = await getListDepartment();
    if (res.status) {
      setDepartmentList(res.data);
    }
    // 5.7 - Danh sách các yếu tố liên quan đến hoàn ứng: đơn hàng, nhân viên
    res = await getPOReimbursement();
    if (res.status) {
      setListStaff(res.options.staff);
    }
    // 5.7 - Danh sách loại hoá đơn
    res = await vatList();
    if (res.status) {
      setListTaxCode(res.data);
    }
    // 5.3 - Công ty
    res = await companyList(page);
    if (res.status) {
      setCompany(res.data);
    }
    dispatch(setSpinner(false));
  };

  // 5-1 Lấy danh sách phương thức thanh toán
  const paymentMethodList = async (page = 1, keywords = "") => {
    setLoadingSelect(true);
    if (listPaymentMethod.length <= 0) {
      // 5.3 - PTTT
      let res = await getPaymentMethodList(page, {
        keywords: keywords ? keywords : "",
      });
      if (res.status) {
        setListPaymentMethod(res.data);
      }
    }

    setLoadingSelect(false);
  };

  // 5-1 Lấy danh sách đơn hàng
  const getListOrder = async (paymentMethod) => {
    setLoadingSelect(true);

    // 5.3 - PTTT
    let res = await getOrderList2("", {
      status_payment: true,
      paymentMethod,
    });
    if (res.status) {
      // console.log(res);
      setListOrder(res.data);
    }
    setLoadingSelect(false);
  };

  // // 4 - Lấy thông tin chi tiết phiếu đề nghị thanh toán
  const APIGetPRId = async () => {
    dispatch(setSpinner(true));
    setUpdate(1);

    let res = await getReimbursemenId(importKey);
    if (res.data.success) {
      formik.setValues(res.data.data);
      setProductsSelected(res.data.data.items);
      setCurrent(res.data.data.status * 1);
      formik.setFieldValue("sub_total_items_rounded", true);

      // Gắn giá trị formik cho orderChoosen
      setOrderChoosen({
        ...orderChoosen,
        po_id: res.data.data.po_id,
        list_po_code: res.data.data.po_no,
      });
    }
    dispatch(setSpinner(false));
  };

  // Render lần đầu khi load trang check xem đang Thêm hay Xoá Phiếu đề nghị thanh toán
  useEffect(() => {
    getArr();
    if (importKey) {
      APIGetPRId();
    } else {
      formik.setValues(initial);
    }
  }, [importKey]);

  // 10 - Lấy thông tin chi tiết đơn hàng
  const getOrderInfo = async (id) => {
    dispatch(setSpinner(true));
    let res = await getOrder(id);
    if (res.data.success) {
      dispatch(setSpinner(false));
      return res.data.data;
    } else {
      dispatch(setSpinner(false));
      return false;
    }
  };

  // 11 - getListOrderProduct
  const getListOrderProduct = async () => {
    let items = [];
    for (let item of orderChoosen.po_id) {
      let orderAdd = await getOrderInfo(item * 1);
      orderAdd.items.forEach((item) => {
        items.push(locItem(item, orderAdd.code, orderAdd.id));
      });
    }
    return items;
  };

  // Lọc lại các giá trị cần thiết của sản phẩm
  const locItem = (item, code, id) => {
    return {
      did: null,
      po_did: item.did,
      po_no: code,
      po_id: id.toString(),
      product_id: item.product_id,
      sku: item.sku,
      product_name: item.product_name,
      unit: item.unit,
      price: item.price,
      delivery_actual_date: item.delivery_actual_date,
      qty: item.qty,
      payment_no: "",
      payment_date: "",
      note: "",
      vat: item.vat,
      vat_name: item.vat_name,
      total: item.total * 1,
    };
  };

  // 12 - Khi mã đơn hàng thay đổi => kéo api chi tiết đơn để lấy đc danh sách sản phẩm
  const onChangeOrder = async (value, option) => {
    dispatch(setSpinner(true));

    let items = [...orderChoosen.items];
    let list_po_id = [];
    let list_po_code = [];
    if (importKey && orderChoosen.items.length === 0) {
      // Đảm bảo getListOrderProduct() chạy và hoàn tất trước khi mở modal
      items = await getListOrderProduct();
    }

    // TH chọn thêm đơn hàng
    if (option && value.length > orderChoosen.po_id.length) {
      // Đơn cuối là đơn được thêm -> gọi API lấy chi tiết đơn & danh sách sp
      let orderAdd = await getOrderInfo(option[value.length - 1].key * 1);
      orderAdd.items.forEach((item) => {
        items.push(locItem(item, orderAdd.code, orderAdd.id));
      });
    }
    // TH xoá bớt đơn hàng
    else if (option && orderChoosen.po_id.length > value.length) {
      // Filter về danh sách orderChoosen.items mới không chứa sp thuộc order bị xoá vào trong table list sản phẩm cũng xoá đi
      const onlyInArray2 = orderChoosen.list_po_code.filter(
        (item) => !value.includes(item)
      );

      // Xoá các items trong orderChoosen.items mà thuộc đơn hàng onlyInArray2
      items = items.filter((item) => !onlyInArray2.includes(item.po_no));

      // Xoá các items trong formik.values.items mà thuộc đơn hàng onlyInArray2
      const filteredData = formik.values.items.filter(
        (item) => !onlyInArray2.includes(item.po_no)
      );
      setProductsSelected(filteredData);
      setUpdate(0);
    }

    if (value) {
      list_po_code = [...value];
      list_po_id = [...new Set(items.map((item) => item.po_id))]; // Lọc lại các po_id tồn tại
      setOrderChoosen({
        po_id: list_po_id,
        list_po_code,
        items,
      });
    }
    dispatch(setSpinner(false));
  };

  //  Khi
  useEffect(() => {
    if (n > 0 && orderChoosen) {
      formik.setFieldValue("po_id", orderChoosen.po_id);
      formik.setFieldValue("po_no", orderChoosen.list_po_code);
    }
  }, [orderChoosen]);

  // 13 - Khi xác nhận thêm sản phẩm từ đơn hàng
  const handleOkAddProducts = (value) => {
    //  value: mảng sản phẩm được chọn, check xem items thuộc value đã có trong mảng chưa, nếu chưa thì thêm vào mảng
    let arr = [...productsSelected];
    value.forEach((item) => {
      let index = productsSelected.findIndex((product) => {
        return (
          product.product_id === item.product_id && product.po_no === item.po_no
        );
      });

      if (index === -1) {
        arr.push({
          ...item,
          sub_total: Math.round(item.price * 1 * (item.qty * 1)),
          total: Math.round(item.price * 1 * (item.qty * 1)),
          payment_no: "",
          payment_date: "",
          note: "",
          did: "",
        });
      }
    });
    setProductsSelected(arr);
    // formik.setFieldValue("items", arr);
    setIsAddProductsModal(false);
  };

  // Khi huỷ chọn sản phẩm
  const handleCancelAddProducts = () => {
    setIsAddProductsModal(false);
  };

  // Khi productsChoosen thay đổi thì gắn giá trị cho formik products
  useEffect(() => {
    formik.setFieldValue("items", productsSelected);
  }, [productsSelected]);

  //  7 -  Trở về trang Hoàn ứng
  const goToWarehouseImport = () => {
    navigate("/payment_request");
  };

  //  9 - Khi current thay đổi
  useEffect(() => {
    formik.values.status = current;
  }, [current]);

  // 10 - Danh sách các Tabs
  const getItems = [
    {
      key: "info",
      label: <h2 className="text-[1rem] font-medium mb-1">Thông tin phiếu</h2>,
      children: (
        <ImportInfo
          formik={formik}
          filterOption={filterOption}
          current={current}
          importKey={importKey}
          onChangeOrder={onChangeOrder}
          listOrder={listOrder}
          listStaff={listStaff}
          setN={setN}
          departmentList={departmentList}
          listCompany={company}
          setListOrder={setListOrder}
          loadingSelect={loadingSelect}
          getPaymentMethodList={paymentMethodList}
          listPaymentMethod={listPaymentMethod}
          getOrderList={getListOrder}
          setOrderChoosen={setOrderChoosen}
        />
      ),
    },
    {
      key: "products",
      label: <h2 className="text-[1rem] font-medium mb-1">Sản phẩm </h2>,
      children: (
        <Products
          formik={formik}
          filterOption={filterOption}
          productsSelected={productsSelected}
          setProductsSelected={setProductsSelected}
          current={current}
          importKey={importKey}
          setIsAddProductsModal={setIsAddProductsModal}
          handleOkAddProducts={handleOkAddProducts}
          handleCancelAddProducts={handleCancelAddProducts}
          orderChoosen={orderChoosen}
          isAddProductsModal={isAddProductsModal}
          listTaxCode={listTaxCode}
          setUpdate={setUpdate}
          getListOrderProduct={getListOrderProduct}
          setOrderChoosen={setOrderChoosen}
          getOrderInfo={getOrderInfo}
        />
      ),
    },
    {
      key: "summary",
      label: <h2 className="text-[1rem] font-medium mb-1">Summary</h2>,
      children: (
        <Summary
          productsSelected={productsSelected}
          formik={formik}
          orderKey={importKey}
          current={current}
          listTaxCode={listTaxCode}
          update={update}
          setUpdate={setUpdate}
        />
      ),
    },
  ];

  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );

  return (
    <>
      {/* ======================= Content ======================= */}
      <div className="page-content">
        <form
          onSubmit={(event) => {
            event.preventDefault();
            formik.handleSubmit();
          }}
          encType="multipart/form-data"
        >
          {/* ======================= Trạng thái  ======================= */}
          {importKey && (
            <div className="form-group col-span-3 mb-5">
              <div className="form-style">
                <label htmlFor="contractSigningDate" className="mb-2">
                  Trạng thái
                </label>
                <SetStatus2Step
                  statusOptions={importStatus}
                  current={current}
                  setCurrent={setCurrent}
                  page={"reimbursement"}
                  permissionUpdate={permission?.payment_request?.update}
                />
              </div>
            </div>
          )}
          {/* ======================= Tabs lựa chọn ======================= */}
          <Collapse
            bordered={true}
            defaultActiveKey={["info", "products", "summary"]}
            expandIcon={({ isActive }) => (
              <CaretRightOutlined rotate={isActive ? 90 : 0} />
            )}
            items={getItems}
          />
          {/* ======================= warning ======================= */}
          <div className="mt-5">
            <p className="text-[#17a2b8]" id="warning">
              {Object.keys(formik.errors).length > 0
                ? "Biểu mẫu chưa hoàn chỉnh thông tin."
                : ""}
            </p>
          </div>
          {/* ======================= btns ======================= */}
          <div className="flex gap-3 mt-5 justify-between">
            <button className="btn-actions btn-main-bl" type="submit">
              <IoSaveOutline />
              Lưu
            </button>
            <button
              className="btn-actions btn-main-dark"
              onClick={goToWarehouseImport}
              type="button"
            >
              <LiaUndoSolid />
              Huỷ
            </button>
          </div>
        </form>
      </div>
    </>
  );
}
