import { Select, Spin } from "antd";
import React, { useCallback, useEffect } from "react";
import { Field, FormikProvider } from "formik";
import { debounce, stringToDate } from "../../../utils/others";
import moment from "moment";
import { getUser } from "../../../utils/user";

const { Option } = Select;

export default function ImportInfo({
  formik,
  filterOption,
  current,
  importKey,
  onChangeOrder,
  listOrder,
  listStaff,
  setN,
  listCompany,
  getPaymentMethodList,
  loadingSelect,
  listPaymentMethod,
  getOrderList,
  setOrderChoosen,
}) {
  const { handleBlur, touched, errors } = formik;

  const onSearch = (value) => {};

  const checkDepartment = async (value) => {
    let res = await getUser(value * 1);
    if (res.data.success) {
      formik.setFieldValue("department_name", res.data.data.position);
      formik.setFieldValue("department", res.data.data.department);
    }
  };

  return (
    <div className="orderInfo">
      <FormikProvider value={formik}>
        {/* Phiếu Đề nghị thanh toán */}
        <div>
          <h2 className="text-[1rem] font-medium mb-1 text-bl">
            Phiếu Đề nghị thanh toán
          </h2>
          <div
            className={
              current > 1
                ? "pointer-events-none grid grid-cols-2 xl:grid-cols-4 gap-2"
                : "grid grid-cols-2 xl:grid-cols-4 gap-2"
            }
          >
            {/* ======================= Mã phiếu Đề nghị thanh toán ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="code">Mã phiếu Đề nghị thanh toán</label>
                <input
                  className="grow"
                  type="string"
                  placeholder="Hệ thống tự tạo"
                  name="code"
                  id="code"
                  value={formik.values.code || ""}
                  disabled
                />
              </div>
              {touched.code && errors.code ? (
                <p className="mt-1 text-red-500 text-sm" id="code-warning">
                  {errors.code}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Ngày tạo ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="created_at">Ngày tạo</label>
                <input
                  className="grow"
                  type="text"
                  name="created_at"
                  id="created_at"
                  value={
                    importKey
                      ? stringToDate(formik.values.created_at)
                      : stringToDate(moment().format("yyyy-MM-DD"))
                  }
                  disabled
                />
              </div>
              {errors.created_at && touched.created_at ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="created_at-warning"
                >
                  {errors.created_at}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* =======================  Người yêu cầu ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="staff_number">
                  Người yêu cầu <span className="text-red-500">*</span>
                </label>
                <Field name="staff_number">
                  {({ field }) => {
                    return (
                      <Select
                        {...field}
                        showSearch
                        placeholder="Chọn người yêu cầu"
                        optionFilterProp="children"
                        onChange={(value) => {
                          formik.setFieldValue("staff_number", value);
                          checkDepartment(value);
                        }}
                        onSearch={onSearch}
                        filterOption={filterOption}
                        options={listStaff}
                        onBlur={handleBlur}
                        value={formik?.values?.staff_number || ""}
                      />
                    );
                  }}
                </Field>
              </div>
              {touched.staff_number && errors.staff_number ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="staff_number-warning"
                >
                  {errors.staff_number}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Bộ phận ======================= */}
            {formik?.values?.department_name && (
              <div className="form-group">
                <div className="form-style">
                  <label htmlFor="department_name">Bộ phận</label>
                  <input
                    disabled
                    placeholder="Hệ thống tự render"
                    value={formik.values?.department_name || ""}
                  />
                </div>
                {touched.department_name && errors.department_name ? (
                  <p
                    className="mt-1 text-red-500 text-sm"
                    id="department_name-warning"
                  >
                    {errors.department_name}
                  </p>
                ) : (
                  <></>
                )}
              </div>
            )}
            {/* =======================  Công ty ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="company">
                  Công ty <span className="text-red-500">*</span>
                </label>
                <Field name="company">
                  {({ field }) => {
                    return (
                      <Select
                        {...field}
                        showSearch
                        placeholder="Chọn công ty"
                        optionFilterProp="children"
                        onChange={(value) =>
                          formik.setFieldValue("company", value)
                        }
                        onSearch={onSearch}
                        filterOption={filterOption}
                        options={listCompany}
                        onBlur={handleBlur}
                        value={
                          formik.values.company ? formik.values.company : null
                        }
                      />
                    );
                  }}
                </Field>
              </div>
              {touched.company && errors.company ? (
                <p className="mt-1 text-red-500 text-sm" id="company-warning">
                  {errors.company}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* =======================  PTTT ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="paymentMethod">
                  Phương thức thanh toán<span className="text-red-500">*</span>
                </label>
                <Field name="paymentMethod">
                  {({ field }) => {
                    return (
                      <Select
                        {...field}
                        showSearch
                        placeholder="Chọn PTTT"
                        optionFilterProp="children"
                        onChange={(value) => {
                          formik.setFieldValue("paymentMethod", value);
                          setOrderChoosen({
                            po_id: [],
                            list_po_code: [],
                            items: [],
                          });
                          formik.setFieldValue("items", []);
                          formik.setFieldValue("po_id", []);
                          formik.setFieldValue("po_id", []);
                          getOrderList(value);
                        }}
                        onSearch={onSearch}
                        filterOption={filterOption}
                        options={listPaymentMethod}
                        onBlur={handleBlur}
                        value={formik.values.paymentMethod || ""}
                        onDropdownVisibleChange={(open) => {
                          open && getPaymentMethodList(1, "", true);
                        }}
                        notFoundContent={
                          loadingSelect ? <Spin size="small" /> : null
                        }
                      />
                    );
                  }}
                </Field>
              </div>
              {touched.paymentMethod && errors.paymentMethod ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="paymentMethod-warning"
                >
                  {errors.paymentMethod}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/*  ======================= Đơn hàng  ======================= */}
            {formik.values.paymentMethod && (
              <div
                className={
                  current > 1
                    ? "pointer-events-none col-span-2 "
                    : "col-span-2 "
                }
              >
                {/* ======================= Mã đơn hàng ======================= */}
                <div className="form-group">
                  <div className="form-style">
                    <label htmlFor="po_id">
                      Mã đơn hàng <span className="text-red-500">*</span>
                    </label>
                    <Field name="po_no">
                      {({ field }) => {
                        return (
                          <Select
                            mode="multiple"
                            showSearch
                            placeholder="Chọn đơn hàng"
                            optionFilterProp="children"
                            onChange={(value, option) => {
                              setN(1);
                              onChangeOrder(value, option);
                            }}
                            onSearch={onSearch}
                            filterOption={filterOption}
                            onBlur={handleBlur}
                            value={formik?.values?.po_no || []}
                          >
                            {listOrder.map((item) => (
                              <Option key={item.id} value={item.code}>
                                {item.code}
                              </Option>
                            ))}
                          </Select>
                        );
                      }}
                    </Field>
                  </div>
                  {errors.po_no ? (
                    <p className="mt-1 text-red-500 text-sm" id="po_no-warning">
                      {errors.po_no}
                    </p>
                  ) : (
                    <></>
                  )}
                </div>
              </div>
            )}
            {/* =======================  In NCC ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="print_suppliers">In NCC</label>
                <Field name="print_suppliers">
                  {({ field }) => {
                    return (
                      <Select
                        {...field}
                        showSearch
                        placeholder="Chọn "
                        optionFilterProp="children"
                        onChange={(value) => {
                          formik.setFieldValue("print_suppliers", value);
                        }}
                        onSearch={onSearch}
                        filterOption={filterOption}
                        options={[
                          { label: "Không hiển thị", value: 0 },
                          { label: "Hiển thị", value: 1 },
                        ]}
                        onBlur={handleBlur}
                        value={formik.values.print_suppliers}
                        onDropdownVisibleChange={(open) => {
                          open && getPaymentMethodList(1, "", true);
                        }}
                        notFoundContent={
                          loadingSelect ? <Spin size="small" /> : null
                        }
                      />
                    );
                  }}
                </Field>
              </div>
              {touched.print_suppliers && errors.print_suppliers ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="print_suppliers-warning"
                >
                  {errors.print_suppliers}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* =======================  Kiểu ngày in ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="print_date">Kiểu ngày in</label>
                <Field name="print_date">
                  {({ field }) => {
                    return (
                      <Select
                        {...field}
                        showSearch
                        placeholder="Chọn "
                        optionFilterProp="children"
                        onChange={(value) => {
                          formik.setFieldValue("print_date", value);
                        }}
                        onSearch={onSearch}
                        filterOption={filterOption}
                        options={[
                          { label: "Ngày dạng đơn", value: 0 },
                          { label: "Ngày ... đến ngày...", value: 1 },
                        ]}
                        onBlur={handleBlur}
                        value={formik.values.print_date}
                        onDropdownVisibleChange={(open) => {
                          open && getPaymentMethodList(1, "", true);
                        }}
                        notFoundContent={
                          loadingSelect ? <Spin size="small" /> : null
                        }
                      />
                    );
                  }}
                </Field>
              </div>
              {touched.print_date && errors.print_date ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="print_date-warning"
                >
                  {errors.print_date}
                </p>
              ) : (
                <></>
              )}
            </div>
          </div>
        </div>
      </FormikProvider>
    </div>
  );
}
