import React, { useEffect } from "react";
import "./summary.scss";
import { FormikProvider } from "formik";
import InputPriceNotTable from "../../Input/InpurPriceNotTable";
import { NumericFormat } from "react-number-format";

export default function Summary({
  productsSelected,
  formik,
  orderKey,
  current,
  listTaxCode,
  update,
  setUpdate,
}) {
  const onChangeValue = (value, field) => {
    formik.setFieldValue(field, value);
  };

  const { handleBlur, handleChange, touched, errors, values } = formik;

  let calRemainingAmount = () => {
    formik.setFieldValue(
      "remaining_amount",
      formik?.values?.total -
        (formik?.values?.advance_money_first +
          formik?.values?.advance_money_second)
    );
  };

  // Tính tổng còn lại
  useEffect(() => {
    if (update === 0) {
      calRemainingAmount();
    }
  }, [
    formik?.values?.total,
    formik?.values?.advance_money_first,
    formik?.values?.advance_money_second,
  ]);

  let calTotal = () => {
    formik.setFieldValue(
      "total",
      formik?.values?.sub_total + formik?.values?.total_vat
    );
  };
  // Tính tổng
  useEffect(() => {
    if (update === 0) {
      calTotal();
    }
  }, [formik.values.total_vat, formik.values.sub_total]);

  //   Tính và setState cho summary
  let calSummary = () => {
    let totalWithoutTax = 0; // Thành tiền trước VAT
    let totalTax = 0; // Lưu trữ tổng thuế VAT

    // Hàm lấy phần trăm VAT từ listTaxCode dựa vào vat_id
    const getVatPercentage = (vatId) => {
      const tax = listTaxCode.find((taxCode) => taxCode.key == vatId);
      return tax ? parseFloat(tax.vat) : 0;
    };

    productsSelected.forEach((product) => {
      const qty = product.qty * 1;
      const price = product.price * 1;
      const vatId = product.vat * 1; // Đây là vat_id

      let productTotalWithoutTax = 0;
      // Nếu sản phẩm có làm tròn thành tiền (key sub_total_rounded = true )
      if (formik.values?.sub_total_items_rounded) {
        productTotalWithoutTax = Math.round(product.qty * price);
      } else {
        productTotalWithoutTax = qty * price;
      }

      // Tính tổng giá trị chưa bao gồm thuế
      totalWithoutTax += productTotalWithoutTax;

      // Tính thuế cho sản phẩm dựa trên vat
      const vatPercentage = getVatPercentage(vatId);

      // Tính tổng thuế cộng dồn từng sp
      totalTax += productTotalWithoutTax * (vatPercentage / 100);
    });
    // Trả về kết quả, bao gồm tổng trước thuế & tổng vat
    return {
      totalWithoutTax,
      totalTax,
    };
  };

  //   Tính và setState cho summary tự động khi danh sách sản phẩm được chọn thay đổi
  useEffect(() => {
    if (update === 0) {
      let summary = calSummary();
      formik.setFieldValue("sub_total", summary.totalWithoutTax);
      formik.setFieldValue("total_vat", summary.totalTax);
    }
  }, [formik.values.items, listTaxCode]);

  return (
    <FormikProvider value={formik}>
      <div
        className={
          current > 1
            ? "summary max-w-[400px] pointer-events-none"
            : "summary max-w-[400px]"
        }
      >
        {/* ======================= Tổng = tổng thành tiền của các mã sp (ko vat) ======================= */}
        <div className="item">
          <p className="font-bold">
            <b>Tổng :</b>
          </p>
          <p className="text-right grow font-bold">
            {Number(formik?.values?.sub_total).toLocaleString("en-US")}
          </p>
        </div>
        {/* ======================= VAT ======================= */}
        <div className="form-group ">
          <div className="form-style item" style={{ flexDirection: "row" }}>
            <label htmlFor="total_vat" className="w-[50%] ">
              Thuế VAT
            </label>
            <NumericFormat
              thousandSeparator=","
              allowNegative={false}
              displayType="input"
              placeholder=""
              min={0}
              className="grow text-right"
              value={Number(formik.values.total_vat) || 0}
              onValueChange={(value) => {
                if (
                  formik.values.total_vat * 1 !== value.floatValue &&
                  orderKey
                )
                  setUpdate(0);
                //  Cho thay đổi giá trị thuế GTGT
                if (!value.value) {
                  formik.setFieldValue("total_vat", 0);
                } else {
                  formik.setFieldValue("total_vat", value.floatValue);
                }
              }}
            />
          </div>
          {touched.vat && errors.vat ? (
            <p className="mt-1 text-red-500 text-sm" id="vat-warning">
              {errors.vat}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Tổng cộng ======================= */}
        <div className="item font-bold">
          <p className="font-bold">
            <b>Tổng cộng:</b>
          </p>
          <p className="text-right grow ">
            {Number(formik?.values?.total).toLocaleString("en-US")}
          </p>
        </div>
        {/* ======================= Tạm ứng lần 1 ======================= */}
        <div className="form-group">
          <div className="form-style item" style={{ flexDirection: "row" }}>
            <label htmlFor="advance_money_first" className="w-[50%]">
              Tạm ứng lần 1
            </label>
            <InputPriceNotTable
              className="grow text-right"
              placeholder="Nhập số tiền tạm ứng lần 1"
              name="advance_money_first"
              id="advance_money_first"
              value={
                formik?.values?.advance_money_first
                  ? formik.values.advance_money_first
                  : 0
              }
              onChange={(value) => {
                setUpdate(0);
                onChangeValue(value, "advance_money_first");
              }}
              onBlur={handleBlur}
            />
          </div>
          {touched.advance_money_first && errors.advance_money_first ? (
            <p
              className="mt-1 text-red-500 text-sm"
              id="advance_money_first-warning"
            >
              {errors.advance_money_first}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Tạm ứng lần 2 ======================= */}
        <div className="form-group">
          <div className="form-style item" style={{ flexDirection: "row" }}>
            <label htmlFor="advance_money_second" className="w-[50%]">
              Tạm ứng lần 2
            </label>
            <InputPriceNotTable
              className="grow text-right "
              placeholder="Nhập số tiền tạm ứng lần 2"
              name="advance_money_second"
              id="advance_money_second"
              value={
                formik?.values?.advance_money_second
                  ? formik.values.advance_money_second
                  : 0
              }
              onChange={(value) => {
                setUpdate(0);
                onChangeValue(value, "advance_money_second");
              }}
              onBlur={handleBlur}
            />
          </div>
          {touched.advance_money_second && errors.advance_money_second ? (
            <p
              className="mt-1 text-red-500 text-sm"
              id="advance_money_second-warning"
            >
              {errors.advance_money_second}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Còn lại ======================= */}
        <div className="form-group">
          <div className="form-style item" style={{ flexDirection: "row" }}>
            <label htmlFor="remaining_amount" className="w-[50%]">
              <b> Còn lại</b>
            </label>
            <p className="text-right grow">
              <b>
                {Number(formik?.values?.remaining_amount).toLocaleString(
                  "en-US"
                )}
              </b>
            </p>
          </div>
          {touched.remaining_amount && errors.remaining_amount ? (
            <p
              className="mt-1 text-red-500 text-sm"
              id="remaining_amount-warning"
            >
              {errors.remaining_amount}
            </p>
          ) : (
            <></>
          )}
        </div>
      </div>
    </FormikProvider>
  );
}
