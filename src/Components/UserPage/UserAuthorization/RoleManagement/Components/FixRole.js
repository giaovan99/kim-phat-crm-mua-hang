import React, { useEffect } from "react";
import { Modal, Image } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import Swal from "sweetalert2";
import { Select } from "antd";

export default function FixRole({ isModalOpen, setIsModalOpen, role }) {
  const handleOk = () => {
    formik.handleSubmit();
  };

  const handleCancel = () => {
    formik.setValues(role);
    setIsModalOpen(false);
  };

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      role: "",
    },
    validationSchema: yup.object({
      role: yup.string().required("Tên chức vụ không để trống"),
    }),
    onSubmit: async (values) => {
      // console.log(values);
      // alert thông báo
      Swal.fire({
        title: "Sửa chức vụ",
        text: "Sửa chức vụ thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      // reset form
      formik.resetForm();
      //  đóng modal
      setIsModalOpen(false);
    },
  });

  useEffect(() => {
    if (role) {
      formik.setValues(role);
    }
  }, [role]);

  const { handleBlur, handleChange, touched, errors } = formik;
  return (
    <Modal
      title={"Sửa chức vụ"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Cập nhật"
      cancelText="Huỷ"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form id="formAddUser" onSubmit={formik.handleSubmit}>
        <div className="grid grid-cols-2 gap-2">
          {/* ======================= Đặt tên chức vụ ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="role">
                Tên chức vụ <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên chức vụ"
                name="role"
                id="role"
                value={formik.values.role}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.role && errors.role ? (
              <p className="mt-1 text-red-500 text-sm" id="role-warning">
                {errors.role}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
      </form>
    </Modal>
  );
}
