import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { deletedPosition, getListPosition } from "../../../../../utils/user";
import RenderWithFixComponent from "../../../../ButtonComponent/RenderIdFix";
import FixBtnComponent from "../../../../ButtonComponent/FixBtnComponent";
import DeleteBtnItemComponent from "../../../../ButtonComponent/DeleteBtnItemComponent";
import { setSpinner } from "../../../../../redux/slice/spinnerSlice";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "key",
};

export default function TableListRole({
  showModal,
  reload,
  setReload,
  rowSelection,
  permission,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const pageSize = 20;
  const dispatch = useDispatch();
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list Chức vụ theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (keywords) {
      keyParam = { keywords: keywords };
    }
    let res = await getListPosition(page, keyParam);
    if (res.status) {
      setListData(res.data);
      setTotal(res.data.length);
    }
    dispatch(setSpinner(false));
  };
  // 4 - Xác nhận xoá đơn vị tính
  const confirm = (record) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deletePosition(record);
  };

  // 5 - Xoá Chức vụ
  const deletePosition = async (record) => {
    dispatch(setSpinner(true));
    let res = await deletedPosition({
      ...record,
      deleted: true,
    });
    // console.log(res);
    if (res) {
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 6 - Gọi lại API khi chuyển tab
  useEffect(() => {
    if (reload) {
      getListData();
      setReload(false);
    }
  }, [reload]);

  const columns = [
    {
      title: "Chức vụ",
      dataIndex: "label",
      key: "label",
      fixed: "left",
      className: "text-center",
      render: (text, record) =>
        permission?.roles?.update === 1 ? (
          <RenderWithFixComponent
            onClickF={() => {
              // console.log(record);
              showModal(record);
            }}
            text={text}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            {permission?.roles?.update === 1 && (
              <FixBtnComponent
                onClickF={() => {
                  showModal(record);
                }}
              />
            )}

            {/* ======================= Xoá ======================= */}
            {permission?.roles?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => confirm(record)}
                text={"kho hàng"}
              />
            )}
          </div>
        );
      },
    },
  ];

  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        pagination={{
          ...paginationObj,
        }}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        scroll={{
          y: 600,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
