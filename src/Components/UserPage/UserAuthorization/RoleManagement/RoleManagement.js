import React, { useEffect, useState } from "react";
import Actions from "./Components/Actions";
import TableListRole from "./Components/TableListRole";
import Type1 from "../../../ManageUnits/Type1/Type1";
import { resetKeywords } from "../../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../../redux/slice/pageSlice";
import { useDispatch } from "react-redux";

export default function RoleManagement({ keyNumActive, keyTab, permission }) {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [object, setObject] = useState();
  const [reload, setReload] = useState(false);
  const dispatch = useDispatch();
  const [keySelected, setKeySelected] = useState([]); //

  let showModal = (record) => {
    setObject(record);
    setIsModalOpen(true);
  };

  useEffect(() => {
    if (keyNumActive == keyTab) {
      dispatch(resetKeywords());
      dispatch(resetPage());
      setReload(true);
    }
  }, [keyNumActive]);

  useEffect(() => {
    dispatch(resetKeywords());
    dispatch(resetPage());
  }, []);

  const rowSelection = {
    onChange: (selectedRows, info) => {
      setKeySelected(info);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  return (
    <div className="roleManagement">
      {/* ======================= Hành động ======================= */}
      <Actions
        showModal={showModal}
        setReload={setReload}
        keyNumActive={keyNumActive}
        keyTab={keyTab}
        keySelected={keySelected}
        setKeySelected={setKeySelected}
        permission={permission}
      />
      {/* ======================= Danh sách phân quyền ======================= */}
      <TableListRole
        showModal={showModal}
        reload={reload}
        setReload={setReload}
        rowSelection={rowSelection}
        permission={permission}
      />
      {/* ======================= Modals ======================= */}
      <div className="modals">
        <Type1
          title={object?.value ? "Sửa chức vụ" : "Thêm chức vụ"}
          object={object}
          API="position"
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          setObject={setObject}
          setReload={setReload}
        />
      </div>
    </div>
  );
}
