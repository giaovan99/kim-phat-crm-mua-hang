import React, { useEffect, useState } from "react";
import Actions from "./Components/Actions";
import TableListPermission from "./Components/TableListPermission";
import PermissionDetails from "./Components/PermissionDetails";
import { useDispatch } from "react-redux";
import { resetPage } from "../../../../redux/slice/pageSlice";
import { getListFunctions } from "../../../../utils/roles";
import { resetKeywords } from "../../../../redux/slice/keywordsSlice";
import { setSpinner } from "../../../../redux/slice/spinnerSlice";

export default function PermissionsManagement({
  keyNumActive,
  keyTab,
  permission,
}) {
  // Khai báo state
  const [permissionChoosen, setPermissionChoosen] = useState(); //đánh dấu id được chọn
  const [isModalOpen, setIsModalOpen] = useState(); //ẩn hiện modal
  const [listFunction, setListFunction] = useState([]); //
  const [reload, setReload] = useState(false);
  const [keySelected, setKeySelected] = useState([]); //

  const dispatch = useDispatch();

  const showModal = (key) => {
    setIsModalOpen(true);
    setPermissionChoosen(key);
  };

  // Tải lại dữ liệu từ trang 1
  let loadPage = () => {
    // Reset page redux ==1
    dispatch(resetPage());
  };

  // Lấy danh sách tính năng
  const getListFunctionsAPI = async () => {
    dispatch(setSpinner(true));
    let res = await getListFunctions();

    if (res.status) {
      const newData = res.data.map((item) => {
        return { ...item, checkAll: false };
      });
      setListFunction(newData);
    }
    dispatch(setSpinner(false));
  };

  // Lấy danh sách functions mỗi khi load trang
  useEffect(() => {
    getListFunctionsAPI();
    dispatch(resetKeywords());
    loadPage();
  }, []);

  useEffect(() => {
    if (keyNumActive == keyTab) {
      setReload(true);
      dispatch(resetKeywords());
      loadPage();
    }
  }, [keyNumActive]);

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    onChange: (selectedRows) => {
      // console.log(selectedRows);
      setKeySelected(selectedRows);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  return (
    <div className="permissionsManagement">
      {/* ======================= Hành động ======================= */}
      <Actions
        showModal={showModal}
        setReload={setReload}
        keyNumActive={keyNumActive}
        keyTab={keyTab}
        keySelected={keySelected}
        setKeySelected={setKeySelected}
        permission={permission}
      />
      {/* ======================= Danh sách phân quyền ======================= */}
      <TableListPermission
        showModal={showModal}
        loadPage={loadPage}
        reload={reload}
        setReload={setReload}
        setKeySelected={setKeySelected}
        rowSelection={rowSelection}
        permission={permission}
      />
      {/* ======================= Modals ======================= */}
      <div className="modals">
        <PermissionDetails
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          permissionChoosen={permissionChoosen}
          listFunction={listFunction}
          setPermissionChoosen={setPermissionChoosen}
          setReload={setReload}
          permission={permission}
        />
      </div>
    </div>
  );
}
