import React, { useEffect, useState } from "react";
import { Table } from "antd";
import { useSelector, useDispatch } from "react-redux";
import { setPage } from "../../../../../redux/slice/pageSlice";
import { deleteRole, getListRoles } from "../../../../../utils/roles";
import RenderWithFixComponent from "../../../../ButtonComponent/RenderIdFix";
import FixBtnComponent from "../../../../ButtonComponent/FixBtnComponent";
import DeleteBtnItemComponent from "../../../../ButtonComponent/DeleteBtnItemComponent";
import { setSpinner } from "../../../../../redux/slice/spinnerSlice";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListPermission({
  loadPage,
  showModal,
  reload,
  setReload,
  rowSelection,
  permission,
}) {
  // Khai báo state
  const [listRoles, setListRoles] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();
  let page = useSelector((state) => state.pageSlice.value);
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListRolesAPI(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // Kéo data list roles theo page
  const getListRolesAPI = async (page) => {
    dispatch(setSpinner(true));
    let keyParam = {};
    if (keywords) {
      keyParam = { ...keyParam, keywords: keywords };
    }
    let res = await getListRoles(page, keyParam);
    if (res.status) {
      setListRoles(res.data);
      setTotal(res.fullRes.total);
      setPageSize(res.fullRes.per_page);
    }
    dispatch(setSpinner(false));
  };

  // Lần đầu load trang
  useEffect(() => {
    loadPage();
    getListRolesAPI();
  }, []);

  // Xoá phân quyền
  const confirm = async (id) => {
    dispatch(setSpinner(true));
    let res = await deleteRole({ id: id, deleted: 1 });
    if (res) {
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  const columns = [
    {
      title: "Phân quyền",
      dataIndex: "label",
      key: "label",
      width: 150,
      fixed: "left",
      className: "text-center",
      render: (text, record) =>
        permission?.roles?.update === 1 ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record.id);
            }}
            text={text}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Quyền truy cập",
      dataIndex: "permission_name",
      key: "permission_name",
    },
    {
      title: "Trạng thái",
      dataIndex: "active",
      key: "active",
      width: 150,
      render: (record) => {
        return record ? (
          <p className="flex items-center justify-center text-green-700">
            Đang hoạt động
          </p>
        ) : (
          <p className="text-center text-red-700">Ngưng hoạt động</p>
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            {permission?.roles?.update === 1 && (
              <FixBtnComponent
                onClickF={() => {
                  showModal(record.id);
                }}
              />
            )}
            {/* ======================= Xoá ======================= */}
            {permission?.roles?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => confirm(record.id)}
                text={"chức vụ"}
              />
            )}
          </div>
        );
      },
    },
  ];

  useEffect(() => {
    if (reload) {
      // loadPage();
      getListRolesAPI();
      setReload(false);
    }
  }, [reload]);
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        pagination={{ ...paginationObj }}
        scroll={{
          y: 600,
        }}
        columns={columns}
        dataSource={listRoles}
      />
    </div>
  );
}
