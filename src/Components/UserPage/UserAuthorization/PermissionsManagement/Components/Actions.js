import { useFormik } from "formik";
import React, { useEffect } from "react";
import { HiOutlineSearch, HiOutlinePlusCircle } from "react-icons/hi";
import { useDispatch, useSelector } from "react-redux";
import { setKeywords } from "../../../../../redux/slice/keywordsSlice";
import DropdownOptions from "../../../../DropdownOptions/DropdownOptions";
import AddBtnComponent from "../../../../ButtonComponent/AddBtnComponent";

export default function Actions({
  showModal,
  setReload,
  keyNumActive,
  keyTab,
  keySelected,
  setKeySelected,
  permission,
}) {
  let dispatch = useDispatch();
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: async (values) => {
      // console.log(values);
      dispatch(setKeywords(values.search));
      setReload(true);
    },
  });

  useEffect(() => {
    if (keyNumActive == keyTab) {
      formik.resetForm();
    }
  }, [keyNumActive]);

  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div className="flex gap-1">
            {permission?.roles?.update === 1 && (
              <DropdownOptions
                arrId={keySelected}
                page={"roles"}
                setReload={setReload}
                setKeySelected={setKeySelected}
              />
            )}
            <div className="form-group">
              <div className="form-style">
                <input
                  id="search"
                  name="search"
                  type="text"
                  placeholder="Tìm kiếm loại phân quyền"
                  value={formik.values.search}
                  onChange={formik.handleChange}
                />
                <button className="btn-main-yl btn-actions">
                  <HiOutlineSearch />
                  Tìm kiếm
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      <div>
        {permission?.roles?.create === 1 && (
          <AddBtnComponent
            onClickF={() => showModal(null)}
            text={"Thêm phân quyền"}
          />
        )}
      </div>
    </div>
  );
}
