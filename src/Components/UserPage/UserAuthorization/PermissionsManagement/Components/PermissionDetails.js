import React, { useEffect } from "react";
import { Checkbox, Modal } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { Switch } from "antd";
import { addRole, getRoleInfo, updateRole } from "../../../../../utils/roles";
import { setSpinner } from "../../../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

export default function PermissionDetails({
  isModalOpen,
  setIsModalOpen,
  permissionChoosen,
  listFunction,
  setPermissionChoosen,
  setReload,
}) {
  // Khai báo state
  const initial = {
    name: "",
    permission: [...listFunction],
    active: true,
  };
  const dispatch = useDispatch();

  // Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      name: yup.string().required("Tên không để trống"),
      // permission:yup.array().oneOf()
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      let res;
      // let data =
      if (permissionChoosen) {
        // console.log("sửa");
        res = await updateRole(values);
      } else {
        // console.log("thêm");
        res = await addRole(values);
      }
      if (res.status) {
        setPermissionChoosen(null);
        resetForm();
        //  đóng modal
        setIsModalOpen(false);
        //  Load lại trang 1
        setReload(true);
      }
      dispatch(setSpinner(false));
    },
  });

  // Lây thông tin role
  const getRolesInfopAPI = async () => {
    dispatch(setSpinner(true));
    let res = await getRoleInfo(permissionChoosen);
    if (res.status) {
      // formik.setValues(res.data);
      formik.setFieldValue("name", res.data.name);
      formik.setFieldValue("permission", res.data.permission);
      formik.setFieldValue("active", res.data.active);
      formik.setFieldValue("id", res.data.id);
    }
    dispatch(setSpinner(false));
  };

  //   Lần đầu khi load trang => check xem đang thêm hay sửa
  useEffect(() => {
    if (permissionChoosen) {
      // console.log("sứa");
      getRolesInfopAPI();
    } else {
      // console.log("thêm");
      formik.setValues({ ...initial });
    }
  }, [permissionChoosen]);

  // Lưu form
  const handleOk = () => {
    formik.handleSubmit();
  };

  //   Huỷ form
  const handleCancel = () => {
    resetForm();
    setPermissionChoosen(null);
    setIsModalOpen(false);
  };

  //   Thay đổi trạng thái Phân quyền
  const onChangeStatus = (checked) => {
    // console.log(`switch to ${checked}`);
    formik.setFieldValue("active", checked);
  };

  // Khi check box thay đổi
  const onChangeCheckbox = (event, item, index, key) => {
    let active;
    if (event.target.checked) {
      active = true;
    } else {
      active = false;
    }
    item[key] = active;
    if (key === "pfull") {
      item.pcreate = active;
      item.pread = active;
      item.pupdate = active;
      item.pdelete = active;
    } else {
      if (!active) {
        item.pfull = false;
      }
    }

    const newValue = [...formik.values.permission];
    newValue[index] = item;
    formik.setFieldValue("permission", newValue);
  };

  const { handleBlur, handleChange, touched, errors } = formik;

  //   Render table list các quyền
  let renderTbody = () => {
    return formik.values.permission.map((item, index) => {
      return (
        <tr>
          <td className="border border-slate-300 p-2 font-medium">
            {item.function_name}
          </td>
          {/* Xem */}
          <td className="border border-slate-300 p-2 text-center">
            <Checkbox
              onChange={(event) => {
                onChangeCheckbox(event, item, index, "pread");
              }}
              checked={formik.values.permission[index].pread}
            ></Checkbox>
          </td>
          {/* Thêm */}
          <td className="border border-slate-300 p-2 text-center">
            <Checkbox
              onChange={(event) => {
                onChangeCheckbox(event, item, index, "pcreate");
              }}
              checked={formik.values.permission[index].pcreate}
            ></Checkbox>
          </td>
          {/* Xoá */}
          <td className="border border-slate-300 p-2 text-center">
            <Checkbox
              onChange={(event) => {
                onChangeCheckbox(event, item, index, "pdelete");
              }}
              checked={formik.values.permission[index].pdelete}
            ></Checkbox>
          </td>
          {/* Sửa */}
          <td className="border border-slate-300 p-2 text-center">
            <Checkbox
              onChange={(event) => {
                onChangeCheckbox(event, item, index, "pupdate");
              }}
              checked={formik.values.permission[index].pupdate}
            ></Checkbox>
          </td>
          {/* Toàn quyền */}
          <td className="border border-slate-300 p-2 text-center">
            <Checkbox
              // onChange={(event) => {
              //   onChangeCheckboxAll(event, item, index);
              // }}
              // checked={item.checkAll}
              onChange={(event) => {
                onChangeCheckbox(event, item, index, "pfull");
              }}
              checked={formik.values.permission[index].pfull}
            ></Checkbox>
          </td>
        </tr>
      );
    });
  };

  const resetForm = () => {
    formik.resetForm();
    const newList = [...listFunction];
    for (let list of newList) {
      // console.log(list);
      list.pcreate = false;
      list.pdelete = false;
      list.pupdate = false;
      list.pread = false;
      list.pfull = false;
    }
    formik.setValues({ ...initial, permission: newList });
  };

  return (
    <Modal
      title={permissionChoosen ? "Chỉnh sửa phân quyền" : "Thêm phân quyền"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form id="formAddPermission" onSubmit={formik.handleSubmit}>
        <div className="grid grid-cols-3 gap-5">
          {/* ======================= Tên phân quyền ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="name">
                Tên phân quyền <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên "
                name="name"
                id="name"
                value={formik.values.name}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.name && errors.name ? (
              <p className="mt-1 text-red-500 text-sm" id="name-warning">
                {errors.name}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Trạng thái ======================= */}
          <div className="form-group ">
            <div className="form-style">
              <label htmlFor="active">Trạng thái</label>
              <div className="switch-type">
                <Switch
                  onChange={onChangeStatus}
                  className="w-[100px]"
                  checkedChildren="Hoạt động"
                  unCheckedChildren="Ngừng"
                  checked={formik.values.active}
                />
              </div>
            </div>
            {touched.active && errors.active ? (
              <p className="mt-1 text-red-500 text-sm" id="active-warning">
                {errors.active}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
        {/* ======================= Render List Functions ======================= */}
        <table
          className="border-collapse w-full border border-slate-400 mt-5"
          id="tableListFunctions"
        >
          <thead>
            <th className="border border-slate-300">Trang/Quyền</th>
            <th className="border border-slate-300">Xem</th>
            <th className="border border-slate-300">Thêm</th>
            <th className="border border-slate-300">Xoá</th>
            <th className="border border-slate-300">Sửa</th>
            <th className="border border-slate-300">Toàn quyền</th>
          </thead>
          <tbody>
            {formik.values.permission &&
              formik.values.permission.length > 0 &&
              renderTbody()}
          </tbody>
        </table>
      </form>
    </Modal>
  );
}
