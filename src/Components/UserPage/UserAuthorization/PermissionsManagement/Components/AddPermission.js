import React from "react";
import { Checkbox, Modal } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import Swal from "sweetalert2";
import { Switch } from "antd";

export default function AddPermission({
  isModalOpen,
  setIsModalOpen,
  permission,
}) {
  const handleOk = () => {
    formik.handleSubmit();
  };

  const handleCancel = () => {
    formik.resetForm();
    setIsModalOpen(false);
  };

  const onChangeStatus = (checked) => {
    // console.log(`switch to ${checked}`);
    formik.setFieldValue("status", checked);
  };

  const onChangeCheckboxAll = (event, item, index) => {
    let active;
    if (event.target.checked) {
      active = true;
    } else {
      active = false;
    }
    for (let key in item) {
      if (key != "function") {
        item[key] = active;
      }
    }
    const newValue = [...formik.initialValues.permissionsManagerment];
    newValue[index] = item;
    formik.setFieldValue("permissionsManagerment", newValue);
  };

  const onChangeCheckbox = (event, item, index, key) => {
    let active;
    if (event.target.checked) {
      active = true;
    } else {
      active = false;
    }
    item[key] = active;
    const newValue = [...formik.initialValues.permissionsManagerment];
    newValue[index] = item;
    formik.setFieldValue("permissionsManagerment", newValue);
  };

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      permission: "",
      status: true,
      permissionsManagerment: [
        {
          function: "Đơn hàng",
          fix: false,
          add: false,
          delete: false,
        },
        {
          function: "Đề nghị mua hàng",
          fix: false,
          add: false,
          delete: false,
        },
        {
          function: "Nhà cung cấp",
          fix: false,
          add: false,
          delete: false,
        },
        {
          function: "Sản phẩm",
          fix: false,
          add: false,
          delete: false,
        },
        {
          function: "Nhập kho",
          fix: false,
          add: false,
          delete: false,
        },
      ],
    },
    validationSchema: yup.object({
      permission: yup.string().required("Tên không để trống"),
    }),
    onSubmit: async (values) => {
      // console.log(values);
      // alert thông báo
      Swal.fire({
        title: "Thêm phân quyền",
        text: "Thêm phân quyền thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      // reset form
      formik.resetForm();
      //  đóng modal
      setIsModalOpen(false);
    },
  });

  const { handleBlur, handleChange, touched, errors } = formik;
  // console.log(formik.values);

  let renderTbody = () => {
    return formik.initialValues.permissionsManagerment.map((item, index) => {
      // console.log(index);
      let activeAll = false;
      for (let key in item) {
        if (key != "function" && item[key] == false) {
          activeAll = false;
          break;
        } else {
          activeAll = true;
        }
      }

      return (
        <tr>
          <td className="border border-slate-300 p-2 font-medium">
            {item.function}
          </td>
          <td className="border border-slate-300 p-2 text-center">
            <Checkbox
              onChange={(event) => {
                onChangeCheckbox(event, item, index, "add");
              }}
              checked={formik.initialValues.permissionsManagerment[index].add}
            ></Checkbox>
          </td>
          <td className="border border-slate-300 p-2 text-center">
            <Checkbox
              onChange={(event) => {
                onChangeCheckbox(event, item, index, "delete");
              }}
              checked={
                formik.initialValues.permissionsManagerment[index].delete
              }
            ></Checkbox>
          </td>
          <td className="border border-slate-300 p-2 text-center">
            <Checkbox
              onChange={(event) => {
                onChangeCheckbox(event, item, index, "fix");
              }}
              checked={formik.initialValues.permissionsManagerment[index].fix}
            ></Checkbox>
          </td>
          <td className="border border-slate-300 p-2 text-center">
            <Checkbox
              onChange={(event) => {
                onChangeCheckboxAll(event, item, index);
              }}
              checked={activeAll}
            ></Checkbox>
          </td>
        </tr>
      );
    });
  };

  return (
    <Modal
      title={permission ? "Chỉnh sửa phân quyền" : "Thêm phân quyền"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form id="formAddPermission" onSubmit={formik.handleSubmit}>
        <div className="grid grid-cols-3 gap-5">
          {/* ======================= Tên phân quyền ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="permission">
                Tên phân quyền <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên "
                name="permission"
                id="permission"
                value={formik.values.permission}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.permission && errors.permission ? (
              <p className="mt-1 text-red-500 text-sm" id="permission-warning">
                {errors.permission}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Trạng thái ======================= */}
          <div className="form-group ">
            <div className="form-style">
              <label htmlFor="status">Trạng thái</label>
              <div className="switch-type">
                <Switch
                  onChange={onChangeStatus}
                  className="w-[100px]"
                  checkedChildren="Hoạt động"
                  unCheckedChildren="Ngừng"
                  checked={formik.values.status}
                />
              </div>
            </div>
            {touched.status && errors.status ? (
              <p className="mt-1 text-red-500 text-sm" id="status-warning">
                {errors.status}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
        {/* ======================= Render List Functions ======================= */}
        <table className="border-collapse w-full border border-slate-400 mt-5">
          <thead>
            <th className="border border-slate-300">Quyền</th>
            <th className="border border-slate-300">Thêm</th>
            <th className="border border-slate-300">Xoá</th>
            <th className="border border-slate-300">Sửa</th>
            <th className="border border-slate-300">Toàn quyền</th>
          </thead>
          <tbody>{renderTbody()}</tbody>
        </table>
      </form>
    </Modal>
  );
}
