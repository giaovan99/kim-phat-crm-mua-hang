import React, { useEffect } from "react";
import { Modal } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { updateUser } from "../../../utils/user";
import { useDispatch } from "react-redux";
import { resetKeywords } from "../../../redux/slice/keywordsSlice";
import { setSpinner } from "../../../redux/slice/spinnerSlice";

export default function ChangePassword({
  isModalOpen,
  setIsModalOpen,
  user,
  setUser,
  setReload,
}) {
  const handleOk = () => {
    formik.handleSubmit();
  };

  const handleCancel = () => {
    formik.resetForm();
    setUser(null);
    setIsModalOpen(false);
  };

  let dispatch = useDispatch();

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      id: "",
      password: "",
      confirm_password: "",
    },
    validationSchema: yup.object({
      password: yup
        .string()
        .required("Mật khẩu không để trống")
        .matches(/^().{8,}$/, "Mật khẩu yêu cầu ít nhất 8 ký tự"),
      confirm_password: yup
        .string()
        .required("Nhập lại mật khẩu")
        .oneOf([yup.ref("password"), null], "Mật khẩu chưa trùng khớp"),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      // console.log(values);
      let res = await updateUser(values);
      if (res) {
        // set lại keyword = ""
        dispatch(resetKeywords());
        //  đóng modal
        setIsModalOpen(false);
        //  Load lại trang 1
        setReload(true);
      }
      // reset form
      formik.resetForm();
      //  đóng modal
      setIsModalOpen(false);
      dispatch(setSpinner(false));
    },
  });

  const { handleBlur, handleChange, touched, errors } = formik;

  useEffect(() => {
    formik.setFieldValue("id", user);
  }, [user]);

  return (
    <Modal
      title={"Đổi mật khẩu"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Cập nhật"
      cancelText="Huỷ"
      okButtonProps={{
        className: "btn-main-yl inline-flex items-center justify-center",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form id="formAddUser" onSubmit={formik.handleSubmit}>
        <div className="grid grid-cols-2 gap-2">
          {/* ======================= Mật khẩu mới ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="password">
                Mật khẩu mới <span className="text-red-500">*</span>
              </label>
              <input
                type="password"
                placeholder="Nhập mật khẩu mới"
                name="password"
                id="password"
                value={formik.values.password}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.password && errors.password ? (
              <p className="mt-1 text-red-500 text-sm" id="password-warning">
                {errors.password}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Xác nhận mật khẩu ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="confirm_password">
                Xác nhận mật khẩu <span className="text-red-500">*</span>
              </label>
              <input
                type="password"
                placeholder="Nhập mật khẩu"
                name="confirm_password"
                id="confirm_password"
                value={formik.values.confirm_password}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.confirm_password && errors.confirm_password ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="confirm_password-warning"
              >
                {errors.confirm_password}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
      </form>
    </Modal>
  );
}
