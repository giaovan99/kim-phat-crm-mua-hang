import React, { useEffect, useState } from "react";
import { Table, Tooltip, message } from "antd";
import { MdLockReset } from "react-icons/md";
import { useSelector, useDispatch } from "react-redux";
import { setPage } from "../../../redux/slice/pageSlice";
import {
  deleteUserId,
  getListUser,
  otherKeys,
  updateUser,
} from "../../../utils/user";
import DeleteBtnItemComponent from "../../ButtonComponent/DeleteBtnItemComponent";
import RenderWithFixComponent from "../../ButtonComponent/RenderIdFix";
import FixBtnComponent from "../../ButtonComponent/FixBtnComponent";
import UndoBtnComponent from "../../ButtonComponent/UndoBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import TableLength from "../../ButtonComponent/TableLength";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListUser({
  showModal,
  showChangePasswordModal,
  loadPage,
  reload,
  setReload,
  deletedPage,
  rowSelection,
  permission,
}) {
  // 1 - Khai báo state
  const [listUser, setListUser] = useState([]);
  const [total, setTotal] = useState(0);
  const [pageSize, setPageSize] = useState(0);
  const dispatch = useDispatch();

  let page = useSelector((state) => state.pageSlice.value);
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
      getListUsers(page);
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: pageSize,
  };

  // 3 - Kéo data list user theo page
  const getListUsers = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }
    if (keywords) {
      keyParam = { ...keyParam, keywords: keywords };
    }

    // console.log(keyParam);
    let res = await getListUser(page, keyParam);
    if (res.status) {
      setListUser(res.data.data);
      setTotal(res.data.total);
      setPageSize(res.data.per_page);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Xác nhận xoá người dùng
  const confirm = (id, value) => {
    // Quy ước: Nếu value ==1 -> xoá vĩnh viễn (đưa lên BE = true), Nếu value == 0 -> Xoá tạm thời (đưa lên BE == 1)
    // console.log(e);
    deleteUser(id, value);
  };

  // 5 - Xoá người dùng
  const deleteUser = async (id, input) => {
    dispatch(setSpinner(true));
    let res;
    if (input == 1) {
      res = await deleteUserId({ id: id, deleted: true });
    } else {
      res = await deleteUserId({ id: id });
    }
    if (res) {
      // Load lại từ trang 1
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 6 - Hoàn tác xoá
  const undoDelete = async (id) => {
    dispatch(setSpinner(true));
    let res;
    res = await updateUser({ id: id, deleted: false });
    // console.log(res);
    if (res) {
      // console.log("reload trang");
      setReload(true);
    }
    dispatch(setSpinner(false));
  };

  // 7 - Kích hoạt người dùng
  const confirmUndo = (id) => {
    undoDelete(id);
  };

  // 8 - Lần đầu load trang => Kéo về danh sách user & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    // dispatch(resetKeywords());
    // loadPage();
    getListUsers(page);
  }, []);

  // 9 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      // loadPage();
      getListUsers(page);
      setReload(false);
    }
    // console.log("first");
  }, [reload]);

  // 10 - Định nghĩa các cột trong table
  const columns = [
    {
      title: "Họ tên",
      dataIndex: "name",
      key: "name",
      fixed: "left",
      render: (text, record) =>
        permission?.users?.update === 1 ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record.id);
            }}
            text={text}
            textAlign={"left"}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Tên rút gọn",
      dataIndex: "full_name",
      key: "full_name",
      width: 130,
      render: (text, record) =>
        permission?.users?.update === 1 ? (
          <RenderWithFixComponent
            onClickF={() => {
              showModal(record.id);
            }}
            text={text}
            textAlign={"left"}
          />
        ) : (
          <p>{text}</p>
        ),
    },
    {
      title: "Tên đăng nhập",
      dataIndex: "username",
      key: "username",
      width: 120,
    },
    {
      title: "Chức vụ",
      dataIndex: "position",
      key: "position",
      width: 120,
    },
    {
      title: "Bộ phận",
      dataIndex: "department_name",
      key: "department_name",
      width: 120,
    },
    {
      title: "Phân quyền",
      dataIndex: "user_group_id_name",
      key: "user_group_id_name",
      width: 120,
    },
    {
      title: "Số điện thoại",
      dataIndex: "phone",
      key: "phone",
      width: 120,
    },
    {
      title: "Email",
      dataIndex: "email",
      key: "email",
    },

    {
      title: "Trạng thái",
      dataIndex: "active",
      key: "active",
      width: 130,
      render: (record) => {
        return record == 1 ? (
          <p className="flex items-center justify-center text-green-700">
            Đang hoạt động
          </p>
        ) : (
          <p className="text-center text-red-700">Ngưng hoạt động</p>
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Đổi mật khẩu ======================= */}
            {!deletedPage && permission?.users?.update === 1 && (
              <Tooltip placement="bottom" title="Đổi mật khẩu">
                <button
                  onClick={() => {
                    showChangePasswordModal(record.id);
                  }}
                >
                  <MdLockReset className="text-bl" />
                </button>
              </Tooltip>
            )}

            {/* ======================= Chỉnh sửa ======================= */}
            {permission?.users?.update === 1 && (
              <FixBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  showModal(record.id);
                }}
              />
            )}
            {/* ======================= Undo ======================= */}
            {permission?.users?.update === 1 && (
              <UndoBtnComponent
                deleted={deletedPage}
                onClickF={() => {
                  confirmUndo(record.id);
                }}
                text={"người dùng"}
              />
            )}
            {/* ======================= Xoá ======================= */}
            {permission?.users?.delete === 1 && (
              <DeleteBtnItemComponent
                onClickF={() => {
                  if (deletedPage) {
                    confirm(record.id, 1);
                  } else {
                    confirm(record.id, 0);
                  }
                }}
                text={"người dùng"}
              />
            )}
          </div>
        );
      },
    },
  ];

  return (
    <div className="table-render-API">
      <TableLength page={page} pageSize={pageSize} total={total} />
      <Table
        {...tableProps}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        pagination={{ ...paginationObj }}
        scroll={{
          y: 600,
          x: 1500,
        }}
        columns={columns}
        dataSource={listUser}
      />
    </div>
  );
}
