import React, { useEffect } from "react";
import { Modal, Switch } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import { Select } from "antd";
import { createUser, getUser, updateUser } from "../../../utils/user";
import { normalize, showError } from "../../../utils/others";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

// Filter `option.label` match the user type `input`
const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

const addUserVali = yup.object({
  username: yup.string().required("Tên đăng nhập không để trống"),
  name: yup.string().required("Họ tên không để trống"),
  phone: yup
    .string()
    // .required("Số điện thoại không để trống")
    .matches(
      /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
      "Số điện thoại không hợp lệ"
    ),
  email: yup.string().email("Email không hợp lệ"),
  position: yup.string().required("Chức vụ không để trống"),
  // department: yup.string().required("Bộ phận không để trống"),
  user_group_id: yup.string().required("Phân quyền không để trống"),
  password: yup
    .string()
    .required("Mật khẩu không để trống")
    .matches(/^().{8,}$/, "Mật khẩu yêu cầu ít nhất 8 ký tự"),
  confirm_password: yup
    .string()
    .required("Nhập lại mật khẩu")
    .oneOf([yup.ref("password"), null], "Mật khẩu chưa trùng khớp"),
});

const editUserVali = yup.object({
  username: yup.string().required("Tên đăng nhập không để trống"),
  name: yup.string().required("Họ tên không để trống"),
  phone: yup
    .string()
    // .required("Số điện thoại không để trống")
    .matches(
      /^(?!.*[^\d+])(84|0[35789])(\d{8})\b/,
      "Số điện thoại không hợp lệ"
    ),
  email: yup.string().email("Email không hợp lệ"),
  position: yup.string().required("Chức vụ không để trống"),
  // department: yup.string().required("Bộ phận không để trống"),
  user_group_id: yup.string().required("Phân quyền không để trống"),
});

export default function UserDetail({
  isModalOpen,
  setIsModalOpen,
  user,
  listRoles,
  listDepartment,
  listPosition,
  setUser,
  setReload,
  deletedPage,
}) {
  let dispatch = useDispatch();
  // 1 - Khai báo state
  const initial = {
    name: "",
    full_name: "",
    position: "",
    department: "",
    avatar: "",
    user_group_id: "",
    email: "",
    phone: "",
    active: true,
    username: "",
    password: "",
    confirm_password: "",
  };

  // 2 - Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: user ? editUserVali : addUserVali,
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      let res;
      // console.log(values);
      if (user) {
        // console.log("sửa");
        res = await updateUser(values);
        // console.log(res);
      } else {
        res = await createUser(values, formik);
      }
      if (res) {
        setUser(null);
        resetForm();
        //  đóng modal
        setIsModalOpen(false);
        //  Load lại trang 1
        setReload(true);
      }
      dispatch(setSpinner(false));
    },
  });

  // console.log(listRoles);
  const { handleBlur, handleChange, touched, errors } = formik;

  // 3 - Lấy thông tin người dùng
  const getUserInfo = async () => {
    dispatch(setSpinner(true));
    let res = await getUser(user);
    if (res.data.success) {
      let data = res.data.data;
      formik.setValues({
        id: data.id ? data.id : "",
        name: data.name ? data.name : "",
        full_name: data.full_name ? data.full_name : "",
        position: data.position ? data.position : "",
        department: data.department ? data.department : "",
        avatar: data.avatar ? data.avatar : "",
        user_group_id: data.user_group_id ? data.user_group_id : "",
        email: data.email ? data.email : "",
        phone: data.phone ? data.phone : "",
        active: data.active ? data.active : "",
        username: data.username ? data.username : "",
      });
    } else {
      console.log(res);
      showError();
    }
    dispatch(setSpinner(false));
  };

  // 4 - Lần đầu khi load trang => check xem là add hay fix user
  useEffect(() => {
    if (user) {
      // lấy thông tin người dùng trong danh sách người dùng & show ra màn hình
      getUserInfo();
    } else {
      formik.setValues(initial);
    }
  }, [user]);

  // 5 - Khi lưu form
  const handleOk = () => {
    formik.handleSubmit();
  };
  // 6 - Reset Form
  const resetForm = () => {
    formik.resetForm();
    formik.setValues(initial);
  };

  // 7 - Khi huỷ lưu form
  const handleCancel = () => {
    setUser(null);
    resetForm();
    setIsModalOpen(false);
  };

  // 8 - Thay đổi trạng thái người dùng
  const onChangeActive = (checked) => {
    formik.setFieldValue("active", checked);
  };

  // 9 - Khi thay đổi Select
  const onChangeSelect = (value, field) => {
    // console.log(value);
    formik.setFieldValue(field, value);
  };

  // 10 - Khi search
  const onSearch = (value) => {
    // console.log("search:", value);
  };

  return (
    <Modal
      title={user ? "Chỉnh sửa người dùng" : "Thêm người dùng"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: !deletedPage
          ? "btn-main-yl inline-flex items-center justify-center"
          : "hidden",
      }}
      classNames={{ wrapper: "main-modal" }}
    >
      <form
        id="formAddUser"
        onSubmit={formik.handleSubmit}
        className={!deletedPage ? "" : "pointer-events-none"}
      >
        <div className="grid grid-cols-2 gap-2">
          {/* ======================= Họ tên, Phân quyền ======================= */}
          <div className="grid grid-cols-3 gap-5 col-span-2">
            {/* ======================= Họ tên ======================= */}
            <div className="form-group col-span-2">
              <div className="form-style">
                <label htmlFor="name">
                  Họ tên <span className="text-red-500">*</span>
                </label>
                <input
                  className="grow"
                  type="text"
                  placeholder="Nhập họ tên"
                  name="name"
                  id="name"
                  value={formik.values.name}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                />
              </div>
              {touched.name && errors.name ? (
                <p className="mt-1 text-red-500 text-sm" id="name-warning">
                  {errors.name}
                </p>
              ) : (
                <></>
              )}
            </div>
            {/* ======================= Trạng thái ======================= */}
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="active">Trạng thái</label>
                <div className="switch-type">
                  <Switch
                    onChange={onChangeActive}
                    className="w-[100px]"
                    checkedChildren="Hoạt động"
                    unCheckedChildren="Ngừng"
                    checked={formik.values.active}
                  />
                </div>
              </div>
              {touched.active && errors.active ? (
                <p className="mt-1 text-red-500 text-sm" id="active-warning">
                  {errors.active}
                </p>
              ) : (
                <></>
              )}
            </div>
          </div>
          {/* ======================= Tên rút gọn ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="full_name">Tên rút gọn</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên rút gọn"
                name="full_name"
                id="full_name"
                value={formik.values.full_name}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.full_name && errors.full_name ? (
              <p className="mt-1 text-red-500 text-sm" id="full_name-warning">
                {errors.full_name}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Số điện thoại ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="phone">Số điện thoại</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập số điện thoại"
                name="phone"
                id="phone"
                value={formik.values.phone}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.phone && errors.phone ? (
              <p className="mt-1 text-red-500 text-sm" id="phone-warning">
                {errors.phone}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Email ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="email">Email</label>
              <input
                className="grow"
                type="string"
                placeholder="Nhập email"
                name="email"
                id="email"
                value={formik.values.email}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.email && errors.email ? (
              <p className="mt-1 text-red-500 text-sm" id="email-warning">
                {errors.email}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Chức vụ ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="position">
                Chức vụ <span className="text-red-500">*</span>
              </label>
              <Select
                id="position"
                name="position"
                showSearch
                placeholder="Chọn chức vụ"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "position")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={listPosition}
                value={
                  formik.values.position != "" ? formik.values.position : null
                }
                onBlur={handleBlur}
              />
            </div>
            {touched.position && errors.position ? (
              <p className="mt-1 text-red-500 text-sm" id="position-warning">
                {errors.position}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Bộ phận ======================= */}
          <div className="form-group">
            <div className="form-style">
              <label htmlFor="department">Bộ phận</label>
              <Select
                id="department"
                name="department"
                showSearch
                placeholder="Chọn bộ phận"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "department")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={listDepartment}
                value={
                  formik.values.department != ""
                    ? formik.values.department
                    : null
                }
                onBlur={handleBlur}
              />
            </div>
            {touched.department && errors.department ? (
              <p className="mt-1 text-red-500 text-sm" id="department-warning">
                {errors.department}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Phân quyền ======================= */}
          <div className="form-group col-span-full">
            <div className="form-style">
              <label htmlFor="user_group_id">
                Phân quyền <span className="text-red-500">*</span>
              </label>
              <Select
                id="user_group_id"
                name="user_group_id"
                showSearch
                placeholder="Chọn phân quyền"
                optionFilterProp="children"
                onChange={(value) => onChangeSelect(value, "user_group_id")}
                onSearch={onSearch}
                filterOption={filterOption}
                options={listRoles}
                value={
                  formik.values.user_group_id != ""
                    ? formik.values.user_group_id
                    : null
                }
                onBlur={handleBlur}
              />
            </div>
            {touched.user_group_id && errors.user_group_id ? (
              <p
                className="mt-1 text-red-500 text-sm"
                id="user_group_id-warning"
              >
                {errors.user_group_id}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Tên đăng nhập ======================= */}
          <div className="form-group col-span-2">
            <div className="form-style">
              <label htmlFor="username">
                Tên đăng nhập <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên đăng nhập"
                name="username"
                id="username"
                value={formik.values.username}
                onChange={formik.handleChange}
                onBlur={handleBlur}
                disabled={user ? true : false}
              />
            </div>
            {touched.username && errors.username ? (
              <p className="mt-1 text-red-500 text-sm" id="username-warning">
                {errors.username}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Password ======================= */}
          {!user && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="password">
                  Mật khẩu <span className="text-red-500">*</span>
                </label>
                <input
                  type="password"
                  placeholder="Nhập mật khẩu"
                  name="password"
                  id="password"
                  value={formik.values.password}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                />
              </div>
              {touched.password && errors.password ? (
                <p className="mt-1 text-red-500 text-sm" id="password-warning">
                  {errors.password}
                </p>
              ) : (
                <></>
              )}
            </div>
          )}
          {/* ======================= Xác nhận mật khẩu ======================= */}
          {!user && (
            <div className="form-group">
              <div className="form-style">
                <label htmlFor="confirm_password">
                  Xác nhận mật khẩu <span className="text-red-500">*</span>
                </label>
                <input
                  type="password"
                  placeholder="Nhập lại mật khẩu"
                  name="confirm_password"
                  id="confirm_password"
                  value={formik.values.confirm_password}
                  onChange={formik.handleChange}
                  onBlur={handleBlur}
                />
              </div>
              {touched.confirm_password && errors.confirm_password ? (
                <p
                  className="mt-1 text-red-500 text-sm"
                  id="confirm_password-warning"
                >
                  {errors.confirm_password}
                </p>
              ) : (
                <></>
              )}
            </div>
          )}
        </div>
      </form>
    </Modal>
  );
}
