import React, { useEffect, useState } from "react";
import { Modal, Popconfirm, Table, Tooltip, message } from "antd";
import { useFormik } from "formik";
import * as yup from "yup";
import {
  createOption,
  getOptionId,
  updateOption,
} from "../../../utils/setting";
import { HiOutlinePlusCircle, HiOutlineTrash } from "react-icons/hi";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { useDispatch } from "react-redux";

export default function OptionDetails({
  isModalOpen,
  setIsModalOpen,
  advance,
  setAdvance,
  setReload,
  deletedPage,
  statusOptions,
}) {
  // 1 - Khai báo state
  let [current, setCurrent] = useState(1); //vị trí của trạng thái phiếu tạm ứng
  const dispatch = useDispatch();

  const initial = {
    Name: "",
    Field: "",
    Options: [],
  };

  // 2 - Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({
      Name: yup.string().required("Nhập tên thông số"),
      Field: yup.string().required("Nhập field"),
      Options: yup.array(),
    }),
    onSubmit: async (values) => {
      dispatch(setSpinner(true));
      let res;
      const data = values.Options.map((item) => {
        let cloneKey = item.newKey;
        delete item.newKey;
        return { ...item, key: cloneKey };
      });
      // console.log(data);
      if (!advance) {
        // console.log("API tạo");
        res = await createOption(
          {
            Field: values.Field,
            Options: JSON.stringify(data),
            Name: values.Name,
          },
          formik
        );
      } else {
        // console.log("API sửa");
        res = await updateOption(
          {
            id: values.id,
            Field: values.Field,
            Options: JSON.stringify(data),
            Name: values.Name,
          },
          formik
        );
      }
      if (res) {
        resetForm();
        setAdvance(null);
        setReload(true);
        setIsModalOpen(false);
      }
      dispatch(setSpinner(false));
    },
  });

  const { handleBlur, touched, errors } = formik;
  // console.log(formik.errors);

  // Lấy thông tin chi tiết option
  const getDetailId = async () => {
    dispatch(setSpinner(true));
    let res = await getOptionId(advance);
    if (res.status) {
      formik.setValues(res.data);
      const newData = JSON.parse(res.data.Options);
      formik.setFieldValue(
        "Options",
        newData.map((item) => {
          return { ...item, newKey: item.key };
        })
      );
    }
    dispatch(setSpinner(false));
  };

  const cancel = (e) => {
    // console.log(e);
    message.error("Đã huỷ");
  };

  // 4 - Lần đầu khi load trang => check xem là add hay fix phiếu tạm ứng
  useEffect(() => {
    // Lấy danh sách nhà cung cấp, phương thức vận chuyển, phương thức thanh toán, kho, danh sách nhân viên, danh sách sản phẩm
    if (advance) {
      // console.log("sửa ");
      // API lấy thông tin chi tiết của phiếu tạm ứng
      getDetailId();
    } else {
      // console.log("thêm đơn hàng");
      formik.setValues(initial);
    }
  }, [advance]);

  // 6 - Reset Form
  const resetForm = () => {
    setAdvance(null);
    formik.setValues(initial);
    formik.resetForm();
    setIsModalOpen(false);
  };

  // 7 - Khi lưu form
  const handleOk = () => {
    formik.handleSubmit();
  };

  // 8 - Khi cancel form
  const handleCancel = () => {
    resetForm();
  };

  //  9 - Khi current thay đổi
  useEffect(() => {
    formik.values.status = current;
  }, [current]);

  const handleAddOptions = () => {
    let arr = [...formik.values.Options];
    arr.push({
      name: "",
      key: "",
    });
    formik.setFieldValue("Options", arr);
  };

  const columns = [
    {
      title: "Tên lựa chọn",
      dataIndex: "name",
      key: "name",
      render: (text, record, index) => (
        <div className="form-group">
          <div className="form-style">
            <input
              className="grow"
              type="text"
              placeholder="Nhập tên"
              // name="name"
              value={text}
              onChange={(event) => {
                let arr = [...formik.values.Options];
                arr[index].name = event.target.value;
                formik.setFieldValue("Options", [...arr]);
              }}
              // onBlur={handleBlur}
            />
          </div>
        </div>
      ),
    },
    {
      title: "Key",
      dataIndex: "newKey",
      key: "key",
      render: (text, record, index) => (
        <div className="form-group">
          <div className="form-style">
            <input
              className="grow"
              type="text"
              placeholder="Nhập key"
              // name="key"
              value={text}
              onChange={(event) => {
                let arr = [...formik.values.Options];
                arr[index].newKey = event.target.value;
                // console.log(arr);
                formik.setFieldValue("Options", [...arr]);
              }}
              // onBlur={handleBlur}
            />
          </div>
        </div>
      ),
    },

    {
      title: (
        <p className="flex gap-5 items-center">
          <span>Hành động</span>
          <Tooltip placement="bottom" title="Thêm lựa chọn">
            <button onClick={handleAddOptions} type="button">
              <HiOutlinePlusCircle />
            </button>
          </Tooltip>
        </p>
      ),
      key: "action",
      width: 150,
      fixed: "right",
      render: (text, record, index) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Xoá ======================= */}
            <Popconfirm
              placement="topRight"
              title="Xoá "
              description="Bạn chắc chắn muốn xoá?"
              onConfirm={() => {
                let arr = [...formik.values.Options];
                arr.splice(index, 1);
                formik.setFieldValue("Options", arr);
              }}
              onCancel={cancel}
              okText="Xoá"
              cancelText="Huỷ"
              okButtonProps={{ className: "btn-main-yl" }}
              className="popover-danger"
            >
              <Tooltip placement="bottom" title="Xoá">
                <button type="button">
                  <HiOutlineTrash className="text-rose-600" />
                </button>
              </Tooltip>
            </Popconfirm>
          </div>
        );
      },
    },
  ];

  return (
    <Modal
      title={advance ? "Chỉnh sửa thông số" : "Tạo thông số"}
      centered
      open={isModalOpen}
      onOk={handleOk}
      onCancel={handleCancel}
      okText="Lưu"
      cancelText="Huỷ"
      okButtonProps={{
        className: !deletedPage
          ? "btn-main-yl inline-flex items-center justify-center"
          : "hidden",
      }}
      classNames={{ wrapper: "main-modal " }}
    >
      <form
        id="formAddSupplier"
        onSubmit={formik.handleSubmit}
        className={!deletedPage ? "" : "pointer-events-none"}
      >
        <div className="grid grid-cols-2 gap-2">
          {/* ======================= Tên ======================= */}
          <div className="form-group ">
            <div className="form-style">
              <label htmlFor="Name">
                Tên <span className="text-red-500">*</span>
              </label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập tên"
                name="Name"
                id="Name"
                value={formik.values.Name}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.Name && errors.Name ? (
              <p className="mt-1 text-red-500 text-sm" id="Name-warning">
                {errors.Name}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Field ======================= */}
          <div className="form-group ">
            <div className="form-style">
              <label htmlFor="Field">Field</label>
              <input
                className="grow"
                type="text"
                placeholder="Nhập field"
                name="Field"
                id="Field"
                value={formik.values.Field}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
            {touched.Field && errors.Field ? (
              <p className="mt-1 text-red-500 text-sm" id="Field-warning">
                {errors.Field}
              </p>
            ) : (
              <></>
            )}
          </div>
          {/* ======================= Options ======================= */}
          <div className="col-span-2">
            <label htmlFor="Option" className="font-medium">
              Lựa chọn <span className="text-red-500">*</span>
            </label>
            <div className="flex justify-end mb-2"></div>
            <Table
              bordered
              dataSource={formik.values.Options}
              columns={columns}
              scroll={{
                y: 600,
              }}
              pagination={{
                position: ["none", "bottomCenter"],
                pageSize: 20,
                hideOnSinglePage: true,
              }}
            />
            {touched.Options && errors.Options ? (
              <p className="mt-1 text-red-500 text-sm" id="Options-warning">
                {errors.Options}
              </p>
            ) : (
              <></>
            )}
          </div>
        </div>
      </form>
    </Modal>
  );
}
