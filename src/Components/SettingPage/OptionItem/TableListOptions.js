import React, { useEffect, useState } from "react";
import { Select, Table } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getOptions } from "../../../utils/setting";
import { resetPage, setPage } from "../../../redux/slice/pageSlice";
import { otherKeys } from "../../../utils/user";
import RenderWithFixComponent from "../../ButtonComponent/RenderIdFix";
import FixBtnComponent from "../../ButtonComponent/FixBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";

const tableProps = {
  bordered: true,
  size: "small",
  rowKey: "id",
};

export default function TableListOptions({
  showModal,
  reload,
  setReload,
  deletedPage,
  rowSelection,
}) {
  // 1 - Khai báo state
  const [listData, setListData] = useState([]);
  const [total, setTotal] = useState(0);
  const dispatch = useDispatch();
  let page = useSelector((state) => state.pageSlice.value);
  let keywords = useSelector((state) => state.keywordsSlice.keywords);

  // 2 - Set up paginations
  const paginationObj = {
    position: ["none", "bottomCenter"],
    current: page,
    onChange: (page) => {
      dispatch(setPage(page));
    },
    hideOnSinglePage: true,
    total: total,
    pageSize: total,
  };

  // 3 - Kéo data list thông số hệ thống theo page
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (deletedPage) {
      keyParam = await otherKeys(["deleted"]); // hàm chuyển đổi các key cần truyền thành object
    }
    if (keyParam || keywords) {
      keyParam = { ...keyParam, keywords };
    }

    let res = await getOptions(page, keyParam);
    if (res.status) {
      setListData(res.data);
      setTotal(res.data.length);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Lần đầu load trang => Kéo về danh sách kho & page = 1 & get total => khi pagination thay đổi thì change Page => kéo api lại
  useEffect(() => {
    dispatch(resetPage());
    getListData(page);
  }, []);

  // 5 - Khi reload == true thì sẽ load lại trang
  useEffect(() => {
    if (reload) {
      getListData(page);
      setReload(false);
    }
  }, [reload]);

  // 8 - Định nghĩa các cột trong table
  const columns = [
    {
      title: "Tên",
      dataIndex: "Name",
      key: "Name",
      fixed: "left",
      render: (text, record) => (
        <RenderWithFixComponent
          onClickF={() => {
            showModal(record.id);
          }}
          text={text}
        />
      ),
    },
    {
      title: "Field",
      dataIndex: "Field",
      key: "Field",
    },
    {
      title: "Lựa chọn",
      dataIndex: "Options",
      key: "Options",
      render: (text, record) => {
        let recordArr = JSON.parse(text);
        //  Chỉ lấy cặp key value của name
        let newArr = recordArr.map(({ name }) => ({
          name,
        }));
        return (
          <Select
            defaultValue={newArr.length > 0 ? newArr[0].name : ""}
            className="w-full"
            options={newArr}
            fieldNames={{ label: "name", value: "name" }}
          ></Select>
        );
      },
    },
    {
      title: "Hành động",
      key: "action",
      width: 120,
      fixed: "right",
      render: (record) => {
        return (
          <div className="flex gap-2 justify-center">
            {/* ======================= Chỉnh sửa ======================= */}
            <FixBtnComponent
              onClickF={() => {
                showModal(record.id);
              }}
            />
            {/* ======================= Xoá =======================
            <DeleteBtnItemComponent
              onClickF={() => {
                if (deletedPage) {
                  confirm(record.id, 1);
                } else {
                  confirm(record.id, 0);
                }
              }}
              text={"thông số"}
            /> */}
          </div>
        );
      },
    },
  ];
  return (
    <div className="table-render-API">
      <Table
        {...tableProps}
        rowSelection={{
          type: "checkbox",
          ...rowSelection,
        }}
        pagination={{
          ...paginationObj,
        }}
        scroll={{
          y: 600,
          x: 250,
        }}
        columns={columns}
        dataSource={listData}
      />
    </div>
  );
}
