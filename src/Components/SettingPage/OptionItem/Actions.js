import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch, HiOutlinePlusCircle } from "react-icons/hi";
import { useDispatch } from "react-redux";
import AddBtnComponent from "../../ButtonComponent/AddBtnComponent";
import DropdownOptions from "../../DropdownOptions/DropdownOptions";
import { setKeywords } from "../../../redux/slice/keywordsSlice";
export default function Actions({
  showModal,
  deletedPage,
  setReload,
  keySelected,
  setKeySelected,
}) {
  let dispatch = useDispatch();

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: async (values) => {
      // console.log(values);
      dispatch(setKeywords(values.search));
      setReload(true);
    },
  });
  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div className="flex gap-1">
            <DropdownOptions
              arrId={keySelected}
              deletedPage={deletedPage}
              page={"optionItem"}
              setReload={setReload}
              setKeySelected={setKeySelected}
            />
            <div className="form-group">
              <div className="form-style">
                <input
                  id="search"
                  name="search"
                  type="text"
                  placeholder="Tìm tên thông số"
                  value={formik.values.search}
                  onChange={formik.handleChange}
                />
                <button className="btn-main-yl btn-actions">
                  <HiOutlineSearch />
                  Tìm kiếm
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
      {/* ======================= btns ======================= */}
      {!deletedPage && (
        <div className="flex gap-3">
          <AddBtnComponent
            onClickF={() => showModal(null)}
            text={"Thêm thông số"}
          />
        </div>
      )}
    </div>
  );
}
