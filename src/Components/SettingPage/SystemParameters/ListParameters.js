import React, { useEffect, useState } from "react";
import { BiUpload } from "react-icons/bi";
import { Image, Select } from "antd";
import { Field, FormikProvider, useFormik } from "formik";
import * as yup from "yup";
import { IoSaveOutline } from "react-icons/io5";
import { getSetting, updateSetting } from "../../../utils/setting";
import { useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { setSpinner } from "../../../redux/slice/spinnerSlice";

export default function ListParameters() {
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  let [setting, setSetting] = useState();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  // Khai báo formik
  const formik = useFormik({
    initialValues: {},
    validationSchema: yup.object({}),
    onSubmit: async (values) => {
      let res = await updateSetting(convertValues(values)); // alert thông báo
      if (res) {
        navigate(0);
      }
    },
  });

  const convertValues = (values) => {
    let result = Object.entries(values).map(([key, value]) => ({
      id: parseInt(key),
      value: value ? value : "",
    }));
    return result;
  };

  const getSettingInfo = async () => {
    dispatch(setSpinner(true));
    let res = await getSetting();
    if (res.status) {
      setSetting(res.data);
      res.data.map((item) => {
        // console.log(item);
        formik.setFieldValue(
          item.id.toString(),
          item.type === "image" ? item.url : item.value
        );
      });
    }
    dispatch(setSpinner(false));
  };

  // Lần đầu load trang lấy thông tin cài đặt
  useEffect(() => {
    // lấy thông tin cài đặt
    getSettingInfo();
  }, []);

  const renderSetting = () => {
    return setting.map((item) => {
      if (item.type === "image") {
        let key = item.id;
        // let url = item.url;
        return (
          <div className="form-group mb-7" key={item.id}>
            <div className="form-style">
              <label className="mb-3">{item.name}</label>
              <div className="flex gap-3 items-center flex-col">
                <Image
                  className="overflow-hidden object-cover object-center"
                  width={180}
                  height={180}
                  src={
                    typeof formik.values[key] == "object" && formik.values[key]
                      ? URL.createObjectURL(formik.values[key])
                      : formik.values[key]
                  }
                />
                {permission?.config?.update === 1 && (
                  <label
                    htmlFor={key}
                    className="flex gap-3 items-center btn-main-yl"
                  >
                    <BiUpload /> Tải ảnh lên
                  </label>
                )}
              </div>
              <input
                id={key}
                name={key}
                type="file"
                accept="image/*"
                className="hidden"
                onChange={(event) => {
                  formik.setFieldValue(key, event.target.files[0]);
                }}
              />
            </div>
          </div>
        );
      } else if (item.type === "text") {
        return (
          <div className="form-group" key={item.id}>
            <div className="form-style">
              <label htmlFor="copyrightText">{item.name}</label>
              <input
                className="grow"
                type="text"
                placeholder={item.name}
                name={item.id}
                value={formik.values?.[item.id]}
                onChange={formik.handleChange}
                onBlur={handleBlur}
              />
            </div>
          </div>
        );
      } else {
        return (
          <div className="form-group" key={item.id}>
            <div className="form-style">
              <label htmlFor="timeout">{item.name}</label>
              <Field name={item.id}>
                {({ field }) => {
                  let arrOptions;
                  try {
                    arrOptions = Object.entries(JSON.parse(item?.options));
                  } catch (error) {
                    console.error("Error parsing options:", error);
                    arrOptions = [];
                  }
                  if (arrOptions.length > 0) {
                    return (
                      <Select
                        {...field}
                        showSearch
                        placeholder="Chọn"
                        onChange={(value) => {
                          formik.setFieldValue(item.id, value);
                        }}
                        options={arrOptions.map(([key, value]) => ({
                          value: key,
                          label: value,
                        }))}
                        value={
                          formik.values[item.id] != ""
                            ? formik.values[item.id]
                            : null
                        }
                      />
                    );
                  }
                }}
              </Field>
            </div>
          </div>
        );
      }
    });
  };

  const { handleBlur } = formik;
  return (
    <div className="listParameters">
      {setting && (
        <FormikProvider value={formik}>
          <div className="grid grid-cols-2 xl:grid-cols-4 gap-2">
            {renderSetting()}
          </div>
        </FormikProvider>
      )}

      {/* Btns */}
      {permission?.config?.update === 1 && (
        <div className="mt-5 flex gap-3">
          <button
            className="btn-actions btn-main-yl"
            type="button"
            onClick={(e) => {
              e.preventDefault();
              formik.handleSubmit();
            }}
          >
            <IoSaveOutline />
            Cập nhật
          </button>
        </div>
      )}
    </div>
  );
}
