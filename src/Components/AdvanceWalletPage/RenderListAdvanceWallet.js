import React, { useEffect, useState } from "react";
import CountUp from "react-countup";
import { Card, Statistic } from "antd";
import { useDispatch, useSelector } from "react-redux";
import { getAdvanceWalletList } from "../../utils/advance";
import { setSpinner } from "../../redux/slice/spinnerSlice";
export default function RenderListAdvanceWallet({ reload, setReload }) {
  const [listData, setListData] = useState([]);
  let keywords = useSelector((state) => state.keywordsSlice.keywords);
  let dispatch = useDispatch();

  // 3 - Kéo data list data
  const getListData = async (page) => {
    dispatch(setSpinner(true));
    // Check xem cần truyền lên APIs key deleted không
    let keyParam = {};
    if (keywords) {
      keyParam = { keywords: keywords };
    }
    // console.log(keyParam);
    let res = await getAdvanceWalletList(page, keyParam);
    if (res.status) {
      setListData(res.data);
    }
    dispatch(setSpinner(false));
  };
  const formatter = (value) => (
    <CountUp end={value} separator="," className="text-[30px]" />
  );

  let renderWallet = () => {
    return listData.map((item, index) => (
      <div className="walletItem" key={index}>
        <Card bordered={false}>
          <Statistic
            title={item.full_name}
            value={item.advance_wallet}
            formatter={formatter}
            titleFontSize={"20px"}
          />
        </Card>
      </div>
    ));
  };

  useEffect(() => {
    if (reload) {
      getListData();
      setReload(false);
    }
  }, [reload]);

  useEffect(() => {
    getListData();
  }, []);

  // console.log(data);
  return (
    <div className="renderListAdvanceWallet">
      <div className="grid grid-cols-2 xl:grid-cols-3 gap-5">
        {listData.length > 0 && renderWallet()}
      </div>
    </div>
  );
}
