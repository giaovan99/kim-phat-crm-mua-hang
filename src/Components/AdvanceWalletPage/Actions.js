import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
import { useDispatch } from "react-redux";
import { setKeywords } from "../../redux/slice/keywordsSlice";
export default function Actions({ setReload }) {
  let dispatch = useDispatch();

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      search: "",
    },
    onSubmit: async (values) => {
      dispatch(setKeywords(values.search));
      setReload(true);
    },
  });
  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div className="form-group">
            <div className="form-style">
              <input
                id="search"
                name="search"
                type="text"
                placeholder="Tìm tên nhân viên"
                value={formik.values.search}
                onChange={formik.handleChange}
              />
              <button className="btn-main-yl btn-actions">
                <HiOutlineSearch />
                Tìm kiếm
              </button>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
