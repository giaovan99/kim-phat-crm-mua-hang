import { Button, Popconfirm, Steps, message } from "antd";
import React, { useState, useEffect } from "react";
import { useLocation } from "react-router-dom";

import { ExclamationCircleOutlined } from "@ant-design/icons";
import { LuUndo } from "react-icons/lu";
import { MdDone } from "react-icons/md";
import { updatePurchaseRequest } from "../../utils/purchaseRequest";
import { updateImportWarehouse } from "../../utils/warehouse";
import { updateReimbursementSlip } from "../../utils/reimbursement";
import { useSelector } from "react-redux";

export default function SetStatus2Step({
  statusOptions,
  current,
  setCurrent,
  now = null,
  page,
  deletedPage,
  file = null,
  setReload = null,
  permissionUpdate = null,
}) {
  // console.log(file);
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  const location = useLocation();
  // Lấy phần path từ URL
  const pathName = location.pathname;

  const [fix, setFix] = useState(false);
  const confirmNext = (e) => {
    // console.log(e);
    next();
  };
  const confirmPrev = (e) => {
    // console.log(e);
    prev();
  };
  const cancel = (e) => {
    // console.log(e);
    message.error("Huỷ");
  };

  const next = async () => {
    let res = true;
    let step = current;
    let checkFile = true;
    // TH cần check có file mới cho đổi trạng thái phiếu
    // console.log(file);
    if (file) {
      file.forEach((item) => {
        if (item.key === "paymentRequestFile" && item.fileName) {
          document.getElementById("warning").innerText = "";
          step = current + 1;
        } else if (item.key === "paymentRequestFile" && !item.fileName) {
          document.getElementById("warning").innerText =
            "Files đính kèm chưa đủ, kiểm tra lại";
          checkFile = false;
        }
      });
    } else {
      step = current + 1;
      document.getElementById("warning").innerText = "";
    }
    // Nếu như now = true (cập nhật API ngay lập tức thì chạy API update status)
    if (now && checkFile) {
      switch (page) {
        case "purchaseProposal": {
          res = await updatePurchaseRequest({ id: now, status: step });
          break;
        }
        case "importWarehouse": {
          res = await updateImportWarehouse({ id: now, status: step });
          break;
        }
        case "reimbursement": {
          res = await updateReimbursementSlip({ id: now, status: step });
          break;
        }
        default: {
          console.log("chưa xét trường hợp");
          break;
        }
      }
    }
    if (res) {
      setCurrent(step);
      if (setReload) {
        setReload(true);
      }
    }
  };

  useEffect(() => {
    document.getElementById("warning").innerText = "";
  }, [file]);

  const prev = async () => {
    let res = true;
    if (current - 1 < 1 || current == statusOptions.length) {
      document.getElementById("warning").innerText =
        "Không thể hoàn tác xuống trạng thái thấp hơn ";
      return;
    } else {
      let step = current - 1;
      if (now) {
        switch (page) {
          case "purchaseProposal": {
            res = await updatePurchaseRequest({ id: now, status: step });
            break;
          }
          case "importWarehouse": {
            res = await updateImportWarehouse({ id: now, status: step });
            break;
          }
          case "reimbursement": {
            res = await updateReimbursementSlip({ id: now, status: step });
            break;
          }
          default: {
            console.log("chưa xét trường hợp");
            break;
          }
        }
      }
      if (res) {
        document.getElementById("warning").innerText = "";
        setCurrent(step);
        if (setReload) {
          setReload(true);
        }
      }
    }
  };

  useEffect(() => {
    if (permission) {
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      // console.log(pathSegments[1]);
      // Check xem có quyền chỉnh sửa hay ko mới hiển thị nút update trạng thái
      if (permission[pathSegments[1]].update) {
        setFix(true);
      }
    }
  }, [permission, pathName]);

  return (
    <div className="flex flex-col items-center justify-center ">
      {/* ======================= Btns ======================= */}
      <div className="flex gap-3">
        {/* ======================= Hoàn tác ======================= */}
        {current > 0 && fix && !deletedPage && permissionUpdate && (
          <Popconfirm
            placement="topRight"
            title="Hoàn tác trạng thái"
            description="Xác nhận hoàn tác trạng thái này?"
            onConfirm={confirmPrev}
            onCancel={cancel}
            okText="Xác nhận"
            cancelText="Huỷ"
            okButtonProps={{ className: "btn-main-yl" }}
            className="btn-undo shrink-0"
            icon={
              <ExclamationCircleOutlined className="warning-icon-popconfirm" />
            }
          >
            <Button>
              <LuUndo className="text-white" />
            </Button>
          </Popconfirm>
        )}
        <div className="shrink">
          {/* ======================= Steps ======================= */}
          <Steps
            current={current}
            items={statusOptions}
            labelPlacement="vertical"
            size="small"
          />
        </div>
        {/* ======================= Hoàn thành ======================= */}
        {current < statusOptions?.length &&
          fix &&
          !deletedPage &&
          permissionUpdate && (
            <Popconfirm
              placement="topRight"
              title="Hoàn thành trạng thái"
              description="Xác nhận hoàn thành trạng thái này?"
              onConfirm={confirmNext}
              onCancel={cancel}
              okText="Xác nhận"
              cancelText="Huỷ"
              okButtonProps={{ className: "btn-main-yl" }}
              className="btn-success shrink-0"
              icon={
                <ExclamationCircleOutlined className="warning-icon-popconfirm" />
              }
            >
              <Button>
                <MdDone className="text-white" />
              </Button>
            </Popconfirm>
          )}
      </div>
      <p id="warning" className="text-red-600"></p>
    </div>
  );
}
