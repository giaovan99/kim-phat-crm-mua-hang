import { Tooltip, message } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { HiOutlinePencilSquare } from "react-icons/hi2";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

export default function FixBtnComponent({ deleted = false, onClickF }) {
  let [check, setCheck] = useState(false);
  const location = useLocation();
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  useEffect(() => {
    if (permission) {
      const pathName = location.pathname;
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      let key = pathSegments[1];
      if (permission[key] && permission[key]?.update) {
        setCheck(true);
      } else {
        setCheck(false);
      }
    }
  }, [permission]);

  return (
    <>
      {/* Check == true thì mới hiển thị */}
      {!deleted && check && (
        <Tooltip placement="bottom" title="Chỉnh sửa">
          <button onClick={onClickF}>
            <HiOutlinePencilSquare className="text-teal-700" />
          </button>
        </Tooltip>
      )}
    </>
  );
}
