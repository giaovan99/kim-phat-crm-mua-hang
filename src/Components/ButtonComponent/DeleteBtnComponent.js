import { Tooltip } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { PiListBulletsFill, PiTrashBold } from "react-icons/pi";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

export default function DeleteBtnComponent({
  deleted,
  onClickF,
  text1,
  text2,
}) {
  let [check, setCheck] = useState(false);
  const location = useLocation();
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  useEffect(() => {
    if (permission) {
      const pathName = location.pathname;
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      let key = pathSegments[1];
      if (permission[key] && permission[key]?.read) {
        setCheck(true);
      } else {
        setCheck(false);
      }
    }
  }, [permission]);

  return (
    <>
      {/* Check == true thì mới hiển thị */}

      {check && !deleted ? (
        <div className="icon-actions space-x-2">
          <Tooltip placement="bottom" title={text1}>
            <button onClick={onClickF}>
              <PiTrashBold className="text-red-500" />
            </button>
          </Tooltip>
        </div>
      ) : (
        <div className="icon-actions space-x-2">
          <Tooltip placement="bottom" title={text2}>
            <button onClick={onClickF}>
              <PiListBulletsFill className="text-blue-700" />
            </button>
          </Tooltip>
        </div>
      )}
    </>
  );
}
