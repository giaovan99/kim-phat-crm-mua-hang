import React, { useState } from "react";
import { useEffect } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

export default function RenderWithFixComponent({
  text,
  onClickF,
  textAlign = null,
}) {
  let [check, setCheck] = useState(false);
  const location = useLocation();
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  useEffect(() => {
    if (permission) {
      const pathName = location.pathname;
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      let key = pathSegments[1];
      if (permission[key] && permission[key]?.update) {
        setCheck(true);
      } else {
        setCheck(false);
      }
    }
  }, [permission]);

  return (
    <>
      {check ? (
        <p
          className={
            textAlign
              ? "text-blue-500 cursor-pointer hover:text-blue-400"
              : "text-blue-500 cursor-pointer hover:text-blue-400 text-center"
          }
          onClick={onClickF}
        >
          {text}
        </p>
      ) : (
        <p
          className={
            textAlign ? "text-blue-500  " : "text-blue-500   text-center"
          }
        >
          {text}
        </p>
      )}
    </>
  );
}
