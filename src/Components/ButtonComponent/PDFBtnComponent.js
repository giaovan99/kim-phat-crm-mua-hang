import { Popconfirm, Tooltip, message } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { HiOutlinePlusCircle, HiOutlineTrash } from "react-icons/hi";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import PrintAM from "../ExportToPDF/AdvanceMoneyPDF/PrintAM";
import ReactToPrint from "react-to-print";

export default function PDFBtnComponent({
  deleted,
  title,
  onAfterPrint,
  onBeforeGetContent,
  onBeforePrint,
  trigger,
  componentRef,
  details,
  page,
}) {
  let [check, setCheck] = useState(false);
  const location = useLocation();
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  useEffect(() => {
    if (permission) {
      const pathName = location.pathname;
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      let key = pathSegments[1];
      if (permission[key] && permission[key]?.read) {
        setCheck(true);
      } else {
        setCheck(false);
      }
    }
  }, [permission]);

  const cancel = (e) => {
    // console.log(e);
    message.error("Đã huỷ");
  };
  const printPage = () => {
    switch (page) {
      case "PrintAM":
        return (
          <div className="hidden">
            <PrintAM forwardedRef={componentRef} details={details} />
          </div>
        );
    }
  };

  return (
    <>
      {/* Check == true thì mới hiển thị */}
      {/* {check && (
        
      )} */}
      {!deleted && check && (
        <Tooltip placement="bottom" title="Xuất file PDF phiếu đã chọn">
          <div>
            <ReactToPrint
              content={() => componentRef.current}
              documentTitle={title}
              onAfterPrint={onAfterPrint}
              onBeforeGetContent={onBeforeGetContent}
              onBeforePrint={onBeforePrint}
              removeAfterPrint
              trigger={trigger}
            />
            {page && printPage()}
          </div>
        </Tooltip>
      )}
    </>
  );
}
