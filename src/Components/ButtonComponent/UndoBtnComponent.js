import { Popconfirm, Tooltip, message } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { HiMiniArrowUturnLeft } from "react-icons/hi2";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

export default function UndoBtnComponent({ deleted, onClickF, text }) {
  const cancel = (e) => {
    // console.log(e);
    message.error("Đã huỷ");
  };
  let [check, setCheck] = useState(false);
  const location = useLocation();
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  useEffect(() => {
    if (permission) {
      const pathName = location.pathname;
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      let key = pathSegments[1];
      if (permission[key] && permission[key]?.delete) {
        setCheck(true);
      } else {
        setCheck(false);
      }
    }
  }, [permission]);

  return (
    <>
      {/* Check == true thì mới hiển thị */}
      {/* {check && (
        
      )} */}
      {deleted && check && (
        <Popconfirm
          placement="topRight"
          title={"Kích hoạt " + text}
          description={"Bạn chắc chắn muốn kích hoạt " + text + " này?"}
          onConfirm={onClickF}
          onCancel={cancel}
          okText="Kích hoạt"
          cancelText="Huỷ"
          okButtonProps={{ className: "btn-main-yl" }}
          className="popover-danger"
        >
          <Tooltip placement="bottom" title="Kích hoạt">
            <button>
              <HiMiniArrowUturnLeft className="text-blue-700" />
            </button>
          </Tooltip>
        </Popconfirm>
      )}
    </>
  );
}
