import React, { useState } from "react";
import { useEffect } from "react";
import { HiOutlinePlusCircle } from "react-icons/hi";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

export default function AddBtnComponent({ onClickF, text }) {
  let [check, setCheck] = useState(false);
  const location = useLocation();
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  useEffect(() => {
    if (permission) {
      // Lấy phần path từ URL
      const pathName = location.pathname;
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      let key = pathSegments[1];
      if (permission[key] && permission[key]?.create) {
        setCheck(true);
      } else {
        setCheck(false);
      }
    }
  }, [permission]);

  return (
    <>
      {/* Check == true thì mới hiển thị */}
      {check && (
        <button className="btn-actions btn-main-yl" onClick={onClickF}>
          <HiOutlinePlusCircle />
          {text}
        </button>
      )}
    </>
  );
}
