import { Tooltip } from "antd";
import React from "react";
import { BiSolidFileExport } from "react-icons/bi";

const ExportDetailsBtnComponent = ({ text, onClickF }) => {
  return (
    <>
      <Tooltip placement="bottom" title={text}>
        <button onClick={onClickF}>
          <BiSolidFileExport className="text-green-700" />
        </button>
      </Tooltip>
    </>
  );
};

export default ExportDetailsBtnComponent;
