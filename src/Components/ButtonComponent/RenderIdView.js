import React from "react";

export default function RenderWithViewComponent({
  text,
  onClickF,
  textAlign = null,
}) {
  return (
    <>
      <p
        className={
          textAlign
            ? "text-blue-500 cursor-pointer hover:text-blue-400"
            : "text-blue-500 cursor-pointer hover:text-blue-400 text-center"
        }
        onClick={onClickF}
      >
        {text}
      </p>
    </>
  );
}
