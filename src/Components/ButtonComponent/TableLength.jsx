import React from "react";

const TableLength = ({ page, pageSize, total }) => {
  return (
    <p className="text-gray-500 text-right">
      Hiển thị {(page - 1) * pageSize + 1} đến{" "}
      {Math.min(page * pageSize, total)} trong số {total} mục
    </p>
  );
};

export default TableLength;
