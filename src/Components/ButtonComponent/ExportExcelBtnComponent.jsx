import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";
import { BiSolidFileExport } from "react-icons/bi";
import { Tooltip } from "antd";

const ExportExcelBtnComponent = ({ deleted, onClickF, text }) => {
  let [check, setCheck] = useState(false);
  const location = useLocation();
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  useEffect(() => {
    if (permission) {
      const pathName = location.pathname;
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      let key = pathSegments[1];
      if (permission[key] && permission[key]?.create) {
        setCheck(true);
      } else {
        setCheck(false);
      }
    }
  }, [permission]);
  return (
    <>
      {!deleted && check && (
        <div className="icon-actions space-x-2">
          <Tooltip placement="bottom" title={text}>
            <button onClick={onClickF}>
              <BiSolidFileExport className="text-green-700" />
            </button>
          </Tooltip>
        </div>
      )}
    </>
  );
};

export default ExportExcelBtnComponent;
