import { Popconfirm, Tooltip, message } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { HiOutlinePlusCircle, HiOutlineTrash } from "react-icons/hi";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

export default function DeleteBtnItemComponent({ onClickF, text }) {
  let [check, setCheck] = useState(false);
  const location = useLocation();
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  useEffect(() => {
    if (permission) {
      const pathName = location.pathname;
      // Tách đường dẫn bằng dấu gạch chéo
      const pathSegments = pathName.split("/");
      let key = pathSegments[1];
      if (permission[key] && permission[key]?.delete) {
        setCheck(true);
      } else {
        setCheck(false);
      }
    }
  }, [permission]);

  const cancel = (e) => {
    // console.log(e);
    message.error("Đã huỷ");
  };

  return (
    <>
      {/* Check == true thì mới hiển thị */}
      {check && (
        <Popconfirm
          placement="topRight"
          title={"Xoá " + text}
          description={"Bạn chắc chắn muốn xoá " + text + " này?"}
          onConfirm={onClickF}
          onCancel={cancel}
          okText="Xoá"
          cancelText="Huỷ"
          okButtonProps={{ className: "btn-main-yl" }}
          className="popover-danger"
        >
          <Tooltip placement="bottom" title="Xoá">
            <button>
              <HiOutlineTrash className="text-rose-600" />
            </button>
          </Tooltip>
        </Popconfirm>
      )}
    </>
  );
}
