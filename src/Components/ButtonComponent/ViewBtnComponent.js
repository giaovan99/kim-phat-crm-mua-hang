import { Tooltip } from "antd";
import React, { useState } from "react";
import { useEffect } from "react";
import { HiOutlineEye } from "react-icons/hi2";
import { useSelector } from "react-redux";
import { useLocation } from "react-router-dom";

export default function ViewBtnComponent({ onClickF, text }) {
  // let [check, setCheck] = useState(false);
  // const location = useLocation();
  // let permission = useSelector(
  //   (state) => state.loginInfoSlice.loginInfo.permission
  // );
  // useEffect(() => {
  //   if (permission) {
  //     let key = location.pathname.replace("/", "");
  //     if (permission[key] && permission[key]?.read) {
  //       setCheck(true);
  //     } else {
  //       setCheck(false);
  //     }
  //   }
  // }, [permission]);

  return (
    <>
      <Tooltip placement="bottom" title={text}>
        <button onClick={onClickF}>
          <HiOutlineEye className="text-bl" />
        </button>
      </Tooltip>
    </>
  );
}
