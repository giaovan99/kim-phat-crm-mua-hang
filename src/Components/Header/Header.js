import React, { useEffect, useState } from "react";
import { MenuFoldOutlined, MenuUnfoldOutlined } from "@ant-design/icons";
import { Layout, Button, Dropdown, Space } from "antd";
import {
  IoSettingsOutline,
  IoPersonOutline,
  IoLogOutOutline,
} from "react-icons/io5";
import "./header.scss";
import { Link } from "react-router-dom";

import { logout } from "../../utils/auth";
import { useDispatch, useSelector } from "react-redux";
import { getProfile } from "../../utils/userProfile";
import { setInfo } from "../../redux/slice/userProfileSlice";

const { Header } = Layout;

export default function HeaderLayout({ collapsed, setCollapsed }) {
  let [avatar, setAvatar] = useState("./img/imgNotFound/not-found.jpeg");
  let profileUser = useSelector((state) => state.userProfileSlice.userInfo);
  let dispatch = useDispatch();

  // Danh sách các list
  const items = [
    {
      key: "1",
      label: (
        <Link to="/profile" className="flex items-center gap-2">
          <IoPersonOutline /> Trang cá nhân
        </Link>
      ),
    },
    {
      key: "2",
      label: (
        <Link className="flex items-center gap-2" onClick={logout}>
          <IoLogOutOutline /> Đăng xuất
        </Link>
      ),
    },
  ];

  let checkAva = () => {
    if (profileUser) {
      if (profileUser.avatar) {
        setAvatar(profileUser.avatar);
      } else {
        setAvatar("./img/imgNotFound/not-found.jpeg");
      }
    }
  };

  useEffect(() => {
    checkAva();
  }, [profileUser]);

  return (
    <section className="section section-header">
      <Header style={{ padding: 0, background: "#fff" }}>
        <div className="flex justify-between">
          <Button
            type="text"
            icon={collapsed ? <MenuUnfoldOutlined /> : <MenuFoldOutlined />}
            onClick={() => setCollapsed(!collapsed)}
            style={{
              fontSize: "16px",
              width: 64,
              height: 64,
            }}
          />
          <Space
            direction="vertical"
            className="mr-5 flex items-center justify-center"
          >
            <Dropdown
              menu={{
                items,
              }}
              placement="bottomRight"
            >
              <Button className="flex gap-3 items-center justify-center">
                <img
                  src={avatar}
                  width={24}
                  height={24}
                  className="rounded-full"
                />
                <IoSettingsOutline />
              </Button>
            </Dropdown>
          </Space>
        </div>
      </Header>
    </section>
  );
}
