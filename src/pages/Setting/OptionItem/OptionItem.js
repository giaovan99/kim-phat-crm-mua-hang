import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { resetKeywords } from "../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../redux/slice/pageSlice";
import Actions from "../../../Components/SettingPage/OptionItem/Actions";
import TableListOptions from "../../../Components/SettingPage/OptionItem/TableListOptions";
import OptionDetails from "../../../Components/SettingPage/OptionItem/OptionDetails";

export default function OptionItem() {
  // 1 - Khai báo state
  const [isModalOpen, setIsModalOpen] = useState();
  const [advanceChoosen, setAdvanceChoosen] = useState(null);
  const [listStatus, setListStatus] = useState([]); //Danh sách phân quyền
  const dispatch = useDispatch();
  const [deletedPage, setDeletedPage] = useState(false); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const [keySelected, setKeySelected] = useState([]); //

  // 2 -  Hiển thị Modal chỉnh sửa
  const showModal = (id) => {
    setAdvanceChoosen(id);
    setIsModalOpen(true);
  };

  // 4 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    document.title = "Thông số hệ thống";
    dispatch(resetKeywords());
    dispatch(resetPage());
    // getArrs();
  }, []);

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys) => {
      setKeySelected(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };
  return (
    <section className="section section-system-parameters">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Thông số hệ thống</h2>
        {/* ======================= Btns ======================= */}
      </div>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        {/* ======================= Actions ======================= */}
        <Actions
          showModal={showModal}
          setReload={setReload}
          deletedPage={deletedPage}
          keySelected={keySelected}
          setKeySelected={setKeySelected}
        />
        {/* ======================= Table ======================= */}
        <TableListOptions
          showModal={showModal}
          reload={reload}
          setReload={setReload}
          deletedPage={deletedPage}
          rowSelection={rowSelection}
        />
      </div>
      {/* ======================= Modals ======================= */}
      <div className="modals">
        <OptionDetails
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          advance={advanceChoosen}
          statusOptions={listStatus}
          setAdvance={setAdvanceChoosen}
          setReload={setReload}
          deletedPage={deletedPage}
        />
      </div>
    </section>
  );
}
