import React, { useEffect } from "react";
import ListParameters from "../../../Components/SettingPage/SystemParameters/ListParameters";

export default function SystemParameters() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Cấu hình";
  }, []);
  return (
    <section className="section section-system-parameters">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Cấu hình</h2>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        <ListParameters />
      </div>
      {/* ======================= Modals ======================= */}
    </section>
  );
}
