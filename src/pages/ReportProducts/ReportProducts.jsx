import React, { useEffect, useState } from "react";
import { Tooltip } from "antd";
import { PiMicrosoftExcelLogoFill } from "react-icons/pi";
import { exportReportExcel, showError } from "../../utils/others";
import Actions from "../../Components/ReportProduct/Actions";
import { resetKeywords } from "../../redux/slice/keywordsSlice";
import { resetPage } from "../../redux/slice/pageSlice";
import { useDispatch } from "react-redux";
import { getListDepartment } from "../../utils/user";
import { setSpinner } from "../../redux/slice/spinnerSlice";
import { useFormik } from "formik";
import TableListData from "../../Components/ReportProduct/TableListData";
import { useNavigate } from "react-router-dom";

const ReportProducts = () => {
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  let [listDepartment, setListDepartment] = useState([]); //hiển thị danh sách nhân viên
  const dispatch = useDispatch();
  const navigate = useNavigate();

  // 3 - Lấy danh sách mảng trạng thái phiếu tạm ứng
  const getArrs = async () => {
    dispatch(setSpinner(true));
    let res = await getListDepartment();
    if (res.status) {
      setListDepartment(res.data);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    document.title = "BÁO CÁO TỔNG HỢP HÀNG HÓA, VẬT TƯ, MMTB CỦA CÁC BỘ PHẬN";
    dispatch(resetKeywords());
    dispatch(resetPage());
    getArrs();
  }, []);

  //   Formik Filter
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      start_date: "",
      end_date: "",
      department: "",
    },
    onSubmit: async (values) => {
      // console.log(values);
      setReload(true);
    },
  });

  return (
    <section className="section section-advance-money">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>BÁO CÁO TỔNG HỢP HÀNG HÓA, VẬT TƯ, MMTB CỦA CÁC BỘ PHẬN</h2>
        {/* ======================= Btns ======================= */}
        <div className="flex gap-2">
          <div className="icon-actions space-x-2">
            <Tooltip placement="bottom" title="Xuất file Excel">
              <button
                onClick={async () => {
                  dispatch(setSpinner(true));
                  let res = await exportReportExcel(
                    "report_product",
                    formik.values
                  );
                  if (res.status) {
                    window.location.href = res.data.file;
                    dispatch(setSpinner(false));
                  } else {
                    showError();
                  }
                }}
              >
                <PiMicrosoftExcelLogoFill className="text-green-700" />
              </button>
            </Tooltip>
          </div>
        </div>
      </div>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        {/* ======================= Actions ======================= */}
        <Actions
          setReload={setReload}
          listDepartment={listDepartment}
          formik={formik}
        />
        {/* ======================= Table ======================= */}
        <TableListData reload={reload} setReload={setReload} formik={formik} />
      </div>
    </section>
  );
};

export default ReportProducts;
