import React, { useEffect, useState } from "react";
import { BsShieldLock } from "react-icons/bs";
import { GiFamilyTree } from "react-icons/gi";
import { PiOfficeChair } from "react-icons/pi";

import { Tabs } from "antd";
import PermissionsManagement from "../../../Components/UserPage/UserAuthorization/PermissionsManagement/PermissionsManagement";
import RoleManagement from "../../../Components/UserPage/UserAuthorization/RoleManagement/RoleManagement";
import DepartmentManagement from "../../../Components/UserPage/UserAuthorization/DepartmentManagement/DepartmentManagement";
import { useSelector } from "react-redux";

export default function UserAuthorization() {
  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );

  let [keyNumActive, setKeyNumeActive] = useState(1);
  // Set title cho trang
  useEffect(() => {
    document.title = "Phân quyền người dùng";
  }, []);

  const changeKeyNum = (activeKey) => {
    setKeyNumeActive(activeKey);
  };

  const items = [
    {
      label: (
        <span className="tab-item">
          <BsShieldLock />
          Quản lý phân quyền
        </span>
      ),
      key: 1,
      children: (
        <PermissionsManagement
          keyNumActive={keyNumActive}
          keyTab={1}
          permission={permission}
        />
      ),
    },
    {
      label: (
        <span className="tab-item">
          <PiOfficeChair />
          Quản lý chức vụ
        </span>
      ),
      key: 2,
      children: (
        <RoleManagement
          keyNumActive={keyNumActive}
          keyTab={2}
          permission={permission}
        />
      ),
    },
    {
      label: (
        <span className="tab-item">
          <GiFamilyTree />
          Quản lý bộ phận
        </span>
      ),
      key: 3,
      children: (
        <DepartmentManagement
          keyNumActive={keyNumActive}
          keyTab={3}
          permission={permission}
        />
      ),
    },
  ];
  return (
    <section className="section section-authorization">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Phân quyền người dùng</h2>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        <Tabs
          defaultActiveKey={1}
          items={items}
          onChange={(activeKey) => changeKeyNum(activeKey)}
        />
      </div>
    </section>
  );
}
