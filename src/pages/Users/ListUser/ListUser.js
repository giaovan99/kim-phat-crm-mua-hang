import React, { useEffect, useState } from "react";
import Actions from "../../../Components/UserPage/ListUserPage/Actions";
import TableListUser from "../../../Components/UserPage/ListUserPage/TableListUser";
import ChangePassword from "../../../Components/UserPage/ListUserPage/ChangePassword";
import UserDetail from "../../../Components/UserPage/ListUserPage/UserDetail";
import { getListRoles } from "../../../utils/roles";
import { getListDepartment, getListPosition } from "../../../utils/user";
import { useDispatch, useSelector } from "react-redux";
import { resetPage } from "../../../redux/slice/pageSlice";
import { resetKeywords } from "../../../redux/slice/keywordsSlice";
import DeleteBtnComponent from "../../../Components/ButtonComponent/DeleteBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";

export default function ListUser() {
  // 1 - Khai báo state
  const [isModalOpen, setIsModalOpen] = useState(false); // Ẩn hiện modal thêm/fix người dùng
  const [isChangePasswordModalOpen, setIsChangePasswordModalOpen] =
    useState(false); // Ẩn hiện modal chỉnh sửa mật khẩu
  const [userChoosen, setUserChoosen] = useState(); //Quản lý id user được chọn
  const [listRoles, setListRoles] = useState([]); //Danh sách phân quyền
  const [listDepartment, setListDepartment] = useState([]); //Danh sách bộ phận
  const [listPosition, setListPosition] = useState([]); //Danh sách chức vụ
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const dispatch = useDispatch();
  const [deletedPage, setDeletedPage] = useState(false); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const [keySelected, setKeySelected] = useState([]); //
  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  // 2 - Hiển thị modal thêm/fix người dùng & gắn gắn id của user đc chọn
  const showModal = (id) => {
    setUserChoosen(id);
    setIsModalOpen(true);
  };

  // 3 - Hiển thị modal đổi mật khẩu
  const showChangePasswordModal = (user) => {
    setIsChangePasswordModalOpen(true);
    setUserChoosen(user);
  };

  // 4 - Lấy danh sách mảng chức vụ, bộ phận, roles -> sử dụng trong việc tạo người dùng (select options các vị trí)
  const getArrs = async () => {
    dispatch(setSpinner(true));
    let res = await getListRoles();
    if (res.status) {
      setListRoles(res.data);
    }
    res = await getListPosition();
    if (res.status) {
      setListPosition(res.data);
    }
    res = await getListDepartment();
    if (res.status) {
      setListDepartment(res.data);
    }
    dispatch(setSpinner(false));
  };

  // 5 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    document.title = "Danh sách người dùng";
    dispatch(resetKeywords());
    loadPage();
    getArrs();
  }, []);

  // 6 - Tải lại dữ liệu từ trang 1
  let loadPage = () => {
    // Reset page redux ==1
    dispatch(resetPage());
  };

  // 7 - Thay đổi API danh sách người dùng
  const changeStateAPI = () => {
    if (deletedPage) {
      setDeletedPage(false);
      setReload(true);
    } else {
      setDeletedPage(true);
      setReload(true);
    }
  };

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys) => {
      setKeySelected(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  return (
    <section className="section section-list-user">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Danh sách người dùng</h2>
        {/* ======================= Btns ======================= */}
        <DeleteBtnComponent
          deleted={deletedPage}
          onClickF={() => changeStateAPI()}
          text1="Danh sách người dùng đã xoá"
          text2="Danh sách người dùng đang có"
        />
      </div>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        {/* ======================= Actions ======================= */}
        <Actions
          showModal={showModal}
          setReload={setReload}
          deletedPage={deletedPage}
          keySelected={keySelected}
          setKeySelected={setKeySelected}
          permission={permission}
        />
        {/* ======================= Table ======================= */}
        <TableListUser
          showModal={showModal}
          showChangePasswordModal={showChangePasswordModal}
          loadPage={loadPage}
          reload={reload}
          setReload={setReload}
          deletedPage={deletedPage}
          rowSelection={rowSelection}
          permission={permission}
        />
      </div>
      {/* ======================= Modals ======================= */}
      <div className="modals">
        {/* ======================= Change Password ======================= */}
        <ChangePassword
          isModalOpen={isChangePasswordModalOpen}
          setIsModalOpen={setIsChangePasswordModalOpen}
          user={userChoosen}
          setUser={setUserChoosen}
          setReload={setReload}
        />
        <UserDetail
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          user={userChoosen}
          listRoles={listRoles}
          listDepartment={listDepartment}
          listPosition={listPosition}
          setUser={setUserChoosen}
          setReload={setReload}
          deletedPage={deletedPage}
        />
      </div>
    </section>
  );
}
