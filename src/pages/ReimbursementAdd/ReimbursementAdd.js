import React, { useEffect } from "react";
import ReimbursementDetail from "../../Components/ReimbursementDetail/ReimbursementDetail";

export default function ReimbursementAdd() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Tạo phiếu Đề nghị thanh toán";
  }, []);
  return (
    <section className="section section-add-order">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Tạo phiếu Đề nghị thanh toán</h2>
      <ReimbursementDetail />
    </section>
  );
}
