import React, { useEffect, useState } from "react";
import { Tooltip } from "antd";
import { PiMicrosoftExcelLogoFill } from "react-icons/pi";
import { exportReportExcel, showError } from "../../utils/others";
import { resetKeywords } from "../../redux/slice/keywordsSlice";
import { resetPage } from "../../redux/slice/pageSlice";
import { useDispatch } from "react-redux";

import { useFormik } from "formik";
import Actions from "../../Components/ReportAM/Actions";
import TableListData from "../../Components/ReportAM/TableListData";
import { getStaffPurchaseRequest } from "../../utils/purchaseRequest";
import { setSpinner } from "../../redux/slice/spinnerSlice";

const ReportAM = () => {
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const dispatch = useDispatch();
  let [listStaff, setListStaff] = useState([]); //hiển thị danh sách nhân viên

  // 3 - Lấy danh sách mảng trạng thái phiếu tạm ứng
  const getArrs = async () => {
    dispatch(setSpinner(true));
    // 5.6 - Danh sách nhân viên đề nghị
    let res = await getStaffPurchaseRequest();
    if (res.status) {
      setListStaff(res.data);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    document.title =
      "BÁO CÁO TẠM ỨNG/ HOÀN ỨNG TIỀN MẶT CÁ NHÂN XUẤT THEO THỜI GIAN";
    dispatch(resetKeywords());
    dispatch(resetPage());
    getArrs();
  }, []);

  //   Formik Filter
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      start_date: "",
      end_date: "",
      staff_number: "",
    },
    onSubmit: async (values) => {
      setReload(true);
    },
  });

  return (
    <section className="section section-advance-money">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2 className="uppercase">
          BÁO CÁO TẠM ỨNG/ HOÀN ỨNG TIỀN MẶT CÁ NHÂN XUẤT THEO THỜI GIAN
        </h2>
        {/* ======================= Btns ======================= */}
        <div className="flex gap-2">
          <div className="icon-actions space-x-2">
            <Tooltip placement="bottom" title="Xuất file Excel">
              <button
                onClick={async () => {
                  dispatch(setSpinner(true));
                  let res = await exportReportExcel("report_am", formik.values);
                  if (res.status) {
                    window.location.href = res.data.file;
                    dispatch(setSpinner(false));
                  } else {
                    showError();
                  }
                }}
              >
                <PiMicrosoftExcelLogoFill className="text-green-700" />
              </button>
            </Tooltip>
          </div>
        </div>
      </div>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        {/* ======================= Actions ======================= */}
        <Actions formik={formik} listStaff={listStaff} />
        {/* ======================= Table ======================= */}
        <TableListData reload={reload} setReload={setReload} formik={formik} />
      </div>
    </section>
  );
};

export default ReportAM;
