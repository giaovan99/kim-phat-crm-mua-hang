import React, { useEffect } from "react";
import PurchaseProposalDetail from "../../Components/PurchaseProposalDetail/PurchaseProposalDetail";

export default function PurchaseProposalAdd() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Tạo phiếu đề nghị mua hàng";
  }, []);
  return (
    <section className="section section-add-order">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Tạo phiếu đề nghị mua hàng</h2>
      <PurchaseProposalDetail />
    </section>
  );
}
