import React, { useEffect } from "react";
import OrderDetailComponents from "../../Components/OrderDetailComponents/OrderDetailComponents";

export default function OrderFix() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Sửa đơn hàng";
  }, []);
  return (
    <section className="section section-add-order">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Chỉnh sửa đơn hàng</h2>
      {/* ======================= Content ======================= */}
      <OrderDetailComponents />
    </section>
  );
}
