import React, { useEffect, useState } from "react";
import {
  MdOutlineLockReset,
  MdOutlineLockPerson,
  MdOutlineLogin,
} from "react-icons/md";
import { SmileOutlined } from "@ant-design/icons";

import { useFormik } from "formik";
import * as yup from "yup";
import { Link, useLocation, useNavigate, useParams } from "react-router-dom";
import { authService } from "../../services/authService";
import { checkToken, resetPassword } from "../../utils/auth";
import { Button, Result } from "antd";
// import { changePwd } from "../../utils/changePwd";
export default function ResetPassword() {
  // Khai báo state
  let [success, setSuccess] = useState(false);
  let [apiSuccess, setAPISuccess] = useState(false);
  let location = useLocation();

  let navigator = useNavigate();

  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      token: "",
      password: "",
      confirm_password: "",
    },
    validationSchema: yup.object({
      password: yup
        .string()
        .required("Nhập mật khẩu mới")
        .matches(/^.{8,}$/, "Mật khẩu yêu cầu ít nhất 8 ký tự"),
      confirm_password: yup
        .string()
        .required("Xác nhận lại mật khẩu")
        .oneOf([yup.ref("password"), null], "Mật khẩu chưa trùng khớp"),
    }),

    onSubmit: async (values, formik) => {
      // console.log(values);
      let res = await resetPassword(values);
      // console.log(res);
      if (res) {
        setAPISuccess(true);
      }
    },
  });

  const { handleBlur, handleChange, touched, errors } = formik;

  const checkTokenURL = async (token) => {
    let res = await checkToken(token);
    // console.log(token);
    if (res) {
      setSuccess(true);
      formik.setFieldValue("token", token);
    } else {
      setSuccess(false);
      formik.setFieldValue("token", "");
    }
  };
  // Set title cho trang
  useEffect(() => {
    document.title = "Reset Password";
    const tokenParams = new URLSearchParams(location.search).get("token");
    checkTokenURL(tokenParams);
  }, []);

  // console.log(apiSuccess);

  return (
    <section className="login100-form validate-form flex flex-col justify-between section section-forget-password">
      {apiSuccess ? (
        <Result
          status="success"
          icon={<SmileOutlined />}
          title="Đổi mật khẩu !"
          subTitle="Đổi mật khẩu thành công!"
          extra={[
            <Button type="default" key="console" onClick={() => navigator("/")}>
              Quay lại đăng nhập!
            </Button>,
          ]}
        />
      ) : (
        <>
          <p className="login100-form-title">Tạo lại Mật khẩu</p>
          {/* ======================= Form Reset Password ======================= */}
          {success ? (
            <form
              onSubmit={(event) => {
                event.preventDefault();
                formik.handleSubmit();
              }}
              className="accountLayout"
            >
              {/* ======================= Mật khẩu mới ======================= */}
              <div className="form-group">
                <div className=" flex items-center space-x-2">
                  <label htmlFor="password">
                    <MdOutlineLockPerson />
                  </label>
                  <input
                    className="grow"
                    type="password"
                    placeholder="Nhập mật khẩu mới"
                    name="password"
                    id="password"
                    value={formik.values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
                {touched.password && errors.password ? (
                  <p
                    className="mt-1 text-red-500 text-sm "
                    id="password-warning"
                  >
                    {errors.password}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= Nhập lại mật khẩu ======================= */}
              <div className="form-group">
                <div className=" flex items-center space-x-2 mt-2">
                  <label htmlFor="confirm_password">
                    <MdOutlineLockReset />
                  </label>
                  <input
                    className="grow"
                    type="password"
                    placeholder="Nhập lại mật khẩu"
                    name="confirm_password"
                    id="confirm_password"
                    value={formik.values.confirm_password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </div>
                {touched.confirm_password && errors.confirm_password ? (
                  <p
                    className="mt-1 text-red-500 text-sm "
                    id="confirm_password-warning"
                  >
                    {errors.confirm_password}
                  </p>
                ) : (
                  <></>
                )}
              </div>
              {/* ======================= API Messege ======================= */}
              <p
                className={
                  success
                    ? "mt-1 text-green-700 text-sm  mb-1"
                    : "mt-1 text-red-700	 text-sm  mb-1"
                }
                id="warningAPI"
              ></p>
              {/* ======================= btn lưu ======================= */}
              <div className="form-group mt-5">
                <button
                  type="sumbit"
                  className="w-full rounded bg-yl py-2 font-medium"
                  id="submitBtn"
                >
                  Lưu
                </button>
              </div>
            </form>
          ) : (
            <div className="text-center text-red-700">Token không hợp lệ</div>
          )}
          {/* ======================= Quay lại quên mật khẩu ======================= */}
          <div className="text-center p-t-12 mt-2">
            <Link
              to="/forgot-password"
              className="inline-flex gap-2 items-center hover:underline underline-offset-4"
            >
              Quay lại quên mật khẩu <MdOutlineLogin />
            </Link>
          </div>
        </>
      )}
    </section>
  );
}
