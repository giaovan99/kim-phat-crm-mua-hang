import React, { useEffect } from "react";
import ReimbursementDetail from "../../Components/ReimbursementDetail/ReimbursementDetail";

export default function ReimbursementFix() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Sửa phiếu Đề nghị thanh toán";
  }, []);
  return (
    <section className="section section-add-order">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Sửa phiếu Đề nghị thanh toán</h2>
      <ReimbursementDetail />
    </section>
  );
}
