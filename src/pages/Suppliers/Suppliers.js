import React, { useEffect, useState } from "react";
import TableSuppliers from "../../Components/SuppliersPage/TableSuppliers";
import Actions from "../../Components/SuppliersPage/Actions";
import ModalSupplierDetail from "../../Components/SuppliersPage/ModalSupplierDetail";
import { useDispatch, useSelector } from "react-redux";
import { resetPage } from "../../redux/slice/pageSlice";
import { resetKeywords } from "../../redux/slice/keywordsSlice";
import ImportFilesComponent from "../../Components/ImportFilesComponents/ImportFilesComponents";
import DeleteBtnComponent from "../../Components/ButtonComponent/DeleteBtnComponent";
import ImportExcelBtnComponent from "../../Components/ButtonComponent/ImportExcelBtnComponent";

export default function Suppliers() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [supplierChoosen, setSupplierChoosen] = useState();
  const [reload, setReload] = useState(false);
  const dispatch = useDispatch();
  const [deletedPage, setDeletedPage] = useState(false); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const [keySelected, setKeySelected] = useState([]); //
  const [isViewImportFile, setIsViewImportFile] = useState(false);

  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  const showModal = (key) => {
    setIsModalOpen(true);
    setSupplierChoosen(key);
  };
  // Nhập file sản phẩm từ Excel
  const showImportFile = () => {
    setIsViewImportFile(true);
  };

  // Set title cho trang
  useEffect(() => {
    document.title = "Danh sách nhà cung cấp";
    dispatch(resetKeywords());
    loadPage();
  }, []);

  // Tải lại dữ liệu từ trang 1
  let loadPage = () => {
    // Reset page redux ==1
    dispatch(resetPage());
  };

  // Thay đổi API danh sách người dùng
  const changeStateAPI = () => {
    if (deletedPage) {
      setDeletedPage(false);
      setReload(true);
    } else {
      setDeletedPage(true);
      setReload(true);
    }
  };

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys) => {
      setKeySelected(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  return (
    <section className="section section-suppliers">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Nhà cung cấp</h2>
        {/* ======================= Btns ======================= */}
        <div className="flex gap-2">
          {permission?.suppliers?.update === 1 && (
            <ImportExcelBtnComponent
              deleted={deletedPage}
              onClickF={() => showImportFile()}
              text={"Thêm NCC từ file Excel"}
            />
          )}
          <DeleteBtnComponent
            deleted={deletedPage}
            onClickF={() => changeStateAPI()}
            text1="Danh sách NCC đã xoá"
            text2="Danh sách NCC đang có"
          />
        </div>
      </div>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        {/* ======================= Actions ======================= */}
        <Actions
          showModal={showModal}
          setReload={setReload}
          deletedPage={deletedPage}
          keySelected={keySelected}
          setKeySelected={setKeySelected}
          permission={permission}
        />
        {/* ======================= Table ======================= */}
        <TableSuppliers
          showModal={showModal}
          loadPage={loadPage}
          reload={reload}
          setReload={setReload}
          deletedPage={deletedPage}
          rowSelection={rowSelection}
          permission={permission}
        />
      </div>
      <div className="modals">
        <ModalSupplierDetail
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          supplier={supplierChoosen}
          setSupplier={setSupplierChoosen}
          setReload={setReload}
          deletedPage={deletedPage}
        />
        {isViewImportFile && (
          <ImportFilesComponent
            isModalOpen={isViewImportFile}
            setIsModalOpen={setIsViewImportFile}
            confirmFileStyle={2}
            title={"Import file NCC"}
            module={"suppliers"}
            setReload={setReload}
          />
        )}
      </div>
    </section>
  );
}
