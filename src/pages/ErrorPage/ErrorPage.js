import React, { useEffect } from "react";

export default function ErrorPage() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Error Page";
  }, []);
  return (
    <section id="error-page">
      <div className="flex flex-col items-center justify-center h-[100vh] gap-8">
        <h1 className="text-5xl font-medium">404 ERROR</h1>
        <p className="text-3xl">Trang này không tồn tại.</p>
      </div>
    </section>
  );
}
