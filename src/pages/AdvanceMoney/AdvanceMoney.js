import React, { useEffect, useState } from "react";
import Actions from "../../Components/AdvanceMoneyPage/Actions";
import TableListAdvance from "../../Components/AdvanceMoneyPage/TableListAdvance";
import AdvanceMoneyDetails from "../../Components/AdvanceMoneyPage/AdvanceMoneyDetails";
import { useDispatch, useSelector } from "react-redux";
import { resetKeywords } from "../../redux/slice/keywordsSlice";
import { resetPage } from "../../redux/slice/pageSlice";
import DeleteBtnComponent from "../../Components/ButtonComponent/DeleteBtnComponent";
import ImportExcelBtnComponent from "../../Components/ButtonComponent/ImportExcelBtnComponent";
import ImportFilesComponent from "../../Components/ImportFilesComponents/ImportFilesComponents";

export default function AdvanceMoney() {
  // 1 - Khai báo state
  const [isModalOpen, setIsModalOpen] = useState();
  const [advanceChoosen, setAdvanceChoosen] = useState(null);
  const [listStatus, setListStatus] = useState([]); //Danh sách phân quyền
  const dispatch = useDispatch();
  const [deletedPage, setDeletedPage] = useState(false); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const [keySelected, setKeySelected] = useState([]); //
  const [isViewImportFile, setIsViewImportFile] = useState(false);
  const [listStaff, setListStaff] = useState([]); //hiển thị danh sách nhân viên
  const [typeList, setTypeList] = useState([]); //Danh sách loại phiếu
  const [searchFilter, setSearchFilter] = useState({}); //Danh sách loại phiếu
  const [itemSelected, setItemSelected] = useState([]); //Mảng chứa chi tiết các phiếu được chọn để xét status

  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  // 2 -  Hiển thị Modal xem chi tiết && trạng thái
  const showModal = (id) => {
    setAdvanceChoosen(id);
    setIsModalOpen(true);
  };

  // 4 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    document.title = "Tạm ứng";
    dispatch(resetKeywords());
    dispatch(resetPage());
  }, []);

  // 5 - Thay đổi API trang xoá
  const changeStateAPI = () => {
    if (deletedPage) {
      setDeletedPage(false);
      setReload(true);
    } else {
      setDeletedPage(true);
      setReload(true);
    }
  };

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys, selectedRows) => {
      setKeySelected(selectedRowKeys);
      setItemSelected(selectedRows);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  // Nhập file sản phẩm từ Excel
  const showImportFile = () => {
    setIsViewImportFile(true);
  };
  return (
    <section className="section section-advance-money">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Danh sách phiếu tạm ứng</h2>
        {/* ======================= Btns ======================= */}
        <div className="flex gap-2">
          {permission?.requests_for_advance?.create === 1 && (
            <ImportExcelBtnComponent
              deleted={deletedPage}
              onClickF={() => showImportFile()}
              text={"Import phiếu từ file Excel"}
            />
          )}
          <DeleteBtnComponent
            deleted={deletedPage}
            onClickF={() => changeStateAPI()}
            text1="Danh sách tạm ứng đã xoá"
            text2="Danh sách tạm ứng đang có"
          />
        </div>
      </div>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        {/* ======================= Actions ======================= */}
        <Actions
          showModal={showModal}
          setReload={setReload}
          deletedPage={deletedPage}
          keySelected={keySelected}
          setKeySelected={setKeySelected}
          permission={permission}
          listStaff={listStaff}
          listStatus={listStatus}
          typeList={typeList}
          setSearchFilter={setSearchFilter}
          itemSelected={itemSelected}
          setItemSelected={setItemSelected}
        />
        {/* ======================= Table ======================= */}
        <TableListAdvance
          showModal={showModal}
          reload={reload}
          setReload={setReload}
          deletedPage={deletedPage}
          rowSelection={rowSelection}
          setListStaff={setListStaff}
          setListStatus={setListStatus}
          permission={permission}
          searchFilter={searchFilter}
        />
      </div>
      {/* ======================= Modals ======================= */}
      <div className="modals">
        <AdvanceMoneyDetails
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          advance={advanceChoosen}
          statusOptions={listStatus}
          setAdvance={setAdvanceChoosen}
          setReload={setReload}
          deletedPage={deletedPage}
          listStaff={listStaff}
          setListStaff={setListStaff}
          permission={permission}
          setTypeList={setTypeList}
          typeList={typeList}
        />
        {isViewImportFile && (
          <ImportFilesComponent
            isModalOpen={isViewImportFile}
            setIsModalOpen={setIsViewImportFile}
            confirmFileStyle={2}
            title={"Import phiếu tạm ứng"}
            module={"requests_for_advance"}
            setReload={setReload}
          />
        )}
      </div>
    </section>
  );
}
