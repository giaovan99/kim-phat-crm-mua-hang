import React, { useEffect, useState } from "react";
import { FileSyncOutlined } from "@ant-design/icons";
import { Tabs } from "antd";
import ReimbursementSlip from "../../Components/Reimbursement/ReimbursementSlip/ReimbursementSlip";
import { useDispatch } from "react-redux";
import { resetKeywords } from "../../redux/slice/keywordsSlice";
import { resetPage } from "../../redux/slice/pageSlice";
import DeleteBtnComponent from "../../Components/ButtonComponent/DeleteBtnComponent";

export default function Reimbursement() {
  let [keyNumActive, setKeyNumeActive] = useState(1);
  const [deletedPage, setDeletedPage] = useState(false); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const dispatch = useDispatch();

  const changeKeyNum = (activeKey) => {
    setKeyNumeActive(activeKey);
    dispatch(resetKeywords());
    dispatch(resetPage());
  };

  // 4 - Thay đổi API trang xoá
  const changeStateAPI = () => {
    if (deletedPage) {
      setDeletedPage(false);
    } else {
      setDeletedPage(true);
    }
  };

  const items = [
    {
      label: (
        <span className="tab-item">
          <FileSyncOutlined />
          Phiếu đề nghị thanh toán
        </span>
      ),
      key: 1,
      children: (
        <ReimbursementSlip
          keyNumActive={keyNumActive}
          keyTab={1}
          changeStateAPI={changeStateAPI}
          deletedPage={deletedPage}
          setDeletedPage={setDeletedPage}
        />
      ),
    },
    // {
    //   label: (
    //     <span className="tab-item">
    //       <BarcodeOutlined />
    //       Sản phẩm hoàn ứng
    //     </span>
    //   ),
    //   key: 2,
    //   children: (
    //     <ReimbursementProducts keyNumActive={keyNumActive} keyTab={2} />
    //   ),
    // },
  ];
  // Set title cho trang
  useEffect(() => {
    document.title = "Danh sách phiếu hoàn ứng";
  }, []);
  return (
    <section className="section section-reimbursement">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Đề nghị thanh toán</h2>
        {/* ======================= Btns ======================= */}
        <div className="icon-actions space-x-2">
          {/* {keyNumActive == 2 && (
            <Tooltip placement="bottom" title="Xuất file PDF sản phẩm đã chọn">
              <button>
                <FaFilePdf className="text-[#B6190D]" />
              </button>
            </Tooltip>
          )} */}
          {keyNumActive == 1 && (
            <DeleteBtnComponent
              deleted={deletedPage}
              onClickF={() => changeStateAPI()}
              text1="Danh sách phiếu ĐNTT đã xoá"
              text2="Danh sách phiếu ĐNTT đang có"
            />
          )}
        </div>
      </div>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        <Tabs
          defaultActiveKey={1}
          items={items}
          onChange={(activeKey) => changeKeyNum(activeKey)}
        />
      </div>
    </section>
  );
}
