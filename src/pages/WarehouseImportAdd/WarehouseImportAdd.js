import React, { useEffect } from "react";
import WarehouseImportDetails from "../../Components/WarehouseImportDetails/WarehouseImportDetails";

export default function WarehouseImportAdd() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Tạo phiếu nhập kho";
  }, []);
  return (
    <section className="section section-add-import-warehouse">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Tạo phiếu nhập kho</h2>
      {/* ======================= Content ======================= */}
      <WarehouseImportDetails />
    </section>
  );
}
