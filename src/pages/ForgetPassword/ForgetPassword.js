import React, { useEffect, useState } from "react";
import { MdOutlineEmail, MdOutlineLogin } from "react-icons/md";

import { useFormik } from "formik";
import * as yup from "yup";
import { Link } from "react-router-dom";
import { forgotPassword } from "../../utils/auth";
export default function ForgetPassword() {
  let [resendSuccess, setResendSuccess] = useState(false);

  // Khai báo formik
  const forgetPasswordFormik = useFormik({
    initialValues: {
      email: "",
    },
    validationSchema: yup.object({
      email: yup.string().email("Email không hợp lệ").required("Nhập email"),
    }),
    onSubmit: async (values) => {
      // Khi đã điền đầy đủ email => đẩy lên BE
      let api = await forgotPassword(values);
      if (api.status) {
        setResendSuccess(true);
        document.getElementById("warningAPI").innerText = api.mess;
      } else {
        //   // Hiển thị message
        setResendSuccess(false);
        document.getElementById("warningAPI").innerText = api.mess;
      }
    },
  });
  const { handleBlur, handleChange, touched, errors } = forgetPasswordFormik;

  // Set title cho trang
  useEffect(() => {
    document.title = "Quên mật khẩu";
  }, []);

  return (
    <section className="login100-form validate-form flex flex-col justify-between section section-forget-password">
      <p className="login100-form-title">Quên mật khẩu</p>
      {/* ======================= Form quên mật khẩu ======================= */}
      <form
        onSubmit={(event) => {
          event.preventDefault();
          forgetPasswordFormik.handleSubmit();
        }}
        className="accountLayout"
      >
        {/* ======================= Tài khoản ======================= */}
        <div className="form-group">
          <div className=" flex items-center space-x-2">
            <label htmlFor="email">
              <MdOutlineEmail />
            </label>
            <input
              className="grow"
              type="text"
              placeholder="Email"
              name="email"
              id="email"
              value={forgetPasswordFormik.values.email}
              onChange={forgetPasswordFormik.handleChange}
              disabled={resendSuccess ? true : false}
            />
          </div>
          {touched.email && errors.email ? (
            <p className="mt-1 text-red-500 text-sm " id="email-warning">
              {errors.email}
            </p>
          ) : (
            <></>
          )}
        </div>
        {!errors.email && (
          <p
            className={
              resendSuccess
                ? "mt-1 text-green-700 text-sm  mb-1"
                : "mt-1 text-red-700	 text-sm  mb-1"
            }
            id="warningAPI"
          ></p>
        )}
        {/* ======================= btn lấy lại mật khẩu ======================= */}
        {resendSuccess ? (
          <></>
        ) : (
          <div className="form-group mt-5" id="sendSubmit">
            <button
              type="sumbit"
              className="w-full rounded bg-yl py-2 font-medium"
            >
              Gửi
            </button>
          </div>
        )}
      </form>
      {/* ======================= Quay lại đăng nhập ======================= */}
      <div className="text-center p-t-12">
        <Link
          to="/"
          className="inline-flex gap-2 items-center hover:underline underline-offset-4"
        >
          Quay lại đăng nhập <MdOutlineLogin />
        </Link>
      </div>
    </section>
  );
}
