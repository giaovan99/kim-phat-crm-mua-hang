import React, { useEffect } from "react";
import WarehouseImportDetails from "../../Components/WarehouseImportDetails/WarehouseImportDetails";

export default function WarehouseImportFix() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Chỉnh sửa phiếu nhập kho";
  }, []);
  return (
    <section className="section section-add-import-warehouse">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Chỉnh sửa phiếu nhập kho</h2>
      {/* ======================= Content ======================= */}
      <WarehouseImportDetails />
    </section>
  );
}
