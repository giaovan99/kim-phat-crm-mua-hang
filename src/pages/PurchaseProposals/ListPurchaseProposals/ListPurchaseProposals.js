import React, { useEffect, useState } from "react";
import Actions from "../../../Components/PurchaseProposals/ListPurchaseProposal/Actions";
import TableListPurchaseProposals from "../../../Components/PurchaseProposals/ListPurchaseProposal/TableListPurchaseProposals";
import ViewDetails from "../../../Components/PurchaseProposals/ListPurchaseProposal/ViewDetails";
import ImportFilesComponent from "../../../Components/ImportFilesComponents/ImportFilesComponents";
import { useDispatch, useSelector } from "react-redux";
import { getPurchaseRequestStatusList } from "../../../utils/purchaseRequest";
import { resetKeywords } from "../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../redux/slice/pageSlice";
import DeleteBtnComponent from "../../../Components/ButtonComponent/DeleteBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { getListDepartment, getListUser } from "../../../utils/user";
import StepsFilter from "../../../Components/FilterComponents/StepsFilter";

export default function ListPurchaseProposals() {
  // 1 - Khai báo state
  const [isViewDetailModalOpen, setIsViewDetailModalOpen] = useState(false);
  const [proposalChoosen, setProposalChoosen] = useState();
  const [isViewImportFile, setIsViewImportFile] = useState(false);
  const [listStatus, setListStatus] = useState([]); //Danh sách phân quyền
  const dispatch = useDispatch();
  const [listStatusFilter, setListStatusFilter] = useState([]); //Danh sách trạng thái dùng cho filter
  const [deletedPage, setDeletedPage] = useState(false); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const [keySelected, setKeySelected] = useState([]); //Mảng chứa id các phiếu được chọn
  const [itemSelected, setItemSelected] = useState([]); //Mảng chứa chi tiết các phiếu được chọn để xét status
  const [searchFilter, setSearchFilter] = useState({}); //Search filter
  const [listDepartment, setListDepartment] = useState([]); //Danh sách trạng thái
  const [listStaff, setListStaff] = useState([]); //hiển thị danh sách nhân viên

  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  // 2 -  Hiển thị Modal xem chi tiết sản phẩm && trạng thái
  const showViewDetailModal = (id) => {
    setProposalChoosen(id);
    setIsViewDetailModalOpen(true);
  };

  // 3 - Lấy danh sách mảng trạng thái đề nghị mua hàng
  const getArrs = async () => {
    dispatch(setSpinner(true));
    // Danh sách trạng thái
    let res = await getPurchaseRequestStatusList();
    if (res.status) {
      setListStatus(res.data);
      setListStatusFilter([
        {
          key: "",
          label: "Tất cả",
          name: "Tất cả",
          title: "Tất cả",
          value: "",
        },
        ...res.data,
      ]);
    }

    // 5.7 - Danh sách bộ phận
    res = await getListDepartment();

    if (res.status) {
      setListDepartment(res.data);
    }
    // 5.7 - Danh sách staff
    res = await getListUser(1, { department: 4 });

    if (res.status) {
      const transformedArray = res.data.data.map((item) => ({
        value: item.id,
        label: item.full_name,
      }));
      setListStaff(transformedArray);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    document.title = "Danh sách đề nghị mua hàng";
    dispatch(resetKeywords());
    dispatch(resetPage());
    getArrs();
  }, []);

  // 5 - Thay đổi API trang xoá
  const changeStateAPI = () => {
    if (deletedPage) {
      setDeletedPage(false);
      setReload(true);
    } else {
      setDeletedPage(true);
      setReload(true);
    }
  };

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys, selectedRows) => {
      setKeySelected(selectedRowKeys);
      setItemSelected(selectedRows);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  return (
    <section className="section section-list-purchase-proposals">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Danh sách đề nghị mua hàng</h2>
        {/* ======================= Btns Delete & Undo ======================= */}
        <DeleteBtnComponent
          deleted={deletedPage}
          onClickF={() => changeStateAPI()}
          text1="Danh sách đề nghị đã xoá"
          text2="Danh sách đơn đề nghị đang có"
        />
      </div>
      {/* ======================= Content ======================= */}
      <div className="page-content">
        {/* ======================= Filter Steps ======================= */}
        <StepsFilter sttArr={listStatusFilter} setReload={setReload} />
        <Actions
          setReload={setReload}
          deletedPage={deletedPage}
          keySelected={keySelected}
          setKeySelected={setKeySelected}
          listStatus={listStatus}
          itemSelected={itemSelected}
          setItemSelected={setItemSelected}
          permission={permission}
          listDepartment={listDepartment}
          setSearchFilter={setSearchFilter}
          listStaff={listStaff}
        />
        <TableListPurchaseProposals
          showViewDetailModal={showViewDetailModal}
          statusOptions={listStatus}
          reload={reload}
          setReload={setReload}
          deletedPage={deletedPage}
          rowSelection={rowSelection}
          setProposal={setProposalChoosen}
          searchFilter={searchFilter}
          permission={permission}
          setListStaff={setListStaff}
        />
      </div>
      {/* ======================= Modals ======================= */}
      <div className="modals">
        {proposalChoosen && (
          <ViewDetails
            isModalOpen={isViewDetailModalOpen}
            setIsModalOpen={setIsViewDetailModalOpen}
            proposal={proposalChoosen}
            statusOptions={listStatus}
            setProposal={setProposalChoosen}
            deletedPage={deletedPage}
            setReload={setReload}
            permission={permission}
          />
        )}
        {isViewImportFile && (
          <ImportFilesComponent
            isModalOpen={isViewImportFile}
            setIsModalOpen={setIsViewImportFile}
            confirmFileStyle={1}
            title="Import file Đề nghị mua hàng"
          />
        )}
      </div>
    </section>
  );
}
