import React, { useEffect } from "react";
import OrderDetailComponents from "../../Components/OrderDetailComponents/OrderDetailComponents";

export default function OrderAdd() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Tạo đơn hàng";
  }, []);
  return (
    <section className="section section-add-order">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Tạo đơn hàng</h2>
      {/* ======================= Content ======================= */}
      <OrderDetailComponents />
    </section>
  );
}
