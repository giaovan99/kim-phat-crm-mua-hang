import { Tabs, Tooltip } from "antd";
import React, { useEffect, useState } from "react";
import { ShoppingCartOutlined, BarcodeOutlined } from "@ant-design/icons";
import ListProducts from "../../../Components/Warehouse/ImportWarehouse/ListProducts/ListProducts";
import { FaFilePdf } from "react-icons/fa6";
import ListWarehouseSlip from "../../../Components/Warehouse/ImportWarehouse/ListOrders/ListWarehouseSlip";
import { resetKeywords } from "../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../redux/slice/pageSlice";
import { useDispatch } from "react-redux";
import { stockImportPrint } from "../../../utils/warehouse";
import ReactToPrint from "react-to-print";
import PrintFromHTML from "../../../Components/ExportToPDF/PrintFromHTML/PrintFromHTML";
import DeleteBtnComponent from "../../../Components/ButtonComponent/DeleteBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import ListProductsWaiting from "../../../Components/Warehouse/ImportWarehouse/ListProductsWaiting/ListProductsWaiting";

export default function ImportWareHouse() {
  let [keyNumActive, setKeyNumeActive] = useState(1);
  const [deletedPage, setDeletedPage] = useState(false); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const [listIds, setIds] = useState([]); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const dispatch = useDispatch();

  // Set title cho trang
  useEffect(() => {
    document.title = "Danh sách phiếu nhập kho";
  }, []);

  const changeKeyNum = (activeKey) => {
    setKeyNumeActive(activeKey);
    dispatch(resetKeywords());
    dispatch(resetPage());
  };

  // 4 - Thay đổi API trang xoá
  const changeStateAPI = () => {
    if (deletedPage) {
      setDeletedPage(false);
    } else {
      setDeletedPage(true);
    }
  };

  {
    /* ======================= Start IN  ======================= */
  }
  // Lấy đoạn html
  const getDetailId = async () => {
    dispatch(setSpinner(true));
    let res = await stockImportPrint({ ids: [...listIds] });
    setDetails(res.data.data);
    dispatch(setSpinner(false));
  };

  const componentRef = React.useRef(null);

  const onBeforeGetContentResolve = React.useRef(null);

  const [loading, setLoading] = React.useState(false);
  const [PPDetails, setDetails] = useState();

  const handleAfterPrint = () => {
    // console.log("`onAfterPrint` called");
    onBeforeGetContentResolve.current = null;
    setLoading(false);
  };

  const handleBeforePrint = React.useCallback(() => {
    // console.log("`onBeforePrint` called");
  }, []);

  const handleOnBeforeGetContent = async () => {
    // console.log("`onBeforeGetContent` called");
    await getDetailId();
    // console.log("dont");
    // return componentRef.current;
  };

  React.useEffect(() => {
    if (loading && onBeforeGetContentResolve.current) {
      // Resolves the Promise, letting `react-to-print` know that the DOM updates are completed
      onBeforeGetContentResolve.current();
    }
  }, [loading]);

  const reactToPrintTrigger = React.useCallback(() => {
    // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
    // to the root node of the returned component as it will be overwritten.

    // Bad: the `onClick` here will be overwritten by `react-to-print`
    // return <button onClick={() => alert('This will not work')}>Print this out!</button>;

    // Good
    return (
      <button id="btnPdf">
        <FaFilePdf className="text-[#B6190D]" />
      </button>
    );
  }, []);

  {
    /* ======================= End IN  ======================= */
  }

  const items = [
    {
      label: (
        <span className="tab-item">
          <ShoppingCartOutlined />
          Phiếu Đã nhập kho
        </span>
      ),
      key: 1,
      children: (
        <ListWarehouseSlip
          keyNumActive={keyNumActive}
          keyTab={1}
          changeStateAPI={changeStateAPI}
          deletedPage={deletedPage}
          setDeletedPage={setDeletedPage}
        />
      ),
    },
    {
      label: (
        <span className="tab-item">
          <BarcodeOutlined />
          Sản phẩm chờ kiểm tra
        </span>
      ),
      key: 3,
      children: <ListProductsWaiting keyNumActive={keyNumActive} keyTab={3} />,
    },
    {
      label: (
        <span className="tab-item">
          <BarcodeOutlined />
          Sản phẩm nhập kho
        </span>
      ),
      key: 2,
      children: (
        <ListProducts
          keyNumActive={keyNumActive}
          keyTab={2}
          listIds={listIds}
          setIds={setIds}
        />
      ),
    },
  ];
  return (
    <section className="section section-manage-product">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Nhập kho</h2>
        {/* ======================= Btns ======================= */}
        <div className="icon-actions space-x-2">
          {keyNumActive == 2 && (
            <Tooltip placement="bottom" title="Xuất file PDF sản phẩm đã chọn">
              <div>
                <ReactToPrint
                  content={() => componentRef.current}
                  documentTitle="Xuất danh sách sản phẩm nhập kho"
                  onAfterPrint={handleAfterPrint}
                  onBeforeGetContent={() => {
                    return new Promise(async (resolve) => {
                      await handleOnBeforeGetContent();
                      onBeforeGetContentResolve.current = resolve;
                      setLoading(true);
                    });
                  }}
                  onBeforePrint={handleBeforePrint}
                  removeAfterPrint
                  trigger={reactToPrintTrigger}
                />
                <div className="hidden">
                  <PrintFromHTML
                    forwardedRef={componentRef}
                    details={PPDetails}
                  />
                </div>
              </div>
            </Tooltip>
          )}
          {keyNumActive == 1 && (
            <DeleteBtnComponent
              deleted={deletedPage}
              onClickF={() => changeStateAPI()}
              text1="Danh sách phiếu nhập kho đã xoá"
              text2="Danh sách phiếu nhập kho đang có"
            />
          )}
        </div>
      </div>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        <Tabs
          defaultActiveKey={1}
          items={items}
          onChange={(activeKey) => changeKeyNum(activeKey)}
        />
      </div>
    </section>
  );
}
