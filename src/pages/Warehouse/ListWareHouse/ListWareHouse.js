import React, { useEffect, useState } from "react";
import Actions from "../../../Components/Warehouse/ListWarehousePage/Actions";
import TableListWareHouse from "../../../Components/Warehouse/ListWarehousePage/TableListWarehouse";
import WarehouseDetails from "../../../Components/Warehouse/ListWarehousePage/WarehouseDetails";
import { resetPage } from "../../../redux/slice/pageSlice";
import { resetKeywords } from "../../../redux/slice/keywordsSlice";
import { useDispatch, useSelector } from "react-redux";
import { getStaffOrder } from "../../../utils/order";

export default function ListWareHouse() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [warehouseChoosen, setWarehouseChoosen] = useState();
  const [reload, setReload] = useState(false);
  const dispatch = useDispatch();
  const [keySelected, setKeySelected] = useState([]); //
  const [listUsers, setListUsers] = useState(); //Danh sách người dùng

  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );

  const showModal = (key) => {
    setIsModalOpen(true);
    setWarehouseChoosen(key);
  };

  // Set title cho trang
  useEffect(() => {
    document.title = "Quản lý kho hàng";
    dispatch(resetKeywords());
    loadPage();
    getArr();
  }, []);

  let getArr = async () => {
    // 5.6 - Danh sách nhân viên
    let res = await getStaffOrder();
    if (res.status) {
      setListUsers(res.data);
    }
  };

  // Tải lại dữ liệu từ trang 1
  let loadPage = () => {
    // Reset page redux ==1
    dispatch(resetPage());
  };
  // rowSelection object indicates the need for row selection
  const rowSelection = {
    onChange: (selectedRows, info) => {
      setKeySelected(info);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  return (
    <section className="section section-list-warehouse">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Quản lý kho hàng</h2>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        {/* ======================= Actions ======================= */}
        <Actions
          showModal={showModal}
          setReload={setReload}
          keySelected={keySelected}
          setKeySelected={setKeySelected}
          permission={permission}
        />
        {/* ======================= Table ======================= */}
        <TableListWareHouse
          showModal={showModal}
          loadPage={loadPage}
          reload={reload}
          setReload={setReload}
          rowSelection={rowSelection}
        />
      </div>
      <div className="modals">
        <WarehouseDetails
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          warehouse={warehouseChoosen}
          setWarehouse={setWarehouseChoosen}
          setReload={setReload}
          userOptions={listUsers}
        />
      </div>
    </section>
  );
}
