import React, { useEffect } from "react";
import { Tabs } from "antd";
import { BiSolidUserDetail } from "react-icons/bi";
import { RiLockPasswordLine } from "react-icons/ri";
import Detail from "../../Components/PersonalPage/Detail/Detail";
import ChangePassword from "../../Components/PersonalPage/ChangePassword/ChangePassword";

const items = [
  {
    label: (
      <span className="tab-item">
        <BiSolidUserDetail />
        Thông tin chi tiết
      </span>
    ),
    key: 1,
    children: <Detail />,
  },
  {
    label: (
      <span className="tab-item">
        <RiLockPasswordLine />
        Đổi mật khẩu
      </span>
    ),
    key: 2,
    children: <ChangePassword />,
  },
];
export default function PersonalPage() {
  // Set title cho trang
  useEffect(() => {
    document.title = "Trang cá nhân";
  }, []);
  return (
    <section className="section section-personal-page">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Trang cá nhân</h2>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        <Tabs defaultActiveKey="1" items={items} />
      </div>
    </section>
  );
}
