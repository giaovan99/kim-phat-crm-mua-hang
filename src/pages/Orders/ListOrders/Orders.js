import React, { useEffect, useState } from "react";
import Actions from "../../../Components/OrdersPage/ListOrders/Actions";
import TableListOrders from "../../../Components/OrdersPage/ListOrders/TableListOrders";
import ViewDetails from "../../../Components/OrdersPage/ListOrders/ViewDetails";
import { useDispatch, useSelector } from "react-redux";
import { resetPage } from "../../../redux/slice/pageSlice";
import { getOrderStatusList } from "../../../utils/order";
import { resetCurrent } from "../../../redux/slice/statusSlice";
import StepsFilter from "../../../Components/FilterComponents/StepsFilter";
import DeleteBtnComponent from "../../../Components/ButtonComponent/DeleteBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import TotalBillOrder from "../../../Components/OrdersPage/ListOrders/TotalBill";
import { getListUser } from "../../../utils/user";

export default function Orders() {
  // 1 - Khai báo state
  const [isViewDetailModalOpen, setIsViewDetailModalOpen] = useState(false);
  const [orderChoosen, setOrderChoosen] = useState();
  const [listStatus, setListStatus] = useState([]); //Danh sách phân quyền
  const [listStatusFilter, setListStatusFilter] = useState(""); //Danh sách phân quyền
  const dispatch = useDispatch();
  const [deletedPage, setDeletedPage] = useState(false); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const [keySelected, setKeySelected] = useState([]); //Mảng chứa id các phiếu được chọn
  const [itemSelected, setItemSelected] = useState([]); //Mảng chứa chi tiết các phiếu được chọn để xét status
  const [optionSummary, setOptionSummary] = useState(); //Mảng chứa thông tin PTTT & tổng giá trị đơn hàng của nó
  const [searchFilter, setSearchFilter] = useState({}); //Search filter
  const [listStaff, setListStaff] = useState([]); //hiển thị danh sách nhân viên

  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  // User info
  let userInfo = useSelector((state) => state.userProfileSlice.userInfo);

  // 2 -  Hiển thị Modal xem chi tiết sản phẩm && trạng thái
  const showViewDetailModal = (id) => {
    setOrderChoosen(id);
    setIsViewDetailModalOpen(true);
  };

  // 3 - Lấy danh sách mảng trạng thái đơn hàng
  const getArrs = async () => {
    dispatch(setSpinner(true));
    let res = await getOrderStatusList();
    if (res.status) {
      let arr = [...res.data];
      arr.unshift({
        key: "",
        label: "Tất cả",
        name: "Tất cả",
        title: "Tất cả",
        value: "",
      });
      setListStatusFilter(arr);
      setListStatus(res.data);
    }
    // 5.7 - Danh sách staff
    res = await getListUser(1, { department: 4 });

    if (res.status) {
      const transformedArray = res.data.data.map((item) => ({
        value: item.id,
        label: item.full_name,
      }));
      setListStaff(transformedArray);
    }
    dispatch(setSpinner(false));
  };

  // 4 - Lần đầu khi load trang
  useEffect(() => {
    // Set title cho trang
    document.title = "Danh sách đơn hàng";
    dispatch(resetPage());
    dispatch(resetCurrent());
    getArrs();
  }, []);

  // 5 - Thay đổi API trang xoá
  const changeStateAPI = () => {
    if (deletedPage) {
      setDeletedPage(false);
      setReload(true);
    } else {
      setDeletedPage(true);
      setReload(true);
    }
  };

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys, selectedRows) => {
      setKeySelected(selectedRowKeys);
      setItemSelected(selectedRows);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  return (
    <section className="section section-orders">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Danh sách đơn hàng</h2>
        {/* ======================= Btns Delete & Undo ======================= */}
        <DeleteBtnComponent
          deleted={deletedPage}
          onClickF={() => changeStateAPI()}
          text1="Danh sách đơn hàng đã xoá"
          text2="Danh sách đơn hàng đang có"
        />
      </div>
      {/* ======================= Content ======================= */}
      <div className="page-content">
        {!deletedPage &&
          (userInfo?.role === "Administrator" ||
            userInfo?.role === "Mua hàng") && (
            <TotalBillOrder optionSummary={optionSummary} />
          )}

        {/* ======================= Filter Steps ======================= */}
        <StepsFilter sttArr={listStatusFilter} setReload={setReload} />
        <Actions
          setReload={setReload}
          deletedPage={deletedPage}
          keySelected={keySelected}
          setKeySelected={setKeySelected}
          itemSelected={itemSelected}
          listStatus={listStatus}
          setItemSelected={setItemSelected}
          permission={permission}
          setSearchFilter={setSearchFilter}
          listStaff={listStaff}
        />
        <TableListOrders
          showViewDetailModal={showViewDetailModal}
          statusOptions={listStatus}
          reload={reload}
          setReload={setReload}
          deletedPage={deletedPage}
          rowSelection={rowSelection}
          permission={permission}
          setOptionSummary={setOptionSummary}
          searchFilter={searchFilter}
        />
      </div>
      {/* ======================= Modals ======================= */}
      <div className="modals">
        {orderChoosen && (
          <ViewDetails
            isModalOpen={isViewDetailModalOpen}
            setIsModalOpen={setIsViewDetailModalOpen}
            order={orderChoosen}
            statusOptions={listStatus}
            setOrder={setOrderChoosen}
            deletedPage={deletedPage}
            setReload={setReload}
            permission={permission}
          />
        )}
      </div>
    </section>
  );
}
