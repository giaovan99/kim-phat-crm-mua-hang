import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import {
  MdOutlineBusiness,
  MdOutlineDescription,
  MdOutlineLocalShipping,
  MdOutlinePayment,
} from "react-icons/md";
import ShippingMethod from "../../../Components/OrdersPage/ManageOrder/ShippingMethod/ShippingMethod";
import PaymentMethod from "../../../Components/OrdersPage/ManageOrder/PaymentMethod/PaymentMethod";
import ListCompany from "../../../Components/OrdersPage/ManageOrder/ListCompany/ListCompany";
import InvoiceRequest from "../../../Components/OrdersPage/ManageOrder/InvoiceRequest/InvoiceRequest";
import { getLocalStorage, setLocalStorage } from "../../../utils/localStorage";
import { useSelector } from "react-redux";

export default function ManageOrder() {
  let [keyNumActive, setKeyNumeActive] = useState();
  // Set title cho trang
  useEffect(() => {
    document.title = "Quản lý đơn vị đơn hàng";
  }, []);
  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );
  const changeKeyNum = (activeKey) => {
    setKeyNumeActive(activeKey);
  };

  const items = [
    {
      label: (
        <span className="tab-item">
          <MdOutlineLocalShipping />
          Phương thức giao hàng
        </span>
      ),
      key: 1,
      children: (
        <ShippingMethod
          keyNumActive={keyNumActive}
          keyTab={1}
          permission={permission}
        />
      ),
    },
    {
      label: (
        <span className="tab-item">
          <MdOutlinePayment />
          Phương thức thanh toán
        </span>
      ),
      key: 2,
      children: (
        <PaymentMethod
          keyNumActive={keyNumActive}
          keyTab={2}
          permission={permission}
        />
      ),
    },
    {
      label: (
        <span className="tab-item">
          <MdOutlineDescription />
          Yêu cầu về hoá đơn
        </span>
      ),
      key: 4,
      children: (
        <InvoiceRequest
          keyNumActive={keyNumActive}
          keyTab={4}
          permission={permission}
        />
      ),
    },
    {
      label: (
        <span className="tab-item">
          <MdOutlineBusiness />
          Danh sách công ty
        </span>
      ),
      key: 3,
      children: (
        <ListCompany
          keyNumActive={keyNumActive}
          keyTab={3}
          permission={permission}
        />
      ),
    },
  ];
  return (
    <section className="section section-manage-order">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Quản lý đơn vị đơn hàng</h2>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        <Tabs
          defaultActiveKey={
            getLocalStorage("manageOrder") ? getLocalStorage("manageOrder") : 1
          }
          items={items}
          onChange={(activeKey) => {
            setLocalStorage("manageOrder", activeKey);
            changeKeyNum(activeKey);
          }}
        />
      </div>
    </section>
  );
}
