import { useFormik } from "formik";
import React from "react";
import { HiOutlineSearch } from "react-icons/hi";
import { Select } from "antd";
import { normalize } from "../../../../utils/others";

export default function Actions({
  setReload,
  setSearchFilter,
  listCodeDH,
  listCodeNK,
}) {
  // Khai báo formik
  const formik = useFormik({
    initialValues: {
      keywords: "",
      nk_code: "",
      hd_code: "",
    },

    onSubmit: async (values) => {
      setSearchFilter(values);
      setReload(true);
    },
  });
  const onSearch = (value) => {
    // console.log("search:", value);
  };

  const filterOption = (input, option) =>
    normalize(option?.label ?? "").includes(normalize(input));

  const onChangeSelect = (value, field) => {
    formik.setFieldValue(field, value);
  };
  return (
    <div className="actions">
      {/* ======================= Tìm kiếm ======================= */}
      <div className="filter-search">
        <form className="formSearch" onSubmit={formik.handleSubmit}>
          <div
            className="flex flex-wrap-reverse gap-2"
            style={{ alignItems: "end" }}
          >
            <div className="form-group ">
              <div className="form-style items-end flex-wrap">
                <div>
                  <label>Tìm kiếm mã SP</label>
                  <input
                    id="keywords"
                    name="keywords"
                    type="text"
                    placeholder="Tìm kiếm mã SP"
                    value={formik.values.keywords}
                    onChange={formik.handleChange}
                    className="max-w-[150px]"
                  />
                </div>
                {/* Mã phiếu nhập kho */}
                <div>
                  <label>Mã phiếu nhập kho</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "nk_code")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listCodeNK}
                    value={
                      formik.values.nk_code != "" ? formik.values.nk_code : null
                    }
                    className="w-[150px]"
                  />
                </div>
                {/* Mã đơn hàng */}
                <div>
                  <label>Mã đơn hàng</label>
                  <Select
                    showSearch
                    placeholder="Chọn"
                    optionFilterProp="children"
                    onChange={(value) => onChangeSelect(value, "hd_code")}
                    onSearch={onSearch}
                    filterOption={filterOption}
                    options={listCodeDH}
                    value={
                      formik.values.hd_code != "" ? formik.values.hd_code : null
                    }
                    className="w-[150px]"
                  />
                </div>

                <button className="btn-main-yl btn-actions">
                  <HiOutlineSearch />
                  Tìm
                </button>
                <button
                  className="ant-btn btn-actions text-white bg-black ant-btn-default rounded-lg"
                  type="button"
                  onClick={formik.handleReset}
                >
                  Reset
                </button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  );
}
