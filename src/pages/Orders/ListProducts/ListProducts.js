import React, { useEffect, useState } from "react";
import Actions from "./Components/Action";
import TableListProducts from "./Components/TableListProducts";
import { useDispatch } from "react-redux";
import { resetKeywords } from "../../../redux/slice/keywordsSlice";
import { resetPage } from "../../../redux/slice/pageSlice";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import { FaFilePdf } from "react-icons/fa6";
import { stockImportPrintPO } from "../../../utils/order";

export default function ListProducts() {
  const [reload, setReload] = useState(false);
  const [listIds, setIds] = useState([]); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const [searchFilter, setSearchFilter] = useState({}); //Danh sách loại phiếu
  const [listCodeNK, setListCodeNK] = useState([]); //hiển thị danh sách nhân viên
  const [listCodeDH, setListCodeDH] = useState([]); //Danh sách loại phiếu

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(resetKeywords());
    dispatch(resetPage());
    setReload(true);
  }, []);

  const rowSelection = {
    selectedRowKeys: listIds,
    preserveSelectedRowKeys: true,
    onChange: (selectedRowKeys) => {
      // let newArr = [...listIds, ...selectedRowKeys];
      setIds(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  {
    /* ======================= Start IN  ======================= */
  }
  // Lấy đoạn html
  const getDetailId = async () => {
    if (listIds.length > 0) {
      dispatch(setSpinner(true));
      let res = await stockImportPrintPO({ ids: [...listIds] });
      setDetails(res.data.data);
      dispatch(setSpinner(false));
    }
  };

  const componentRef = React.useRef(null);

  const onBeforeGetContentResolve = React.useRef(null);

  const [loading, setLoading] = React.useState(false);
  const [PPDetails, setDetails] = useState();

  const handleAfterPrint = () => {
    // console.log("`onAfterPrint` called");
    onBeforeGetContentResolve.current = null;
    setLoading(false);
  };

  const handleBeforePrint = React.useCallback(() => {
    // console.log("`onBeforePrint` called");
  }, []);

  const handleOnBeforeGetContent = async () => {
    // console.log("`onBeforeGetContent` called");
    await getDetailId();
    // console.log("dont");
    // return componentRef.current;
  };

  React.useEffect(() => {
    if (loading && onBeforeGetContentResolve.current) {
      // Resolves the Promise, letting `react-to-print` know that the DOM updates are completed
      onBeforeGetContentResolve.current();
    }
  }, [loading]);

  const reactToPrintTrigger = React.useCallback(() => {
    // NOTE: could just as easily return <SomeComponent />. Do NOT pass an `onClick` prop
    // to the root node of the returned component as it will be overwritten.

    // Bad: the `onClick` here will be overwritten by `react-to-print`
    // return <button onClick={() => alert('This will not work')}>Print this out!</button>;

    // Good
    return (
      <button id="btnPdf">
        <FaFilePdf className="text-[#B6190D]" />
      </button>
    );
  }, []);

  {
    /* ======================= End IN  ======================= */
  }

  return (
    <section className="section section-orders">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Sản phẩm nhập kho</h2>
        {/* ======================= Btns Delete & Undo ======================= */}
        {/* <div className="icon-actions space-x-2">
          <Tooltip placement="bottom" title="Xuất file PDF sản phẩm đã chọn">
            <div>
              <ReactToPrint
                content={() => componentRef.current}
                documentTitle="Xuất danh sách sản phẩm nhập kho"
                onAfterPrint={handleAfterPrint}
                onBeforeGetContent={() => {
                  return new Promise(async (resolve) => {
                    await handleOnBeforeGetContent();
                    onBeforeGetContentResolve.current = resolve;
                    setLoading(true);
                  });
                }}
                onBeforePrint={handleBeforePrint}
                removeAfterPrint
                trigger={reactToPrintTrigger}
              />
              <div className="hidden">
                <PrintFromHTML
                  forwardedRef={componentRef}
                  details={PPDetails}
                />
              </div>
            </div>
          </Tooltip>
        </div> */}
      </div>
      {/* ======================= Content ======================= */}
      <div className="page-content listProductsWarehouse">
        {/* ======================= Filter Steps ======================= */}
        <Actions
          setReload={setReload}
          setSearchFilter={setSearchFilter}
          listCodeNK={listCodeNK}
          listCodeDH={listCodeDH}
        />
        {/* ======================= Danh sách sản phẩm nhập kho ======================= */}
        <TableListProducts
          reload={reload}
          setReload={setReload}
          rowSelection={rowSelection}
          searchFilter={searchFilter}
          setListCodeDH={setListCodeDH}
          setListCodeNK={setListCodeNK}
        />
      </div>
    </section>
  );
}
