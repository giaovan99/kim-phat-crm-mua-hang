import React, { useEffect, useState } from "react";
import TableList from "../../../Components/ProductPage/ProductHistory/TableList";
import { useDispatch } from "react-redux";
import { resetPage } from "../../../redux/slice/pageSlice";

const ProductHistory = () => {
  const [reload, setReload] = useState(false);
  const dispatch = useDispatch();
  // Set title cho trang
  useEffect(() => {
    document.title = "Lịch sự cập nhật sản phẩm";
  }, []);
  // Tải lại dữ liệu từ trang 1
  let loadPage = () => {
    // Reset page redux ==1
    dispatch(resetPage());
  };
  return (
    <section className="section section-product">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Lịch sử cập nhật sản phẩm</h2>
      </div>

      {/* ======================= Content ======================= */}
      <div className="page-content">
        <TableList loadPage={loadPage} reload={reload} setReload={setReload} />
      </div>
    </section>
  );
};

export default ProductHistory;
