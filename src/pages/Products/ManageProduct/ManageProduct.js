import { Tabs } from "antd";
import React, { useEffect, useState } from "react";
import { MdOutlineScale, MdOutlinePercent } from "react-icons/md";
import Unit from "../../../Components/ProductPage/ManageProduct/Unit/Unit";
import TaxCode from "../../../Components/ProductPage/ManageProduct/TaxCode/TaxCode";
import { useSelector } from "react-redux";

export default function ManageProduct() {
  let [keyNumActive, setKeyNumeActive] = useState();
  // Set title cho trang
  useEffect(() => {
    document.title = "Quản lý đơn vị sản phẩm";
  }, []);
  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );

  const changeKeyNum = (activeKey) => {
    setKeyNumeActive(activeKey);
  };
  const items = [
    {
      label: (
        <span className="tab-item">
          <MdOutlineScale />
          Quản lý đơn vị tính
        </span>
      ),
      key: 1,
      children: (
        <Unit keyNumActive={keyNumActive} keyTab={1} permission={permission} />
      ),
    },
    {
      label: (
        <span className="tab-item">
          <MdOutlinePercent />
          Quản lý loại hoá đơn
        </span>
      ),
      key: 2,
      children: (
        <TaxCode
          keyNumActive={keyNumActive}
          keyTab={2}
          permission={permission}
        />
      ),
    },
  ];
  return (
    <section className="section section-manage-product">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Quản lý đơn vị sản phẩm</h2>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        <Tabs
          defaultActiveKey={1}
          items={items}
          onChange={(activeKey) => changeKeyNum(activeKey)}
        />
      </div>
    </section>
  );
}
