import React, { useEffect, useState } from "react";
import Actions from "../../../Components/ProductPage/ListProduct/Actions";
import TableListProduct from "../../../Components/ProductPage/ListProduct/TableListProduct";
import ImportFilesComponent from "../../../Components/ImportFilesComponents/ImportFilesComponents";
import ModalProductDetails from "../../../Components/ProductPage/ListProduct/ModalProductDetails";
import { useDispatch, useSelector } from "react-redux";
import { resetPage } from "../../../redux/slice/pageSlice";
import { getUnit, productExport, vatList } from "../../../utils/product";
import { getListSupplier } from "../../../utils/supplier";
import DeleteBtnComponent from "../../../Components/ButtonComponent/DeleteBtnComponent";
import ImportExcelBtnComponent from "../../../Components/ButtonComponent/ImportExcelBtnComponent";
import { setSpinner } from "../../../redux/slice/spinnerSlice";
import ViewHistory from "../../../Components/ProductPage/ListProduct/ViewHistory";
import ShowHistoryBtn from "../../../Components/ButtonComponent/ShowHistory";
import { useNavigate } from "react-router-dom";
import ExportExcelBtnComponent from "../../../Components/ButtonComponent/ExportExcelBtnComponent";
import { normalize } from "../../../utils/others";
import { getListDepartment, getListUser } from "../../../utils/user";

const filterOption = (input, option) =>
  normalize(option?.label ?? "").includes(normalize(input));

export default function Products() {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [isViewImportFile, setIsViewImportFile] = useState(false);
  const [isViewHistory, setIsViewHistory] = useState(false);
  const [productChoosen, setProductChoosen] = useState();
  const [isShowHistoryProductModalOpen, setIsShowHistoryProductModalOpen] =
    useState(false);
  const [reload, setReload] = useState(false);
  const dispatch = useDispatch();
  const [deletedPage, setDeletedPage] = useState(false); // Quản lý việc khách chọn trang Danh sách chưa xoá/ Danh sách đã xoá
  const [listUnit, setListUnit] = useState([]); //Danh sách đơn vị tính
  const [listStaff, setListStaff] = useState([]); //Danh sách người dùng
  const [listVAT, setListVAT] = useState([]); //Danh sách loại hoá đơn
  const [listNCC, setListNCC] = useState([]); //Danh sách nhà cung cấp
  const [keySelected, setKeySelected] = useState([]); //
  const [loadingSelect, setLoadingSelect] = useState(false);
  const [searchFilter, setSearchFilter] = useState({}); //Search filter
  const [listDepartment, setListDepartment] = useState([]); //Danh sách bộ phận

  let navigate = useNavigate();
  // phân quyền
  let permission = useSelector(
    (state) => state.loginInfoSlice.loginInfo.permission
  );

  // Hiển thị modal Thêm/Sửa sản phẩm
  const showModal = (key) => {
    setProductChoosen(key);
    setIsModalOpen(true);
  };

  // Hiển thị modal Lịch sử
  const showHistoryProductModal = (key) => {
    setIsShowHistoryProductModalOpen(true);
    setProductChoosen(key);
  };

  // Lấy danh sách mảng đơn vị tính, loại hoá đơn, nhân viên -> sử dụng trong việc thêm sản phẩm (select options các vị trí)
  const getArrs = async () => {
    dispatch(setSpinner(true));
    let res = await getListUser(1, { department: 4 });
    if (res.status) {
      const transformedArray = res.data.data.map((item) => ({
        value: item.id,
        label: item.full_name,
      }));
      setListStaff(transformedArray);
    }
    res = await getUnit();
    if (res.status) {
      setListUnit(res.data);
    }
    res = await vatList();
    if (res.status) {
      setListVAT(res.data);
    }
    res = await getListSupplier("", { show_all: true });
    if (res.status) {
      setListNCC(res.data);
    }
    // 5.7 - Danh sách bộ phận
    res = await getListDepartment();
    if (res.status) {
      setListDepartment(res.data);
    }
    dispatch(setSpinner(false));
  };

  // Nhập file sản phẩm từ Excel
  const showImportFile = () => {
    setIsViewImportFile(true);
  };

  // Set title cho trang
  useEffect(() => {
    document.title = "Danh sách sản phẩm";
    getArrs();
  }, []);

  // Tải lại dữ liệu từ trang 1
  let loadPage = () => {
    // Reset page redux ==1
    dispatch(resetPage());
  };

  // Thay đổi API danh sách
  const changeStateAPI = () => {
    if (deletedPage) {
      setDeletedPage(false);
      setReload(true);
    } else {
      setDeletedPage(true);
      setReload(true);
    }
  };

  // Thay đổi API danh sách
  const showHistory = () => {
    setIsViewHistory(true);
  };

  // rowSelection object indicates the need for row selection
  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys) => {
      setKeySelected(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };

  const exportFile = async () => {
    const res = await productExport();
    // Tạo thẻ <a> để tải file
    const link = document.createElement("a");
    link.href = res;
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  };

  return (
    <section className="section section-product">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Danh sách sản phẩm</h2>
        {/* ======================= Btns ======================= */}
        <div className="flex gap-2">
          {permission?.product?.create === 1 && (
            <ImportExcelBtnComponent
              deleted={deletedPage}
              onClickF={() => showImportFile()}
              text={"Thêm sản phẩm từ file Excel"}
            />
          )}
          {permission?.product?.create === 1 && (
            <ExportExcelBtnComponent
              deleted={deletedPage}
              onClickF={() => exportFile()}
              text={"Xuất file danh sách sản phẩm"}
            />
          )}
          {permission && permission["product/product_history"]?.read === 1 && (
            <ShowHistoryBtn
              deleted={deletedPage}
              onClickF={() => navigate("/product/product_history")}
              text1="Lịch sử cập nhật sản phẩm"
            />
          )}
          <DeleteBtnComponent
            deleted={deletedPage}
            onClickF={() => changeStateAPI()}
            text1="Danh sách sản phẩm đã xoá"
            text2="Danh sách sản phẩm đang có"
          />
        </div>
      </div>

      {/* ======================= Content ======================= */}
      <div className="page-content">
        <Actions
          showModal={showModal}
          filterOption={filterOption}
          deletedPage={deletedPage}
          setReload={setReload}
          keySelected={keySelected}
          setKeySelected={setKeySelected}
          permission={permission}
          setSearchFilter={setSearchFilter}
          listNCC={listNCC}
          listDepartment={listDepartment}
        />
        <TableListProduct
          showHistoryProductModal={showHistoryProductModal}
          showModal={showModal}
          loadPage={loadPage}
          reload={reload}
          setReload={setReload}
          deletedPage={deletedPage}
          rowSelection={rowSelection}
          setKeySelected={setKeySelected}
          permission={permission}
          searchFilter={searchFilter}
        />
      </div>
      {/* ======================= Modals ======================= */}
      <div className="modals">
        <ModalProductDetails
          isModalOpen={isModalOpen}
          setIsModalOpen={setIsModalOpen}
          product={productChoosen}
          listUnit={listUnit}
          listStaff={listStaff}
          listVAT={listVAT}
          listNCC={listNCC}
          setProduct={setProductChoosen}
          setReload={setReload}
          deletedPage={deletedPage}
          getSupplier={false}
          loadingSelect={loadingSelect}
        />

        {isViewImportFile && (
          <ImportFilesComponent
            isModalOpen={isViewImportFile}
            setIsModalOpen={setIsViewImportFile}
            confirmFileStyle={2}
            title={"Import file sản phẩm"}
            module={"product"}
            setReload={setReload}
          />
        )}
        {isShowHistoryProductModalOpen && (
          <ViewHistory
            isModalOpen={isShowHistoryProductModalOpen}
            setIsModalOpen={setIsShowHistoryProductModalOpen}
            product={productChoosen}
            setProduct={setProductChoosen}
          />
        )}
      </div>
    </section>
  );
}
