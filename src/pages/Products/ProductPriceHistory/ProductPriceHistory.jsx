import React, { useEffect, useState } from "react";
import TableList from "../../../Components/ProductPage/ProductPriceHistory/TableList";
import { useDispatch } from "react-redux";
import { resetPage } from "../../../redux/slice/pageSlice";
import Actions from "../../../Components/ProductPage/ProductPriceHistory/Actions";

const ProductPriceHistory = () => {
  const [reload, setReload] = useState(false);
  const dispatch = useDispatch();
  const [searchFilter, setSearchFilter] = useState({}); //Search filter
  const [keySelected, setKeySelected] = useState([]); //Mảng chứa id các hàng đuuwo

  // Set title cho trang
  useEffect(() => {
    document.title = "Lịch sử thay đổi giá";
  }, []);
  // Tải lại dữ liệu từ trang 1
  let loadPage = () => {
    // Reset page redux ==1
    dispatch(resetPage());
  };
  // rowSelection object indicates the need for row selection
  const rowSelection = {
    selectedRowKeys: keySelected,
    onChange: (selectedRowKeys, selectedRows) => {
      setKeySelected(selectedRowKeys);
    },
    getCheckboxProps: (record) => ({
      // disabled: record.id === "Disabled User",
      // Column configuration not to be checked
      id: record.id,
    }),
    preserveSelectedRowKeys: true,
  };
  return (
    <section className="section section-product">
      {/* ======================= Tilte ======================= */}
      <div className="page-header flex justify-between items-center">
        <h2>Lịch sử thay đổi giá</h2>
      </div>

      {/* ======================= Content ======================= */}
      <div className="page-content">
        <Actions setReload={setReload} setSearchFilter={setSearchFilter} />
        <TableList
          loadPage={loadPage}
          reload={reload}
          setReload={setReload}
          searchFilter={searchFilter}
          rowSelection={rowSelection}
        />
      </div>
    </section>
  );
};

export default ProductPriceHistory;
