import React, { useEffect } from "react";
import { Link, useNavigate } from "react-router-dom";
import { MdPersonOutline, MdPassword, MdOutlineLogin } from "react-icons/md";
import { useFormik } from "formik";
import * as yup from "yup";
import "./login.scss";
import { login } from "../../utils/auth";
import { getProfile } from "../../utils/userProfile";
import { useDispatch } from "react-redux";
import { setInfo } from "../../redux/slice/userProfileSlice";
import { setLoginInfo } from "../../redux/slice/loginInfoSlice";
import { setLocalStorage } from "../../utils/localStorage";
export default function Login() {
  let dispatch = useDispatch();
  // Khai báo navigate
  const navigate = useNavigate();
  // Khai báo formik đăng nhập
  const loginFormik = useFormik({
    // Giá trị nguyên thuỷ
    initialValues: {
      username: "",
      password: "",
    },
    // Validations
    validationSchema: yup.object({
      username: yup.string().required("Nhập tên đăng nhập"),
      password: yup.string().required("Nhập mật khẩu"),
    }),
    // Submit
    onSubmit: async (values) => {
      let api = await login(values);
      if (api) {
        dispatch(setLoginInfo(api.data));
        setLocalStorage("loginInfo", JSON.stringify(api.data));
        // Lấy thông tin người dùng và lưu vào LocalStorage
        fSaveProfile();
        // reset
        resetAll();
        if (api.data.side_bar[0].children) {
          navigate("/" + api.data.side_bar[0].children[0].key);
        } else {
          navigate("/" + api.data.side_bar[0].key);
        }
      }
    },
  });
  const { handleBlur, handleChange, touched, errors } = loginFormik;

  const fSaveProfile = async () => {
    let res = await getProfile();
    // console.log("ok");
    if (res.status) {
      dispatch(setInfo(res.data));
    }
  };

  // Reset warning + formik
  const resetAll = () => {
    document.getElementById("warningAPI").innerText = "";
    loginFormik.resetForm();
  };

  // Set title cho trang
  useEffect(() => {
    document.title = "Đăng nhập";
  }, []);

  // Khi user nhập lại (formik.value thay đổi)
  useEffect(() => {
    document.getElementById("warningAPI").innerHTML = "";
  }, [loginFormik.values]);

  return (
    <section className="login100-form validate-form flex flex-col justify-between section section-login">
      <p className="login100-form-title">Đăng Nhập</p>
      {/* ======================= Form đăng nhập ======================= */}
      <form
        onSubmit={(event) => {
          event.preventDefault();
          loginFormik.handleSubmit();
        }}
        className="accountLayout"
      >
        {/* ======================= Tài khoản ======================= */}
        <div className="form-group ">
          <div className="flex items-center space-x-2">
            <label htmlFor="username">
              <MdPersonOutline />
            </label>
            <input
              className="grow"
              type="text"
              placeholder="Tên đăng nhập"
              name="username"
              id="username"
              value={loginFormik.values.username}
              onChange={loginFormik.handleChange}
              onBlur={handleBlur}
            />
          </div>
          {touched.username && errors.username ? (
            <p className="mt-1 text-red-500 text-sm" id="username-warning">
              {errors.username}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Mật khẩu ======================= */}
        <div className="form-group passwordInput ">
          <div className="flex items-center space-x-2">
            <label htmlFor="password">
              <MdPassword />
            </label>
            <input
              className="grow"
              type="password"
              placeholder="Mật khẩu"
              name="password"
              id="password"
              value={loginFormik.values.password}
              onChange={loginFormik.handleChange}
              onBlur={handleBlur}
            />
          </div>
          {touched.password && errors.password ? (
            <p className="mt-1 text-red-500 text-sm" id="password-warning">
              {errors.password}
            </p>
          ) : (
            <></>
          )}
        </div>
        {/* ======================= Show Errors API ======================= */}
        {errors != {} && (
          <p
            className="my-1 text-red-500 text-sm pl-[40px]"
            id="warningAPI"
          ></p>
        )}
        {/* ======================= btn đăng nhập ======================= */}
        <div>
          <button
            type="sumbit"
            className="w-full rounded bg-yl py-2 font-medium"
          >
            Đăng nhập
          </button>
        </div>
      </form>
      {/* ======================= Quên mật khẩu ======================= */}
      <div className="text-center p-t-12">
        <Link
          to="/forgot-password"
          className="inline-flex gap-2 items-center hover:underline underline-offset-4"
        >
          Quên mật khẩu? <MdOutlineLogin />
        </Link>
      </div>
    </section>
  );
}
