import React, { useEffect, useState } from "react";
import Actions from "../../Components/AdvanceWalletPage/Actions";
import RenderListAdvanceWallet from "../../Components/AdvanceWalletPage/RenderListAdvanceWallet";
import { useDispatch } from "react-redux";
import { resetKeywords } from "../../redux/slice/keywordsSlice";
import { resetPage } from "../../redux/slice/pageSlice";

export default function AdvanceWallet() {
  // 1 - Khai báo state
  const [reload, setReload] = useState(false); //Quản lý việc load lại trang (khi true -> load lại từ trang 1)
  const dispatch = useDispatch();

  // Set title cho trang
  useEffect(() => {
    document.title = "Ví tiền tạm ứng";
    dispatch(resetKeywords());
    dispatch(resetPage());
  }, []);
  return (
    <section className="section section-advance-wallet">
      {/* ======================= Tilte ======================= */}
      <h2 className="page-header">Ví tiền tạm ứng</h2>
      {/* ======================= Contents ======================= */}
      <div className="page-content">
        {/* ======================= Actions ======================= */}
        <Actions setReload={setReload} />
        {/* ======================= Table ======================= */}
        <RenderListAdvanceWallet reload={reload} setReload={setReload} />
      </div>
    </section>
  );
}
