// import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";
import { getLocalStorage } from "../../utils/localStorage";
const initialState = {
  userInfo: getLocalStorage("profile"),
};

export const userProfileSlice = createSlice({
  name: "userProfile",
  initialState,
  reducers: {
    setInfo: (state, actions) => {
      // console.log(actions);
      state.userInfo = actions.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setInfo } = userProfileSlice.actions;

export default userProfileSlice.reducer;
