// import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  loginInfo: {},
  settingInfo: {},
};

export const loginInfoSlice = createSlice({
  name: "loginProfile",
  initialState,
  reducers: {
    setLoginInfo: (state, actions) => {
      // console.log(actions);
      state.loginInfo = actions.payload;
    },
    setSettingInfo: (state, action) => {
      state.settingInfo = action.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setLoginInfo, setSettingInfo } = loginInfoSlice.actions;

export default loginInfoSlice.reducer;
