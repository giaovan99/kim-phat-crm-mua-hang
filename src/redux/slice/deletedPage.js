// import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  value: false,
};

export const deletedPageSlice = createSlice({
  name: "deletedPage",
  initialState,
  reducers: {
    //Khi cần thay đổi
    setDeletedPage: (state, actions) => {
      state.value = actions.payload;
    },
    //Khi load lại trang mới
    resetDeletedPage: (state) => {
      state.value = initialState;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setDeletedPage, resetDeletedPage } = deletedPageSlice.actions;

export default deletedPageSlice.reducer;
