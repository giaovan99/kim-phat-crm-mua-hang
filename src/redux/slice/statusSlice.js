// import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  current: 1,
};

export const statusSlice = createSlice({
  name: "current",
  initialState,
  reducers: {
    //Khi cần keywords thay đổi
    setCurrent: (state, actions) => {
      state.current = actions.payload;
    },
    //Khi load lại trang mới
    resetCurrent: (state) => {
      state.current = 1;
    },
  },
});

// Action creators are generated for each case reducer function
export const { resetCurrent, setCurrent } = statusSlice.actions;

export default statusSlice.reducer;
