// import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";
const initialState = [];

export const listRolesSlice = createSlice({
  name: "listRoles",
  initialState,
  reducers: {
    setListRoles: (state, actions) => {
      state = [...actions.payload];
    },
  },
});

// Action creators are generated for each case reducer function
export const { setListRoles } = listRolesSlice.actions;

export default listRolesSlice.reducer;
