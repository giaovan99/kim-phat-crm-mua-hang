// import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  value: false,
};

export const spinnerSlice = createSlice({
  name: "page",
  initialState,
  reducers: {
    //Khi cần page thay đổi
    setSpinner: (state, actions) => {
      state.value = actions.payload;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setSpinner } = spinnerSlice.actions;

export default spinnerSlice.reducer;
