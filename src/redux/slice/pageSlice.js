// import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  value: 1,
};

export const pageSlice = createSlice({
  name: "page",
  initialState,
  reducers: {
    //Khi cần page thay đổi
    setPage: (state, actions) => {
      state.value = actions.payload;
    },
    //Khi load lại trang mới
    resetPage: (state) => {
      state.value = 1;
    },
  },
});

// Action creators are generated for each case reducer function
export const { setPage, resetPage } = pageSlice.actions;

export default pageSlice.reducer;
