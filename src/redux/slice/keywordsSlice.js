// import { getLocalStorage, setLocalStorage } from "@/utils/localStorage";
import { createSlice } from "@reduxjs/toolkit";
const initialState = {
  keywords: "",
  startDate: "",
  endDate: "",
};

export const keywordsSlice = createSlice({
  name: "keywords",
  initialState,
  reducers: {
    //Khi cần keywords thay đổi
    setKeywords: (state, actions) => {
      state.keywords = actions.payload;
    },
    //Khi cần keywords thay đổi
    setStartDate: (state, actions) => {
      state.startDate = actions.payload;
    },
    //Khi cần keywords thay đổi
    setEndDate: (state, actions) => {
      state.endDate = actions.payload;
    },
    //Khi load lại trang mới
    resetKeywords: (state) => {
      state.keywords = "";
      state.startDate = "";
      state.endDate = "";
    },
  },
});

// Action creators are generated for each case reducer function
export const { setKeywords, resetKeywords, setStartDate, setEndDate } =
  keywordsSlice.actions;

export default keywordsSlice.reducer;
