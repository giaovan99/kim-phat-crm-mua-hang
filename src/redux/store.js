import { configureStore } from "@reduxjs/toolkit";
import pageSlice from "./slice/pageSlice";
import listRolesSlice from "./slice/listRolesSlice";
import userProfileSlice from "./slice/userProfileSlice";
import keywordsSlice from "./slice/keywordsSlice";
import statusSlice from "./slice/statusSlice";
import loginInfoSlice from "./slice/loginInfoSlice";
import spinnerSlice from "./slice/spinnerSlice";

export const store = configureStore({
  reducer: {
    pageSlice,
    listRolesSlice,
    userProfileSlice,
    keywordsSlice,
    statusSlice,
    loginInfoSlice,
    spinnerSlice,
  },
});
