import Products from "../pages/Products/ListProduct/Products";
import ListUser from "../pages/Users/ListUser/ListUser";
import UserAuthorization from "../pages/Users/UserAuthorization/UserAuthorization";
import AdvanceMoney from "../pages/AdvanceMoney/AdvanceMoney";
import Suppliers from "../pages/Suppliers/Suppliers";
import Login from "../pages/Login/Login";
import ErrorPage from "../pages/ErrorPage/ErrorPage";
import MainLayout from "../templates/MainLayout";
import ForgetPassword from "../pages/ForgetPassword/ForgetPassword";
import AccountTemplate from "../templates/AccountTemplate";
import Reimbursement from "../pages/Reimbursement/Reimbursement";
import AdvanceWallet from "../pages/AdvanceWallet/AdvanceWallet";
import PersonalPage from "../pages/PersonalPage/PersonalPage";
import OrderAdd from "../pages/OrderAdd/OrderAdd";
import ManageProduct from "../pages/Products/ManageProduct/ManageProduct";
import Orders from "../pages/Orders/ListOrders/Orders";
import ManageOrder from "../pages/Orders/ManageOrder/ManageOrder";
import OrderFix from "../pages/OrderFix/OrderFix";
import ImportWareHouse from "../pages/Warehouse/ImportWareHouse/ImportWareHouse";
import ListWareHouse from "../pages/Warehouse/ListWareHouse/ListWareHouse";
import ListPurchaseProposals from "../pages/PurchaseProposals/ListPurchaseProposals/ListPurchaseProposals";
import PurchaseProposalAdd from "../pages/PurchaseProposalAdd/PurchaseProposalAdd";
import PurchaseProposalFix from "../pages/PurchaseProposalFix/PurchaseProposalFix";
import SystemParameters from "../pages/Setting/SystemParameters/SystemParameters";
import OptionItem from "../pages/Setting/OptionItem/OptionItem";
import ResetPassword from "../pages/ResetPassword/ResetPassword";
import WarehouseImportAdd from "../pages/WarehouseImportAdd/WarehouseImportAdd";
import WarehouseImportFix from "../pages/WarehouseImportFix/WarehouseImportFix";
import ReimbursementAdd from "../pages/ReimbursementAdd/ReimbursementAdd";
import ReimbursementFix from "../pages/ReimbursementFix/ReimbursementFix";
import ListProducts from "../pages/Orders/ListProducts/ListProducts";
import ReportProducts from "../pages/ReportProducts/ReportProducts";
import ReportProductsPrice from "../pages/ReportProductsPrice/ReportProductsPrice";
import ReportAM from "../pages/ReportAM/ReportAM";
import ProductHistory from "../pages/Products/ProductHistory/ProductHistory";
import ProductPriceHistory from "../pages/Products/ProductPriceHistory/ProductPriceHistory";

export const routes = [
  {
    // Đăng nhập
    path: "/",
    element: <AccountTemplate Components={Login} />,
  },
  {
    // Quên mật khẩu
    path: "/forgot-password",
    element: <AccountTemplate Components={ForgetPassword} />,
  },
  {
    // Lấy lại mật khẩu
    path: "/reset-password",
    element: <AccountTemplate Components={ResetPassword} />,
  },
  {
    // Danh sách sản phẩm
    path: "/product",
    element: <MainLayout Components={Products} />,
  },
  {
    // Quản lý sản phẩm
    path: "/product/manage",
    element: <MainLayout Components={ManageProduct} />,
  },
  {
    // Người dùng
    path: "/users",
    element: <MainLayout Components={ListUser} />,
  },
  {
    // Phân quyền
    path: "/roles",
    element: <MainLayout Components={UserAuthorization} />,
  },
  {
    // Danh sách phiếu nhập kho
    path: "/stock_import",
    element: <MainLayout Components={ImportWareHouse} />,
  },
  {
    // Tạo phiếu nhập kho
    path: "/stock_import/add",
    element: <MainLayout Components={WarehouseImportAdd} />,
  },
  {
    // Sửa phiếu nhập kho
    path: "/stock_import/fix/:importKey",
    element: <MainLayout Components={WarehouseImportFix} />,
  },
  {
    // Kho hàng
    path: "/warehouse",
    element: <MainLayout Components={ListWareHouse} />,
  },
  {
    // Danh sách phiếu tạm ứng
    path: "/requests_for_advance",
    element: <MainLayout Components={AdvanceMoney} />,
  },
  {
    // Danh sách nhà cung cấp
    path: "/suppliers",
    element: <MainLayout Components={Suppliers} />,
  },
  {
    // Danh sách đơn hàng
    path: "/purchase_order",
    element: <MainLayout Components={Orders} />,
  },
  {
    // Danh sách đơn hàng
    path: "/purchase_order/purchase_order_stock_import",
    element: <MainLayout Components={ListProducts} />,
  },
  {
    // Quản lý đơn vị đơn hàng
    path: "/purchase_order/manage",
    element: <MainLayout Components={ManageOrder} />,
  },
  {
    // Tạo đơn hàng
    path: "/purchase_order/add",
    element: <MainLayout Components={OrderAdd} />,
  },
  {
    // Sửa đơn hàng
    path: "/purchase_order/fix/:orderKey",
    element: <MainLayout Components={OrderFix} />,
  },
  {
    // Danh sách đề nghị mua hàng
    path: "/purchase_proposal",
    element: <MainLayout Components={ListPurchaseProposals} />,
  },
  {
    // Tạo đề nghị mua hàng
    path: "/purchase_proposal/add",
    element: <MainLayout Components={PurchaseProposalAdd} />,
  },
  {
    // Sửa đề nghị mua hàng
    path: "/purchase_proposal/fix/:proposalKey",
    element: <MainLayout Components={PurchaseProposalFix} />,
  },
  {
    // Danh sách hoàn ứng
    path: "/payment_request",
    element: <MainLayout Components={Reimbursement} />,
  },
  {
    // Tạo hoàn ứng
    path: "/payment_request/add",
    element: <MainLayout Components={ReimbursementAdd} />,
  },
  {
    // Sửa hoàn ứng
    path: "/payment_request/fix/:importKey",
    element: <MainLayout Components={ReimbursementFix} />,
  },
  {
    // Ví tiền tạm ứng
    path: "/requests_for_advance/advance_wallet",
    element: <MainLayout Components={AdvanceWallet} />,
  },
  {
    // Trang cá nhân
    path: "/profile",
    element: <MainLayout Components={PersonalPage} />,
  },
  {
    // Cấu hình
    path: "/config",
    element: <MainLayout Components={SystemParameters} />,
  },
  {
    // Thông số hệ thống
    path: "/option_items_keynum",
    element: <MainLayout Components={OptionItem} />,
  },
  {
    // BÁO CÁO TỔNG HỢP HÀNG HÓA, VẬT TƯ, MMTB CỦA CÁC BỘ PHẬN
    path: "/report_products",
    element: <MainLayout Components={ReportProducts} />,
  },
  {
    // BÁO CÁO TỔNG HỢP HÀNG HÓA, VẬT TƯ, MMTB CỦA CÁC BỘ PHẬN
    path: "/report_products_price",
    element: <MainLayout Components={ReportProductsPrice} />,
  },
  {
    // BÁO CÁO TỔNG HỢP HÀNG HÓA, VẬT TƯ, MMTB CỦA CÁC BỘ PHẬN
    path: "/report_am_and_pr",
    element: <MainLayout Components={ReportAM} />,
  },
  {
    // Lịch sử cập nhật sản phẩm
    path: "/product/product_history",
    element: <MainLayout Components={ProductHistory} />,
  },
  {
    // Lịch sử thay đổi giá
    path: "/product_price_history",
    element: <MainLayout Components={ProductPriceHistory} />,
  },
  {
    // Trang không tồn tại
    path: "*",
    element: <ErrorPage />,
  },
];
