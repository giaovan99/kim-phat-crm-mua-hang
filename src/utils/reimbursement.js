import Swal from "sweetalert2";
import {
  convertArr,
  convertArrStt,
  convertObjectToArray,
  convertSlipToArray,
  convertSlipToArrayNotNumber,
  getToken,
  showError,
} from "./others";
import { reimbursementService } from "../services/reimbursementService";

// 1 - Import file lên sever
export const importFile = async (data) => {
  try {
    // Lấy token từ localStorage
    // console.log(data);
    let token = getToken();
    let res = await reimbursementService.importFile({
      token: token,
      file: data.file,
    });
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 1 - Import file lên sever
export const accessFile = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await reimbursementService.accessFile({
      token: token,
      ...data,
    });
    return res;
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 1 - Import file lên sever
export const processFile = async (data) => {
  try {
    // Lấy token từ localStorage
    // console.log(data);
    let token = getToken();
    let res = await reimbursementService.processFile({
      token: token,
      ...data,
    });
    return res;
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 2 - Lấy danh sách phiếu hoàn ứng
export const getReimbursementList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await reimbursementService.reimbursementList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        if (res.data.data.data) {
          return { status: true, data: res.data.data };
        } else {
          return {
            status: true,
            data: convertArr(res.data.data),
          };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 8 - Lấy thông tin phiếu hoàn ứng cụ thể
export const getReimbursemenId = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await reimbursementService.getReimbursementSlip({
      token: token,
      id: id,
    });
    // console.log(res);
    return res;
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 9 - Cập nhật phiếu hoàn ứng
export const updateReimbursementSlip = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await reimbursementService.updateReimbursementSlip({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật phiếu hoàn ứng",
          text: "Cập nhật phiếu hoàn ứng thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật phiếu hoàn ứng",
        text: "Cập nhật phiếu hoàn ứng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 10 - Xoá phiếu hoàn ứng
export const deleteReimbursementSlipId = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await reimbursementService.deleteReimbursementSlip({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá phiếu hoàn ứng",
          text: "Xoá phiếu hoàn ứng thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá phiếu hoàn ứng",
        text: "Xoá phiếu hoàn ứng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 12 - Tạo phiếu hoàn ứng
export const createReimbursementSlip = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await reimbursementService.createReimbursementSlip({
      ...values,
      token: token,
    });
    if (res.data.success) {
      Swal.fire({
        title: "Tạo phiếu hoàn ứng",
        text: "Tạo phiếu hoàn ứng thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 13 - Lấy danh sách staff
export const getStaffReimbursement = async (page = 1) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await reimbursementService.reimbursementList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      let data = [];
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      return {
        status: true,
        data: convertObjectToArray(res?.data?.options?.staff),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 13 - Lấy danh sách các options của hoàn ứng
export const getPOReimbursement = async (page = 1) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await reimbursementService.reimbursementList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      return {
        status: true,
        data: convertSlipToArray(res?.data?.options?.purchase_order),
        options: {
          purchase_order: convertSlipToArrayNotNumber(
            res?.data?.options?.purchase_order
          ),
          staff: convertObjectToArray(res?.data?.options?.staff),
        },
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 13 - Lấy danh sách tạm ứng được phép
export const getRAReimbursement = async (page = 1) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await reimbursementService.reimbursementList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      return {
        status: true,
        data: convertArr(res?.data?.options?.requests_for_advance),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 1 - Lấy danh sách trạng thái
export const getReimbursementStatusList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await reimbursementService.reimbursementStatus({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArrStt(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 11 - Xuất file export table sản phẩm
export const reimbursementExport = async (keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await reimbursementService.reimbursementExport({
      id: keysParam.id,
      type: keysParam.type,
      token,
    });
    if (res.data.success) {
      return res.data.data.file;
    } else {
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
  }
};
