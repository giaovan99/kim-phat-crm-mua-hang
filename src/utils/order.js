import Swal from "sweetalert2";
import { orderService } from "../services/orderService";
import {
  convertArr,
  convertArrStt,
  convertObjectToArray,
  getToken,
  showError,
} from "./others";

// 1 - Lấy danh sách phương thức vận chuyển
export const getShippingMethodList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.shippingMethodList({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArr(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};
// 2 - Xoá phương thức vận chuyển
export const deletedShippingMethod = async (data, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.updateShippingMethod({
      ...data,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá phương thức vận chuyển",
          text: "Xoá phương thức vận chuyển thành công!",
          icon: "success",
        });
      }

      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};
// 3 - Cập nhật phương thức vận chuyển
export const updateShippingMethod = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.updateShippingMethod({
      ...data,
      token: token,
    });

    if (res.data.success) {
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 4 - Lấy danh sách phương thức thanh toán
export const getPaymentMethodList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.paymentMethodList({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArr(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};
// 5 - Xoá phương thức thanh toán
export const deletedPaymentMethod = async (data, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.updatePaymentMethod({
      ...data,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá phương thức thanh toán",
          text: "Xoá phương thức thanh toán thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};
// 6 - Cập nhật phương thức thanh toán
export const updatePaymentMethod = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.updatePaymentMethod({
      ...data,
      token: token,
    });

    if (res.data.success) {
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 4 - Lấy danh sách yêu cầu về hoá đơn
export const getInvoiceRequestList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.invoiceRequestList({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArr(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};
// 5 - Xoá yêu cầu về hoá đơn
export const deletedInvoiceRequest = async (data, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.updateInvoiceRequest({
      ...data,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá yêu cầu về hoá đơn",
          text: "Xoá yêu cầu về hoá đơn thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};
// 6 - Cập nhật yêu cầu về hoá đơn
export const updateInvoiceRequest = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.updateInvoiceRequest({
      ...data,
      token: token,
    });

    if (res.data.success) {
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 7 - Lấy danh sách trạng thái đơn hàng
export const getOrderStatusList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.orderStatus({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArrStt(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 8 - Lấy danh sách đơn hàng
export const getOrderList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await orderService.orderList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        if (res.data.data.data) {
          return {
            status: true,
            data: res.data.data,
            option: res.data.options,
          };
        } else {
          return {
            status: true,
            data: convertArr(res.data.data),
          };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};
// 8 - Lấy danh sách đơn hàng trong phiếu đề nghị mua hàng
export const getOrderList2 = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await orderService.orderList2({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        if (res.data.data.data) {
          return {
            status: true,
            data: res.data.data,
          };
        } else {
          return {
            status: true,
            data: convertArr(res.data.data),
          };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 9 - Lấy thông tin đơn hàng cụ thể
export const getOrder = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await orderService.getOrder({
      token: token,
      id: id,
    });
    return res;
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 10 - Cập nhật đơn hàng
export const updateOrder = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.updateOrder({ ...values, token: token });

    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật đơn hàng",
          text: "Cập nhật đơn hàng thành công!",
          icon: "success",
        });
      }

      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật đơn hàng",
        text: "Cập nhật đơn hàng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 11 - Xoá đơn hàng
export const deleteOrderId = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.deleteOrder({ ...values, token: token });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá đơn hàng",
          text: "Xoá đơn hàng thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá đơn hàng",
        text: "Xoá đơn hàng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 12 - Tạo đơn hàng
export const createOrder = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await orderService.createOrder({ ...values, token: token });
    if (res.data.success) {
      Swal.fire({
        title: "Tạo đơn hàng",
        text: "Tạo đơn hàng thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 13 - Lấy danh sách staff
export const getStaffOrder = async (page = 1) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.orderList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      return {
        status: true,
        data: convertObjectToArray(res.data.options.staff),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 1 - Lấy danh sách cty
export const companyList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data
      let res = await orderService.companyList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        // console.log(res.data.data);
        if (res.data.data.data) {
          return {
            status: true,
            data: convertArr(res.data.data.data),
            dataRaw: res.data.data,
          };
        } else {
          return {
            status: true,
            data: convertArr(res.data.data),
          };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 2 - Lấy thông tin 1 cty cụ thể  => cần fix
export const getCompany = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await orderService.getCompany({
      token: token,
      id: id,
    });
    return res;
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 3 - Cập nhật cty
export const companyUpdate = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.companyUpdate({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật cty",
          text: "Cập nhật cty thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật cty",
        text: "Cập nhật cty thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 5 - Thêm cty
export const createCompany = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await orderService.createCompany({
      ...values,
      token: token,
    });
    if (res.data.success) {
      Swal.fire({
        title: "Thêm cty",
        text: "Thêm cty thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      Swal.fire({
        title: "Thêm cty thất bại",
        text: "Thêm cty thất bại",
        icon: "error",
        confirmButtonText: "Đóng",
      });
      console.log(res);
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 6 - Xoá cty
export const deleteCompanyId = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await orderService.deleteCompany({
      ...values,
      token: token,
    });
    console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá cty",
          text: "Xoá cty thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá cty",
        text: "Xoá cty thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 7 - Lấy danh sách sản phẩm nhập kho từ đơn hàng
export const stockImportPartListPO = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await orderService.stockImportPartListPO({
        token: token,
        page: page,
        ...keysParam,
      });

      if (res.data.success) {
        if (res.data.data.data) {
          return {
            status: true,
            data: res.data.data,
            options: res.data.options,
          };
        } else {
          return {
            status: true,
            data: convertArr(res.data.data),
            options: res.data.options,
          };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 1 - In danh sách sản phẩm nhập kho từ đơn hàng
export const stockImportPrintPO = async (ids) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log("đẩy lên sever", {
    //   token: token,
    //   ...ids,
    // });
    let res = await orderService.stockImportPrintPO({
      token: token,
      ...ids,
    });
    return res;
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 11 - Xuất file excel cho đơn hàng
export const orderExport = async (keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await orderService.orderExport({
      id: keysParam.id,
      type: keysParam.type,
      token,
    });
    if (res.data.success) {
      return res.data.data.file;
    } else {
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
  }
};
