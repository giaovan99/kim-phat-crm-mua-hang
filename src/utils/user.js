import Swal from "sweetalert2";
import { userService } from "../services/userService";
import { convertArr, getToken, showError } from "./others";

// 1 - Lấy danh sách người dùng
export const getListUser = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await userService.userList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        return { status: true, data: res.data.data };
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 1.1 - Lấy thông tin 1 user cụ thể  => cần fix
export const getUser = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await userService.getUser({
      token: token,
      id: id,
    });
    return res;
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 2 - Cập nhật người dùng
export const updateUser = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await userService.userUpdate({ ...values, token: token });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật người dùng",
          text: "Cập nhật người dùng thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật người dùng",
        text: "Cập nhật người dùng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 5 - Thêm người dùng
export const createUser = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await userService.createUser({ ...values, token: token });
    if (res.data.success) {
      Swal.fire({
        title: "Thêm người dùng",
        text: "Thêm người dùng thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      Swal.fire({
        title: "Thêm người dùng thất bại",
        text: `${res.data.error.username[0]}`,
        icon: "error",
        confirmButtonText: "Đóng",
      });
      console.log(res);
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 3 - Lấy danh sách chức vụ
export const getListPosition = async (page, keyParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await userService.positionList({
      token: token,
      ...keyParam,
    });
    // console.log(res);
    if (res.data.success) {
      return {
        status: true,
        data: convertArr(res.data.data),
      };
    } else {
      console.log(res);
      showError();
      return { status: false };
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 4 - Lấy danh sách bộ phận
export const getListDepartment = async (page, keyParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await userService.departmentList({
      token: token,
      ...keyParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArr(res.data.data),
      };
    } else {
      console.log(res);
      showError();
      return { status: false };
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 6 - Xoá người dùng
export const deleteUserId = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await userService.deleteUsser({ ...values, token: token });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá người dùng",
          text: "Xoá người dùng thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá người dùng",
        text: "Xoá người dùng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 7 - convert các mảng keys ra object
export const otherKeys = (array) => {
  return array.reduce((acc, key) => {
    acc[key] = true;
    return acc;
  }, {});
};

// 10 - Xoá chức vụ
export const deletedPosition = async (data, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await userService.updatePosition({ ...data, token: token });
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá chức vụ",
          text: "Xoá chức vụ thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 11 - Cập nhật chức vụ
export const updatePosition = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await userService.updatePosition({ ...data, token: token });
    if (res.data.success) {
      Swal.fire({
        title: "Cập nhật chức vụ",
        text: "Cập nhật chức vụ thành công!",
        icon: "success",
      });
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 12 - Xoá bộ phận
export const deletedDepartment = async (data, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(data);
    let res = await userService.updateDepartment({ ...data, token: token });
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá bộ phận",
          text: "Xoá bộ phận thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 13 - Cập nhật bộ phận
export const updateDepartment = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await userService.updateDepartment({ ...data, token: token });
    if (res.data.success) {
      Swal.fire({
        title: "Cập nhật bộ phận",
        text: "Cập nhật bộ phận thành công!",
        icon: "success",
      });
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};
