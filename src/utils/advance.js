import Swal from "sweetalert2";
import { advanceService } from "../services/advanceService";
import {
  convertArr,
  convertObjectToArray,
  getToken,
  showError,
} from "./others";

// 1 - Lấy danh sách ví tiền tạm ứng
export const getAdvanceWalletList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();

    let res = await advanceService.advanceWalletList({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArr(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    return { status: false };
  }
};

// 2 - Lấy danh sách phiếu tạm ứng
export const getAdvanceList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage

    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await advanceService.advanceList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        if (res.data.data.data) {
          return {
            status: true,
            data: res.data.data,
            options: {
              staff: convertObjectToArray(res.data.options.staff),
              status: convertObjectToArray(res.data.options.status),
            },
          };
        } else {
          return {
            status: true,
            data: convertArr(res.data.data),
            options: {
              staff: convertObjectToArray(res.data.options.staff),
              status: convertObjectToArray(res.data.options.status),
            },
          };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 3 - Lấy thông tin phiếu tạm ứng cụ thể
export const getAdvanceSlip = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await advanceService.getAdvanceSlip({
      token: token,
      id: id,
    });
    return res;
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 10 - Cập nhật phiếu tạm ứng
export const updateAdvanceSlip = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await advanceService.updateAdvanceSlip({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật phiếu tạm ứng",
          text: "Cập nhật phiếu tạm ứng thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật phiếu tạm ứng",
        text: "Cập nhật phiếu tạm ứng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 11 - Xoá phiếu tạm ứng
export const deleteAdvanceSlipId = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await advanceService.deleteAdvanceSlip({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá phiếu tạm ứng",
          text: "Xoá phiếu tạm ứng thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá phiếu tạm ứng",
        text: "Xoá phiếu tạm ứng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 12 - Tạo phiếu tạm ứng
export const createAdvanceSlip = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await advanceService.createAdvanceSlip({
      ...values,
      token: token,
    });
    if (res.data.success) {
      Swal.fire({
        title: "Tạo phiếu tạm ứng",
        text: "Tạo phiếu tạm ứng thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 13 - Lấy danh sách trạng thái phiếu tạm ứng
export const getAdvanceStatusList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await advanceService.advanceList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      // console.log(res);
      return {
        status: true,
        data: convertObjectToArray(res.data.options.status),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    console.log(err);
    return { status: false };
  }
};
// 13 - Lấy danh sách loại phiếu tạm ứng
export const getTypeAM = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await advanceService.getTypeAM({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArr(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 13 - Lấy danh sách staff
export const getStaffAdvance = async (page = 1) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await advanceService.advanceList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      return {
        status: true,
        data: convertObjectToArray(res.data.options.staff),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};
