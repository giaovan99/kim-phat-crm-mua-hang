import Swal from "sweetalert2";
import { getToken, showError } from "./others";
import { settingService } from "../services/settingService";
import { throwException } from "./importFile";

// 1 - Lấy thông tin setting
export const getSetting = async () => {
  let token = getToken();
  try {
    // Lấy token từ localStorage
    if (token) {
      // Gửi token lên BE -> lấy data danh sách NCC
      let res = await settingService.getSetting({
        token: token,
      });
      // console.log(res);
      if (res.data.success) {
        return { status: true, data: res.data.data };
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    showError();
    return { status: false };
  }
};

// 2 - Cập nhật setting
export const updateSetting = async (values) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await settingService.updateSetting({
      data: [...values],
      token: token,
    });
    if (res.data.success) {
      Swal.fire({
        title: "Cập nhật",
        text: "Cập nhật thành công!",
        icon: "success",
      });
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật",
        text: "Cập nhật thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    showError();
    return { status: false };
  }
};

// Lấy danh sách options
export const getOptions = async (page = 1, keyParam) => {
  let token = getToken();
  // Lấy token từ localStorage
  if (token) {
    // Gửi token lên BE -> lấy data danh sách NCC
    let res = await settingService.getOptions({
      token: token,
      ...keyParam,
    });
    // console.log(res);
    if (res.data.success) {
      return { status: true, data: res.data.data };
    } else {
      console.log(res);
      showError();
      return { status: false };
    }
  }
};

//Lấy thông tin option cụ thể
export const getOptionId = async (id) => {
  let token = getToken();
  // Lấy token từ localStorage
  if (token) {
    // Gửi token lên BE -> lấy data danh sách NCC
    let res = await settingService.getOptionId({
      token: token,
      id,
    });
    // console.log(res);
    if (res.data.success) {
      return { status: true, data: res.data.data };
    } else {
      console.log(res);
      showError();
      return { status: false };
    }
  }
};

// Tạo thông số
export const createOption = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await settingService.createOption({ ...values, token: token });
    // console.log(res.data.success);
    if (res.data.success) {
      Swal.fire({
        title: "Tạo thông số",
        text: "Tạo thông số thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      return true;
    } else {
      Swal.fire({
        title: "Tạo thông số",
        text: `Tạo thông số thất bại`,
        icon: "error",
        confirmButtonText: "Đóng",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError(); showError();
    return false;
  }
};

// Cập nhật thông số
export const updateOption = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await settingService.updateOption({ ...values, token: token });
    if (res.data.success) {
      Swal.fire({
        title: "Cập nhật thông số",
        text: "Cập nhật thông số thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      return true;
    } else {
      Swal.fire({
        title: "Cập nhật thông số",
        text: `Cập nhật thông số thất bại`,
        icon: "error",
        confirmButtonText: "Đóng",
      });
      console.log(res);
      return false;
    }
  } catch (err) {
    console.log(err);
    showError();
    return false;
  }
};
