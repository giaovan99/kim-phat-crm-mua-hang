import { getLocalStorage, removeLocalStorage } from "./localStorage";
import Swal from "sweetalert2";

// Step: Check đăng nhập -----(Y)----> Check thời hạn truy cập -----(Y)----> true
//                      |                                     |
//                      |                                      -----(N)----> false
//                       -----(N)----> false

const checkLogin = () => {
  // lấy token_exprise từ localStorage
  let token = getLocalStorage("access_token");

  // Kiểm tra xem đã đăng nhập chưa
  if (!token) {
    return false;
  } else {
    // Kiểm tra hạn truy cập
    // Lấy giá trị thời gian hiện tại
    let time = new Date().getTime();

    // Check xem còn hạn truy cập không
    if (token.expiry - time <= 0) {
      removeLocalStorage("access_token");
      return false;
    } else {
      return true;
    }
  }
};

export default checkLogin;
