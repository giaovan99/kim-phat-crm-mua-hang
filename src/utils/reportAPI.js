import { reportService } from "../services/reportService";
import { throwException } from "./importFile";
import { getToken, showError } from "./others";

// 1 - Lấy danh sách report theo san pham
export const reportProductList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await reportService.reportProductList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        return { status: true, data: res.data.data };
      }
    }
  } catch (err) {
    console.log(err);
    throwException({
      data: keysParam,
      message: err,
    });
    showError();
    return { status: false };
  }
};
// 2 - Xuất danh sách report theo san pham
export const exportReportProduct = async (keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await reportService.exportReportProduct({
        token: token,
        ...keysParam,
      });
      if (res.data.success) {
        return { status: true, data: res.data.data };
      }
    }
  } catch (err) {
    console.log(err);
    throwException({
      data: keysParam,
      message: err,
    });
    showError();
    return { status: false };
  }
};

// 3 - Lấy danh sách report theo san pham theo giá
export const reportProductPriceList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await reportService.reportProductPriceList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        return { status: true, data: res.data.data };
      }
    }
  } catch (err) {
    console.log(err);
    throwException({
      data: keysParam,
      message: err,
    });
    showError();
    return { status: false };
  }
};
// 4 - Xuất danh sách report theo san pham theo giá
export const exportReportProductPrice = async (keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await reportService.exportReportProductPrice({
        token: token,
        ...keysParam,
      });
      if (res.data.success) {
        return { status: true, data: res.data.data };
      }
    }
  } catch (err) {
    console.log(err);
    throwException({
      data: keysParam,
      message: err,
    });
    showError();
    return { status: false };
  }
};
// 5 - Lấy danh sách report theo phieu tam ung va hoan ung
export const reportAMList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await reportService.reportAMList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        return { status: true, data: res.data };
      }
    }
  } catch (err) {
    console.log(err);
    throwException({
      data: keysParam,
      message: err,
    });
    showError();
    return { status: false };
  }
};
// 6 - Xuất danh sách report theo san pham theo phieu tam ung va hoan ung
export const exportReportAM = async (keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await reportService.exportReportAM({
        token: token,
        ...keysParam,
      });
      if (res.data.success) {
        return { status: true, data: res.data.data };
      }
    }
  } catch (err) {
    console.log(err);
    throwException({
      data: keysParam,
      message: err,
    });
    showError();
    return { status: false };
  }
};
