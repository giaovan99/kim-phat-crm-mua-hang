// Set local storage
export const setLocalStorage = (name, value) => {
  return localStorage.setItem(name, value);
};

//   get local storage
export const getLocalStorage = (name) => {
  if (typeof window !== "undefined") {
    // Client-side-only code
    return localStorage.getItem(name)
      ? JSON.parse(localStorage.getItem(name) || "")
      : null;
  }
};

// remove local storage
export const removeLocalStorage = (name) => {
  if (typeof window !== "undefined") {
    // Client-side-only code
    return localStorage.removeItem(name);
  }
};

export function extractLinkInfo(htmlString) {
  // Tạo một phần tử div ẩn để chuyển đổi chuỗi HTML thành DOM
  const tempDiv = document.createElement("div");
  tempDiv.innerHTML = htmlString;

  // Lấy phần tử con đầu tiên của div (chỉ giả sử có một phần tử con)
  const firstChild = tempDiv.firstChild;

  // Lấy giá trị thuộc tính 'to'
  const to = firstChild.getAttribute("to");

  // Lấy nội dung của phần tử
  const content = firstChild.textContent;

  // Trả về đối tượng chứa thông tin đường dẫn và nội dung
  return { to, content };
}
