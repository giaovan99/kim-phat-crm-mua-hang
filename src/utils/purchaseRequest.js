import Swal from "sweetalert2";
import { purchaseRequestService } from "../services/purchaseRequestService";
import {
  convertArrStt,
  convertObjectToArray,
  getToken,
  showError,
} from "./others";

// 1 - Lấy danh sách trạng thái đơn đề nghị mua hàng
export const getPurchaseRequestStatusList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await purchaseRequestService.requestStatus({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArrStt(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 2 - Lấy danh sách đơn đề nghị mua hàng
export const getPurchaseRequestList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await purchaseRequestService.requestList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        return { status: true, data: res.data.data, options: res.data.options };
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 3 - Lấy thông tin đơn đề nghị mua hàng cụ thể
export const getPurchaseRequest = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await purchaseRequestService.getPurchaseRequest({
      token: token,
      id: id,
    });
    return res;
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 4 - Cập nhật đơn đề nghị mua hàng
export const updatePurchaseRequest = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await purchaseRequestService.updatePurchaseRequest({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật đề nghị mua hàng",
          text: "Cập nhật đề nghị mua hàng thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật đề nghị mua hàng",
        text: "Cập nhật đề nghị mua hàng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 5 - Xoá đề nghị mua hàng
export const deletePurchaseRequestId = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await purchaseRequestService.deletePurchaseRequest({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá đề nghị mua hàng",
          text: "Xoá đề nghị mua hàng thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá đề nghị mua hàng",
        text: "Xoá đề nghị mua hàng thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 12 - Tạo đơn đề nghị mua hàng
export const createPurchaseRequest = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await purchaseRequestService.createPurchaseRequest({
      ...values,
      token: token,
    });
    if (res.data.success) {
      Swal.fire({
        title: "Tạo đề nghị mua hàng",
        text: "Tạo đề nghị mua hàng thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 13 - Lấy danh sách staff
export const getStaffPurchaseRequest = async (page = 1) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await purchaseRequestService.requestList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      return {
        status: true,
        data: convertObjectToArray(res.data.options.staff),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 13 - Lấy loại Đề nghị
// 1 - Lấy danh sách trạng thái đơn đề nghị mua hàng
export const getTypePP = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await purchaseRequestService.requestType({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArrStt(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

export const printPP = async (value) => {
  try {
    let token = getToken();
    let res = await purchaseRequestService.printPP({
      token: token,
      ...value,
    });
    // if (res.data.success) {
    //   return {
    //     status: true,
    //     data: res.data.data,
    //   };
    // } else {
    //   showError();
    //   console.log(res);
    //   return { status: false };
    // }
    return res;
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 11 - Xuất file excel cho Đề nghị mua hàng
export const ppExport = async (keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await purchaseRequestService.ppExport({
      id: keysParam.id,
      type: keysParam.type,
      token,
    });
    if (res.data.success) {
      return res.data.data.file;
    } else {
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
  }
};
