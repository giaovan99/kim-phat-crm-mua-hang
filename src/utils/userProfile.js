import Swal from "sweetalert2";
import { setLocalStorage } from "./localStorage";
import { userProfileService } from "../services/userProfileService";
import { getToken, showError } from "./others";

// 1 - Đổi mật khẩu
export const changePwd = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    if (token) {
      let res = await userProfileService.changePassword({
        ...values,
        token: token,
      });
      if (res.data.success) {
        // console.log("first");
        Swal.fire({
          title: "Thành công",
          text: "Đổi mật khẩu thành công!",
          icon: "success",
        });
        formik.resetForm();
      } else {
        console.log("err");
        Swal.fire({
          title: "Oops...",
          text: (
            <>
              Đã có lỗi xảy ra!
              <br /> JSON.stringify(res.error)
            </>
          ),
          icon: "error",
        });
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 2 - Cập nhật thông tin
export const updateProfile = async (values) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await userProfileService.updateProfile({
      ...values,
      token: token,
    });
    if (res.data.active) {
      return true;
    } else {
      // console.log("that bai 1");
      console.log(res);
      showError();
    }
  } catch (err) {
    // console.log("that bai 2");
    console.log(err);
    // showError();
  }
};

// 3 - Lấy thông tin user
export const getProfile = async () => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await userProfileService.profile(token);
    if (res.status == 200) {
      // return res.data;
      setLocalStorage("profile", JSON.stringify(res.data));
      return { status: true, data: res.data };
    }
  } catch (err) {
    // showError();
  }
};
