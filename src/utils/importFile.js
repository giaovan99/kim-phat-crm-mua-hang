import { importFileService } from "../services/importFileService";
import { getToken } from "./others";

// 3 - Cập nhật phương thức vận chuyển
export const throwException = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await importFileService.throwException({
      ...data,
      token: token,
    });
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};
