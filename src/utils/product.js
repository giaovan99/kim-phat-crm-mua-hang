import Swal from "sweetalert2";
import {
  convertArr,
  convertObjectToArray,
  getToken,
  showError,
} from "./others";
import { productService } from "../services/productService";

// 1 - Lấy danh sách sản phẩm
export const getListProduct = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    if (token) {
      // Gửi token lên BE -> lấy data danh sách NCC
      let res = await productService.productList({
        token: token,
        page: page,
        ...keysParam,
      });
      // console.log(res);
      if (res.data.success) {
        if (res.data.data.data) {
          return {
            status: true,
            data: convertArr(res.data.data.data),
            rawData: res.data.data,
          };
        } else {
          return { status: true, data: convertArr(res.data.data) };
        }
      } else {
        showError();
        console.log(res);
        return { status: false };
      }
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 2 - Lấy danh sách staff
export const getStaff = async (page = 1, department = "") => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await productService.productList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      return {
        status: true,
        data: convertObjectToArray(res.data.options.staff),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 3 - Lấy danh sách đơn vị tính
export const getUnit = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await productService.unitList({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArr(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 4 - Lấy danh sách loại hoá đơn
export const vatList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await productService.vatList({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      let arrNew = [];
      res.data.data.forEach((item) =>
        arrNew.push({
          ...item,
          vat: item.value,
          value: item.key,
          label: item.name,
        })
      );
      return {
        status: true,
        data: arrNew,
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 5 - Xoá sản phẩm
export const deleteProductId = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await productService.deleteProduct({ ...values, token: token });
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá sản phẩm",
          text: "Xoá sản phẩm thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá sản phẩm",
        text: "Xoá sản phẩm thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 6 - Cập nhật sản phẩm
export const updateProduct = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await productService.updateProduct({ ...values, token: token });
    // console.log(res);
    if (!res.data.success && res.data.status === "warning") {
      Swal.fire({
        title: "Cập nhật sản phẩm",
        text: `${res.data.message}`,
        icon: "warning",
        confirmButtonText: "Đóng",
      });
    } else if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật sản phẩm",
          text: "Cập nhật sản phẩm thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật sản phẩm",
        text: "Cập nhật sản phẩm thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 8 - Tạo sản phẩm
export const createProduct = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await productService.createProduct({ ...values, token: token });
    if (!res.data.success && res.data.status === "warning") {
      Swal.fire({
        title: "Thêm sản phẩm",
        text: `${res.data.message}`,
        icon: "warning",
        confirmButtonText: "Đóng",
      });
    } else if (res.data.success) {
      Swal.fire({
        title: "Thêm sản phẩm",
        text: "Thêm sản phẩm thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 9 - Lấy thông tin chi tiết 1 sản phẩm
export const getProductDetails = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await productService.getProductDetails({
      token: token,
      id: id,
    });
    return res;
  } catch (err) {
    // showError();
    console.log(err);
  }
};

// 10 - Xoá đơn vị tính
export const deletedUnit = async (data, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await productService.updateUnut({ ...data, token: token });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá đơn vị tính",
          text: "Xoá đơn vị tính thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 11 - Cập nhật ĐVT
export const updateUnit = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await productService.updateUnut({ ...data, token: token });

    if (res.data.success) {
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};
// 12 - Cập nhật loại hoá đơn
export const updateTaxCode = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await productService.updateTaxCode({ ...data, token: token });

    if (res.data.success) {
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 10 - Xoá đơn loại hoá đơn
export const deletedTaxCode = async (data, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await productService.updateTaxCode({ ...data, token: token });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá đơn loại hoá đơn",
          text: "Xoá đơn loại hoá đơn thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 11 - Lấy lịch sử sản phẩm
export const productHistory = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await productService.productHistory({
      token: token,
      id: id,
    });
    return res;
  } catch (err) {
    // showError();
    console.log(err);
  }
};
// 11 - Lấy lịch sử sản phẩm
export const productHistoryAll = async (data) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await productService.productHistoryAll({
      ...data,
      token,
    });
    return res;
  } catch (err) {
    // showError();
    console.log(err);
  }
};

// 11 - Lấy lịch sử sản phẩm
export const productExport = async () => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await productService.productExport({
      token,
    });
    if (res.data.success) {
      return res.data.data.file;
    }
  } catch (err) {
    // showError();
    console.log(err);
  }
};
