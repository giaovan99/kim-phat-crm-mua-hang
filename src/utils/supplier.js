import Swal from "sweetalert2";
import { supplierService } from "../services/supplierService";
import { getToken, showError } from "./others";

// 1 - Lấy danh sách nhà cung cấp
export const getListSupplier = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    if (token) {
      // Gửi token lên BE -> lấy data danh sách NCC
      let res = await supplierService.supplierList({
        token: token,
        page: page,
        ...keysParam,
      });
      // console.log(res);
      if (res.data.success) {
        // console.log(res);
        let arr = [];
        if (res.data.data.data) {
          res.data.data.data.forEach((item) =>
            arr.push({
              ...item,
              value: item.id,
              label: item.company_name_short,
            })
          );
          // console.log(arr);
          return { status: true, data: arr, rawData: res.data.data };
        } else {
          res.data.data.forEach((item) =>
            arr.push({
              ...item,
              value: item.id,
              label: item.company_name_short,
            })
          );
          return { status: true, data: arr };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 1.1 - Lấy thông tin 1 NCC cụ thể  => cần fix
export const getNCC = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin NCC
    let res = await supplierService.getNCC({
      token: token,
      id: id,
    });
    return res;
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 2 - Cập nhật nhà cung cấp
export const updateNCC = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await supplierService.nccUpdate({ ...values, token: token });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật nhà cung cấp",
          text: "Cập nhật nhà cung cấp thành công!",
          icon: "success",
        });
      }

      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật nhà cung cấp",
        text: "Cập nhật nhà cung cấp thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 3 - Thêm nhà cung cấp
export const createNCC = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await supplierService.createNCC({ ...values, token: token });
    if (res.data.success) {
      Swal.fire({
        title: "Thêm nhà cung cấp",
        text: "Thêm nhà cung cấp thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      Swal.fire({
        title: "Thêm nhà cung cấp thất bại",
        text: `${res.data.error.username[0]}`,
        icon: "error",
        confirmButtonText: "Đóng",
      });
      console.log(res);
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 4 - Xoá nhà cung cấp
export const deleteNCCId = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await supplierService.deleteNCC({ ...values, token: token });
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá nhà cung cấp",
          text: "Xoá nhà cung cấp thành công!",
          icon: "success",
        });
      }

      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá nhà cung cấp",
        text: "Xoá nhà cung cấp thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};
