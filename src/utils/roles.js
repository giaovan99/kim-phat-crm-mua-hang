import Swal from "sweetalert2";
import { rolesService } from "../services/rolesService";
import { getToken, showError } from "./others";
// import { useDispatch } from "react-redux";
// import { setListRoles } from "../redux/slice/listRolesSlice";

// 1 - Lấy danh sách phân quyền còn active
export const getListRoles = async (page = 1, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await rolesService.roleList({ token, page: page, ...keysParam });
    // console.log(res);
    if (res.data.success) {
      let arr = [];
      res.data.data.data.forEach((item) =>
        arr.push({ ...item, value: item.id, label: item.name })
      );
      return { status: true, data: arr, fullRes: res.data.data };
    } else {
      console.log(res);
      showError();
      return { status: false };
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 2 - Thêm phân quyền
export const addRole = async (values) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    //  Gửi lên BE
    let res = await rolesService.addRole({ token: token, ...values });
    if (res.data.success) {
      Swal.fire({
        title: "Thêm phân quyền",
        text: "Thêm phân quyền thành công!",
        icon: "success",
      });
      return { status: true, data: res.data.data };
    } else {
      console.log(res);
      showError();
      return { status: false };
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 3 - Lấy thông tin 1 phân quyền cụ thể
export const getRoleInfo = async (id) => {
  try {
    let token = getToken();
    let res = await rolesService.getRoles({ token, id });
    if (res.data.success) {
      return { status: true, data: res.data.data };
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 4 - Cập nhật phân quyền
export const updateRole = async (values) => {
  try {
    let token = getToken();
    let res = await rolesService.updateRole({ token, ...values });
    console.log(res);
    if (res.data.success) {
      Swal.fire({
        title: "Cập nhật phân quyền",
        text: "Cập nhật phân quyền thành công!",
        icon: "success",
      });
      return { status: true, data: res.data.data };
    } else {
      console.log(res);
      showError();
      return { status: false };
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 5 - Lấy danh sách tính năng
export const getListFunctions = async () => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await rolesService.funtions({ token: token });
    // console.log(res);
    if (res.data.success) {
      let arr = Object.entries(res.data.data).map(([key, value]) => ({
        function_id: parseInt(key),
        function_name: value,
        pread: false,
        pcreate: false,
        pupdate: false,
        pdelete: false,
        pfull: false,
      }));
      return { status: true, data: arr };
    } else {
      console.log(res);
      showError();
      return { status: false };
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 6 - Xoá phân quyền
export const deleteRole = async (values, all = false) => {
  try {
    let token = getToken();
    let res = await rolesService.deleteRole({ ...values, token: token });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá phân quyền",
          text: "Xoá phân quyền thành công!",
          icon: "success",
        });
      }

      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá phân quyền",
        text: "Xoá phân quyền thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};
