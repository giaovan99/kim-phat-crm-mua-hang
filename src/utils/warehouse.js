import Swal from "sweetalert2";
import { warehouseService } from "../services/warehouseService";
import {
  convertArr,
  convertArrStt,
  convertObjectToArray,
  convertSlipToArray,
  getToken,
  showError,
} from "./others";

// 1 - Lấy danh sách kho
export const warehouseList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await warehouseService.warehouseList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        // console.log(res.data.data);
        if (res.data.data.data) {
          return {
            status: true,
            data: convertArr(res.data.data.data),
            dataRaw: res.data.data,
          };
        } else {
          return {
            status: true,
            data: convertArr(res.data.data),
          };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 2 - Lấy thông tin 1 kho cụ thể  => cần fix
export const getWarehouse = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await warehouseService.getWarehouse({
      token: token,
      id: id,
    });
    return res;
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 3 - Cập nhật kho
export const warehouseUpdate = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await warehouseService.warehouseUpdate({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật kho",
          text: "Cập nhật kho thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật kho",
        text: "Cập nhật kho thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 5 - Thêm kho
export const createWarehouse = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await warehouseService.createWarehouse({
      ...values,
      token: token,
    });
    if (res.data.success) {
      Swal.fire({
        title: "Thêm kho",
        text: "Thêm kho thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      Swal.fire({
        title: "Thêm kho thất bại",
        text: "Thêm kho thất bại",
        icon: "error",
        confirmButtonText: "Đóng",
      });
      console.log(res);
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 6 - Xoá kho
export const deleteWarehouseId = async (values, all = false) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await warehouseService.deleteWarehouse({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Xoá kho",
          text: "Xoá kho thành công!",
          icon: "success",
        });
      }

      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá kho",
        text: "Xoá kho thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 7 - Lấy danh sách phiếu nhập kho
export const getImportWarehouseList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await warehouseService.importWarehouseList({
        token: token,
        page: page,
        ...keysParam,
      });
      if (res.data.success) {
        if (res.data.data.data) {
          return {
            status: true,
            data: res.data.data,
            options: res.data.options,
          };
        } else {
          return {
            status: true,
            data: convertArr(res.data.data),
            options: res.data.options,
          };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 8 - Lấy thông tin phiếu nhập kho cụ thể
export const getImportWarehouse = async (id) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi token lên BE -> lấy thông tin user
    let res = await warehouseService.getImportWarehouse({
      token: token,
      id: id,
    });
    // console.log(res);
    return res;
  } catch (err) {
    console.log(err);
    // showError();
  }
};

// 9 - Cập nhật phiếu nhập kho
export const updateImportWarehouse = async (values, all = false) => {
  try {
    console.log("123");
    // Lấy token từ localStorage
    let token = getToken();
    let res = await warehouseService.updateImportWarehouse({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all) {
        Swal.fire({
          title: "Cập nhật phiếu nhập kho",
          text: "Cập nhật phiếu nhập kho thành công!",
          icon: "success",
        });
      }
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Cập nhật phiếu nhập kho",
        text: "Cập nhật phiếu nhập kho thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 10 - Xoá phiếu nhập kho
export const deleteImportWarehouseId = async (values, all = false) => {
  try {
    console.log(values);
    // Lấy token từ localStorage
    let token = getToken();
    let res = await warehouseService.deleteImportWarehouse({
      ...values,
      token: token,
    });
    // console.log(res);
    if (res.data.success) {
      if (!all)
        Swal.fire({
          title: "Xoá phiếu nhập kho",
          text: "Xoá phiếu nhập kho thành công!",
          icon: "success",
        });
      return true;
    } else {
      console.log(res);
      Swal.fire({
        title: "Xoá phiếu nhập kho",
        text: "Xoá phiếu nhập kho thất bại!",
        icon: "error",
      });
      return false;
    }
  } catch (err) {
    console.log(err);
    // showError();
    return false;
  }
};

// 12 - Tạo phiếu nhập kho
export const createImportWarehouse = async (values, formik) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // Gửi formik values lên BE
    let res = await warehouseService.createImportWarehouse({
      ...values,
      token: token,
    });
    if (res.data.success) {
      Swal.fire({
        title: "Tạo phiếu nhập kho",
        text: "Tạo phiếu nhập kho thành công!",
        icon: "success",
        confirmButtonText: "Đóng",
      });
      formik.resetForm();
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};

// 13 - Lấy danh sách staff
export const getStaffWarehouse = async (page = 1) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await warehouseService.importWarehouseList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      return {
        status: true,
        data: convertObjectToArray(res.data.options.staff),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 13 - Lấy danh sách đơn hàng được phép
export const getPOWarehouse = async (page = 1) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await warehouseService.importWarehouseList({
      token: token,
      page: page,
    });
    if (res.data.success) {
      // Do res.data.options.staff đang có định dạng là object nên -> convert ra mảng có định dạng value, label
      return {
        status: true,
        data: convertSlipToArray(res.data.options.purchase_order),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 1 - Lấy danh sách trạng thái
export const getImportWarehouseStatusList = async (page, keysParam = null) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    let res = await warehouseService.requestStatus({
      token: token,
      ...keysParam,
    });
    if (res.data.success) {
      return {
        status: true,
        data: convertArrStt(res.data.data),
      };
    } else {
      showError();
      console.log(res);
      return { status: false };
    }
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};

// 7 - Lấy danh sách sản phẩm nhập kho
export const stockImportPartList = async (page, keysParam) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log(keysParam);
    if (token) {
      // Gửi token lên BE -> lấy data danh sách user
      let res = await warehouseService.stockImportPartList({
        token: token,
        page: page,
        ...keysParam,
      });

      if (res.data.success) {
        if (res.data.data.data) {
          return { status: true, data: res.data.data };
        } else {
          return {
            status: true,
            data: convertArr(res.data.data),
          };
        }
      } else {
        console.log(res);
        showError();
        return { status: false };
      }
    }
  } catch (err) {
    console.log(err);
    // showError();
    return { status: false };
  }
};

// 1 - In danh sách sản phẩm nhập kho
export const stockImportPrint = async (ids) => {
  try {
    // Lấy token từ localStorage
    let token = getToken();
    // console.log("đẩy lên sever", {
    //   token: token,
    //   ...ids,
    // });
    let res = await warehouseService.stockImportPrint({
      token: token,
      ...ids,
    });
    return res;
  } catch (err) {
    // showError();
    console.log(err);
    return { status: false };
  }
};
