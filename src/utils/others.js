import Swal from "sweetalert2";
import { getLocalStorage } from "./localStorage";
import {
  exportReportAM,
  exportReportProduct,
  exportReportProductPrice,
} from "./reportAPI";
import dayjs from "dayjs";

// 1 - convert Json from API to Array
export const convertToArray = (jsonString) => {
  let jsonArray = JSON.parse(jsonString);
  return jsonArray;
};

// 2 - Trả về trang chủ
export const goToLogin = (err) => {
  showError();
};

export const goToLogin2 = () => {
  localStorage.clear();
  window.location = "/";
};

// 3 - Show alert lỗi
export const showError = () => {
  Swal.fire({
    title: "Oops...",
    text: "Đã có lỗi xảy ra!",
    icon: "error",
  });
};

// 4 - Lấy token từ local storage
export const getToken = () => {
  // Lấy token từ localStorage
  let token = getLocalStorage("access_token");
  if (!token) {
    goToLogin2();
  } else {
    return token.token;
  }
};

// 5 - Chuyển đổi ngày
export const stringToDate = (inputDate) => {
  const formattedDate = dayjs(inputDate).format("DD/MM/YYYY");
  return formattedDate;
};

// 5.1 - Chuyển đổi định dạng YYYY-MM-DD thành DD-MM-YYYY
export const formatDatePicker = (inputDate) => {
  // Parse the input date with the format YYYY-MM-DD
  const date = dayjs(inputDate, "YYYY-MM-DD");

  // Format the date to DD-MM-YYYY
  const formattedDate = date.format("DD-MM-YYYY");

  return formattedDate;
};

// 5.2 - Chuyển đổi ngày gửi lên BE
export const stringToDateBE = (inputDate) => {
  console.log(inputDate);
  // Parse the input date with the format YYYY-MM-DD
  const date = dayjs(inputDate, "DD-MM-YYYY");

  // Format the date to DD-MM-YYYY
  let formattedDate = "";
  if (inputDate) {
    formattedDate = date.format("YYYY-MM-DD");
  }

  return formattedDate;
};

// 6 - Chuyển đổi object từ API thành mảng có thể search được
export const convertObjectToArray = (object) => {
  const resultArray = Object.entries(object).map(([key, value]) => ({
    value: isNaN(key * 1) ? key : key * 1,
    label: value,
    title: value,
  }));
  return resultArray;
};

// 6 - Chuyển đổi object từ API thành mảng có thể search được
export const convertSlipToArray = (object) => {
  const resultArray = Object.entries(object).map(([key, value]) => ({
    value: Number(key),
    label: value,
  }));

  return resultArray;
};

export const convertSlipToArrayNotNumber = (object) => {
  const resultArray = Object.entries(object).map(([key, value]) => ({
    value: key,
    label: value,
  }));
  return resultArray;
};
//  7 - Chuyển đổi mảng thành mảng để search được
export const convertArr = (arr) => {
  let arrNew = [];
  arr.forEach((item) => {
    // console.log(item);
    let label;
    let shortName;
    if (item.key) {
      if (item.value) {
        shortName = item.value;
      }
      label = item.code ? item.code : item.name;
      arrNew.push({ ...item, value: item.key, label, shortName });
    } else {
      label = item.code ? item.code : item.name;
      arrNew.push({ ...item, value: item.id, label });
    }
  });
  return arrNew;
};
//  8 - Chuyển đổi mảng thành mảng để search được dành riêng cho status
export const convertArrStt = (arr) => {
  let arrNew = [];
  arr.forEach((item) => {
    if (item.key) {
      arrNew.push({
        ...item,
        value: item.key,
        label: item.name,
        title: item.name,
      });
    } else {
      arrNew.push({
        ...item,
        value: item.id,
        label: item.name,
        title: item.name,
      });
    }
  });
  return arrNew;
};

// Check xem có số lượng thực tế chưa
export const checkQuantity = (record) => {
  return record.qty;
};

// Check vat
export const checkVAT = (record, taxCode) => {
  // console.log(taxCode.find((item) => item.key == record.vat));
  let obj = taxCode.find((item) => item.key == record.vat);
  return obj ? obj : { vat: 0 };
};

// Tính thành tiền trước VAT
export const calBeforeVAT = (record, rounded = false) => {
  let quantity = checkQuantity(record);
  if (rounded) {
    return Math.round(Number(quantity) * Number(record.price));
  } else {
    return Number(quantity) * Number(record.price);
  }
};

// Tính thành tiền sau VAT
export const calAfterVAT = (record, taxCode) => {
  let quantity = checkQuantity(record);
  let vat = checkVAT(record, taxCode).vat;
  return Number(quantity) * Number(record.price) * (1 + vat / 100);
};

// check price
export const checkPrice = (value) => {
  let checkValue = value <= 0 ? 1 : value;
  return checkValue;
};

// Check number
export const checkNumber = (value) => {
  let checkValue = value < 0 ? 1 : value;
  return checkValue;
};

// =============== Hàm chuyển số tiền thành chữ ===============
export const convertToWord = (number) => {
  let subnumber = Math.abs(number);
  let result = docNhieuSo(subnumber);
  if (number > 0) {
    // Viết hoa chữ cái đầu
    let capitalizedStr = result.charAt(0).toUpperCase() + result.slice(1);
    return capitalizedStr + " đồng";
  } else {
    return "Âm " + result + " đồng";
  }
};

function doc3So(numb) {
  var numbName = [
    "không",
    "một",
    "hai",
    "ba",
    "bốn",
    "năm",
    "sáu",
    "bảy",
    "tám",
    "chín",
  ];
  var result = numb
    .toString()
    .split("")
    .reverse()
    .map((val, index) => {
      let level = ["", " mươi", " trăm"];
      return numbName[val - 0] + level[index];
    })
    .reverse()
    .join(" ")
    .replace("không mươi", "linh")
    .replace("một mươi", "mười")
    .replace("mươi không", "mươi")
    .replace("mười không", "mười")
    .replace("mười năm", "mười lăm")
    .replace("mươi năm", "mươi lăm")
    .replace("mươi một", "mươi mốt") // Sửa lỗi ở đây
    .replace("mười bốn", "mười bốn")
    .replace("linh bốn", "linh tư")
    .replace("linh không", "");

  return result;
}

function docNhieuSo(numb) {
  return numb
    .toLocaleString("en")
    .split(",")
    .reverse()
    .map((val, index) => {
      let level = ["", " nghìn", " triệu", " tỉ", " nghìn", " triệu"];
      if (!(val - 0)) {
        if (index === 3) {
          return level[index];
        }
        return "";
      }
      return doc3So(val) + level[index];
    })
    .reverse()
    .join(" ")
    .trim()
    .replace("  ", " ");
}

// =============== Hàm chuyển số tiền thành chữ  ===============

// =============== Hàm xuất file excel report  ===============
export const exportReportExcel = async (page, key) => {
  switch (page) {
    case "report_product": {
      let res = await exportReportProduct(key);
      return res;
    }
    case "report_product_price": {
      let res = await exportReportProductPrice(key);
      return res;
    }
    case "report_am": {
      let res = await exportReportAM(key);
      return res;
    }
    default:
      return;
  }
};
// =============== Hàm xuất file excel report  ===============

// =============== Hàm làm tròn giá tiền - Đang ko sử dụng nữa  ===============
// - Làm tròn theo rule: "Giá trị tổng sau thuế" nếu có hàng trăm bắt đầu >=6 thì làm tròn lên, <6 giữ nguyên. Không lấy phần thập phân.
// Ví dụ: 14,699,999.88 -> 14,700,000
//        14,699,599.88 -> 14,699,499

export const roundMoney = (money) => {
  let floorMoney = Math.floor(money);
  // Lấy phần ngàn
  let thousands = floorMoney / 1000;

  // Lấy số hàng trăm bằng cách lấy phần dư của phép chia cho 1000 và chia cho 100
  let hundreds = (floorMoney % 1000) / 100;

  if (hundreds >= 5) {
    return Math.floor(thousands + 1) * 1000;
  } else {
    return Math.floor(money);
  }
};
// =============== Hàm làm tròn giá tiền - Đang ko sử dụng nữa  ===============

export const returnCompanyInfo = (id, companyArr) => {
  let result = companyArr.filter((item) => id === item.id);
  return result[0];
};

// =============== Hàm debounce search  ===============
export const debounce = (func, delay) => {
  let debounceTimer;
  return function (...args) {
    const context = this;
    clearTimeout(debounceTimer);
    debounceTimer = setTimeout(() => func.apply(context, args), delay);
  };
};
// =============== Hàm debounce search  ===============

// Tạo ra root
export const rootDetailPage = {
  product: {
    per_page: 50,
  },
};

export const normalize = (text) => {
  return text
    .normalize("NFD") // chuyển đổi chuỗi sang dạng NFD (Normalization Form Decomposed)
    .replace(/[\u0300-\u036f]/g, "") // loại bỏ các dấu điều khiển (combining diacritical marks)
    .toLowerCase(); // chuyển về dạng chữ thường
};

// =============== Hàm check vị trí chính xác của phần tử trong mảng - Dùng khi xài andt table, chuyển trang sẽ bị reset vị trí mới  ===============
export const actualIndex = (currentPage, pageSize = 20, index) => {
  return (currentPage - 1) * pageSize + index;
};
// =============== Hàm check vị trí chính xác của phần tử trong mảng - Dùng khi xài andt table, chuyển trang sẽ bị reset vị trí mới  ===============
