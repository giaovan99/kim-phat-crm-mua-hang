import { authService } from "../services/authService";

import {
  getLocalStorage,
  removeLocalStorage,
  setLocalStorage,
} from "./localStorage";
import { goToLogin, goToLogin2, showError } from "./others";

// 1 - Đăng nhập
export const login = async (values) => {
  try {
    // Khi đã điền đầy đủ username + password => đẩy lên BE
    const res = await authService.login(values);
    if (res.data.access_token) {
      // Lấy giá trị thời gian hiện tại + expires_in -> đẩy lên localStorage, mục đích: xác định thời hạn đăng nhập, sử dụng token cho các trang khác
      let now = new Date();
      const access_token = {
        token: res.data.access_token,
        expiry: now.getTime() + res.data.expires_in * 1000,
        token_type: res.data.token_type,
      };
      setLocalStorage("access_token", JSON.stringify(access_token));
      return res;
    } else {
      console.log(res);
      document.getElementById("warningAPI").innerHTML = res.data.message;
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
  }
};

// 2 - Đăng xuất
export const logout = async () => {
  try {
    let token = getLocalStorage("access_token");
    console.log(token);
    let res = await authService.logout(token.token);
    console.log(res);
    if (res.data) {
      removeLocalStorage("access_token");
      removeLocalStorage("profile");
      goToLogin2();
    }
  } catch (err) {
    // alert thông báo
    removeLocalStorage("access_token");
    removeLocalStorage("profile");
    console.log(err);
    goToLogin();
  }
};

// 3 - Quên mật khẩu
export const forgotPassword = async (values) => {
  try {
    let res = await authService.forgotPassword(values);
    if (res.data.success) {
      return { status: true, mess: res.data.message };
    } else {
      return { status: false, mess: res.data.error.email[0] };
    }
  } catch (err) {
    // showError();
    console.log(err);
  }
};

// 4 - Check token
export const checkToken = async (token) => {
  try {
    if (token) {
      let res = await authService.checkToken(token);
      if (res.data.success) {
        return true;
      } else {
        showError();
        console.log(res);
        return false;
      }
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};
// 5 - Reset Password
export const resetPassword = async (values) => {
  try {
    let res = await authService.resetPassword(values);
    if (res.data.success) {
      return true;
    } else {
      showError();
      console.log(res);
      return false;
    }
  } catch (err) {
    // showError();
    console.log(err);
    return false;
  }
};
